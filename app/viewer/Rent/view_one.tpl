<h5>Última modificação em {$rent->get('last_modify', true)}</h5>
<div class="row">
	<div class="col-sm-12">
		<b>Renovações nesta locação:</b> {$rent->get('renew_qtt')}
	</div>
</div>
<div class="row">
	<div class="col-sm-6 form-group">
		<label>Membro (clique para abrir)</label> <br>
		<a href="/member/view/{$rent->get('id_user')}" target="_blank">
			{$rent->get('id_user', true)->get('name')}
		</a>
	</div>
	<div class="col-sm-6 form-group">
		<label>Jogo enviado</label>
		<input disabled class="form-control" 
			value="{$rent->get('id_game', true)->get('name')} ({$rent->get('platform', true)})">
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group">
		<label>Início da locação</label>
		<input disabled class="form-control" value="{$rent->get('start', true)}">
	</div>
	<div class="col-sm-6 form-group">
		<label>Final da locação</label>
		<input disabled class="form-control" value="{$rent->get('end', true)}">
	</div>
</div>
<div class="row">
	<div class="col-sm-4 form-group">
		<label>Método de envio</label>
		<input disabled class="form-control" value="{$rent->get('carrier_mode', true)}">
	</div>
	<div class="col-sm-4 form-group">
		<label>Código de rastreio</label>
		<input disabled class="form-control" value="{$rent->get('carrier_code')}">
	</div>
	<div class="col-sm-4 form-group alert alert-info" style="padding: 10px">
		<label style="color: white">Status</label>
		<input disabled class="form-control" value="{$rent->get('status', true)}" style="color: white">
	</div>
</div>

<div class="row">
	<div class="col-sm-4 form-group">
		<label>Obs. administrativas</label>
		<textarea disabled class="form-control">{$rent->get('ps_adm')}</textarea>
	</div>
	<div class="col-sm-4 form-group">
		<label>Obs. sobre transporte</label>
		<textarea disabled class="form-control">{$rent->get('ps_carrier')}</textarea>
	</div>
	<div class="col-sm-4 form-group">
		<label>Obs. do membro</label>
		<textarea disabled class="form-control">{$rent->get('ps_user')}</textarea>
	</div>
</div>
