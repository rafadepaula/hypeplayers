<?php
/* Smarty version 3.1.28, created on 2017-10-12 19:45:08
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\view_one.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59dff074bfdbc2_00776695',
  'file_dependency' => 
  array (
    'bab538d17dca95fca64d3873bc7f68bdd4ce3e38' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\view_one.tpl',
      1 => 1505431911,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59dff074bfdbc2_00776695 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['modal']->value) {
echo '<script'; ?>
 src="plugins/gallery/dist/jquery.magnific-popup.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="assets/js/gallery.js"><?php echo '</script'; ?>
>
<link rel="stylesheet" href="plugins/gallery/dist/magnific-popup.css">
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['game']->value->get('ps4')) {?>
	<?php $_smarty_tpl->tpl_vars["ps4"] = new Smarty_Variable("ps4", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "ps4", 0);
} else { ?>
	<?php $_smarty_tpl->tpl_vars["ps4"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "ps4", 0);
}
if ($_smarty_tpl->tpl_vars['game']->value->get('xone')) {?>
	<?php $_smarty_tpl->tpl_vars["xone"] = new Smarty_Variable("xone", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "xone", 0);
} else { ?>
	<?php $_smarty_tpl->tpl_vars["xone"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "xone", 0);
}
if ($_smarty_tpl->tpl_vars['game']->value->get('switch')) {?>
	<?php $_smarty_tpl->tpl_vars["switch"] = new Smarty_Variable("switch", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "switch", 0);
} else { ?>
	<?php $_smarty_tpl->tpl_vars["switch"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "switch", 0);
}?>
<div class="row" style="margin-top: 25px">

	<div class="col-sm-4 visible-xs">
		<div class="card card-profile">
			<div class="card-avatar game-cover">
				<img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['cover']->value;?>
" />
			</div>

			<div class="content">
				<h6 class="category text-gray"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
</h6>
				<h4 class="card-title"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('id_genre',true)->get('name');?>
</h4>
				<?php if (!$_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
					<p class="text-warning"><b>JOGO NÃO DISPONÍVEL ATUALMENTE</b></p>
				<?php }?>
				<p class="card-content">
					<?php echo $_smarty_tpl->tpl_vars['game']->value->platforms();?>

				</p>
				<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
					<a href="/game/edit/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-success btn-round">
						Editar jogo
					</a>
					<a href="/game/addMedia/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-success btn-round">
						Cad. Mídia
					</a>
				<?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
						<button type="button" class="btn btn-success btn-round add-fav" data-game="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
"
							data-platforms="<?php echo $_smarty_tpl->tpl_vars['ps4']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['xone']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['switch']->value;?>
">
							Adicionar para minha lista
						</button>
					<?php }?>
				<?php }?>
			</div>
		</div>

		<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
			<div class="card card-profile">
				<div class="content">
					<h4 class="card-title">Infos. Administrativas</h4>
					<div class="col-sm-12">
						<p class="text-justify">
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('ps4'))) {?>
								<b>Qtd. disp. PS4:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('ps4_qtt');?>
 <br>
							<?php }?>
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('xone'))) {?>
								<b>Qtd. disp. XBOX One:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('xone_qtt');?>
 <br>
							<?php }?>
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('switch'))) {?>
								<b>Qtd. disp. Nintendo Switch:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('switch_qtt');?>
 
							<?php }?>
						</p>
						<a href="/game/delete/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-danger btn-round confirm-link">
							Deletar jogo
						</a>
						<a href="/game/toggle/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-warning btn-round">
							<?php if ($_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
								Remover da lista
							<?php } else { ?>
								Disponibilizar na lista
							<?php }?>
						</a>
					</div>
				</div>
			</div>
		<?php }?>
	</div>

 	<div class="col-sm-8">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="material-icons">videogame_asset</i>
            </div>
            <div class="card-content">
            	<div class="col-sm-12">
	                <p class="category">Perfil de jogo</p>
	                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h3>
                </div>

                <div class="col-sm-12">
	                <h4 class="text-left">Descrição</h4>
	                <p class="text-justify"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('description',true);?>
</p>
                </div>

                <div class="col-sm-12">
                	<h4 class="text-left">Imagens</h4>
                	<div id="images" class="row">
    	                <?php
$_from = $_smarty_tpl->tpl_vars['images']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_image_0_saved_item = isset($_smarty_tpl->tpl_vars['image']) ? $_smarty_tpl->tpl_vars['image'] : false;
$_smarty_tpl->tpl_vars['image'] = new Smarty_Variable();
$__foreach_image_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_image_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['image']->value) {
$__foreach_image_0_saved_local_item = $_smarty_tpl->tpl_vars['image'];
?>
    	                	<div class="col-xs-4">
								<span href="/<?php echo $_smarty_tpl->tpl_vars['image']->value->get('source');?>
" title="Imagem de <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
" class="game-media-thumb">
							        <img class="align-center game-media" src="/<?php echo $_smarty_tpl->tpl_vars['image']->value->get('thumb');?>
" alt="Imagem de <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
">
							    </span>
							    <?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
								    <a class="confirm-link" 
								    	href="/game/deleteMedia/<?php echo $_smarty_tpl->tpl_vars['image']->value->get('id');?>
">
								    	<small class="fa fa-remove text-danger"></small>
								    </a>
							    <?php }?>
						    </div>
                		<?php
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_0_saved_local_item;
}
}
if ($__foreach_image_0_saved_item) {
$_smarty_tpl->tpl_vars['image'] = $__foreach_image_0_saved_item;
}
?>
					</div>
                </div>

                <div class="col-sm-12" style="margin-bottom: 25px;">
                	<h4 class="text-left">Vídeos</h4>
                	<?php
$_from = $_smarty_tpl->tpl_vars['videos']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_video_1_saved_item = isset($_smarty_tpl->tpl_vars['video']) ? $_smarty_tpl->tpl_vars['video'] : false;
$_smarty_tpl->tpl_vars['video'] = new Smarty_Variable();
$__foreach_video_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_video_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['video']->value) {
$__foreach_video_1_saved_local_item = $_smarty_tpl->tpl_vars['video'];
?>
                		<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
						    <a class="confirm-link" 
						    	href="/game/deleteMedia/<?php echo $_smarty_tpl->tpl_vars['video']->value->get('id');?>
">
						    	<small class="fa fa-remove text-danger"></small>
						    </a>
					    <?php }?>
	                	<div class="video-container">
		                	<iframe class="video" src="<?php echo $_smarty_tpl->tpl_vars['video']->value->get('source',true);?>
"
			                    frameborder="0" allowfullscreen></iframe>
	                    </div>
                    <?php
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_1_saved_local_item;
}
}
if ($__foreach_video_1_saved_item) {
$_smarty_tpl->tpl_vars['video'] = $__foreach_video_1_saved_item;
}
?>
            	</div>

            </div>
        </div>
    </div>

	<div class="col-sm-4 hidden-xs">
		<div class="card card-profile">
			<div class="card-avatar game-cover">
				<img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['cover']->value;?>
" />
			</div>


			<div class="content">
				<h6 class="category text-gray"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
</h6>
				<h4 class="card-title"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('id_genre',true)->get('name');?>
</h4>
				<?php if (!$_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
					<p class="text-warning"><b>JOGO NÃO DISPONÍVEL ATUALMENTE</b></p>
				<?php }?>
				<p class="card-content">
					<?php echo $_smarty_tpl->tpl_vars['game']->value->platforms();?>

				</p>
				<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
					<a href="/game/edit/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-success btn-round">
						Editar jogo
					</a>
					<a href="/game/addMedia/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-success btn-round">
						Cad. Mídia
					</a>
				<?php } else { ?>
					<?php if ($_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
						<button type="button" class="btn btn-success btn-round add-fav" data-game="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
"
							data-platforms="<?php echo $_smarty_tpl->tpl_vars['ps4']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['xone']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['switch']->value;?>
">
							Adicionar para minha lista
						</button>
					<?php }?>
				<?php }?>
			</div>
		</div>

		<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'admin') {?>
			<div class="card card-profile">
				<div class="content">
					<h4 class="card-title">Infos. Administrativas</h4>
					<div class="col-sm-12">
						<p class="text-justify">
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('ps4'))) {?>
								<b>Qtd. disp. PS4:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('ps4_qtt');?>
 <br>
							<?php }?>
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('xone'))) {?>
								<b>Qtd. disp. XBOX One:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('xone_qtt');?>
 <br>
							<?php }?>
							<?php if (($_smarty_tpl->tpl_vars['game']->value->get('switch'))) {?>
								<b>Qtd. disp. Nintendo Switch:</b> <?php echo $_smarty_tpl->tpl_vars['game']->value->get('switch_qtt');?>
 
							<?php }?>
						</p>
						<a href="/game/delete/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-danger btn-round confirm-link">
							Deletar jogo
						</a>
						<a href="/game/toggle/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" class="btn btn-warning btn-round">
							<?php if ($_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
								Remover da lista
							<?php } else { ?>
								Disponibilizar na lista
							<?php }?>
						</a>
					</div>
				</div>
			</div>
		<?php }?>
	</div>
</div><?php }
}
