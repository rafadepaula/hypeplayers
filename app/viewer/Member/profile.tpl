<div class="row">
	<div class="col-sm-8">
    	<div class="card">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Dados pessoais</h4>
            </div>
            <div class="card-content">
            	<div class="row">
            		<div class="col-sm-4 form-group">
            			<label>Nome</label> <br>
            			{$user->get('name')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>E-mail</label> <br>
            			{$user->get('email')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>Telefone</label> <br>
            			{$member->get('phone')}
            		</div>
            	</div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label>RG</label><br>
                        {$member->get('rg')}
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>CPF</label><br>
                        {$member->get('cpf')}
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Data de nascimento</label><br>
                        {$member->get('birthday', true)}
                    </div>
                </div>
            	<div class="row">
            		<div class="col-sm-4 form-group">
            			<label>Rua</label> <br>
            			{$member->get('street')}
            		</div>
            		<div class="col-sm-2 form-group">
            			<label>Número</label> <br>
            			{$member->get('number')}
            		</div>

            		<div class="col-sm-2 form-group">
            			<label>Coomplemento</label> <br>
            			{$member->get('complement')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>Bairro</label> <br>
            			{$member->get('neighborhood')}
            		</div>
            	</div>
            	<div class="row">
            		<div class="col-sm-8 form-group">
            			<label>Cidade</label> <br>
            			{$member->get('city')} - {$member->get('state')}
            		</div>

            		<div class="col-sm-4 form-group">
            			<label>CEP</label> <br>
        				{$member->get('cep')}
            		</div>
            	</div>
            </div>
    	</div>
    </div>

	<div class="col-sm-4">
		<div class="card card-profile">
			<div class="card-avatar">
				<img class="img" src="{$picture}" />
			</div>

			<div class="content">
				<h6 class="category text-gray">
					{$signature->get('level', true)} (R$ {$signature->get('value', true)})
				</h6>
				<h4 class="card-title">Status: {$signature->get('status', true)}</h4>
				<p class="card-content">
					<p>
						Membro desde {$signature->get('since', true)} <br>
					</p>
				<a href="/user/edit" class="btn btn-success btn-round">Editar dados</a>
				</p>
			</div>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-sm-8">
    	<div class="card">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Histórico</h4>
            </div>
            <div class="card-content">
            	<div class="row">
            		<div class="col-sm-12">
		            	<div class="table-responsive">
		            		<table class="table">
		            			<thead>
		            				<tr>
			                            <th>Jogo</th>
			                            <th>Status</th>
			                            <th>Período</th>
			                            <th>Ações</th>
			                        </tr>
		            			</thead>
                                <tbody>
                                    {foreach $rents as $rent}
				                        <tr>
				                            <td>
				                                {$rent->get('id_game', true)->get('name')}
				                                ({$rent->get('platform', true)})
				                            </td>
				                            <td>
				                                {$rent->get('status', true)}
				                            </td>
				                            <td>
				                                {$rent->get('start', true)} até {$rent->get('end', true)}
				                            </td>
				                            <td class="td-actions">
				                                <a>
				                                    <button type="button" rel="tooltip" title="Visualizar locação"
				                                            class="btn btn-info btn-simple view-one"
				                                            data-id-rent="{$rent->get('id')}">
				                                        <i class="fa fa-eye"></i>
				                                    </button>
				                                </a>
				                            </td>
				                        </tr>
				                    {/foreach}
                                </tbody>
                            </table>
		            	</div>
		            </div>
		        </div>
			</div>
    	</div>
    </div>
	<div class="col-sm-4">
    	<div class="card">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Favoritos</h4>
            </div>
            <div class="card-content">
            	<div class="row">
            		<div class="col-sm-12">
		            	<div class="table-responsive">
		            		<table class="table">
		            			<thead>
		            				<tr>
		            					<th>#</th>
		            					<th>Jogo</th>
		            				</tr>
		            			</thead>
                                <tbody>
                                    {foreach $favorites as $favorite}
                                        <tr>
                                            <td>{$favorite->get('position') + 1}</td>
                                            <td>
                                                {$favorite->get('id_game', true)->get('name')}
                                                ({$favorite->get('platform', true)})
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
		            	</div>
		            </div>
		        </div>
			</div>
    	</div>
    </div>

</div>

{include file=$smarty.current_dir|cat:"/../Rent/changeStatus_modal.tpl"}
{include file=$smarty.current_dir|cat:"/../Rent/view_modal.tpl"}