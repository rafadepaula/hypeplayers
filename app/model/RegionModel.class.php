<?php
    
    class RegionModel extends AppModel {
        public function getRegion($id){
            return $this->getDto('region', 'id', $id);
        }

        public function getRegionByCep($cep){
            $sql = 'SELECT * FROM region WHERE '.$cep.' BETWEEN REPLACE(cep_init, "-", "") AND  REPLACE(cep_end, "-", "")';
            $regions = $this->query($sql);
            if(!count($regions))
                return new Region();
            else
                return $this->getDto('region', 'id', $regions[0]['id'], $regions[0]);
        }
        
        public function save($id = null){
            $region = new Region();
            $ret  = $this->makeDto($region, $id);
            $region = $ret[0];
            
            $errors = $ret[1];
            
            if($errors != ''){
                Viewer::flash($errors, 'e');
                
                return false;
            }else{
                
                $this->initTransaction();
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('region', $region) : $this->update('region', $region, array('id' => $id));
                if($ret){
                    if(is_null($id)){
                        if(!$this->deleteRequests($region)){
                            Viewer::flash('Não foi possível eliminar as solicitações.', 'e');
                            $this->cancelTransaction();
                            return false;
                        }
                    }
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    $this->endTransaction();

                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    $this->cancelTransaction();

                    return false;
                }
            }
        }

        public function saveRequest($cepInfo){
            $regionRequest = new Region_request();
            $regionRequest->set('cep', $cepInfo['cep']);
            $regionRequest->set('location', $cepInfo['cidade'].' - '.$cepInfo['estado_info']['nome']);
            $regionRequest->set('date', date('Y-m-d H:i:s'));
            $notificationModel = new NotificationModel();
            if(!$this->exists('region_request', 'location', $regionRequest->get('location'))){
                $this->insert('region_request', $regionRequest);
                $text = 'Uma região solicita atenção: '.$regionRequest->get('cep');
                $notificationModel->send('admin', $text, '/region/requests', false);
                $id_region_request = $this->lastInserted('region_request');
            }else {
	            $region = $this->getDto('region_request', 'location', $regionRequest->get('location'));
	            $id_region_request = $region->get('id');
            }

            $memberRequest = new Member_request();
            $memberRequest->set('id_region_request', $id_region_request);
            $memberRequest->set('name', $_POST['name']);
            $memberRequest->set('lastname', $_POST['lastname']);
            $memberRequest->set('birthday', $_POST['birthday']);
            $memberRequest->set('rg', $_POST['rg']);
            $memberRequest->set('cpf', $_POST['cpf']);
            $memberRequest->set('phone', $_POST['phone']);
            $memberRequest->set('email', $_POST['email']);
            $memberRequest->set('created', date('Y-m-d H:i:s'));
            $this->insert('member_request', $memberRequest);
            $text = $memberRequest->get('name').' tentou se cadastrar.';
	        $notificationModel->send('admin', $text, '/member/requests', false);
	        $textEmail = $memberRequest->get('name').' '.$memberRequest->get('lastname').' de e-mail '.$memberRequest->get('email').' e telefone '.$memberRequest->get('phone').' tentou realizar um cadastro mas sua região não é aceita.';
	        $notificationModel->email('admin', $textEmail);
            return;
        }

        public function deleteRequests($region){
            $cep_init = str_replace('-', '', $region->get('cep_init'));
            $cep_end = str_replace('-', '', $region->get('cep_end'));

            $sql = 'DELETE FROM region_request WHERE cep BETWEEN "'.$cep_init.'" AND "'.$cep_end.'"';
            return $this->sql($sql);
        }

        public function checkCep($cep, $echo = true){
            $cepInfo = json_decode(Controller::curl(_CEP_URL_.$cep), true);
            if(is_null($cepInfo)){
            	if($echo)
                    echo 'false';
                return false;
            }
            $cep = str_replace('-', '', $cep);
            $sql = 'SELECT * FROM region WHERE '.$cep.' BETWEEN REPLACE(cep_init, "-", "") AND  REPLACE(cep_end, "-", "")';
            $regions = $this->query($sql);
            if(!count($regions)){
            	if($echo)
                    echo 'false';
                $this->saveRequest($cepInfo);
                return false;
            }
            if($echo)
                echo 'true';
            return true;
        }
        
    }