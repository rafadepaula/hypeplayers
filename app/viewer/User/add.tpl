<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/user/add">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>E-mail</label>
                            <input required type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Senha</label>
                            <input required type="password" name="password" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Confirme a senha</label>
                            <input required type="password" name="passwordConfirm" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
