<?php
    
    /**
     * Reads the URL to know what controller and what action to call
     * If not found, executes the _MAIN_CLASS->_MAIN_METHOD.
     *
     * If the url are login/, redirects automatically to user/login
     *
     * Be careful when changing this code!
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     *
     */
    if (!isset($_SESSION['phpError']))
        $_SESSION['phpError'] = '';
    $friendlyUrl = isset($_GET['ctrl']) && isset($_GET['act']) ? false : true;
    if ($friendlyUrl) {
        
        $comp = strrchr($_SERVER['REQUEST_URI'], '?');
        $end = str_replace($comp, '', $_SERVER['REQUEST_URI']);
        $url = explode('/', $end);
        array_shift($url);
        if (isset($url[0])) {
            if (isset($url[1])) {
                if (trim($url[1]) != '')
                    $action = $url[1];
                else
                    $action = 'view';
            } else {
                $action = 'view';
            }
            $controller = ucfirst($url[0]) . 'Controller';
        } else {
            $controller = _MAIN_CLASS;
        }
        
        $params = '';
        if (isset($url[2])) {
            for ($i = 2; $i < count($url); $i++) {
                if (trim($url[$i]) != '')
                    $params .= '"' . $url[$i] . '",';
            }
        }
        $params = trim($params, ',');
        
        if ($controller == 'LoginController') {
            $controller = 'UserController';
            $action = 'login';
        }
        if (class_exists($controller)) {
            eval('$controller = new ' . $controller . '("' . $controller . '","' . $action . '");');
            if (method_exists($controller, $action)) {
                eval('$controller->' . $action . '(' . $params . ');');
            } else {
                eval('$controller = new ' . _MAIN_CLASS . '("' . _MAIN_CLASS . '","' . _MAIN_METHOD . '");');
                eval('$controller->' . _MAIN_METHOD . '();');
            }
        } else {
            eval('$controller = new ' . _MAIN_CLASS . '("' . _MAIN_CLASS . '","' . _MAIN_METHOD . '");');
            eval('$controller->' . _MAIN_METHOD . '();');
        }
        
        
    } else {
        if (isset($_GET['ctrl']) && isset($_GET['act'])) {
            eval('$controller = new ' . ucfirst($_GET['ctrl']) . 'Controller("' . $_GET['ctrl'] . '","' . $_GET['act'] . '");');
            if (isset($_GET['param']))
                $controller->{$_GET['act']}($_GET['param']);
            else
                $controller->{$_GET['act']}();
            
        } else {
            eval('$controller = new ' . _MAIN_CLASS . '("' . _MAIN_CLASS . '","' . _MAIN_METHOD . '");');
            $controller->{_MAIN_METHOD}();
        }
    }