<?php
    
    /**
     * Default Excel generation. Receives some
     * arrays of header row and data row, simply puts in the file.
     */
    class BaseExcel extends PHPExcel{
        use Excel;
        use GetSet;

        private $type = '';
        private $headerColumns;

        private $data;

        public function __construct(){
            parent::__construct();

        }

        public function Header(){
            $this->SetFont('Arial');
            if($this->type != '')
                $this->PutRow(array(_TYPE.': '.$this->type));
            $this->BreakLine();

            $this->SetFont('Arial', 11, 'bold');
            $this->SetBorder('bottom');
            $this->PutRow($this->headerColumns);
            $this->SetBorder('');
            $this->SetFont('Arial');
        }

        public function ShowData(){
            foreach($this->data as $row){
                $this->PutRow($row);
            }
        }

        public function Footer(){

        }

        public function Initialize(){
            $this->setActiveSheetIndex(0);

            $this->getProperties()->setCreator('');
            $this->getProperties()->setTitle('');
            $this->getActiveSheet()->setTitle('');
            $this->Header();
        }
    }