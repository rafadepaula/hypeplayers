<div id="modalLibrary" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title"></h4>
			</div>
			<div class="modal-body" id="modal-body">
				<h4 class="text-center">
					<i class="fa fa-spinner fa-spin"></i>
				</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
			</div>
		</div>

	</div>
</div>

<div id="modalSaveChanges" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Salvar alterações</h4>
			</div>
			<div class="modal-body" id="modal-body-savechanges">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					Cancelar
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal" id="finish-save">
					Confirmar!
				</button>
			</div>
		</div>

	</div>
</div>