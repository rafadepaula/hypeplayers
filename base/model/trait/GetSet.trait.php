<?php
    
    /**
     * Used as helper to get and set values in the class
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    trait GetSet{
    
        /**
         * Method used to set a value to an attribute
         *
         * @param String $name - name of attribute
         * @param String $value - value of attribute
         * @return void
         *
         **/
        public function set($name, $value) {
            if (property_exists($this, $name)) {
                $this->{$name} = trim($value);
            }
        }
    
        /**
         * Method used to return an attribute value
         *
         * @param String $name - attribute name
         * @param Boolean $mask - use the dto's mask (optional)
         * @return Mixed - attribute value
         *
         **/
        public function get($name, $mask = null) {
            if (!isset($this->{$name}))
                return null;
            $value = $this->{$name};
            return $value;
        }
    }