jQuery(function($) {'use strict',

	
	// all Parallax Section
	$(window).load(function(){'use strict',
		$("#services").parallax("50%", 0.3);
		$("#clients").parallax("50%", 0.3);
	});
	
	// jogos filter
	/*$(window).load(function(){'use strict',
		$jogos_selectors = $('.jogos-filter >li>a');
		if($jogos_selectors!='undefined'){
			$jogos = $('.jogos-items');
			$jogos.isotope({
				itemSelector : 'inner',
				layoutMode : 'fitRows'
			});

			$jogos_selectors.on('click', function(){
				$jogos_selectors.removeClass('active');
				$(this).addClass('active');
				var selector = $(this).attr('data-filter');
				$jogos.isotope({ filter: selector });
				return false;
			});
		}
		$jogos_selectors.first().trigger('click');
	});*/
	
	//Pretty Photo
	 $("a[data-gallery^='prettyPhoto']").prettyPhoto({
	  social_tools: false
	 });


	// Contact form validation
	var form = $('.contact-form');
	form.submit(function () {'use strict',
		$this = $(this);
		$.post($(this).attr('action'), function(data) {
			$this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
		},'json');
		return false;
	});


	// Navigation Scroll
	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').click(function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 79}, 1000);
		return false;
	});

});

// Preloder script
jQuery(window).load(function(){'use strict';
	$(".preloader").delay(1600).fadeOut("slow").remove();
});

//Preloder script
jQuery(window).load(function(){'use strict';

    $('#home .carousel-inner .item').each(function(){
        var url = $(this).css('background-image');
        url = url.replace('url(', '').replace("'", '').replace('"', '');
        url = url.substr(0,url.length - 2);
        var img = loadImage(url, $(this));
    });

    var topHeight = $('#topBar').height();
    $('#home').css('margin-top', topHeight);
	/*// Slider Height
	var slideHeight = $(window).height();
	$('#home .carousel-inner .item, #home .video-container').css('height',slideHeight);
    */
	$(window).resize(function(){'use strict';
        var topHeight = $('#topBar').height();
        $('#home').css('margin-top', topHeight);
	});

});


// User define function
function Scroll() {
	var contentTop      =   [];
	var contentBottom   =   [];
	var winTop      =   $(window).scrollTop();
	var rangeTop    =   200;
	var rangeBottom =   500;
	$('.navbar-collapse').find('.scroll a').each(function(){
	    if($(this).attr('href') != '/user/login') {
            contentTop.push($($(this).attr('href')).offset().top);
            contentBottom.push($($(this).attr('href')).offset().top + $($(this).attr('href')).height());
        }
	})
	$.each( contentTop, function(i){
		if ( winTop > contentTop[i] - rangeTop ){
			$('.navbar-collapse li.scroll')
			.removeClass('active')
			.eq(i).addClass('active');			
		}
	})

};


	// Skill bar Function

	jQuery(document).ready(function(){
	jQuery('.skillbar').each(function(){
		jQuery(this).find('.skillbar-bar').animate({
			width:jQuery(this).attr('data-percent')
		},6000);
	});
});

$('html').on('click', '.game-library', function(){
    if(($("#modalLibraryAll").data('bs.modal') || {isShown: false}).isShown){
        game = $(this);
        name = game.data('name');
        id = game.data('id');
        $('#modal-title-all').text(name);
        filter = $('.filter-button-active').data('filter');
        $('#modal-body-all').html(
            '<h4 class="text-center">' +
            '<i class="fa fa-spinner fa-spin"></i>' +
            '</h4>'
        );
        $('#modal-body-all').load("/game/libraryModal/" + id + "/" + filter);
    }else{
        $('#modalLibrary').modal({
            backdrop: true
        }, $(this));
    }
});
$('#modalLibrary').on('show.bs.modal', function(e){
    $('#modal-body').html();
    game = e.relatedTarget;
    name = game.data('name');
    id = game.data('id');
    $('#modal-title').text(name);
    //filter = $('.filter-button-active').data('filter');
    $('#modal-body').load("/game/libraryModal/" + id + "/" + 'ps4');
});
$('#modalLibrary').on('hidden.bs.modal', function(e){
    $('#modal-body').html(
        '<h4 class="text-center">' +
        '<i class="fa fa-spinner fa-spin"></i>' +
        '</h4>'
    );;
    $('#modal-title').text('');
});
$('.all-games').click(function(){
    $('#modalLibraryAll').modal({
        backdrop: true
    }, $(this));
});
$('#modalLibraryAll').on('show.bs.modal', function(e){
    loadAll();
});
$('#modalLibraryAll').on('hide.bs.modal', function(e){
    if($('#modal-title-all').html() != ''){
        $('#modal-body-all').html(
            '<h4 class="text-center">' +
            '<i class="fa fa-spinner fa-spin"></i>' +
            '</h4>'
        );
        $('#modal-title-all').text('');
        loadAll();
        return false;
    }
});
function loadAll(){
    $('#modal-body-all').load('/index/libraryModal/', function(){
        filterButton($('#ps4'));
        $('.carousel').carousel({
            interval: false
        });
    });
}
$('#faq').click(function(){
    $('#modalFaq').modal({
        backdrop: true
    }, $(this));
});

$("html").on('click', '.filter-button', function(){
    var value = $(this).attr('data-filter');

    if(value == "all"){
        $('.filter').show('1000');
    }
    else{
        $(".filter").not('.'+value).hide('3000');
        $('.filter').filter('.'+value).show('3000');
    }
    $(".filter-button").removeClass("filter-button-active");
    $(this).addClass("filter-button-active");
});

function filterButton(button){
    var value = button.attr('data-filter');

    if(value == "all"){
        $('.filter').show('1000');
    }else{
        $(".filter").not('.'+value).hide('3000');
        $('.filter').filter('.'+value).show('3000');
    }
    $(".filter-button").removeClass("filter-button-active");
    button.addClass("filter-button-active");
}

function loadImage(path, target) {
    var img = new Image();
    img.src = path;
    $(img).addClass('heightCalc');
    $(img).load(function(){
        resize($(this).height(), target);
        $(this).hide();
    });
    var body = $('body');
    body.append(img);
    return img;
}

function resize(height, target){
    target.css('height', height);
}