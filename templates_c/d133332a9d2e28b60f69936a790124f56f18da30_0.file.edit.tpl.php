<?php
/* Smarty version 3.1.28, created on 2017-08-10 16:38:53
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Region\edit.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_598cb64d286aa8_57169685',
  'file_dependency' => 
  array (
    'd133332a9d2e28b60f69936a790124f56f18da30' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Region\\edit.tpl',
      1 => 1502393929,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598cb64d286aa8_57169685 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/region/edit/<?php echo $_smarty_tpl->tpl_vars['region']->value->get('id');?>
">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['region']->value->get('name');?>
">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Primeiro CEP da faixa</label>
                            <input required type="text" name="cep_init" class="form-control mask-cep" value="<?php echo $_smarty_tpl->tpl_vars['region']->value->get('cep_init');?>
">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Último CEP da faixa</label>
                            <input required type="text" name="cep_end" class="form-control mask-cep" value="<?php echo $_smarty_tpl->tpl_vars['region']->value->get('cep_end');?>
">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }
}
