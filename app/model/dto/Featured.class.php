<?php

class Featured extends DTO {
    public $id;
    public $name;
    public $platform;
    public $cover;
    public $description;
    public $position;

    public $FieldsValidation = array(
        'name' => 'notEmpty',
        'platform' => 'notEmpty',
        'description' => 'notEmpty',
        'cover' => array('validFile', ['jpg', 'jpeg', 'png']),
    );

    public $FieldsErrors = array(
        'name' => 'Informe um nome válido.',
        'cover' => 'Informe uma capa válida.',
	    'description' => 'Informe uma descrição válida',
	    'platform' => 'Informe uma plataforma válida.',
    );

}