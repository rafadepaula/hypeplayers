<?php
    
    
    class FavoriteController extends AppController {
    	public function view(){
    		if($this->user->get('role') != 'member'){
    			return $this->siteIndex();
    		}
            $id_user = $this->user->get('id');

            $favorites = $this->model->search('favorite', '*', array('id_user' => $id_user), 'position');
            $this->viewer->set('favorites', $favorites);

            $calcLastModify = $this->model->calcLastModify($favorites);
            foreach($calcLastModify as $key => $value){
                $this->viewer->set($key, $value);
            }


            $this->viewer->set('hasFavorites', count($favorites) > 0);

            $this->viewer->addJs('assets/js/favorite.js');
    		return $this->viewer->show('view', 'Minhas preferências');
    	}

    	public function reorder($total = false){
            if($this->user->get('role') != 'member'){
                return;
            }
            if($total){
                return $this->model->reorder($total);
            }else{
                if(!isset($_POST['diff'])){
                    echo "Diferenças não encontradas.";
                    return;
                }
                if(empty($_POST['diff'])){
                    echo 1;
                    return;
                }
                $diff = $_POST['diff'];
                $sql = $this->model->reorderSql($diff);
                $this->model->sql($sql);
                echo 1;
            }
            return;
    	}

        public function addFav($id_game){
            if($this->user->get('role') != 'member'){
                return;
            }
            return $this->model->addFav($id_game);
        }

        public function existsPlatforms($id){
            $platforms = $_POST['platforms'];
            return $this->model->existsPlatforms($id, $platforms);
        }

        public function saveChangesModal(){
            if($this->user->get('role') != 'member'){
                return;
            }
            $favorites = $this->model->saveChangesModal();
            
            if($this->model->exists('favorite', 'id_user', $this->user->get('id')))
                $hasFavorites = true;
            else
                $hasFavorites = false;
            $this->viewer->set('hasFavorites', $hasFavorites);

            $this->viewer->set('favorites', $favorites);
            return $this->viewer->show('save_changes', 'Salvar alterações', true, true);
        }

        public function finish(){
            if(isset($_COOKIE['favorites'])){
                echo $this->model->finish();
            }else{
                echo json_encode(array(
                    'type' => 'error',
                    'text' => 'Não foi possível encontrar suas preferências.',
                    'return' => 0
                ));
            }
        }

        public function removeFavCookie($position){
            if(isset($_COOKIE['favorites'])){
                $favorites = unserialize($_COOKIE['favorites']);
                if(isset($favorites[$position])){
                    unset($favorites[$position]);
                    $favorites = array_values($favorites);
                }
                if(count($favorites)){
                    $favorites = serialize($favorites);
                    setcookie('favorites', $favorites, time() + 60 * 60 * 24, '/');
                }else{
                    setcookie('favoriets', null, time() - 3600, '/');
                    unset($_COOKIE['favorites']);
                }
                if(count($_COOKIE['favorites']) < 9){
                    echo 'Preferência removida da lista de alterações!';
                }else{
                    echo 'Preferência removida. Selecione outro jogo para poder continuar.';
                }
            }
        }

        public function delete($position){
            if($this->user->get('role') != 'member'){
                return;
            }
            if(!$this->model->exists('favorite', 'id_user', $this->user->get('id'))){
                Viewer::flash('Você não possui preferências criadas!', 'e');
                return $this->view();
            }
            $cond = ['id_user' => $this->user->get('id')];
            $favorites = $this->model->search('favorite', '*', $cond, 'position');
            if(count($favorites) < 6){
                Viewer::flash("Você precisa manter no mínimo 5 jogos em sua lista.", 'e');
                return $this->view();
            }elseif(!isset($favorites[$position])){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            $favorite = $favorites[$position];
            $this->model->initTransaction();
            if(!$this->model->delete('favorite', array('id' => $favorite->get('id')))){
                $this->model->cancelTransaction();
                Viewer::flash(_DELETE_ERROR, 'e');
            }else{
                if(!$this->reorder(true)){
                    Viewer::flash(_DELETE_ERROR, 'e');
                    $this->model->cancelTransaction();
                }else{
                    Viewer::flash(_DELETE_SUCCESS, 's');    
                    $this->model->endTransaction();
                }
            }
            return $this->view();
        }

        public function viewFavorites($id_user){
            if($this->user->get('role') != 'admin'){
                return $this->permissionError();
            }

            return;
        }

        public function lowQttCron(){
            $this->model->lowQttCron();
        }
    }
