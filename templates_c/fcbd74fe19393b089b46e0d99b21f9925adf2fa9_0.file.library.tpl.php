<?php
/* Smarty version 3.1.28, created on 2017-10-12 19:45:00
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\library.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59dff06c25af65_24142586',
  'file_dependency' => 
  array (
    'fcbd74fe19393b089b46e0d99b21f9925adf2fa9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\library.tpl',
      1 => 1506612967,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59dff06c25af65_24142586 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'member' && !$_smarty_tpl->tpl_vars['hasFavorites']->value && $_smarty_tpl->tpl_vars['inCookie']->value == 0) {?>
	<div class="row list-not-done">
		<div class="col-sm-12 alert alert-warning text-center">
			<p>Você ainda não fez sua lista de preferências!</p>
		</div>
	</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'member' && !$_smarty_tpl->tpl_vars['hasFavorites']->value && $_smarty_tpl->tpl_vars['inCookie']->value > 0 && $_smarty_tpl->tpl_vars['inCookie']->value < 10) {?>
	<div class="row list-not-done">
		<div class="col-sm-12 alert alert-info text-center">
			<p>Você não finalizou sua lista de preferências! Faltam <?php echo 10-$_smarty_tpl->tpl_vars['inCookie']->value;?>
 jogos para terminar!</p>
		</div>
	</div>
<?php }?>


<div class="row list-must-save" <?php if ($_smarty_tpl->tpl_vars['inCookie']->value < 10 && !$_smarty_tpl->tpl_vars['mustSave']->value) {?> style="display: none" <?php }?> >
	<div class="col-sm-12 alert alert-danger text-center">
		<p>Salve suas alterações para continuar!!</p>
	</div>
</div>

<div class="fixed-alert-bottom" <?php if ($_smarty_tpl->tpl_vars['inCookie']->value < 10 && !$_smarty_tpl->tpl_vars['mustSave']->value) {?> style="display: none" <?php }?> id="save-changes">
	<button class="btn btn-round btn-danger" id="save-changes-button">
		<i class="fa fa-exclamation-triangle"></i> 
		Salvar alterações
	</button>
</div>

<div class="row" style="margin-bottom: 35px">
	<div class="text-center col-sm-8 col-sm-offset-2 col-xs-12">
		<b class="text-center">
	        Última modificação:
	        <span class="<?php echo $_smarty_tpl->tpl_vars['colorModify']->value;?>
">
	            <?php echo $_smarty_tpl->tpl_vars['lastModify']->value;
if ($_smarty_tpl->tpl_vars['lastModify']->value != 'nunca') {?> (<?php echo $_smarty_tpl->tpl_vars['datediff']->value;?>
 dias atrás)<?php }?>.
	        </span>
	        <?php if ($_smarty_tpl->tpl_vars['freeModify']->value != '') {?>
                Liberação de alterações na data <?php echo $_smarty_tpl->tpl_vars['freeModify']->value;?>
.
            <?php }?>
	    </b>
	</div>
	<div class="col-sm-8 col-sm-offset-2 col-xs-12">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-2 text-center input-group">
						<input type="text" class="form-control center-placeholder" 
							   placeholder="Pesquisar por nome" id="game-search">
						<div class="input-group-addon">
							<button class="btn btn-white btn-round btn-just-icon" id="submit-search">
								<i class="material-icons">search</i><div class="ripple-container"></div>
							</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
				        <button id="ps4" class="btn btn-default filter-button filter-button-active" data-filter="ps4">PS4</button>
				        <button class="btn btn-default filter-button" data-filter="xone">XBOX One</button>
				        <button class="btn btn-default filter-button" data-filter="switch">Switch</button>
				        <select class="form-control" id="filter-genre">
			        		<option value="all">Gênero</option>
			        		<?php
$_from = $_smarty_tpl->tpl_vars['genres']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_genre_0_saved_item = isset($_smarty_tpl->tpl_vars['genre']) ? $_smarty_tpl->tpl_vars['genre'] : false;
$_smarty_tpl->tpl_vars['genre'] = new Smarty_Variable();
$__foreach_genre_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_genre_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['genre']->value) {
$__foreach_genre_0_saved_local_item = $_smarty_tpl->tpl_vars['genre'];
?>
			        			<option value="<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
"><?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
</option>
			        		<?php
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_local_item;
}
}
if ($__foreach_genre_0_saved_item) {
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_item;
}
?>
			        	</select>
			        </div>
			    </div>

		    </div>
	    </div>
    </div>
</div>
<div class="row">
	<?php
$_from = $_smarty_tpl->tpl_vars['games']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_game_1_saved_item = isset($_smarty_tpl->tpl_vars['game']) ? $_smarty_tpl->tpl_vars['game'] : false;
$_smarty_tpl->tpl_vars['game'] = new Smarty_Variable();
$__foreach_game_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_game_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['game']->value) {
$__foreach_game_1_saved_local_item = $_smarty_tpl->tpl_vars['game'];
?>
		<?php if ($_smarty_tpl->tpl_vars['game']->value->get('ps4')) {?>
			<?php $_smarty_tpl->tpl_vars["ps4"] = new Smarty_Variable("ps4", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "ps4", 0);?>
		<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars["ps4"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "ps4", 0);?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['game']->value->get('xone')) {?>
			<?php $_smarty_tpl->tpl_vars["xone"] = new Smarty_Variable("xone", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "xone", 0);?>
		<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars["xone"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "xone", 0);?>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['game']->value->get('switch')) {?>
			<?php $_smarty_tpl->tpl_vars["switch"] = new Smarty_Variable("switch", null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "switch", 0);?>
		<?php } else { ?>
			<?php $_smarty_tpl->tpl_vars["switch"] = new Smarty_Variable('', null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "switch", 0);?>
		<?php }?>
		<div class="col-sm-3 col-xs-12 filter <?php echo $_smarty_tpl->tpl_vars['ps4']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['xone']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['switch']->value;?>
 genre_<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id_genre');?>
 game-library"
			style="margin-bottom: 15px">
			<div class="card card-profile">
				<div class="card-avatar game-cover library-game"
					data-id="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
" >
					<img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['game']->value->firstCover();?>
"/>
				</div>

				<div class="content">
					<h6 class="game_name"><b><?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
</b></h6>
					<p><?php echo $_smarty_tpl->tpl_vars['game']->value->platforms();?>
</p>	
					
					<?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('role') == 'member') {?>
						<?php if (isset($_smarty_tpl->tpl_vars['alreadyHave']->value[$_smarty_tpl->tpl_vars['game']->value->get('id')])) {?>
							<p><small>Já adicionado: <?php echo trim($_smarty_tpl->tpl_vars['alreadyHave']->value[$_smarty_tpl->tpl_vars['game']->value->get('id')],', ');?>
</small></p>
						<?php }?>
						<?php if ($_smarty_tpl->tpl_vars['game']->value->get('available') && $_smarty_tpl->tpl_vars['freeModify']->value == '') {?>
							<button type="button" class="btn btn-success btn-round add-fav" data-game="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
"
								data-platforms="<?php echo $_smarty_tpl->tpl_vars['ps4']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['xone']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['switch']->value;?>
">
								Adicionar
							</button>
						<?php }?>
					<?php }?>
				</div>
			</div>
		</div>
	<?php
$_smarty_tpl->tpl_vars['game'] = $__foreach_game_1_saved_local_item;
}
}
if ($__foreach_game_1_saved_item) {
$_smarty_tpl->tpl_vars['game'] = $__foreach_game_1_saved_item;
}
?>

</div>


<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, (dirname($_smarty_tpl->source->filepath)).("/modal_library.tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, (dirname($_smarty_tpl->source->filepath)).("/platform_select.tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}
}
