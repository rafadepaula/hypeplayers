<a name="topo"></a>
{if $actualUser->get('role') == 'member' && !$hasFavorites && $inCookie == 0}
	<div class="row list-not-done">
		<div class="col-sm-12 alert alert-warning text-center">
			<p>Você ainda não fez sua lista de preferências!</p>
		</div>
	</div>
{/if}

{if $actualUser->get('role') == 'member' && !$hasFavorites && $inCookie > 0 && $inCookie < 10}
	<div class="row list-not-done">
		<div class="col-sm-12 alert alert-info text-center">
			<p>Você não finalizou sua lista de preferências! Faltam {10 - $inCookie} jogos para terminar!</p>
		</div>
	</div>
{/if}


<div class="row list-must-save" {if $inCookie lt 10 && !$mustSave} style="display: none" {/if} >
	<div class="col-sm-12 alert alert-danger text-center">
		<p>Salve suas alterações para continuar!!</p>
	</div>
</div>

<div class="fixed-alert-bottom fixed-alert-bottom-centered help" {if $inCookie lt 10 && !$mustSave} style="display: none" {/if} id="save-changes" data-help="finish-add">
	<button class="btn btn-round btn-danger" id="save-changes-button">
		<i class="fa fa-exclamation-triangle"></i> 
		Salvar alterações
	</button>
</div>

<div class="fixed-alert-bottom">
    <button class="btn btn-round btn-info topo">
        <i class="fa fa-caret-up"></i>&nbsp;
        Topo
    </button>
</div>

<div class="row" style="margin-bottom: 35px">
	<div class="col-sm-8 col-sm-offset-2 col-xs-12 help" data-help="search-library">
		<div class="card">
			<div class="card-content">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-2 text-center input-group">
						<input type="text" class="form-control center-placeholder" 
							   placeholder="Pesquisar por nome" id="game-search">
						<div class="input-group-addon">
							<button class="btn btn-white btn-round btn-just-icon" id="submit-search">
								<i class="material-icons">search</i><div class="ripple-container"></div>
							</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
				        <button id="ps4" class="btn btn-default filter-button filter-button-active" data-filter="ps4">PS4</button>
				        <button class="btn btn-default filter-button" data-filter="xone">XBOX One</button>
				        <button class="btn btn-default filter-button" data-filter="switch">Switch</button>
				        <select class="form-control" id="filter-genre">
			        		<option value="all">Gênero</option>
			        		{foreach $genres as $genre}
			        			<option value="{$genre->get('id')}">{$genre->get('name')}</option>
			        		{/foreach}
			        	</select>
			        </div>
			    </div>

		    </div>
	    </div>
    </div>
</div>
<div class="row">
	{foreach $games as $game}
		{if $game->get('ps4')}
			{assign var="ps4" value="ps4"}
		{else}
			{assign var="ps4" value=""}
		{/if}
		{if $game->get('xone')}
			{assign var="xone" value="xone"}
		{else}
			{assign var="xone" value=""}
		{/if}
		{if $game->get('switch')}
			{assign var="switch" value="switch"}
		{else}
			{assign var="switch" value=""}
		{/if}
		<div class="col-sm-3 col-xs-12 filter {$ps4} {$xone} {$switch} genre_{$game->get('id_genre')} game-library help"
			style="margin-bottom: 15px" data-help="game-container">
			<div class="card card-profile">
				<div class="card-avatar game-cover library-game help"
                     data-help="game-cover"
					 data-id="{$game->get('id')}" data-name="{$game->get('name')}" >
					<img class="img" src="/{$game->firstCover()}"/>
				</div>

				<div class="content">
					<h6 class="game_name"><b>{$game->get('name')}</b></h6>
					<p>{$game->platforms()}</p>	
					{* <p class="text-success"><strong>Alta</strong></p> *}
					{if $actualUser->get('role') == 'member'}
						{if $game->get('id')|in_array:$alreadyHave}
							<p><small>Já adicionado na lista</small></p>
                        {else}
                            {if $game->get('available')}
                                <button type="button" class="btn btn-success btn-round add-fav help" data-game="{$game->get('id')}" data-help="game-add">
                                    Adicionar
                                </button>
                            {/if}
						{/if}
					{/if}
				</div>
			</div>
		</div>
	{/foreach}

</div>


{include file=$smarty.current_dir|cat:"/modal_library.tpl"}
{include file=$smarty.current_dir|cat:"/platform_select.tpl"}