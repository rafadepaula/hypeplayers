<form action="/rent/changeStatus/{$rent->get('id')}" method="post" id="send-form">
	<div class="row">
		<div class="col-sm-12">
			<b>Método de envio:</b> {$rent->get('carrier_mode', true)} <br>
			<b>Última modificação:</b> {$rent->get('last_modify', true)}
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>Status</label>
			<select class="form-control" name="status" required>
				<option {if $rent->get('status') == 'transit'} selected {/if} value="transit">
					Em trânsito para entrega
				</option>
				<option {if $rent->get('status') == 'delivered'} selected {/if} value="delivered">
					Entregue
				</option>
				<option {if $rent->get('status') == 'transit_back'} selected {/if} value="transit_back">
					Em trânsito para devolução
				</option>
				<option {if $rent->get('status') == 'complete'} selected {/if} value="complete">
					Completo
				</option>
				<option {if $rent->get('status') == 'complete_problem'} selected {/if} value="complete_problem">
					Completo mas com problemas
				</option>
				<option {if $rent->get('status') == 'delayed'} selected {/if} value="delayed">
					Atrasado
				</option>
				<option {if $rent->get('status') == 'waiting_post'} selected {/if} value="waiting_post">
					Aguardando postagem
				</option>
				<option {if $rent->get('status') == 'giveback_requested'} selected {/if} value="giveback_requested">
					Devolução requisitada
				</option>
				<option {if $rent->get('status') == 'change_requested'} selected {/if} value="change_requested">
					Troca requisitada
				</option>
				<option {if $rent->get('status') == 'renew_requested'} selected {/if} value="renew_requested">
					Renovação requisitada
				</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-group">
			<label>Obs. administrativas</label>
			<textarea class="form-control" name="ps_adm">{$rent->get('ps_adm')}</textarea>
		</div>
		<div class="col-sm-6 form-group">
			<label>Obs. sobre transporte</label>
			<textarea class="form-control" name="ps_carrier"
				      placeholder="Será mostrado ao membro">{$rent->get('ps_carrier')}</textarea>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 form-group">
			<label>Observações feitas pelo membro</label>
			<textarea disabled class="form-control">{$rent->get('ps_user')}</textarea>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<button class="btn btn-success btn-block" type="submit">
				Alterar
			</button>
		</div>
	</div>
</form>