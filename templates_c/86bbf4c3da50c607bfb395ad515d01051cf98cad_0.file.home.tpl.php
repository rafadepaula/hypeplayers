<?php
/* Smarty version 3.1.28, created on 2017-10-16 05:00:58
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Member\home.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59e4592aa9dc06_26962655',
  'file_dependency' => 
  array (
    '86bbf4c3da50c607bfb395ad515d01051cf98cad' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Member\\home.tpl',
      1 => 1508130919,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e4592aa9dc06_26962655 ($_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Pending' || $_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Taxed') {?>
<div class="row">
    <div class="col-lg-12 alert alert-danger text-center">
        Sua conta necessita atenção!
    </div>
    <?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Taxed') {?>
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Você recebeu uma multa</p>
                    <h4 class="title">Clique para acessar a página de multas e reativar seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">money_off</i>
                        Você recebeu uma multa. Acesse a página de multas para regularizar a situação.
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
    <div class="col-sm-8 col-sm-offset-2 col-xs-12">
        <a href="/signature/activate">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Pagamento pendente</p>
                    <h4 class="title">Clique para ativar o seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Você está com pagamento pendente! Ative sua conta efetivando o pagamento de sua assinatura.
                    </div>
                </div>
            </div>
        </a>
    </div>
    <?php }?>

</div>
<?php } else { ?>
<div class="row">
    <?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Preactivated') {?>
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">access_time</i>
                </div>
                <div class="card-content">
                    <p class="category">Processando pagamento</p>
                    <h4 class="title">Quase lá! O seu pagamento está sendo processado.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        A solicitação de assinatura foi registrada. Agora, nosso sistema está
                        se comunicando com a empresa responsável pelo processamento do cartão
                        de crédito para liberar o seu acesso!
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Active') {?>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">account_circle</i>
                </div>
                <div class="card-content">
                    <p class="category">Plano atual</p>
                    <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['signature']->value->get('level',true);?>
</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">date_range</i>
                        Membro desde <?php echo $_smarty_tpl->tpl_vars['signature']->value->get('since',true);?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">alarm</i>
                </div>
                <div class="card-content">
                    <p class="category">Renovação</p>
                    <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['signature']->value->get('expiry',true);?>
</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">attach_money</i>
                        Cobrança automática
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12">
            <a href="/address/view">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="green">
                        <i class="material-icons">add_location</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Mais endereços</p>
                        <h4 class="title">Cadastrar endereço</h4>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <i class="material-icons">add_location</i>
                            Cadastre endereços alternativos
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php }?>

</div>
<?php }?>


<div class="row" style="margin-top: 15px;">
    <?php
$_from = $_smarty_tpl->tpl_vars['recents']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rent_0_saved_item = isset($_smarty_tpl->tpl_vars['rent']) ? $_smarty_tpl->tpl_vars['rent'] : false;
$_smarty_tpl->tpl_vars['rent'] = new Smarty_Variable();
$__foreach_rent_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_rent_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['rent']->value) {
$__foreach_rent_0_saved_local_item = $_smarty_tpl->tpl_vars['rent'];
?>
        <?php $_smarty_tpl->tpl_vars["game"] = new Smarty_Variable($_smarty_tpl->tpl_vars['rent']->value->get('id_game',true), null);
$_smarty_tpl->ext->_updateScope->updateScope($_smarty_tpl, "game", 0);?>
        <div class="col-sm-4">
            <div class="card card-profile">
                <div class="card-avatar game-cover library-game"
                    data-id="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" data-name="<?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
" 
                    data-platform="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('platform');?>
">
                    <img class="img" src="/<?php echo $_smarty_tpl->tpl_vars['game']->value->firstCover();?>
"/>
                </div>
                <div class="card-content">
                    <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['game']->value->get('data-name');?>
</h4>
                    <p>
                        <b><?php echo $_smarty_tpl->tpl_vars['rent']->value->get('status',true);?>
</b>
                    </p>
                    <p class="category">
                        <a>
                            <button type="button" rel="tooltip" title="Visualizar locação"
                                    class="btn btn-default btn-simple view-one"
                                    data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
">
                                <i class="fa fa-eye"></i>
                            </button>
                        </a>
                        <a>
                            <button type="button" rel="tooltip" title="Enviar comentário"
                                    class="btn btn-default btn-simple send-comment"
                                    data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
"
                                    data-url="/member/home">
                                <i class="fa fa-edit"></i>
                            </button>
                        </a>
                        <?php if ($_smarty_tpl->tpl_vars['rent']->value->get('status') == 'delivered' || $_smarty_tpl->tpl_vars['rent']->value->get('status') == 'delayed') {?>
                            <a>
                                <button type="button" rel="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['changeText']->value;?>
"
                                        class="btn btn-success btn-simple give-back"
                                        data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
">
                                    <i class="fa fa-exchange"></i>
                                </button>
                            </a>
                            <?php if ($_smarty_tpl->tpl_vars['rent']->value->get('renew_qtt') < 2) {?>
                                <a>
                                    <button type="button" rel="tooltip" title="Solicitar renovação"
                                            class="btn btn-info btn-simple request-renew"
                                            data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                </a>
                            <?php }?>
                        <?php }?>
                        <?php if (false) {?>
                            <a>
                                <button type="button" rel="tooltip" title="Confirmar recebimento"
                                        class="btn btn-success btn-simple confirm-deliver"
                                        data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
">
                                    <i class="fa fa-check"></i>
                                </button>
                            </a>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['rent']->value->get('status') == 'waiting_post') {?>
                            <a>
                                <button type="button" rel="tooltip" title="Confirmar postagem"
                                        class="btn btn-success btn-simple confirm-post"
                                        data-id-rent="<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id');?>
">
                                    <i class="fa fa-truck"></i>
                                </button>
                            </a>
                        <?php }?>
                    </p>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">access_time</i> modificado em <?php echo $_smarty_tpl->tpl_vars['rent']->value->get('last_modify',true);?>

                    </div>
                </div>
            </div>
        </div>
    <?php
$_smarty_tpl->tpl_vars['rent'] = $__foreach_rent_0_saved_local_item;
}
}
if ($__foreach_rent_0_saved_item) {
$_smarty_tpl->tpl_vars['rent'] = $__foreach_rent_0_saved_item;
}
?>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="card card-nav-tabs">
            <div class="card-header" data-background-color="purple">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#favorites" data-toggle="tab">
                                    <i class="material-icons" style="width: 30px;">star_rate</i>
                                    Preferências
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="">
                                <a href="#history" data-toggle="tab">
                                    <i class="material-icons">history</i>
                                    Histórico
                                <div class="ripple-container"></div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="favorites">
                        <table class="table">
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['favorites']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_favorite_1_saved_item = isset($_smarty_tpl->tpl_vars['favorite']) ? $_smarty_tpl->tpl_vars['favorite'] : false;
$_smarty_tpl->tpl_vars['favorite'] = new Smarty_Variable();
$__foreach_favorite_1_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_favorite_1_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['favorite']->value) {
$__foreach_favorite_1_saved_local_item = $_smarty_tpl->tpl_vars['favorite'];
?>
                                    <tr>
                                        <td><?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position')+1;?>
</td>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('id_game',true)->get('name');?>

                                            (<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('platform',true);?>
)
                                        </td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_1_saved_local_item;
}
}
if ($__foreach_favorite_1_saved_item) {
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_1_saved_item;
}
?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="history">
                        <table class="table datatable">
                            <tbody>
                                <?php
$_from = $_smarty_tpl->tpl_vars['history']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_rent_2_saved_item = isset($_smarty_tpl->tpl_vars['rent']) ? $_smarty_tpl->tpl_vars['rent'] : false;
$_smarty_tpl->tpl_vars['rent'] = new Smarty_Variable();
$__foreach_rent_2_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_rent_2_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['rent']->value) {
$__foreach_rent_2_saved_local_item = $_smarty_tpl->tpl_vars['rent'];
?>
                                    <tr>
                                        <td>
                                            <?php echo $_smarty_tpl->tpl_vars['rent']->value->get('id_game',true)->get('name');?>

                                            (<?php echo $_smarty_tpl->tpl_vars['rent']->value->get('platform',true);?>
)
                                        </td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['rent']->value->get('status',true);?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['rent']->value->get('start',true);?>
 - <?php echo $_smarty_tpl->tpl_vars['rent']->value->get('end',true);?>
</td>
                                    </tr>
                                <?php
$_smarty_tpl->tpl_vars['rent'] = $__foreach_rent_2_saved_local_item;
}
}
if ($__foreach_rent_2_saved_item) {
$_smarty_tpl->tpl_vars['rent'] = $__foreach_rent_2_saved_item;
}
?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, (dirname($_smarty_tpl->source->filepath)).("/../Game/modal_library.tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>

<?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, (dirname($_smarty_tpl->source->filepath)).("/../Rent/view_modal.tpl"), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
}
}
