<?php

    class SignatureModel extends AppModel {
        public function getSignature($id_user){
            return $this->getDto('signature', 'id_user', $id_user);
        }

        public function getPreapproval($id){
            return $this->getDto('pre_approval', 'id', $id);
        }
        
        public function create($member, $secret){
            $id_user = $member->get('id_user');
            $signature = new Signature();
            $signature->set('id_user', $id_user);
            if(!isset($_POST['levelToPost'])){
                echo '<li>Escolha seu plano de assinatura!</li>';
                return false;
            }else{
                if(empty($_POST['levelToPost'])){
                    echo '<li>Escolha seu plano de assinatura!</li>';
                    return false;
                }
            }
            if(!$signature->set('level', ($_POST['levelToPost']))){
                echo '<li>Plano de assinatura inválido!</li>';
                return false;
            }
            $signature->set('value', constant('_LEVEL_'.$signature->get('level').'_VALUE'));
            $signature->set('since', date('Y-m-d'));
            $signature->set('expiry', date('Y-m-d', strtotime('+1 month')));
            $signature->set('available_rent', 0);
            $signature->set('change_requested', 0);
            $signature->set('renew_requested', 0);
            $signature->set('can_change', $signature->get('level') == 3);
            $signature->set('simultaneous', $signature->get('level') == 1 ? 1 : 2);
            $signature->set('change_count', 0);
            $signature->set('status', 'Pending');
            $signature->set('reference', $secret);
            $signature->set('holder_cpf', $member->get('cpf'));
            $signature->set('holder_birthdate', $member->get('birthday'));

            if(!$this->insert('signature', $signature)){
                echo '<li>'._INSERT_ERROR.'</li>';
                return false;
            }
            return true;

        }

        public function savePreapproval($id = null){
            $preApproval = $this->getPreapproval($id);
            $ret = $this->makeDto($preApproval, $id);
            $preApproval = $ret[0];
            $errors = $ret[1];

            if($errors != ''){
                Viewer::flash($errors, 'e');
                return false;
            }else{
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('pre_approval', $preApproval) : $this->update('pre_approval', $preApproval, array('id' => $id));
                if($ret){
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }

        public function paypalcheckout($apiContext, $result){
            $id_plan = $_COOKIE['id_plan'];
            setcookie('checkoutProccess', null, time() - 3600, '/');
            setcookie('id_plan', null, time() - 3600, '/');

            $plan = Plan::get($id_plan, $apiContext);

            if(!$result){
                $this->cancelActivationByError($plan, null, $apiContext);
                return false;
            }

            $token = $_GET['token'];
            $agreement = new Agreement();
            try {
                $agreement->execute($token, $apiContext);
            } catch (Exception $ex) {
                $this->cancelActivationByError($plan, null, $apiContext);
                return false;
            }

            // Recupera o agreement e o plan para salvar seus ids
            $agreement = Agreement::get($agreement->getId(), $apiContext);
            $payer = $agreement->getPayer();
            $payerInfo = $payer->getPayerInfo();

            $id_user = unserialize($_SESSION['user'])->get('id');
            $signature = $this->getSignature($id_user);
            $signature->set('id_agreement', $agreement->getId());
            $signature->set('id_plan', $id_plan);
            $signature->set('id_payer', $payerInfo->getPayerId());
            
            $this->checkActivatePayment($agreement, $signature);
            
            $this->initTransaction();
            $planHistory = new Plan_history();
            $planHistory->set('id_plan', $id_plan);
            $planHistory->set('id_agreement', $agreement->getId());
            $planHistory->set('id_payer', $payerInfo->getPayerId());
            $planHistory->set('id_user', $id_user);
            if(!$this->insert('plan_history', $planHistory)){
                $this->cancelActivationByError($plan, $agreement, $apiContext);
                $this->cancelTransaction();
                return false;
            }
            $agreementDetails = $agreement->getAgreementDetails();
            $payment = new Payment();
            $payment->set('id_plan_history', $this->lastInserted('plan_history'));
            $payment->set('date', date('Y-m-d'));
            $payment->set('value', $agreementDetails->getLastPaymentAmount()->getValue());
            if(!$this->insert('payment', $payment)){
                $this->cancelActivationByError($plan, $agreement, $apiContext);
                $this->cancelTransaction();
                return false;
            }
            if(!$this->update('signature', $signature, array('id_user' => $signature->get('id_user')))){
                $this->cancelActivationByError($plan, $agreement, $apiContext);
                $this->cancelTransaction();
                return false;
            }

            $this->endTransaction();
            
            return true;
        }

        public function updateAvailableRent(Rent $rent, $action = 'remove'){
            $signature = $this->getSignature($rent->get('id_user'));
            if($action == 'remove'){
                if($signature->get('available_rent') > 0){
                    $signature->set('available_rent', $signature->get('available_rent') - 1);
                    return $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
                }else{
                    return true;
                }
            }else{
            	if($action == 'change_completed'){
            		if($signature->get('level') > 1){
	                    $signature->set('available_rent', $signature->get('available_rent') + 1);
	                }else{
	                    return false;
	                }
	                return $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
	            }else {
            		if($rent->get('can_sum')){
            			$signature->set('available_rent', $signature->get('available_rent') + 1);
            			return $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
		            }
		            return true;
		            /*if($signature->get('level') === 1){
						if($signature->get('available_rent') >= 1)
							return false;
						$signature->set('available_rent', 1);
					}else{
						if($signature->get('available_rent') >= 2)
							return false;
						$signature->set('available_rent', $signature->get('available_rent') + 1);
					}
					return $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));*/
	            }
            }
        }

        public function canChangeCron(){
            $cond = array(
                'level' => 3,
                'conscond1' => 'and',
                'status' => 'Active',
            );
            $signatures = $this->search('signature', '*', $cond);
            foreach($signatures as $signature){
                $today = date('d');
                $sigDay = date('d', strtotime($signature->get('since')));
                if($today == $sigDay){
                    $signature->set('can_change', 1);
                    $signature->set('change_count', 0);
                    $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
                }
            }
        }

        public function completeChange($rent){
            $sql = 'select * from change_request where id_rent = '.$rent->get('id').' or id_rent2 = '.$rent->get('id');
            $changeRequests = $this->query($sql);
            $changeRequests = $this->query2dto($changeRequests, 'change_request');

            if(!count($changeRequests)){
                return false;
            }

            $sumChangeCount = false;
            foreach($changeRequests as $changeRequest){
                if(empty($changeRequest->get('date_done')))
                    $sumChangeCount = true;
                else
                    continue;
                $changeRequest->set('date_done', date('Y-m-d'));
                if(!$this->update('change_request', $changeRequest, array('id' => $changeRequest->get('id')))){
                    return false;
                }
            }
            $signature = $this->getSignature($rent->get('id_user'));
            $signature->set('change_requested', 0);
            if($sumChangeCount){
                $signature->set('change_count', $signature->get('change_count') + 1);
                if($signature->get('level') == 2){
					if($signature->get('change_count') == 1)
						$signature->set('can_change', 0);
                }elseif($signature->get('level') == 3){
	                if($signature->get('change_count') == 2)
	                	$signature->set('can_change', 0);
                }else{
                	return false;
                }
            }
            if(!$this->update('signature', $signature, array('id_user' => $signature->get('id_user')))){
                return false;
            }
            return $this->updateAvailableRent($rent, 'change_completed');
        }

        public function weekWarningCron(){
            $signatures = $this->search('signature', '*', array('status' => 'Active'));
            $notificationModel = new NotificationModel();
            foreach($signatures as $signature){
                if(strtotime($signature->get('end')) == strtotime('+7 days')){
                    $notificationModel->weekWarning($signature->get('id_user'));
                }
            }
        }

        public function exclusionCron(){
            $sql = 'select u.name, u.email, u.id
                    from signature s 
                    join user u on u.id = s.id_user 
                    where s.expiry < "'.date('Y-m-d').'" and 
                          last_payment IS NULL';
            $signatures = $this->query($sql);
            foreach($signatures as $signature){
                $this->exclusionEmail($signature['email'], $signature['name']);
                $this->delete('user', array('id' => $signature['id']));
                $this->delete('member', array('id_user' => $signature['id']));
                $this->delete('signature', array('id_user' => $signature['id']));
            }
        }

        public function exclusionEmail($to, $name){
            require_once 'base/plugins/phpmailer/PHPMailerAutoload.php';
            
            $email = new PHPMailer;
            
            $email->isSMTP();
            $email->Host       = 'mail.hypeplayers.com.br';
            $email->SMTPAuth   = true;
            $email->Username   = 'webmaster@hypeplayers.com.br';
            $email->Password   = 'Zs9w63Q0oy';
            $email->SMTPSecure = false;
            $email->Port       = 26;
            
            $email->From     = 'webmaster@hypeplayers.com.br';
            $email->FromName = 'HypePlayers';
            $email->addBCC($to, $name);
            $email->isHTML(true);
            
            $email->Subject = 'HypePlayers - Desligamento do clube';

            $html = file_get_contents('app/viewer/Member/exclusionEmail.html');
            $html = str_replace('%url_join%', _BASE_URL_.'member/join/', $html);
            $html = str_replace('%url_contact%', 'https://hypeplayers.com.br/contato.php', $html);

            $email->Body    = $html;
            $email->CharSet = 'UTF-8';
            
            return $email->send();
        }


        public function checkout($signature){
	        if(isset($_POST['plan_level'])){
		        $signature->set('level', $_POST['plan_level']);
		        $signature->set('value', constant('_LEVEL_'.$_POST['plan_level'].'_VALUE'));
	        }

            $signature->set('accession_code', $_POST['code']);
            $preApproval = $this->getDto('pre_approval', 'level', $signature->get('level'));
            $signature->set('id_preapproval', $preApproval->get('id'));
            $signature->set('last_payment', null);
            $signature->set('holder_name', $_POST['holder_name']);
            $signature->set('holder_birthdate', $_POST['holder_birthdate']);
            $signature->set('holder_cpf', $_POST['holder_cpf']);
            $signature->set('status', 'Preactivated');
            $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
            echo json_encode(array(
                'type' => 'success',
                'text' => 'O sistema irá registrar o pagamento em breve.',
                'title' => 'Feito! :D',
                'timer' => 5000,
            ));
        }

        public function notificationListener(){
            $notification = $_POST;
            
            $notificationDto = new Notification_pagseguro();
            $notificationDto->set('code', $notification['notificationCode']);
            $notificationDto->set('type', $notification['notificationType']);
            $this->insert('notification_pagseguro', $notificationDto);

            $notificationXml = $this->pagseguro->proccessNotification($notificationDto);
            $this->proccessNotification($notificationXml, $notificationDto->get('type'));

            if(!is_dir(__DIR__.'/../assets/notifications/'))
                mkdir(__DIR__.'/../assets/notifications/');

            $file = __DIR__.'/../assets/notifications/'.date('d-m-Y').'-LISTENER.txt';

            file_put_contents($file, serialize($notification), FILE_APPEND);
            file_put_contents($file, PHP_EOL.'---BREAK---', FILE_APPEND);
        }

        public function proccessNotification($notification, $type){
            $xml = $notification;

            if(!is_dir(__DIR__.'/../assets/notifications/'))
                mkdir(__DIR__.'/../assets/notifications/');

            $file = __DIR__.'/../assets/notifications/'.date('d-m-Y').'-PROCCESS.txt';

            file_put_contents($file, serialize($notification), FILE_APPEND);
            file_put_contents($file, PHP_EOL.'---BREAK---', FILE_APPEND);

            if($type == 'transaction'){
                $paymentModel = new PaymentModel();
                if($paymentModel->proccessTransaction($xml)){
                    $signature = $this->getDto('signature', 'reference', $xml['reference']);
                    $this->updateStatusByPayment($signature);
                }
            }
        }

        public function notActivated($signature){
            $status = $signature->get('status');
            if($status == 'Pending' || $status == 'Canceled' || $status == 'Suspended'){
                return true;
            }
            return false;
        }

        public function updateStatusByPayment($signature){
            $notificationModel = new NotificationModel();
	        if($signature->get('level') == 1){
		        $signature->set('available_rent', 1);
		        $signature->set('can_change', 0);
	        }else{
	        	$rentModel = new RentModel();
	        	$rents = $rentModel->getUserRents($signature->get('id_user'));
	        	$rentModel->updateCanSum($rents);
		        $signature->set('available_rent', 2 - count($rents));
		        $signature->set('can_change', 1);
	        }
	        $signature->set('change_count', 0);
	        $signature->set('last_payment', date('Y-m-d H:i:s'));
	        $signature->set('expiry', date('Y-m-d', strtotime('+1 month')));
	        if($signature->get('status') == 'Preactivated'){
		        $signature->set('since', date('Y-m-d'));
		        $signature->set('status', 'Active');
                $notificationModel->notifyActivation($signature->get('id_user'));
                $notificationModel->newMemberPaid($signature->get('id_user'));
            }elseif($signature->get('status') == 'Active'){
                /*if(!empty($signature->get('last_payment'))){
                    $notificationModel = new NotificationModel();
                    $notificationModel->send('admin', 'Membro reativou assinatura!', '/member/view/'.$signature->get('id_user'), false);
                }*/
            }
            $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
        }

        public function canCancel($id_user){
            $signature = $this->getSignature($id_user);

            if($signature->get('status') != 'Active'){
                if($signature->get('status') == 'Change_requested')
                    return array('error' => 'Você solicitou uma troca de plano.', 'canCancel' => false);
                return array('error' => 'Você não está com sua assinatura em dia.', 'canCancel' => false);
            }

            if($signature->get('available_rent') == 0){
                return array('error' => 'Você possui jogos em sua posse!', 'canCancel' => false);
            }

            $sql = 'select * from rent where status NOT LIKE "%complete%" and id_user = '.$id_user;
            $rents = $this->query($sql);
            if(count($rents) > 0){
                return array('error' => 'Você possui jogos em sua posse!', 'canCancel' => false);
            }

            return array('canCancel' => true);
        }

        public function requestCancel($id_user, $name){
            $signature = $this->getSignature($id_user);
            $signature->set('status', 'Cancel_requested');

            $notificationModel = new NotificationModel();
            $notificationModel->send('admin', $name.' solicitou cancelamento.', '/member/view/'.$id_user, false);

            if($this->update('signature', $signature, array('id_user' => $id_user)))
                echo 1;
            else
                echo 0;
            return;
        }

        public function requestChange($id_user, $name, $toLevel){
            $signature = $this->getSignature($id_user);
            $signature->set('status', 'Change_requested');

            $request = new Plan_change();
            $request->set('id_user', $id_user);
            $request->set('from_level', $signature->get('level'));
            $request->set('to_level', $toLevel);

            $this->initTransaction();
            if(!$this->update('signature', $signature, array('id_user' => $id_user)) ||
               !$this->insert('plan_change', $request)){
                $this->cancelTransaction();
                return false;
            }else {
                $notificationModel = new NotificationModel();
                $notificationModel->send('admin', $name . ' solicitou troca para plano ' . $toLevel . '.', '/member/view/' . $id_user, false);
                $this->endTransaction();
                return true;
            }
        }

        public function cancel($id_user){
            $signature = $this->getSignature($id_user);
            $accession_code = $signature->get('accession_code');

            $signature->set('status', 'Canceled');
            $signature->set('available_rent', 0);
            $signature->set('can_change', 0);
            if($this->update('signature', $signature, array('id_user' => $id_user))){
	            $notificationModel = new NotificationModel();
	            $notificationModel->send($id_user, 'Seu pedido de cancelamento foi realizado.', false);
	            $this->delete('favorite', array('id_user' => $id_user));
	            $this->pagseguro->cancel($accession_code);
                echo '1';
                return true;
            }else{
                echo '0';
                return false;
            }
        }

        public function rejectCancel($id_user){
            $signature = $this->getSignature($id_user);
            $lastPayment = $this->search('payment', '*', array('id_user' => $id_user), 'date ASC', false, '1');
            if(count($lastPayment) < 1){
                $signature->set('status', 'Canceled');
            }else {
                $lastPayment = $lastPayment[0];
                if(strtotime($lastPayment->get('date')) < strtotime('-30 days')){
                    $signature->set('status', 'Canceled');
                }else{
                    $signature->set('status', 'Active');
                }
            }

            if($this->update('signature', $signature, array('id_user' => $id_user))){
                $notificationModel = new NotificationModel();
                $notificationModel->send($id_user, 'Seu pedido de cancelamento foi negado.', false);
                echo '1';
                return true;
            }else{
                echo '0';
                return false;
            }

        }

        public function change($id_user){
            $signature = $this->getSignature($id_user);
            $request = $this->getDto('plan_change', 'id_user', $id_user);
            $preApproval = $this->getDto('pre_approval', 'level', $request->get('to_level'));

            $signature->set('id_preapproval', $preApproval->get('id'));
            $signature->set('level', $request->get('to_level'));
            $signature->set('status', 'Pending');
            $signature->set('value', constant('_LEVEL_'.$signature->get('level').'_VALUE'));

            $this->initTransaction();

            if(!$this->update('signature', $signature, array('id_user' => $id_user)) ||
               !$this->delete('plan_change', array('id_user' => $id_user))){
                $this->cancelTransaction();
                echo '0';
                return false;
            }else{

                $notificationModel = new NotificationModel();
                $notificationModel->send($id_user, 'Seu pedido de troca de plano foi concedido.', false);

                $this->endTransaction();
                $this->pagseguro->cancel($signature->get('accession_code'));
            }
        }

        public function rejectChange($id_user){
            $signature = $this->getSignature($id_user);
            $lastPayment = $this->search('payment', '*', array('id_user' => $id_user), 'date ASC', false, '1');
            if(count($lastPayment) < 1){
                $signature->set('status', 'Canceled');
            }else {
                $lastPayment = $lastPayment[0];
                if(strtotime($lastPayment->get('date')) < strtotime('-30 days')){
                    $signature->set('status', 'Canceled');
                }else{
                    $signature->set('status', 'Active');
                }
            }

            $this->delete('plan_change', array('id_user' => $id_user));

            if($this->update('signature', $signature, array('id_user' => $id_user))){
                $notificationModel = new NotificationModel();
                $notificationModel->send($id_user, 'Seu pedido de troca de plano foi negado.', false);
                echo '1';
                return true;
            }else{
                echo '0';
                return false;
            }
        }

        public function cancelCron(){
            $sql = 'select * from signature where expiry = "'.date('Y-m-d', strtotime('-3 days')).'"';
            $signatures = $this->query($sql);
            $signatures = $this->query2dto($signatures, 'signature');
            $notificationModel = new NotificationModel();
            foreach($signatures as $signature){
                $signature->set('status', 'Suspended');
                $signature->set('available_rent', 0);
                $this->update('signature', $signature, array('id_user' => $signature->get('id_user')));
                $notificationModel->memberSuspended($signature->get('id_user'));
            }
        }

        public function canChange($id_user){
            $signature = $this->getSignature($id_user);
            if($signature->get('status') != 'Active'){
                if($signature->get('status') == 'Change_requested')
                    return array('error' => 'Você já solicitou uma troca de plano.', 'canChange' => false);
                return array('error' => 'Você não está com sua assinatura em dia.', 'canChange' => false);
            }

            $minChangeDay = strtotime('-5 days', strtotime(date($signature->get('expiry'))));
            $today = strtotime(date('Y-m-d'));
            if($today >= $minChangeDay){
                return array('canChange' => true);
            }else{
                $minChangeDay = date('d/m/Y', $minChangeDay);
                $period = '(a partir do dia '.$minChangeDay.')';
                return array('error' => 'Você não está no período de troca de plano '.$period,'.', 'canChange' => false);
            }
        }

	    public function suspendByDeactivation($id_user){
		    $signature = $this->getSignature($id_user);
		    $accession_code = $signature->get('accession_code');

		    $signature->set('status', 'Suspended');
		    $signature->set('available_rent', 0);
		    $signature->set('can_change', 0);
		    if($this->update('signature', $signature, array('id_user' => $id_user))){
			    $this->delete('favorite', array('id_user' => $id_user));
			    return true;
		    }else{
			    return false;
		    }
	    }

	    public function updateCanChangeByRenew($signature){
	    	$signature->set('change_count', $signature->get('change_count') + 1);
	    	if($signature->get('level') == 2){
	    		$signature->set('can_change', 0);
		    }else{
	    		if($signature->get('change_count') == 3){
					$signature->set('can_change', 0);
			    }
		    }
		    return $this->update('signature',$signature, array('id_user' => $signature->get('id_user')));

	    }
    }