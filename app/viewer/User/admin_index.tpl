<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    Banners
                    <a href="/banner/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                    <h4>Banners ativos</h4>
                    <table class="table table-hover table-striped datatable-banners">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Título</th>
                                <th>Subtítulo</th>
                                <th>Botão</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                        {foreach $banners as $banner}
                            {if $banner->get('display')}
                                <tr>
                                    <td>
                                        {$banner->get('position') + 1}
                                    </td>
                                    <td>
                                        {$banner->get('title')}
                                    </td>
                                    <td>
                                        {$banner->get('subtitle')}
                                    </td>
                                    <td>
                                        {if $banner->get('button_show')}
                                            <a href="{$banner->get('button_link')}" target="_blank"
                                                class="button" style="background-color: {$banner->get('button_color')}">
                                                {$banner->get('button_text')}
                                            </a>
                                        {else}
                                            Não mostrar
                                        {/if}
                                    </td>
                                    <td class="td-actions">
                                        <a href="/banner/delete/{$banner->get('id')}" class="confirm-link">
                                            <button type="button" rel="tooltip" title="Deletar banner"
                                                    class="btn btn-danger btn-simple">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        </a>
                                        <a href="/banner/toggle/{$banner->get('id')}" class="confirm-link">
                                            <button type="button" rel="tooltip" title="Desativar banner"
                                                    class="btn btn-warning btn-simple">
                                                <i class="fa fa-minus-circle"></i>
                                            </button>
                                        </a>
                                        <a href="/banner/view/{$banner->get('id')}">
                                            <button type="button" rel="tooltip" title="Visualizar banner"
                                                    class="btn btn-info btn-simple">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
                <hr style="color: black">
                <div class="table-responsive">
                    <h4>Banners inativos</h4>
                    <table class="table table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Subtítulo</th>
                            <th>Botão</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $banners as $banner}
                            {if !$banner->get('display')}
                                <tr>
                                    <td>
                                        {$banner->get('title')}
                                    </td>
                                    <td>
                                        {$banner->get('subtitle')}
                                    </td>
                                    <td>
                                        {if $banner->get('button_show')}
                                            <a href="{$banner->get('button_link')}" target="_blank"
                                               class="button" style="background-color: {$banner->get('button_color')}">
                                                {$banner->get('button_text')}
                                            </a>
                                        {else}
                                            Não mostrar
                                        {/if}
                                    </td>
                                    <td class="td-actions">
                                        <a href="/banner/delete/{$banner->get('id')}" class="confirm-link">
                                            <button type="button" rel="tooltip" title="Deletar banner"
                                                    class="btn btn-danger btn-simple">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                        </a>
                                        <a href="/banner/toggle/{$banner->get('id')}" class="confirm-link">
                                            <button type="button" rel="tooltip" title="Reativar banner"
                                                    class="btn btn-success btn-simple">
                                                <i class="fa fa-plus-circle"></i>
                                            </button>
                                        </a>
                                        <a href="/banner/view/{$banner->get('id')}">
                                            <button type="button" rel="tooltip" title="Visualizar banner"
                                                    class="btn btn-info btn-simple">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    Destaques
                    <a href="/featured/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                    <table class="table table-hover table-striped datatable-featured">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Jogo</th>
                            <th>Plataforma</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $featureds as $featured}
                            <tr>
                                <td>
                                    {$featured->get('position') + 1}
                                </td>
                                <td>
                                    {$featured->get('name')}
                                </td>
                                <td>
                                    {$featured->get('platform')}
                                </td>
                                <td class="td-actions">
                                    <a href="/featured/delete/{$featured->get('id')}" class="confirm-link">
                                        <button type="button" rel="tooltip" title="Deletar destaque"
                                                class="btn btn-danger btn-simple">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </a>
                                    <a href="/featured/view/{$featured->get('id')}">
                                        <button type="button" rel="tooltip" title="Visualizar destaque"
                                                class="btn btn-info btn-simple">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>