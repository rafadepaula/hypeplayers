<?php
    
    class Rent extends DTO {
        public $id;
        public $id_game;
        public $id_user;
        public $start;
        public $end;
        public $ps_adm;
        public $ps_user;
        public $ps_carrier;
        public $status;
        public $carrier_code;
        public $platform;
        public $carrier_mode;
        public $last_modify;
        public $renew_qtt;
        public $can_sum;

        public $FieldsValidation = array(
            'id_game' => array('existsForeign', ['game', 'id']),
            'id_user' => array('existsForeign', ['user', 'id']),
            'start' => 'validDate',
            'end' => 'validDate',
            'carrier_mode' => 'notEmpty,validCarrier',
            'platform' => 'notEmpty,validPlatform',
            'status' => 'validStatus',
        );
        public $FieldsErrors     = array(
            'id_game' => 'Informe um jogo válido para a locação.',
            'id_user' => 'Informe um membro válido para a locação.',
            'start' => 'Informe uma data de início válida.',
            'end' => 'Informe uma data final válida.',
            'carrier_mode' => 'Modo de entrega inválido.',
            'platform' => 'Informe uma plataforma válida.',
        );
        public $FieldsMasks      = array(
            'id_game' => array('getDto', 'game'),
            'id_user' => array('getDto', 'user'),
            'start' => 'dateMask',
            'end' => 'dateMask',
            'carrier_mode' => 'carrierMask',
            'platform' => 'platformMask',
            'status' => 'statusMask',
            'last_modify' => 'datetimeMask',
        );

        public function platformMask($input){
            $platforms = array(
                'ps4' => 'PS4',
                'xone' => 'XBOX One',
                'switch' => 'Nintendo Switch'
            );
            return $platforms[$input];
        }

        public function carrierMask($input){
            $modes = array(
                'courier' => 'Motoboy',
                'mail' => 'Correio',
                'withdraw' => 'Retirada',
            );
            return $modes[$input];
        }

        public function validCarrier($input){
            $valids = array('courier', 'mail', 'withdraw');
            return in_array($input, $valids);
        }

        public function validPlatform($input){
            $valids = array('ps4', 'xone', 'switch');
            return in_array($input, $valids);
        }

        public function statusMask($input){
            $status = array(
                'transit' => 'Em trânsito para entrega',
                'delivered' => 'Entregue',
                'transit_back' => 'Em trânsito para devolução',
                'complete' => 'Completo',
                'complete_problem' => 'Completo mas com problemas',
                'delayed' => 'Atrasado',
                'giveback_requested' => 'Devolução requisitada',
                'change_requested' => 'Troca requisitada',
                'waiting_post' => 'Aguardando postagem',
                'renew_requested' => 'Renovação requisitada',
            );
            return $status[$input];
        }

        public function validStatus($input){
            $status = array('transit', 'delivered', 'transit_back', 'change_requested', 
                            'complete','complete_problem', 'delayed', 'giveback_requested',
                            'renew_requested', 'waiting_post');
            return in_array($input, $status);
        }
    }