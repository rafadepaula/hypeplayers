<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <form action="/signature/editPreapproval/{$preApproval->get('id')}" method="post">
                    <div class="row">
                        <div class="col-sm-8 form-group">
                            <label>Código</label>
                            <input required type="text" name="code" class="form-control" placeholder="Código API do plano" value="{$preApproval->get('code')}">
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>Nível</label>
                            <select required name="level" class="form-control">
                                <option value="">Selecione</option>
                                <option {if $preApproval->get('level') == 1} selected {/if} value="1">1</option>
                                <option {if $preApproval->get('level') == 2} selected {/if} value="2">2</option>
                                <option {if $preApproval->get('level') == 3} selected {/if} value="3">3</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button class="btn btn-success btn-block btn-lg" type="submit">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>