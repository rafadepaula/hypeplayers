<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    {if $totalAddresses < 2}
                        <a href="/address/add" class="pull-right card-header-btn">
                            <button class="btn btn-white btn-round btn-just-icon" >
                                <i class="material-icons">add</i>
                            </button>
                        </a>
                    {/if}
                </h4>

                <p><b>Negrito:</b> endereço padrão para entrega.</p>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="table-responsive">
                                <table class="table table-hover table-striped datatable help" data-help="address-table">
                                    <thead>
                                        <tr>
                                            <th>Rua</th>
                                            <th>Nº</th>
                                            <th>Bairro</th>
                                            <th>Complemento</th>
                                            <th>Cidade</th>
                                            <th>Estado</th>
                                            <th>CEP</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {foreach $addresses as $address}
                                        {if $address->get('is_default')}
                                            {assign var="style" value="style='font-weight: bold'"}
                                        {else}
                                            {assign var="style" value=""}
                                        {/if}
                                        <tr>
                                            <td {$style}>{$address->get('street')}</td>
                                            <td {$style}>{$address->get('number')}</td>
                                            <td {$style}>{$address->get('neighborhood')}</td>
                                            <td {$style}>{$address->get('complement')}</td>
                                            <td {$style}>{$address->get('city')}</td>
                                            <td {$style}>{$address->get('state')}</td>
                                            <td {$style}>{$address->get('cep')}</td>
                                            <td class="td-actions">
                                                <a class="confirm-link" href="/address/delete/{$address->get('id')}">
                                                    <button type="button" rel="tooltip" title="Deletar endereço"
                                                            class="btn btn-danger btn-simple">
                                                        <i class="fa fa-remove"></i>
                                                    </button>
                                                </a>
                                                <a class="confirm-link help" href="/address/toggle/{$address->get('id')}" data-help="address-toggle">
                                                    <button type="button" rel="tooltip" title="Alterar padrão"
                                                            class="btn btn-info btn-simple">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
