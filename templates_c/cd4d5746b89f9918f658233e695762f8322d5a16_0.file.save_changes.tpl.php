<?php
/* Smarty version 3.1.28, created on 2017-09-10 15:24:10
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Favorite\save_changes.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59b5834ae24318_55010653',
  'file_dependency' => 
  array (
    'cd4d5746b89f9918f658233e695762f8322d5a16' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Favorite\\save_changes.tpl',
      1 => 1505067844,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59b5834ae24318_55010653 ($_smarty_tpl) {
?>
<div class="row">
	<div class="col-sm-12">
		<p>
			Para continuar, você deve confirmar os seus jogos favoritos. Lembre-se
			que alterações em sua lista de preferências poderão ser realizadas
			novamente <b>apenas após o prazo de 30 dias</b>, salvo alterações
			que envolvem apenas alteração de prioridades.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
            <table class="table table-hover table-striped datatable">
                <thead>
                    <tr>
                    	<?php if (!$_smarty_tpl->tpl_vars['hasFavorites']->value) {?><th>Prioridade</th><?php }?>
                        <th>Jogo</th>
                        <th>Plataforma</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php
$_from = $_smarty_tpl->tpl_vars['favorites']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_favorite_0_saved_item = isset($_smarty_tpl->tpl_vars['favorite']) ? $_smarty_tpl->tpl_vars['favorite'] : false;
$_smarty_tpl->tpl_vars['favorite'] = new Smarty_Variable();
$__foreach_favorite_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_favorite_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['favorite']->value) {
$__foreach_favorite_0_saved_local_item = $_smarty_tpl->tpl_vars['favorite'];
?>
                    <tr id="favorite-<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position');?>
">
                        <?php if (!$_smarty_tpl->tpl_vars['hasFavorites']->value) {?><td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position')+1;?>

                        </td><?php }?>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('id_game',true)->get('name');?>

                        </td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('platform',true);?>

                        </td>
                        <td class="td-actions">
                            <a >
                                <button onclick="removeSavechanges(<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position');?>
)"
                                		type="button" rel="tooltip" 
                                        class="btn btn-danger btn-simple remove-savechanges">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_local_item;
}
}
if ($__foreach_favorite_0_saved_item) {
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_item;
}
?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php }
}
