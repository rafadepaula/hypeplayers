<div class="row">
    <div class="col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">place</i>
            </div>
            <div class="card-content">
                <p class="category">Maior demanda</p>
                <h3 class="title">{$requestedRegion['name']}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-info">info</i> {$requestedRegion['total']} membros atendidos.
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">local_shipping</i>
            </div>
            <div class="card-content">
                <p class="category">Total</p>
                <h3 class="title">{$regionsCount}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-info">format_list_numbered</i> {$regionsCount} regiões atendidas.
                </div>
            </div>
        </div>
    </div>

    {if $hasRequests}
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="/region/requests/">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">warning</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Solicitações</p>
                        <h3 class="title">{$regionRequests}</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            {if $regionRequests == 1}
                                <i class="material-icons text-danger">error</i> {$regionRequests} região solicitada.
                            {else}
                                <i class="material-icons text-danger">error</i> {$regionRequests} regiões solicitadas.
                            {/if}
                        </div>
                    </div>
                </div>
            </a>
        </div>
    {else}
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">check</i>
                </div>
                <div class="card-content">
                    <p class="category">Solicitações</p>
                    <h3 class="title">0</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-success">check</i> nenhuma solicitação.
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/region/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Intervalo de CEP</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $regions as $region}
                        <tr>
                            <td>
                                {$region->get('name')}
                            </td>
                            <td>
                                {$region->get('cep_init')} - {$region->get('cep_end')}
                            </td>
                            <td class="td-actions">
                                <a href="/region/edit/{$region->get('id')}">
                                    <button type="button" rel="tooltip" title="Editar {$region->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/region/view/{$region->get('id')}">
                                    <button type="button" rel="tooltip" title="Visualizar {$region->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="confirm-link" href="/region/delete/{$region->get('id')}">
                                    <button type="button" rel="tooltip" title="Deletar {$region->get('name')}"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>