<?php
    
    class Address extends DTO {
        public $id;
        public $id_user;
        public $number;
        public $cep;
        public $city;
        public $complement;
        public $id_region;
        public $neighborhood;
        public $state;
        public $street;
        public $is_default = 0;
        
        public $FieldsValidation = array(
            'cep' => 'notEmpty,validCep',
            'neighborhood' => 'notEmpty',
            'number' => 'notEmpty',
            'phone' => 'notEmpty',
            'state' => 'notEmpty',
            'street' => 'notEmpty',
        );
        public $FieldsErrors     = array(
            'cep' => 'CEP inválido!',
            'neighborhood' => 'Informe seu bairro.',
            'number' => 'Informe o número de seu endereço.',
            'phone' => 'Informe seu número de telefone.',
            'state' => 'Informe seu estado.',
            'street' => 'Informe sua rua.',
        );
        public $FieldsMasks      = array(
            'id_user' => ['getDto', ['user', 'id']],
            'id_region' => ['getDto', ['region', 'id']]
        );
        
    }