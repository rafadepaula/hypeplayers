<?php
    
    class GameModel extends AppModel {
        public function getGame($id){
            return $this->getDto('game', 'id', $id);
        }

        public function getMedia($id){
            return $this->getDto('game_media', 'id', $id);
        }
        
        public function save($id = null){
            $game = $this->getGame($id);
            $available = $game->get('available');
            $cover = $game->get('cover');
            $ret  = $this->makeDto($game, $id);
            $game = $ret[0];
            $game->set('cover', $cover);
            $game->set('available', $available);

            if($_FILES['ps4_cover']['name'] != ''){
                $cover = new File(_COVERS_DIR, 'ps4_cover');
                $dir = $cover->finish();
                if(!$game->set('ps4_cover', $dir)){
                    Viewer::flash($cover->FieldsErrors['ps4_cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }
            if($_FILES['xone_cover']['name'] != ''){
                $cover = new File(_COVERS_DIR, 'xone_cover');
                $dir = $cover->finish();
                if(!$game->set('xone_cover', $dir)){
                    Viewer::flash($cover->FieldsErrors['xone_cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }
            if($_FILES['switch_cover']['name'] != ''){
                $cover = new File(_COVERS_DIR, 'switch_cover');
                $dir = $cover->finish();
                if(!$game->set('switch_cover', $dir)){
                    Viewer::flash($cover->FieldsErrors['switch_cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }
            $errors = $ret[1];

            if($errors != ''){
                Viewer::flash($errors, 'e');
                
                return false;
            }else{
                $this->initTransaction();
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('game', $game) : $this->update('game', $game, array('id' => $id));
                if($ret){
                    if(is_null($id)){
                        $id = $this->lastInserted('game');
                        if(!$this->saveMedia($id)){
                            Viewer::flash('Não foi possível salvar as mídias!', 'e');
                            $this->cancelTransaction();
                            return false;
                        }
                    }
                    
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    $this->endTransaction();
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    $this->cancelTransaction();
                    return false;
                }
            }
        }
        
        public function saveMedia($id_game){
            // images
            if(isset($_FILES['images'])){
                if(is_array($_FILES['images']['name'])){
                    for($i = 0; $i < count($_FILES['images']['name']); $i++){
                        $media = new Game_media();
                        $image = new File(_GAME_MEDIA_DIR, 'images', $i);
                        $image = $image->finish($id_game);
                        $media->set('id_game', $id_game);
                        $media->set('source', $image);
                        $media->set('type', '0');
                        $thumb = $this->makeThumb($image);
                        if(!$thumb){
                            return false;
                        }
                        $media->set('thumb', $thumb);
                        if(!$this->insert('game_media', $media))
                            return false;
                    }
                }else{
                    $media = new Game_media();
                    $image = new File(_GAME_MEDIA_DIR, 'images');
                    $image = $image->finish();
                    $media->set('id_game', $id_game);
                    $media->set('source', $image);
                    $media->set('type', '0');
                    $thumb = $this->makeThumb($image);
                    if(!$thumb){
                        return false;
                    }
                    $media->set('thumb', $thumb);
                    if(!$this->insert('game_media', $media))
                        return false;
                }
            }

            // video
            if($_POST['video'] != ''){
                $media = new Game_media();
                $media->set('id_game', $id_game);
                $media->set('type', 1);
                $media->set('source', $_POST['video']);
                if(!$this->insert('game_media', $media))
                    return false;
            }

            return true;
        }

        /**
         * @url <https://davidwalsh.name/create-image-thumbnail-php>
         *
        **/
        public function makeThumb($src){
            $desired_width = '150';
            if(!is_file($src))
                sleep(5);
            if(!is_file($src))
                return false;
            /* read the source image */
            $source_image = $this->imagecreatefromfile($src);
            $width = imagesx($source_image);
            $height = imagesy($source_image);
            
            /* find the "desired height" of this thumbnail, relative to the desired width  */
            $desired_height = floor($height * ($desired_width / $width));
            
            /* create a new, "virtual" image */
            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
            
            /* copy source image at a resized size */
            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
            

            // custom
            if(!is_dir(_GAME_MEDIA_THUMBS_DIR))
                mkdir(_GAME_MEDIA_THUMBS_DIR);
            
            $filename = explode('/', $src);
            $filename = $filename[count($filename) - 1];

            /* create the physical thumbnail image to its destination */
            imagejpeg($virtual_image, _GAME_MEDIA_THUMBS_DIR.$filename);


            // custom return
            return _GAME_MEDIA_THUMBS_DIR.$filename;
        }

        public function updateQttByRent(Rent $rent, $type){
            $game = $this->getGame($rent->get('id_game'));
            $qttName = $rent->get('platform').'_qtt';
            $qtt = $game->get($qttName);
            if($type == 'add'){
                $game->set($qttName, $game->get($qttName) + 1);
            }else{
                if($qtt < 1){
                    return false;
                }
                $game->set($qttName, $game->get($qttName) - 1);
            }
            return $this->update('game', $game, array('id' => $game->get('id')));
        }

    }