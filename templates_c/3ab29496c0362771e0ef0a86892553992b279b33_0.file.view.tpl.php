<?php
/* Smarty version 3.1.28, created on 2017-08-10 15:18:55
  from "C:\xampp\htdocs\hypeplayers\app\viewer\User\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_598ca38fbe3674_48105040',
  'file_dependency' => 
  array (
    '3ab29496c0362771e0ef0a86892553992b279b33' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\User\\view.tpl',
      1 => 1502389134,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598ca38fbe3674_48105040 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/user/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Usuário</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['users']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_user_0_saved_item = isset($_smarty_tpl->tpl_vars['user']) ? $_smarty_tpl->tpl_vars['user'] : false;
$_smarty_tpl->tpl_vars['user'] = new Smarty_Variable();
$__foreach_user_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_user_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['user']->value) {
$__foreach_user_0_saved_local_item = $_smarty_tpl->tpl_vars['user'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['user']->value->get('name');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['user']->value->get('email');?>

                            </td>
                            <td class="td-actions">
                                <?php if ($_smarty_tpl->tpl_vars['actualUser']->value->get('id') == $_smarty_tpl->tpl_vars['user']->value->get('id')) {?>
                                    <a href="/user/edit/<?php echo $_smarty_tpl->tpl_vars['user']->value->get('id');?>
">
                                        <button type="button" rel="tooltip" title="Editar dados pessoais"
                                                class="btn btn-info btn-simple">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                <?php }?>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['user'] = $__foreach_user_0_saved_local_item;
}
}
if ($__foreach_user_0_saved_item) {
$_smarty_tpl->tpl_vars['user'] = $__foreach_user_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
