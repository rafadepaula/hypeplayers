<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <title>HypePlayers :: {$title}</title>
    <link href="/app/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/app/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <style type="text/css">

        body {
            background-image: url('/app/assets/img/cadastro-back2.jpg');
            background-size: cover;
        }

        .wrapper {
            margin-top: 35px;
            margin-bottom: 80px;
        }

        .form-signin {
            max-width: 380px;
            padding: 15px 35px 45px;
            margin: 0 auto;
            background-color: #fff;
            border: 1px solid rgba(0,0,0,0.1);
            margin-bottom: 30px;

        .form-signin-heading,
        .checkbox {
            margin-bottom: 30px;
        }

        .checkbox {
            font-weight: normal;
        }

        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        @include box-sizing(border-box);

        &:focus {
             z-index: 2;
         }
        }

        input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        input[type="password"] {
            margin-bottom: 20px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        }

    </style>
    <div class="container">
        {$_flash}
    </div>
    <div class="wrapper">
        <img src="/app/assets/img/logo_branco.png" class="center-block" style="max-height: 100px; margin-bottom: 30px">
        <form class="form-signin" method="post" action="/user/login">
            <h2 class="form-signin-heading">Entrar no sistema</h2>
            <input type="email" class="form-control" name="email" placeholder="Seu e-mail" required="" autofocus="" />
            <br>
            <input type="password" class="form-control" name="password" placeholder="Senha" required=""/>
            <hr>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
            <hr>
            <span class="text-center">
                <a href="/user/recovery">Esqueceu sua senha?</a><br>
            </span>
        </form>
        <a href="/member/join">
            <button class="btn btn-lg btn-success center-block">
                <i class="fa fa-user"></i> Ainda não é um membro?<br class="visible-cs"> Cadastre-se já!
            </button>
        </a>
    </div>
</html>