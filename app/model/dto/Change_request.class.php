<?php
    
    class Change_request extends DTO {
        public $id;
        public $id_rent;
        public $id_rent2;
        public $date_request;
        public $date_done;
        
        public $FieldsMasks     = array(
            'id_rent'      => array('getDto', ['rent']),
            'id_rent2'     => array('getDto', ['rent']),
            'date_request' => 'dateMask.',
            'date_done'    => 'dateMask',
        );
        
    }