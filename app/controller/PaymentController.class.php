<?php
	class PaymentController extends AppController{
		public function plans($id = null){
			if($this->user->get('role') != 'admin'){
				Viewer::flash(_PERMISSION_ERROR, 'e');
				return $this->siteIndex();
			}

			if(!is_null($id)){
				if(!$this->model->exists('plan_history', 'id', $id)){
					Viewer::flash(_EXISTS_ERROR, 'e');
					return $this->view();
				}
				$planHistory = $this->model->getPlanHistory($id);
				$this->viewer->set('planHistory', $planHistory);

				$user = $planHistory->get('id_user', true);
				$this->viewer->set('user', $user);

				return $this->viewer->show('view_one_planhistory', 'Plano de '.$user->get('user'));
			}

			$planHistories = $this->model->search('plan_history');
			$this->viewer->set('planHistories', $planHistories);

			$payments = $this->model->numRows('payment');
			$this->viewer->set('payments', $payments);

			return $this->viewer->show('view_planhistory', 'Planos PayPal');
		}

		public function view(){
			if($this->user->get('role') != 'admin'){
				Viewer::flash(_PERMISSION_ERROR, 'e');
				return $this->siteIndex();
			}

			$payments = $this->model->search('payment', '*', false, 'date DESC');
			$this->viewer->set('payments', $payments);

			return $this->viewer->show('view_payment', 'Pagamentos');
		}
	}