<?php
    
    
    class NotificationController extends AppController {
        public function read(){
            $this->model->read($this->user);
        }
        
        public function view(){
            switch($this->user->get('role')){
                case 'admin':
                    $notifications = $this->model->search('notification', '*', array('type' => 'admin'), 'created DESC');
                    break;
                default:
                    $notifications = $this->model->search('notification', '*', array('id_user' => $this->user->get('id')), 'created DESC');
                    break;
            }
            $this->viewer->set('notifications', $notifications);
            return $this->viewer->show('view', 'Histórico de Notificações');
        }

        public function pagseguro($file = null){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }
            $dir = __DIR__.'/../assets/notifications/';

            if(!is_null($file) && is_file($dir.$file)){
                $this->viewer->set('file', explode('---BREAK---', file_get_contents($dir.$file)));
                return $this->viewer->show('pagseguro_one', $file);
            }
            if(!is_dir($dir))
                mkdir($dir);

            $files = scandir($dir);
            unset($files[0]);
            unset($files[1]);
            $this->viewer->set('files', $files);
            return $this->viewer->show('pagseguro', 'Notificações PagSeguro');
        }

        public function config(){

        }
    }
