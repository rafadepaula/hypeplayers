<?php
    
    class Member_request extends DTO {
        public $id;
        public $id_region_request;
        public $phone;
        public $rg;
        public $cpf;
        public $birthday;
        public $email;
        public $name;
        public $lastname;
        public $created;
        
        public $FieldsValidation = array(
        );
        public $FieldsErrors     = array(
        );
        public $FieldsMasks      = array(
            'id_region_request' => ['getDto', ['region_request', 'id']],
	        'birthday' => 'dateMask',
	        'created' => 'datetimeMask',
        );
        
    }