<?php

    class NewsController extends AppController{

        public function index(){
            $userController = new UserController();
            return $userController->siteAdmin();
        }

        public function add(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->request()){
                if($this->model->save()){
                    $id = $this->model->lastInserted('news');
                    return $this->view($id);
                }else{
                    unset($_POST);
                    return $this->add();
                }
            }

            return $this->viewer->show('add', 'Cadastrar notícia no blog');
        }

        public function view($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('news', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            $news = $this->model->getNews($id);
            $this->viewer->set('news', $news);

            return $this->viewer->show('view_one', $news->get('title'));
        }

        public function update($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('news', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            $news = $this->model->getNews($id);
            $this->viewer->set('news', $news);

            return $this->viewer->show('update', 'Atualizar notícia do blog');
        }

        public function delete($id){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if (!$this->model->exists('news', 'id', $id)) {
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if ($this->model->delete('news', array('id' => $id))) {
                Viewer::flash(_DELETE_SUCCESS, 's');
            } else {
                Viewer::flash(_DELETE_ERROR, 'e');
            }
            return $this->index();
        }

        public function indexNews(){
            $news = $this->model->search('news', '*', false, 'created');
            return $news;
        }

    }