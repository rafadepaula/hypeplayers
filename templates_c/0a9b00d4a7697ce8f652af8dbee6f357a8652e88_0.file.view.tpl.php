<?php
/* Smarty version 3.1.28, created on 2017-09-29 05:38:31
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Member\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59ce0687317af0_37380044',
  'file_dependency' => 
  array (
    '0a9b00d4a7697ce8f652af8dbee6f357a8652e88' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Member\\view.tpl',
      1 => 1506579413,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59ce0687317af0_37380044 ($_smarty_tpl) {
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Expira em</th>
                            <th>Último pagamento</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['members']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_member_0_saved_item = isset($_smarty_tpl->tpl_vars['member']) ? $_smarty_tpl->tpl_vars['member'] : false;
$_smarty_tpl->tpl_vars['member'] = new Smarty_Variable();
$__foreach_member_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_member_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['member']->value) {
$__foreach_member_0_saved_local_item = $_smarty_tpl->tpl_vars['member'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['member']->value['name'];?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['member']->value['expiry'];?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['member']->value['last_payment'];?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['member']->value['status'];?>

                            <td class="td-actions">
                                <a href="/member/view/<?php echo $_smarty_tpl->tpl_vars['member']->value['id'];?>
">
                                    <button type="button" rel="tooltip" title="Visualizar <?php echo $_smarty_tpl->tpl_vars['member']->value['name'];?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['member'] = $__foreach_member_0_saved_local_item;
}
}
if ($__foreach_member_0_saved_item) {
$_smarty_tpl->tpl_vars['member'] = $__foreach_member_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
