<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/banner/add" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Fundo</label>
                            <input required type="file" name="cover" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Título</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Subtítulo</label>
                            <input type="text" name="subtitle" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 form-group">
                            <label>Mostrar botão</label> <br>
                            <input type="checkbox" name="button_display" class="form-control" id="button_display">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label>Cor de fundo</label> <br>
                            <input type='text' id="button_color" name="button_color" readonly value="#496449">
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>Texto do botão</label>
                            <input type="text" id="button_text" name="button_text" class="form-control" readonly>
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>Link de redirecionamento</label>
                            <input type="text" id="button_link" name="button_link" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
