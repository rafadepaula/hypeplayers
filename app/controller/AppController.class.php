<?php
    
    /**
     * Use this class to extends your controllers.
     * Here you can implement your own methods to all of your controllers.
     *
     **/
    class AppController extends Controller {
        public $user;
        
        public function __construct($controller = '', $function = ''){
            parent::__construct($controller, $function);
            if($this->logged()){
                $this->user = unserialize($_SESSION['user']);
                if($this->user->get('role') == 'member'){
                    $signature = $this->model->getDto('signature', 'id_user', $this->user->get('id'));
                    $this->viewer->set('signature', $signature);
                }
            }else{
                $this->user = new User();
            }
            
            $this->getNotifications();
        }

        public function getNotifications(){
            switch($this->user->get('role')){
                case 'admin':
                    $notifications = $this->model->search('notification', '*', array(
                        'is_read' => 0,
                        'conscond1' => 'AND',
                        'type' => 'admin',
                    ), 'id DESC', false, 5);
                    $numNotifications = count($notifications);
            
                    $this->viewer->set('numNotifications', $numNotifications);
                    $this->viewer->set('notifications', $notifications);
                    break;
                case 'member':
                    $notifications = $this->model->search('notification', '*', array(
                        'is_read' => 0,
                        'conscond1' => 'AND',
                        'id_user' => $this->user->get('id'),
                    ), 'id DESC', false, 5);
                    $this->viewer->set('numNotifications', count($notifications));
                    $this->viewer->set('notifications', $notifications);
                    break;
                default:
                    $this->viewer->set('numNotifications', 0);
                    $this->viewer->set('notifications', array());
                    break;
            }
        }

        public static function siteIndex(){
            $userController = new UserController();
            return $userController->home();
        }

    }