
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/rent/myGames" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">
                                <i class="fa fa-gamepad" style="margin-right: 5px;"></i> Meus jogos
                            </span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable help" data-help="history-table">
                    <thead>
                        <tr>
                            <th>Jogo</th>
                            <th>Status</th>
                            <th>Período</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $rents as $rent}
                        <tr>
                            <td>
                                {$rent->get('id_game', true)->get('name')}
                                ({$rent->get('platform', true)})
                            </td>
                            <td>
                                {$rent->get('status', true)}
                            </td>
                            <td>
                                {$rent->get('start', true)} até {$rent->get('end', true)}
                            </td>
                            <td class="td-actions">
                                <a class="help" data-help="rent-view">
                                    <button type="button" rel="tooltip" title="Visualizar locação"
                                            class="btn btn-info btn-simple view-one"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="help" data-help="rent-comment">
                                    <button type="button" rel="tooltip" title="Enviar comentário"
                                            class="btn btn-default btn-simple send-comment"
                                            data-id-rent="{$rent->get('id')}"
                                            data-url="/rent/history/">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

{include file=$smarty.current_dir|cat:"/changeStatus_modal.tpl"}
{include file=$smarty.current_dir|cat:"/view_modal.tpl"}