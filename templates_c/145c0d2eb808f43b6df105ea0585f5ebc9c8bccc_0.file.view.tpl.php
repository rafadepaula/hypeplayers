<?php
/* Smarty version 3.1.28, created on 2017-08-17 15:36:14
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Genre\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5995e21e8da5e9_22490973',
  'file_dependency' => 
  array (
    '145c0d2eb808f43b6df105ea0585f5ebc9c8bccc' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Genre\\view.tpl',
      1 => 1502994920,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5995e21e8da5e9_22490973 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/game/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">keyboard_arrow_left</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <form action="/genre/add" method="post">
                    <div class="row">
                        <div class="col-sm-12 input-group">
                            <input type="text" name="name" class="form-control" placeholder="Cadastrar gênero">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">Cadastrar</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['genres']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_genre_0_saved_item = isset($_smarty_tpl->tpl_vars['genre']) ? $_smarty_tpl->tpl_vars['genre'] : false;
$_smarty_tpl->tpl_vars['genre'] = new Smarty_Variable();
$__foreach_genre_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_genre_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['genre']->value) {
$__foreach_genre_0_saved_local_item = $_smarty_tpl->tpl_vars['genre'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>

                            </td>
                            <td class="td-actions">
                                <a href="/genre/edit/<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Editar <?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/genre/view/<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Visualizar <?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="confirm-link" href="/genre/delete/<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Deletar <?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_local_item;
}
}
if ($__foreach_genre_0_saved_item) {
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
