<?php
/* Smarty version 3.1.28, created on 2017-08-12 17:26:40
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Region\requests.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_598f6480bdd8b7_13004322',
  'file_dependency' => 
  array (
    'f090a6ec417927999f46d8fc79c0fac2b793b02e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Region\\requests.tpl',
      1 => 1502569597,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598f6480bdd8b7_13004322 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/region/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Localização</th>
                            <th>CEP</th>
                            <th>Data e hora</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['regionRequests']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_region_0_saved_item = isset($_smarty_tpl->tpl_vars['region']) ? $_smarty_tpl->tpl_vars['region'] : false;
$_smarty_tpl->tpl_vars['region'] = new Smarty_Variable();
$__foreach_region_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_region_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['region']->value) {
$__foreach_region_0_saved_local_item = $_smarty_tpl->tpl_vars['region'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['region']->value->get('location');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['region']->value->get('cep');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['region']->value->get('date',true);?>

                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['region'] = $__foreach_region_0_saved_local_item;
}
}
if ($__foreach_region_0_saved_item) {
$_smarty_tpl->tpl_vars['region'] = $__foreach_region_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
