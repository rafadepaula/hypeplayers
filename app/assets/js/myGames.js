$(function(){

	$('.send-comment').click(function(){
		id_rent = $(this).data('id-rent');
		if(typeof $(this).data('url') != 'undefined'){
			urlRedirect = $(this).data('url');
		}else{
			urlRedirect = '/rent/myGames';
		}
		swal({
			type: 'info',
			input: 'text',
			inputClass: 'form-control',
			inputPlaceholder: 'Observação',
			title: 'Enviar observação',
			text: 'Envie uma observação sobre esta locação para os administradores.',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Enviar',
			cancelButtonText: 'Cancelar!',
			showLoaderOnConfirm: true,
			preConfirm: function (ps_user) {
				return new Promise(function (resolve, reject) {
					$.ajax({
						url: '/rent/sendPsUser/' + id_rent,
						data: {ps_user: ps_user},
						method: 'post',
					}).done(function(ret){
						console.log(ret);
						if(!validJson(ret)){
							return swal('Erro interno no sistema.');
						}
						ret = $.parseJSON(ret);
						if(ret.return == '1'){
							resolve(ret.text);
						}else{
							reject(ret.text);
						}
					});
				})
			},

		}).then(function (text) {
			swal({
				type: 'success',
				title: 'Observação enviada',
				text: text,
			});
			window.location = urlRedirect;
		});
	});

	$('.change-all').click(function(){
		giveBackChangeConfirm('all');
	})

	$('.request-renew').click(function(){
		id_rent = $(this).data('id-rent');
		swal({
			title: 'Confirmar renovação',
			text: "Deseja confirmar a renovação desta locação?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, confirmar.',
			cancelButtonText: 'Não, cancelar.',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						url: '/rent/requestRenew/' + id_rent,
					}).done(function(ret){
						console.log(ret);
						if(ret != 'ok'){
							return reject(ret);
						}
						resolve();
					});
				})
			},
		}).then(function () {
			swal({
				title: 'Confirmado!',
				text: 'Em breve, você receberá sua resposta! :)',
				type: 'success',
				showConfirmButton: false,
			});
			
			window.location ='/rent/myGames/';
		})
	});

	$('.give-back').click(function(){
		swal({
			text: 'Aguarde...',
			showConfirmButton: false,
		});
		id_rent = $(this).data('id-rent');
		$.ajax({
			url: '/rent/giveBack/' + id_rent
		}).done(function(ret){
			console.log(ret);
			if(!validJson(ret)){
				return swal('Erro interno no sistema.');
			}
			ret = $.parseJSON(ret);
			if(ret.return == 1){
				giveBack(id_rent, ret.can_change);
			}else{
				swal({
					type: 'error',
					text: ret.text,
				});
			}
		});
	});

	$('.confirm-post').click(function(){
		swal({
			text: 'Aguarde...',
			showConfirmButton: false,
		});
		id_rent = $(this).data('id-rent');
		$.ajax({
			url: '/rent/canConfirmPost/' + id_rent
		}).done(function(ret){
			console.log(ret);
			if(ret == 1){
				confirmPost(id_rent);
			}else{
				swal({
					type: 'error',
					text: ret
				});
			}
		});
	})

	$('.view-one').click(function(){
		$('#modalViewMember').modal({
			backdrop: false
		}, $(this));
		$('#modal-body-member').html('<h4 class="text-center"><i class="fa fa-spinner fa-spin"></i></h4>');
		$('#modal-title-member').html('Locação');
	});

	$('#modalViewMember').on('show.bs.modal', function(e){
		$('.main-panel').scrollTop(0);
		rent = e.relatedTarget;
		id_rent = rent.data('id-rent');
		$.ajax({
			url: '/rent/getTitle/' + id_rent,
		}).done(function(title){
			$('#modal-title-member').html(title);
		})
		$('#modal-body-member').load("/rent/view/" + id_rent);
	});

	$('.confirm-deliver').click(function(){
		id_rent = $(this).data('id-rent');
		swal({
			title: 'Confirmar recebimento?',
			text: "Prossiga apenas se você já está com o jogo em mãos.",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, confirmar.',
			cancelButtonText: 'Não, cancelar.',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						url: '/rent/confirmDeliver/' + id_rent,
					}).done(function(ret){
						console.log(ret);
						if(ret != 'ok'){
							return reject(ret);
						}
						resolve();
					});
				})
			},
		}).then(function () {
			swal(
				'Confirmado!',
				'Aproveite ao máximo! :)',
				'success'
			);
			window.location ='/rent/myGames/';
		})
	})
});

function giveBack(id_rent, can_change){
	if(can_change){
		swal({
			type: 'question',
			title: 'Devolver ou trocar jogo',
			text: 'Selecione a ação a ser tomada:',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Apenas solicitar devolução',
			cancelButtonText: 'Solicitar uma troca',
			showCloseButton: true,
		}).then(function () {
			giveBackConfirm(id_rent);
		}, function (dismiss) {
			if (dismiss === 'cancel') {
				giveBackChangeConfirm(id_rent, false);
			}
		});
	}else{
		giveBackConfirm(id_rent);
	}
}

function giveBackChangeConfirm(id_rent){
	swal({
		type: 'warning',
		title: 'Confirma a solicitação de troca?',
		text: 'Esta ação não poderá ser desfeita após a confirmação.',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sim, desejo trocar!',
		cancelButtonText: 'Não, cancelar!',
		showLoaderOnConfirm: true,
		preConfirm: function () {
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/rent/giveBack/' + id_rent + '/change/',
				}).done(function(ret){
					console.log(ret);
					if(!validJson(ret)){
						return reject('Erro interno no sistema.');
					}
					ret = $.parseJSON(ret);
					if(ret.return == '1'){
						resolve(ret.text);
					}else{
						reject(ret.text);
					}
				});
			})
		},

	}).then(function (text) {
		swal({
			type: 'success',
			title: 'Troca solicitada',
			text: text,
		});
		window.location = '/rent/myGames/';
	});
}

function giveBackConfirm(id_rent){
	swal({
		type: 'warning',
		title: 'Confirma a solicitação de devolução?',
		text: 'Esta ação não poderá ser desfeita após a confirmação.',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sim, desejo devolver o jogo!',
		cancelButtonText: 'Não, cancelar!',
		showLoaderOnConfirm: true,
		preConfirm: function () {
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/rent/giveBack/' + id_rent + '/giveback/',
				}).done(function(ret){
					console.log(ret);
					if(!validJson(ret)){
						return swal('Erro interno no sistema.');
					}
					ret = $.parseJSON(ret);
					if(ret.return == '1'){
						resolve(ret.text);
					}else{
						reject(ret.text);
					}
				});
			})
		},

	}).then(function (text) {
		swal({
			type: 'success',
			title: 'Devolução solicitada',
			text: text,
		});
		window.location = '/rent/myGames/';
	});
}

function confirmPost(id_rent){
	swal({
		type: 'warning',
		input: 'text',
		inputClass: 'form-control',
		inputPlaceholder: 'Código de rastreio do correio',
		title: 'Confirmar a devolução nos correios',
		text: 'Informe o código de rastreio. Deixe em branco se não o possuir.',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Confirmar postagem nos correios.',
		cancelButtonText: 'Não, cancelar!',
		showLoaderOnConfirm: true,
		preConfirm: function (carrier_code) {
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/rent/confirmPost/' + id_rent,
					data: {carrier_code: carrier_code},
					method: 'post',
				}).done(function(ret){
					console.log(ret);
					if(!validJson(ret)){
						return swal('Erro interno no sistema.');
					}
					ret = $.parseJSON(ret);
					if(ret.return == '1'){
						resolve(ret.text);
					}else{
						reject(ret.text);
					}
				});
			})
		},

	}).then(function (text) {
		swal({
			type: 'success',
			title: 'Postagem confirmada',
			text: text,
		});
		window.location = '/rent/myGames/';
	});
}