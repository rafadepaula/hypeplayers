<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <form action="/address/add/" method="post">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Digite seu CEP:</label>
                            <input name="cep" id="cep" type="text" required class="form-control input-lg mask-cep">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Rua</label>
                                <input name="street" type="text" required id="rua" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Nº</label>
                                <input name="number" type="text" required id="numero" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input name="neighborhood" type="text" required id="bairro" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input name="complement" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input name="city" type="text" id="cidade" required readonly class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <input name="state" type="text" id="estado" required readonly class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>