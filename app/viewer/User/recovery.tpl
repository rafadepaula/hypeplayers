<html>
<head>
<meta charset="utf-8" /> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<title>{$title} :: HypePlayers</title>
<link href="/app/assets/css/bootstrap.min.css" rel="stylesheet"/>
<style type="text/css">

    body {
        background: #eee !important;
    }

    .wrapper {
        margin-top: 80px;
        margin-bottom: 80px;
    }

    .form-signin {
        max-width: 380px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0,0,0,0.1);

    .form-signin-heading,
    .checkbox {
        margin-bottom: 30px;
    }

    .checkbox {
        font-weight: normal;
    }

    .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
    @include box-sizing(border-box);

    &:focus {
         z-index: 2;
     }
    }

    input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    input[type="password"] {
        margin-bottom: 20px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    }

</style>
{$_flash}
<div class="wrapper">
    <form class="form-signin" method="post" action="/user/recovery">
        <a href="/user/login">
            <-
            Voltar
        </a>
        <h2 class="form-signin-heading">{$title}</h2>
        <div class="col-sm-12">
            <label class="contrl-label">Informe seu e-mail</label>
            <input type="email" class="form-control" name="recovery" placeholder="E-mail" required=""/>
        </div>
        <div class="col-sm-12">
            <hr>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Recuperar</button>
        <hr>
    </form>
</div>
</html>