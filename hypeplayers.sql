-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28-Ago-2017 às 15:58
-- Versão do servidor: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hypeplayers`
--
create database hypeplayers;
use hypeplayers;

-- --------------------------------------------------------

--
-- Estrutura da tabela `error`
--

CREATE TABLE `error` (
  `id` int(10) UNSIGNED NOT NULL,
  `error` text,
  `file` text,
  `line` text,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `platform` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `ps4` tinyint(1) NOT NULL,
  `xone` tinyint(1) NOT NULL,
  `switch` tinyint(1) NOT NULL,
  `cover` varchar(300) NOT NULL,
  `ps4_qtt` int(11) DEFAULT NULL,
  `xone_qtt` int(11) DEFAULT NULL,
  `switch_qtt` int(11) DEFAULT NULL,
  `description` text,
  `available` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `game`
--

INSERT INTO `game` (`id`, `id_genre`, `name`, `ps4`, `xone`, `switch`, `cover`, `ps4_qtt`, `xone_qtt`, `switch_qtt`, `description`, `available`) VALUES
(1, 6, 'GTA V', 1, 1, 0, 'app/assets/img/covers/medium.jpg', 5, 5, 0, 'Los Santos: uma vasta metrópole ensolarada repleta de gurus de autoajuda, estrelas de cinema e celebridades em decadência, que já foram o orgulho do mundo ocidental e que agora lutam para se manterem em uma época de incerteza econômica e reality shows baratos.\r\nDentro deste cenário, três criminosos, muito diferentes entre si, planejam suas oportunidades de sobrevivência e sucesso: Franklin, um malandro que busca por boas oportunidades de ganhar muito dinheiro; Michael, um ex-assaltante profissional cuja aposentadoria não é bem o mar de rosas que esperava ser; e Trevor, um maníaco violento que pensa somente na próxima dose e na bolada que pode conquistar. Sem muitas opções, a equipe arrisca tudo em uma série de golpes ousados que podem garantir o resto de suas vidas.\r\n\r\nGrand Theft Auto V é o jogo de mundo aberto mais dinâmico, mais variado e mais extenso já desenvolvido. Combina história e jogabilidade de um novo modo, enquanto os jogadores entram e saem repetidamente da vida dos três protagonistas do jogo. Graças a essa dinâmica, participam de todos os aspectos de suas histórias entrelaçadas.\r\n\r\nTodas as características clássicas da inovadora série de jogos estão de volta, incluindo a incrível atenção aos detalhes e o humor cínico de Grand Theft Auto sobre a cultura moderna, somados a uma nova e ambiciosa abordagem no modo multijogador de mundo aberto.', 1),
(2, 7, 'Resident Evil 7', 1, 1, 0, 'app/assets/img/covers/resident-evil-vii-capa.jpg', 3, 3, 0, 'Resident Evil 7 estabelece um novo caminho para a série Resident Evil em direção às raízes da franquia, ao representar uma mudança dramática para a série com sua perspectiva em primeira pessoa e visual produzido com um novo motor gráfico, a RE Engine da Capcom.\r\n\r\nAmbientado numa cidade interiorana dos Estados Unidos nos dias modernos e após os eventos dramáticos de Resident Evil 6, os jogadores vivenciam o terror diretamente da perspectiva em primeira pessoa. A demo Resident Evil 7 Teaser: Beginning Hour, inicialmente disponível apenas para usuários Plus do PlayStation 4, mostra os eventos que levam ao jogo principal e também liga-se diretamente à demo de tecnologia PlayStation VR “KITCHEN”, mostrada pela primeira vez na E3 2015. Nesta nova demonstração teaser, o jogador desperta no interior de uma cabana em ruínas que faz parte de uma ameaçadora mansão em meio a uma plantação. Será que eles conseguirão sair vivos?\r\n\r\nResident Evil 7 incorpora os já conhecidos elementos de jogabilidade da série, com a exploração e a atmosfera tensa que deram origem ao termo “survival horror”; ao mesmo tempo, uma completa atualização dos sistemas de jogo eleva a experiência de horror de sobrevivência ao próximo nível. Criado com a RE Engine em conjunto com tecnologias de ponta de áudio e vídeo, Resident Evil 7 irá proporcionar uma experiência assustadoramente realística aliada à realidade virtual, que irá definir a próxima era do horror.\r\n\r\nResident Evil 7 está previsto para PlayStation 4 (a experiência de jogo completa também estará disponível no já incluso PlayStation VR Mode), Xbox One e Windows PC (Steam e Windows 10 Store) na América do Norte, Brasil e Europa a partir do dia 24 de janeiro de 2017. O jogo terá menus e legendas localizados em português brasileiro.', 1),
(3, 8, 'FIFA 18', 1, 1, 1, 'app/assets/img/covers/fifa-18-3.jpg', 5, 5, 5, 'Com o maior avanço em inovação de jogabilidade na história da série, o FIFA 18 apresenta a Tecnologia de Movimentação Real de Jogadores, um sistema de animação totalmente novo que proporciona um novo nível de responsividade e personalidade dos jogadores – agora o Cristiano Ronaldo e outros grandes craques se movimentam e agem exatamente como no campo real.', 1),
(4, 9, 'Skyrim', 1, 1, 1, 'app/assets/img/covers/TheElderScrollsVSkrim.png', 5, 5, 5, 'The Elder Scrolls V: Skyrim é um RPG eletrônico desenvolvido pela Bethesda Games Studios e publicado pela Bethesda Softworks. É o quinto jogo da série The Elder Scrolls, seguindo The Elder Scrolls IV: Oblivion.', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `game_media`
--

CREATE TABLE `game_media` (
  `id` int(11) NOT NULL,
  `id_game` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `source` text NOT NULL,
  `thumb` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `game_media`
--

INSERT INTO `game_media` (`id`, `id_game`, `type`, `source`, `thumb`) VALUES
(7, 1, 0, 'app/assets/img/game_media/1/gtav.jpg', 'app/assets/img/game_media/thumbs/gtav.jpg'),
(9, 1, 0, 'app/assets/img/game_media/1/gtav2.jpg', 'app/assets/img/game_media/thumbs/gtav2.jpg'),
(10, 1, 1, 'https://www.youtube.com/watch?v=QkkoHAzjnUs', NULL),
(11, 2, 0, 'app/assets/img/game_media/2/re1.jpg', 'app/assets/img/game_media/thumbs/re1.jpg'),
(12, 2, 0, 'app/assets/img/game_media/2/re2.jpg', 'app/assets/img/game_media/thumbs/re2.jpg'),
(13, 2, 1, 'https://www.youtube.com/watch?v=W1OUs3HwIuo', NULL),
(14, 3, 0, 'app/assets/img/game_media/3/fifa1.jpg', 'app/assets/img/game_media/thumbs/fifa1.jpg'),
(15, 3, 0, 'app/assets/img/game_media/3/fifa2.jpg', 'app/assets/img/game_media/thumbs/fifa2.jpg'),
(16, 3, 1, 'https://www.youtube.com/watch?v=Lv-dyfCm6Io', NULL),
(18, 4, 0, 'app/assets/img/game_media/4/sky2.jpg', 'app/assets/img/game_media/thumbs/sky2.jpg'),
(19, 4, 1, 'https://www.youtube.com/watch?v=JSRtYpNRoN0', NULL),
(20, 4, 0, 'app/assets/img/game_media/4/sky1.png', 'app/assets/img/game_media/thumbs/sky1.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `genre`
--

CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `genre`
--

INSERT INTO `genre` (`id`, `name`) VALUES
(6, 'Ação e Aventura'),
(7, 'Terror'),
(8, 'Esportes'),
(9, 'RPG');

-- --------------------------------------------------------

--
-- Estrutura da tabela `member`
--

CREATE TABLE `member` (
  `id_user` int(11) NOT NULL,
  `street` varchar(300) NOT NULL,
  `number` varchar(15) NOT NULL,
  `neighborhood` varchar(50) NOT NULL,
  `complement` varchar(300) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` varchar(50) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `id_region` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `picture` text,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `secret` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `member`
--

INSERT INTO `member` (`id_user`, `street`, `number`, `neighborhood`, `complement`, `city`, `state`, `cep`, `id_region`, `phone`, `picture`, `activated`, `secret`) VALUES
(10, 'Travessa Horácio de Oliveira Barreto', ' 29', 'Humaitá', ' ', 'Bento Gonçalves', 'Rio Grande do Sul', '95705-014', 11, '(54) 99903-1426', NULL, 1, 'cbdd08dffb5c1a454927eef0e9b87844');

-- --------------------------------------------------------

--
-- Estrutura da tabela `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `text` text NOT NULL,
  `link` text,
  `created` datetime NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `id_readby` int(10) UNSIGNED DEFAULT NULL,
  `read_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `notification`
--

INSERT INTO `notification` (`id`, `id_user`, `type`, `text`, `link`, `created`, `is_read`, `id_readby`, `read_date`) VALUES
(1, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:38:22', 1, 1, '2017-08-11 00:10:34'),
(2, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:50:31', 1, 1, '2017-08-11 00:10:11'),
(3, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:51:48', 1, 1, '2017-08-11 00:10:11'),
(4, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:51:57', 1, 1, '2017-08-11 00:10:11'),
(5, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:52:11', 1, 1, '2017-08-11 00:10:11'),
(6, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-10 22:52:27', 1, 1, '2017-08-11 00:10:11'),
(7, NULL, 'admin', 'Uma ou mais regiões tentaram se cadastrar no sistema.', '/region/requests', '2017-08-11 14:36:58', 1, 1, '2017-08-12 17:22:22'),
(8, NULL, 'admin', 'Novo membro com cadastro prévio', '/member/view/9', '2017-08-13 14:06:44', 1, 1, '2017-08-13 14:15:05'),
(9, NULL, 'admin', 'Novo membro com cadastro prévio', '/member/view/10', '2017-08-13 16:16:43', 1, 1, '2017-08-13 16:19:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `recovery`
--

CREATE TABLE `recovery` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `secret` text NOT NULL,
  `date` datetime NOT NULL,
  `done` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `cep_init` varchar(10) NOT NULL,
  `cep_end` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `region`
--

INSERT INTO `region` (`id`, `name`, `cep_init`, `cep_end`) VALUES
(1, 'São Paulo - Capital', '01000-000', '05999-999'),
(2, 'São Paulo - Capital', '08000-000', '08499-000'),
(3, 'São Paulo - Área Metropolitana', '06000-000', '09999-000'),
(11, 'Rio Grande do Sul', '90000-000', '99999-999');

-- --------------------------------------------------------

--
-- Estrutura da tabela `region_request`
--

CREATE TABLE `region_request` (
  `id` int(11) NOT NULL,
  `cep` varchar(10) NOT NULL,
  `location` varchar(200) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `region_request`
--

INSERT INTO `region_request` (`id`, `cep`, `location`, `date`) VALUES
(3, '78580000', 'Alta Floresta - Mato Grosso', '2017-08-11 14:36:58');

-- --------------------------------------------------------

--
-- Estrutura da tabela `signature`
--

CREATE TABLE `signature` (
  `id_user` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `since` date NOT NULL,
  `last_payment` datetime DEFAULT NULL,
  `expiry` date NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `signature`
--

INSERT INTO `signature` (`id_user`, `level`, `since`, `last_payment`, `expiry`, `value`, `active`) VALUES
(10, 2, '2017-08-13', NULL, '2017-09-13', '20.00', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(140) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `name`, `role`) VALUES
(1, 'rafael@bentonet.com.br', '$2a$12$Cf0f11ePArplBJomM0M6a.5nFUyqKyhtrTK/az5PyYZ8JmdtM2gF.', 'Rafael de Paula', 'admin'),
(2, 'fulano@bentonet.com.br', '$2a$12$Cf0f11ePArplBJomM0M6a.5nFUyqKyhtrTK/az5PyYZ8JmdtM2gF.', 'Fulano de Tal', 'admin'),
(10, 'rafaelbg97@live.com', '$2a$12$Cf0f11ePArplBJomM0M6a.LnZB7xiBzhykQhhVg90.IjTNG8nMlb2', 'Rafael de Paula', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `error`
--
ALTER TABLE `error`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_media`
--
ALTER TABLE `game_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recovery`
--
ALTER TABLE `recovery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region_request`
--
ALTER TABLE `region_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `error`
--
ALTER TABLE `error`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `game_media`
--
ALTER TABLE `game_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `recovery`
--
ALTER TABLE `recovery`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `region_request`
--
ALTER TABLE `region_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
