<?php
    
    
    class GenreController extends AppController {
        
        public function add(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->request()){
                $this->model->save();
                unset($_POST);
            }
            return $this->view();
        }
        
        public function view($id = null){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!is_null($id)){
                if(!$this->exists('genre', 'id', $id)){
                    Viewer::flash(_EXISTS_ERROR, 'e');
                    return $this->view();
                }
                $genre = $this->model->getGenre($id);
                $games = $this->model->search('games', '*', array('id_genre' => $id));
                $this->viewer->set('games', $games);
                return $this->viewer->show('view_one', 'Jogos de '.$genre->get('name'));
            }
            $genres = $this->model->search('genre');
            $this->viewer->set('genres', $genres);

            return $this->viewer->show('view', 'Gêneros');
        }
        
        public function edit($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('genre', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->request()){
                if($this->model->save($id, 'admin')){
                    return $this->view();
                }else{
                    unset($_POST);
                    return $this->edit($id);
                }
            }
            $genre = $this->model->getGenre($id);
            $this->viewer->set('genre', $genre);
            return $this->viewer->show('edit', 'Editar '.$genre->get('name'));
        }

        public function delete($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('genre', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->model->exists('game', 'id_genre', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->model->delete('genre', array('id' => $id))){
                Viewer::flash(_DELETE_SUCCESS, 's');
                return $this->view();
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
                return $this->view($id);
            }
        }

    }
