
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Expira em</th>
                            <th>Último pagamento</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $members as $member}
                        <tr>
                            <td>
                                {$member['name']}
                            </td>
                            <td>
                                {$member['expiry']}
                            </td>
                            <td>
                                {$member['last_payment']}
                            </td>
                            <td>
                                {$member['status']}
                            <td class="td-actions">
                                <a href="/member/view/{$member['id']}">
                                    <button type="button" rel="tooltip" title="Visualizar {$member['name']}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>