<?php

    /**
     * Realizes the communication between the model and the viewer
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2015-06-30
     *
    **/
    class Controller {

        public $model;
        public $viewer;
        public $export;
        public $user;

        /**
         * Constructor method
         * @param String $controller - controller name
         * @param String $function - controller's action
        **/
        public function __construct($controller = '', $function = ''){
            $this->model = new Model();
            $this->viewer = new Viewer();
            $controller = ucfirst($controller);
            $controller = str_replace('Controller', '', $controller);


            if (_PERMISSIONS_) {
                if ($controller != '' && $function != '' &&
                    $controller != 'App' && $function != 'permission') {
                    if (isset($_SESSION['user'])) {
                        if (isset(Functions::$publics[$controller])) {
                            if (!in_array($function, Functions::$publics[$controller])) {
                                $permission = unserialize($_SESSION['user'])->get('permission');
                                $permission = Permission::permissionArray($permission);
                                if (!Permission::hasAccess($permission, $controller, $function)) {
                                    if (!in_array(unserialize($_SESSION['user'])->get('id'), unserialize(_MASTERS_ID))) {
                                        return $this->permissionError();
                                    }
                                }
                            }
                        } else {
                            $permission = unserialize($_SESSION['user'])->get('permission');
                            $permission = Permission::permissionArray($permission);
                            if (!Permission::hasAccess($permission, $controller, $function)) {
                                if (!in_array(unserialize($_SESSION['user'])->get('id'), unserialize(_MASTERS_ID))) {
                                    return $this->permissionError();
                                }
                            }
                        }
                    } else {
                        if (isset(Functions::$publics[$controller])) {
                            if (!in_array($function, Functions::$publics[$controller])) {
                                return $this->permissionError();
                            }
                        }
                    }
                }
            }
    
            $className = substr(get_class(debug_backtrace()[0]['object']), 0, -10);
            if($className == '')
                $className = substr(_MAIN_CLASS, 0 -10);
            $model = $className . 'Model';
            $viewer = $className . 'Viewer';
    
    
            if (class_exists($model))
                eval('$this->model = new '.$model.'();');
            if (class_exists($viewer))
                eval('$this->viewer = new '.$viewer.'();');
    
            // Export classes
            $pdf = $className . 'PDF';
            $excel = $className . 'Excel';
            if (!class_exists($pdf))
                $pdf = 'BasePDF';
            if (!class_exists($excel))
                $excel = 'BaseExcel';
            $this->export = new Export($pdf, $excel);
    
            $this->user = $this->logged() ? unserialize($_SESSION['user']) : new User();
            
            return;
        }

        /**
         * Redirect to an specified page
         *
         * @param String $url - url of the page
         *
        **/
        public function redirect($url){
            header('Location: '.$url);
        }

        /**
         * Redirects user to index.
         *
        **/
        static function siteIndex(){
            eval('$controller = new '._MAIN_CLASS.'();');
	        $controller->{_MAIN_METHOD}();
        }

        /**
         * Method used to detect if exists an request by post
         *
         * @return Boolean - requests exists.
         *
        **/
        public function request(){
            if(isset($_POST)){
                if(count($_POST) > 0)
                    return true;
            }
            if(isset($_FILES)){
                return count($_FILES) > 0;
            }
            return false;
        }
        
        /**
         * Sends a flash to the screen and redirects to the index
         *
        **/
        public function permissionError(){
            Viewer::flash(_PERMISSION_ERROR, 'e');
            Controller::siteIndex();
        }

        /**
         * @return Boolean - user is logged or not
         *
        **/
        public function logged(){
            return isset($_SESSION['logged']);
        }
        
        /**
         * Tells if user is logged. If isn't, fires an error
         * and redirects to index.
         *
         * @return Boolean - user is logged or not
         *
        **/
        static function staticLogged(){
            $logged = false;
            if(isset($_SESSION['logged'])){
                if(!is_null($_SESSION['logged'])){
                    $logged = true;
                }
            }
            if(!$logged){
                $_SESSION['flash']['danger'][] = _LOGIN_NEED;
                Controller::siteIndex();
                die;
            }
        }

        /**
         * Do a CURL requests. Optionally, sends data through POST.
         *
         * @param String $url - target URL
         * @param Boolean $send - send data through POST?
         * @param Array $data - data to send through POST.
         * @return String - curl's return
         *
        **/
        public static function curl($url, $send = false, Array $data = null){
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if($send){
                if(!is_array($data))
                    return false;
                curl_setopt($ch, CURLOPT_POST, true); // diz que vai ser requisição post
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
                curl_setopt($ch, CURLOPT_POSTFIELDS, Model::http_build_query_for_curl($data));
            }
            
            $result = curl_exec($ch);
            
            curl_close($ch);
            
            return $result;
        }
    
    }
