<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <form action="/member/edit/" method="post">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input name="name" type="text" requried class="form-control" value="{$actualUser->get('name')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Telefone</label>
                            <input name="phone" type="text" required class="form-control mask-phone" value="{$member->get('phone')}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <h3 class="title">Endereço residencial</h3>
                            <p>
                                O seu endereço de entrega pode ser alterado clicando
                                <a href="/address/view/">aqui</a>.
                            </p>
                            <label>Digite seu CEP:</label>
                            <input name="cep" id="cep" type="text" required class="form-control mask-cep" value="{$member->get('cep')}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 form-group">
                            <label>Rua</label>
                            <input name="street" type="text" required id="rua" class="form-control" value="{$member->get('street')}">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label>Nº</label>
                            <input name="number" type="text" required id="numero" class="form-control" value="{$member->get('number')}">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Bairro</label>
                            <input name="neighborhood" type="text" required id="bairro" class="form-control" value="{$member->get('neighborhood')}">
                        </div>
                        <div class="col-sm-2 form-group">
                            <label>Complemento</label>
                            <input name="complement" type="text" class="form-control" value="{$member->get('complement')}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input name="city" type="text" id="cidade" required readonly class="form-control" value="{$member->get('city')}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Estado</label>
                                <input name="state" type="text" id="estado" required readonly class="form-control" value="{$member->get('state')}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>