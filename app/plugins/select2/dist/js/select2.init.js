$(function () {
    $('.select2').each(function () {
        $(this).select2({
            language: 'pt-BR'
        }).val($(this).attr('value')).trigger('change');
    })
})