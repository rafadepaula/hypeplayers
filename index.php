<?php
    header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
    header("access-control-allow-origin: https://pagseguro.uol.com.br");
    session_start();

    define('_IN_DEV_', false);

    function debug($var){
        echo '<pre>'.var_export($var, true).'</pre>';
    }

    include_once('app/config/appConfiguration.inc.php');
    include_once('base/config/constants.inc.php');
    
    // Constants
    include_once(_APPCONFIG_.'/constants.inc.php');

    if($_SERVER['HTTP_HOST'] != 'localhost:8110'){
        if($_SERVER["HTTPS"] != "on"){
            header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
            exit();
        }
    }

    // Autoload
    include_ONCE(_APP_.'plugins/autoload.php');
    include_once(_BASECONFIG_.'autoload.inc.php');

    // Smarty
    include_once(_BASECONFIG_.'smarty.inc.php');

    // Database connection_status
    include_once(_APPCONFIG_.'dbConnection.inc.php');

    // Errors handler
    include_once(_BASECONFIG_.'errorHandler.inc.php');

    // Error messages
    include_once(_BASECONFIG_.'messagesLocales.inc.php');
    include_once(_APPCONFIG_.'messagesLocales.inc.php');
    
    // $_POST verifications
    include_once(_BASECONFIG_.'formatDate.inc.php');
    include_once(_BASECONFIG_.'passCrypt.inc.php');

    // Code execution
    include_once(_BASECONFIG_.'exec.inc.php');
