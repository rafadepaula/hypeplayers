<?php
/* Smarty version 3.1.28, created on 2017-09-30 14:08:21
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59cfcf8540c763_62689871',
  'file_dependency' => 
  array (
    '3254ca34d590d3490186969d9ac4bb3b2357f8f4' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\view.tpl',
      1 => 1505155459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59cfcf8540c763_62689871 ($_smarty_tpl) {
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/game/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                    <a href="/genre/" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">Gêneros</span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Gênero</th>
                            <th>Plataformas</th>
                            <th>Disponível</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['games']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_game_0_saved_item = isset($_smarty_tpl->tpl_vars['game']) ? $_smarty_tpl->tpl_vars['game'] : false;
$_smarty_tpl->tpl_vars['game'] = new Smarty_Variable();
$__foreach_game_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_game_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['game']->value) {
$__foreach_game_0_saved_local_item = $_smarty_tpl->tpl_vars['game'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['game']->value->get('id_genre',true)->get('name');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['game']->value->platforms();?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['game']->value->get('available',true);?>

                            </td>
                            <td class="td-actions">
                                <a href="/game/edit/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Editar <?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/game/view/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Visualizar <?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <?php if ($_smarty_tpl->tpl_vars['game']->value->get('available')) {?>
                                    <a class="confirm-link" href="/game/toggle/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
">
                                        <button type="button" rel="tooltip" title="Remover <?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
 da lista"
                                                class="btn btn-danger btn-simple">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </a>
                                <?php } else { ?>
                                    <a class="confirm-link" href="/game/toggle/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
">
                                        <button type="button" rel="tooltip" title="Disponibilizar <?php echo $_smarty_tpl->tpl_vars['game']->value->get('name');?>
 na lista"
                                                class="btn btn-success btn-simple">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </a>
                                <?php }?>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['game'] = $__foreach_game_0_saved_local_item;
}
}
if ($__foreach_game_0_saved_item) {
$_smarty_tpl->tpl_vars['game'] = $__foreach_game_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
