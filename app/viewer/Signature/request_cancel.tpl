<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="row text-center">
                    <div class="col-sm-12">
                        <p>
                            <b>ATENÇÃO:</b>
                            ao cancelar a sua assinatura, fique ciente que para retornar novamente
                            ao clube de membros HypePlayers, a adesão será re-cobrada. <u>Proceda
                            somente se possui certeza da ação.</u>
                        </p>
                        <p>
                            <i>Sua solicitação de cancelamento passará por uma avaliação antes de ser efetivada</i>
                        </p>
                    </div>
                    <div class="col-sm-6 col-sm-offset-3">
                        <a style="cursor: pointer;" class="request-cancel">
                            <button class="btn btn-block btn-warning btn-lg">
                                Eu tenho certeza que quero cancelar a minha assinatura
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>