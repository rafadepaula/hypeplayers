<?php
    
    
    class RegionController extends AppController {

        public function __construct($controller = '', $function = ''){
            parent::__construct($controller, $function);
            if($this->user->get('role') != 'admin' && $function != 'checkCep'){
                $this->siteIndex();
                die;
            }
        }
        
        public function add(){
            
            if($this->request()){
                if($this->model->save(null, 'admin')){
                    return $this->view();
                }else{
                    unset($_POST);
                    return $this->add();
                }
            }
            
            return $this->viewer->show('add', 'Cadastrar região');
        }
        
        public function view($id = null){
            if(!is_null($id)){
                $region = $this->model->getRegion($id);
                $cep_init = str_replace('-', '', $region->get('cep_init'));
                $cep_end = str_replace('-', '', $region->get('cep_end'));
                $sql = 'select u.name, u.id, DATE_FORMAT(s.expiry, "%d/%m/%Y") as expiry,
                        DATE_FORMAT(s.last_payment, "%d/%m/%Y") as last_payment,
                        s.status
                        from user u
                        join signature s on s.id_user = u.id
                        join member m on m.id_user = u.id
                        WHERE REPLACE(m.cep, "-", "") BETWEEN '.$cep_init.' AND '.$cep_end;
                        
                $members = $this->model->query($sql);
                $this->viewer->set('members', $members);
                return $this->viewer->show('view_one', 'Membros em '.$region->get('name'));
            }
            $regions = $this->model->search('region');
            $this->viewer->set('regions', $regions);
            $this->viewer->set('regionsCount', count($regions));

            $regionRequests = $this->model->numRows('region_request');
            $this->viewer->set('regionRequests', $regionRequests);
            $this->viewer->set('hasRequests', $regionRequests > 0);

            $sql = 'select r.name, count(m.id_region) as total from region r join member m on r.id = m.id_region GROUP BY m.id_region ORDER BY count(m.id_region) DESC LIMIT 1';
            $requestedRegion = $this->model->query($sql);
            if(!count($requestedRegion))
                $requestedRegion = array(['name' => 'Nenhuma', 'total' => 0]);
            $this->viewer->set('requestedRegion', $requestedRegion[0]);

            return $this->viewer->show('view', 'Regiões Atendidas');
        }
        
        public function edit($id){
            if(!$this->model->exists('region', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->request()){
                if($this->model->save($id, 'admin')){
                    return $this->view();
                }else{
                    unset($_POST);
                    return $this->edit($id);
                }
            }
            $region = $this->model->getRegion($id);
            $this->viewer->set('region', $region);
            return $this->viewer->show('edit', 'Editar região de '.$region->get('name'));
        }

        public function delete($id){
            if(!$this->model->exists('region', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->model->exists('member', 'id_region', $id)){
                Viewer::flash('Existem membros nesta região!', 'e');
                return $this->view($id);
            }
            if($this->model->delete('region', array('id' => $id))){
                Viewer::flash(_DELETE_SUCCESS, 's');
                return $this->view();
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
                return $this->view($id);
            }
        }

        public function checkCep(){
            $this->model->checkCep($_POST['cep']);
        }

        public function requests(){
            $regionRequests = $this->model->search('region_request');
            $this->viewer->set('regionRequests', $regionRequests);

            return $this->viewer->show('requests', 'Regiões Solicitadas');
        }
    }
