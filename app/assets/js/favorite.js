$(function(){
	opts = {
        "bFilter": false,
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
        "iDisplayLength": -1,
        "rowReorder": true,
        columnDefs: [
                    { targets: [1,2,3], orderable: false}
        ],
        autoWidth: true,
        'language':{
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    }

    table = $('.datatable-favorite').DataTable(opts);

    table.on( 'row-reordered', function ( e, diff, edit ) {
    	if(diff.length == 0){
    		return true;
    	}
    	arr = [];
    	for(i in diff){
    		arr.push([diff[i].oldPosition, diff[i].newPosition]);
    	}
    	$.ajax({
    		data: {'diff': arr},
    		method: 'post',
    		url: '/favorite/reorder'
    	}).done(function(ret){
    		if(ret != 1){
    			swal({type:'error', text: ret});
    		}
    	})
	});
})