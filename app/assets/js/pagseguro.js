$.ajax({
	url: '/signature/generateSession'
}).done(function(session){
	console.log(session);
	if(!validJson(session)){
		pagseguroError('Não foi possível gerar uma sessão de pagamento! Tente recarregar a página.');
		return;
	}
	session = $.parseJSON(session);

	if(typeof session.id == 'undefined'){
		pagseguroError('Não foi possível gerar uma sessão de pagamento! Tente recarregar a página.');
	}
	PagSeguroDirectPayment.setSessionId(session.id);

	PagSeguroDirectPayment.getPaymentMethods({
	    success: function(response) {
	    	$.each(response.paymentMethods.CREDIT_CARD.options, function (name, obj) {
			  loadCcImage('https://stc.pagseguro.uol.com.br' + obj.images.SMALL.path, name)
			});
	        console.log(response);
	    },
	    error: function(response) {
	    	console.error(response);
	        return pagseguroError('Não foi possível carregar as bandeiras de cartão. Tente recarregar a página.');
	    },
	});

});

function loadCcImage(src, name){
	$('#ccimages').append('<img src="' + src + '" id="cc' + name + '" class="ccimage">');
}

function pagseguroError(text){
	swal({
		type: 'error',
		title: 'Ops!',
		text: text,
		timer: 3000,
	}).then(function(){
		location.reload();
	}, function(dismiss){
		location.reload();
	});
	return true;
}

$(function(){

	// expirationYear
	start = new Date().getFullYear();
	for(i = start; i <= start + 15; i++){
		$('#expirationYear').append('<option value="' + i + '">' + i + '</option>');
	}

	// brand
	$('#cardNumber').on('keyup', function(){
		$('#cardNumber').parent().removeClass('has-warning');
		$('#cardNumberHelper').html('');
		val = $(this).val().replace(/ /g,'').replace(/_/g,'');
		if(val.length >= 6 && $('#brand').val() == ''){
			bin = val.substring(0, 6);
			PagSeguroDirectPayment.getBrand({
			    cardBin: bin,
			    success: function(response) {
			        console.log(response);
			        $('#cvv').attr('maxlength', response.brand.cvvSize);
			        nameUpper = response.brand.name.toUpperCase();
			        name = response.brand.name;
			        $('.ccimage').hide();
			        $('#cc' + nameUpper).show();
			        $('#brand').val(name);
			    },
			    error: function(response) {
			    	console.error(response);
			        $('#cardNumber').parent().addClass('has-warning');
			        $('#cardNumberHelper').html('Número inválido!');
			    }
			});
		}else{
			if(val.length < 6){
				$('.ccimage').hide();
				$('#brand').val('');
				$('#cvv').removeAttr('maxlength');
			}
		}
	});

	// cancel
	$('.request-cancel').click(function(){
		swal({
            title: 'Aguarde...',
            text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonText: "Aguarde...",
            closeOnConfirm: false,
        });
	    $.ajax({
        	url: '/signature/requestCancel/1',
        }).done(function(ret){
        	console.log(ret);
        	if(ret == 1 || ret == ''){
	        	swal({
	        		type: 'success',
	        		title: 'Feito!',
	        		text: 'Solicitação de cancelamento realizada!',
	        		'timer': 5000,
	        	}).then(function(){
	        		window.location = '/signature/view/';
	        	}, function(dismiss){
	        		window.location = '/signature/view/';
	        	});
	        }else{
	        	swal({
	        		type: 'error',
	        		title: 'Ops! :O',
	        		text: 'Um erro ocorreu ao solicitar cancelamento. Tente novamente.',
	        		'timer': 5000,
	        	}).then(function(){
	        		window.location = '/signature/view/';
	        	}, function(dismiss){
	        		window.location = '/signature/view/';
	        	});
	        }
        });
	});
	$('.confirm-cancel').click(function(){
		swal({
            title: 'Aguarde...',
            text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonText: "Aguarde...",
            closeOnConfirm: false,
        });
        id_user = $(this).data('id-user');
	    $.ajax({
        	url: '/signature/confirmCancel/' + id_user + '/1',
        }).done(function(ret){
        	console.log(ret);
        	if(ret == 1 || ret == ''){
	        	swal({
	        		type: 'success',
	        		title: 'Feito!',
	        		text: 'Assinatura do membro cancelada com sucesso',
	        		'timer': 5000,
	        	}).then(function(){
	        		window.location = '/member/view/' + id_user;
	        	}, function(dismiss){
	        		window.location = '/member/view/' + id_user;
	        	});
	        }else{
	        	swal({
	        		type: 'error',
	        		title: 'Ops! :O',
	        		text: 'Um erro ocorreu ao cancelar a assinatura. Tente novamente.',
	        		'timer': 5000,
	        	}).then(function(){
	        		window.location = '/signature/confirmCancel/' + id_user;
	        	}, function(dismiss){
	        		window.location = '/signature/confirmCancel/' + id_user;
	        	});
	        }
        });
	});

    $('.confirm-change').click(function(){
        swal({
            title: 'Aguarde...',
            text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
            showCancelButton: false,
            showConfirmButton: false,
            confirmButtonText: "Aguarde...",
            closeOnConfirm: false,
        });
        id_user = $(this).data('id-user');
        $.ajax({
            url: '/signature/confirmChange/' + id_user + '/1',
        }).done(function(ret){
            console.log(ret);
            if(ret == 1 || ret == ''){
                swal({
                    type: 'success',
                    title: 'Feito!',
                    text: 'Troca de plano permitida com sucesso.',
                    'timer': 5000,
                }).then(function(){
                    window.location = '/member/view/' + id_user;
                }, function(dismiss){
                    window.location = '/member/view/' + id_user;
                });
            }else{
                swal({
                    type: 'error',
                    title: 'Ops! :O',
                    text: 'Um erro ocorreu ao permitir a troca de plano. Tente novamente.',
                    'timer': 5000,
                }).then(function(){
                    window.location = '/signature/confirmChange/' + id_user;
                }, function(dismiss){
                    window.location = '/signature/confirmChange/' + id_user;
                });
            }
        });
    });
});

$('#startActivation').on('submit', function(e){
	e.preventDefault();

	var senderHash = PagSeguroDirectPayment.getSenderHash();
	console.log(senderHash + ' - senderhash');

	var param = {
	    cardNumber: $('#cardNumber').val().replace(/ /g,'').replace(/_/g,''),
	    cvv: $("#cvv").val(),
	    expirationMonth: $("#expirationMonth").val(),
	    expirationYear: $("#expirationYear").val(),
	    success: function(response) {
	    	console.log(response);
	        cardToken = response.card.token;

	        var post = {
	        	cardToken: cardToken,
	        	holder_name: $('#holder_name').val(),
	        	holder_birthdate: $('#holder_birthdate').val(),
	        	holder_cpf: $('#holder_cpf').val(),
	        	sender_hash: senderHash,
	        	code: '',
	        }
	        $.ajax({
	        	url: '/signature/activate/1',
	        	type: 'post',
	        	data: post,
	        }).done(function(ret){
	        	console.log(ret);
	        	if(!validJson(ret)){
	        		return pagseguroError('Não foi possível gerar o objeto da assinatura');
	        	}
				
	        	ret = $.parseJSON(ret);
	        	if(typeof(ret.code) == 'undefined'){
	        		return pagseguroError('Não foi possível gerar o objeto da assinatura');
	        	}
	        	post['code'] = ret.code;
	        	$.ajax({
	        		url: '/signature/activate/2',
	        		type: 'post',
	        		data: post,
	        	}).done(function(result){
	        		if(!validJson(result)){
	        			$.ajax({
			        		url: '/signature/cancelByError',
			        		type: 'post',
			        		data: post,
			        	}).done(function(){
			        		return pagseguroError('Houve um erro no checkout. Contate a administração urgentemente.');
			        	})
		        	}

		        	result = $.parseJSON(result);
		        	swal(result);
		        	window.location = '/signature/view/';
	        	});

	        });
	    },
	    error: function(response) {
	    	console.error(response);
	        pagseguroError('Não foi possível autenticar seu cartão.');
	    }
	};
	if($("#brand").val() != '') {
	    param.brand = $("#brand").val();
	}
	PagSeguroDirectPayment.createCardToken(param);
});

$('#reactivation').on('submit', function(e){
    e.preventDefault();

    var senderHash = PagSeguroDirectPayment.getSenderHash();
    console.log(senderHash + ' - senderhash');

    var param = {
        cardNumber: $('#cardNumber').val().replace(/ /g,'').replace(/_/g,''),
        cvv: $("#cvv").val(),
        expirationMonth: $("#expirationMonth").val(),
        expirationYear: $("#expirationYear").val(),
        success: function(response) {
            console.log(response);
            cardToken = response.card.token;

            var post = {
                cardToken: cardToken,
                holder_name: $('#holder_name').val(),
                holder_birthdate: $('#holder_birthdate').val(),
                holder_cpf: $('#holder_cpf').val(),
                sender_hash: senderHash,
                plan_level: $('#plan_level').val(),
                code: '',
            }
            $.ajax({
                url: '/signature/activate/1',
                type: 'post',
                data: post,
            }).done(function(ret){
                console.log(ret);
                if(!validJson(ret)){
                    return pagseguroError('Não foi possível gerar o objeto da assinatura');
                }

                ret = $.parseJSON(ret);
                if(typeof(ret.code) == 'undefined'){
                    return pagseguroError('Não foi possível gerar o objeto da assinatura');
                }
                post['code'] = ret.code;
                $.ajax({
                    url: '/signature/activate/2',
                    type: 'post',
                    data: post,
                }).done(function(result){
                    if(!validJson(result)){
                        $.ajax({
                            url: '/signature/cancelByError',
                            type: 'post',
                            data: post,
                        }).done(function(){
                            return pagseguroError('Houve um erro no checkout. Contate a administração urgentemente.');
                        })
                    }

                    result = $.parseJSON(result);
                    swal(result);
                    window.location = '/signature/view/';
                });

            });
        },
        error: function(response) {
            console.error(response);
            pagseguroError('Não foi possível autenticar seu cartão.');
        }
    };
    if($("#brand").val() != '') {
        param.brand = $("#brand").val();
    }
    PagSeguroDirectPayment.createCardToken(param);
});


$('#changeCard').on('submit', function(e){
	e.preventDefault();

	var senderHash = PagSeguroDirectPayment.getSenderHash();
	console.log(senderHash + ' - senderhash');

	var param = {
	    cardNumber: $('#cardNumber').val().replace(/ /g,'').replace(/_/g,''),
	    cvv: $("#cvv").val(),
	    expirationMonth: $("#expirationMonth").val(),
	    expirationYear: $("#expirationYear").val(),
	    success: function(response) {
	    	console.log(response);
	        cardToken = response.card.token;

	        var post = {
	        	cardToken: cardToken,
	        	holder_name: $('#holder_name').val(),
	        	holder_birthdate: $('#holder_birthdate').val(),
	        	holder_cpf: $('#holder_cpf').val(),
	        	sender_hash: senderHash,
	        }
	        $.ajax({
	        	url: '/signature/changeCard/',
	        	type: 'post',
	        	data: post,
	        }).done(function(ret){
	        	console.log(ret);
	        	if(ret == ''){
	        		window.location = '/signature/view/';
	        	}
	        	if(!validJson(ret)){
	        		return pagseguroError("Não foi possível alterar seu cartão de crédito.");
	        	}

	        	ret = $.parseJSON(ret);
	        	if(typeof ret.return != 'undefined'){
	        		return pagseguroError("Não foi possível alterar seu cartão de crédito.");
	        	}else{
	        		if(ret.return != 1){
	        			return pagseguroError("Não foi possível alterar seu cartão de crédito.");
	        		}
	        	}


	        	window.location = '/signature/view/';
	        });
	    },
	    error: function(response) {
	    	console.error(response);
	        pagseguroError('Não foi possível autenticar seu cartão.');
	    }
	};
	if($("#brand").val() != '') {
	    param.brand = $("#brand").val();
	}
	PagSeguroDirectPayment.createCardToken(param);
});