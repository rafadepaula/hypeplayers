$(function(){
    $('#copy-from').on('change', function(){
        id_game = $(this).val();
        $.ajax({
            url: '/game/copy/' + id_game
        }).done(function(data){
            if(!validJson(data)){
                swal({
                    type: 'error',
                    title: 'Ops!',
                    text: 'Erro interno no sistema.',
                });
                return;
            }
            data = $.parseJSON(data);
            if(typeof data.error != 'undefined'){
                swal({
                    type: 'erro',
                    title: 'Ops!',
                    text: data.error,
                });
                return;
            }

            $('#name').val(data.name);
            $('#description').val(data.description);
            $('#platform').val(data.platform);
            $('#cover').html('');
            loadImage('/' + data.cover, $('#cover'));
            $('#cover_input').val(data.cover);
        })
    })
});

function loadImage(path, target) {
    var img = new Image();
    img.src = path;
    $(img).css('width', 'auto').css('max-height', '150px').css('display', 'block').css('margin', '0 auto');
    target.append(img);
}
