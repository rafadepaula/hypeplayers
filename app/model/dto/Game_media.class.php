<?php
    
    class Game_media extends DTO {
        public $id;
        public $id_game;
        public $type;
        public $source;
        public $thumb;
        
        public $FieldsValidation = array(
            'id_game' => array('existsForeign', ['game', 'id']),
        );
        public $FieldsErrors     = array(
            'id_game' => 'Informe um jogo válido para a mídia.',
        );
        public $FieldsMasks      = array(
            'id_game' => array('getDto', 'game'),
            'source' => 'embedMask',
        );

        public function embedMask($input){
            $input = str_replace('watch?v=', 'embed/', $input);
            return $input;
        }
    }