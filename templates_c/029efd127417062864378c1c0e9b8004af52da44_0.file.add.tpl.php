<?php
/* Smarty version 3.1.28, created on 2017-08-10 15:24:19
  from "C:\xampp\htdocs\hypeplayers\app\viewer\User\add.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_598ca4d37bdfe4_70180292',
  'file_dependency' => 
  array (
    '029efd127417062864378c1c0e9b8004af52da44' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\User\\add.tpl',
      1 => 1502389457,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598ca4d37bdfe4_70180292 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/user/add">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>E-mail</label>
                            <input required type="email" name="email" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Senha</label>
                            <input required type="password" name="password" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Confirme a senha</label>
                            <input required type="password" name="passwordConfirm" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }
}
