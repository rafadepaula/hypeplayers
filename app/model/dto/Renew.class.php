<?php
    
    class Renew extends DTO {
        public $id;
        public $id_rent;
        public $created;
        
        public $FieldsMasks = array(
            'id_rent' => array('getDto', ['rent']),
            'created' => 'dateMask',
        );
        
    }