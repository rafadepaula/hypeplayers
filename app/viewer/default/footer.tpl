
                    <footer class="footer">
                        <div class="container-fluid">
                            <p class="copyright pull-right">
                                &copy; <script>document.write(new Date().getFullYear())</script> HypePlayers
                            </p>
                        </div>
                    </footer>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/material.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/qtip2/3.0.3/basic/jquery.qtip.min.js"></script>
    <script src="../base/js/scripts.js"></script>

    <!--  Charts Plugin -->
    {* <script src="assets/js/chartist.min.js"></script> *}

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    {* <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> *}

    <!-- Material Dashboard javascript methods -->
    <script src="assets/js/material-dashboard.js"></script>

    <!-- InputMask -->
    <script src="plugins/input-mask/jquery.inputmask.js"></script>
    <script src="plugins/input-mask/jquery.maskmoney.js"></script>
    <script src="plugins/input-mask/jquery.mask.init.js"></script>

    <!-- Select2 -->
    <script src="plugins/select2/dist/js/select2.min.js"></script>
    <script src="plugins/select2/dist/js/i18n/pt.js"></script>
    <script src="plugins/select2/dist/js/select2.init.js"></script>

    <!-- Datatables -->
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/r-2.2.0/rr-1.2.3/sl-1.2.3/datatables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js"></script>
    <script src="plugins/datatables/datatables.init.js"></script>

    <!-- Timeago !-->
    <script src="plugins/timeago/jquery.timeago.js"></script>
    <script src="plugins/timeago/jquery.timeago.pt-br.js"></script>
    <script src="plugins/timeago/jquery.timeago.init.js"></script>

    <!-- Sweetalert2 !-->
    <script src="plugins/sweetalert2/dist/sweetalert2.min.js"></script>

    <script src="assets/js/notifications.js"></script>

    <!-- Bootstrap Tour !-->
    <script src="plugins/bootstraptour/bootstrap-tour.min.js"></script>
    <script src="assets/js/help.js"></script>


    <!-- Dynamic JS !-->
    {if isset($js)}{$js}{/if}
</html>


