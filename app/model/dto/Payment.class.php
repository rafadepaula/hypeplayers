<?php
    
    class Payment extends DTO {
        public $id;
        public $id_user;
        public $date;
        public $value;
        public $code;
        public $reference;

        public $FieldsMasks = array(
        	'id_plan_history' => array('getDto', ['user']),
            'date' => 'datetimeMask',
            'value' => 'moneyMask',
        );

    }