<?php
    
    /**
     * DTO class for errors
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     * @param String $error - error description
     * @param String $file - error file
     * @param Integer $line - error line
     * @param Datetime $date - date of the error
     *
     **/
    class Error extends DTO {
        public $error;
        public $file;
        public $line;
        public $date;
        
        public function __construct($error, $file, $line, $date) {
            $this->set('error', $error);
            $this->set('file', $file);
            $this->set('line', $line);
            $this->set('date', $date);
        }
    }
