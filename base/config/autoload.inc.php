<?php

    /**
     * Files autoload
     *
     * @param String $class - file to be loaded
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.1 - 2017-08-12
     *
     **/
    function autoload($class) {
        // Base MVC
        if (file_exists(_BASE_ . 'controller/' . $class . '.class.php')) {
            include_once(_BASE_ . 'controller/' . $class . '.class.php');
        }
        elseif (file_exists(_BASE_ . 'model/' . $class . '.class.php')) {
            include_once(_BASE_ . 'model/' . $class . '.class.php');
        }
        elseif (file_exists(_BASE_ . 'viewer/' . $class . '.class.php')) {
            include_once(_BASE_ . 'viewer/' . $class . '.class.php');
        }

        // Traits
        elseif (file_exists(_BASE_ . 'model/trait/' . $class . '.trait.php')) {
            include_once(_BASE_ . 'model/trait/' . $class . '.trait.php');
        }
        elseif (file_exists(_APP_ . 'model/trait/' . $class . '.trait.php')) {
            include_once(_BASE_ . 'model/trait/' . $class . '.trait.php');
        }
        elseif (file_exists(_APP_ . 'model/trait/' . $class . '.trait.php')) {
            include_once(_BASE_ . 'model/trait/' . $class . '.trait.php');
        }

        // App classes
        elseif (file_exists(_APP_ . 'controller/' . $class . '.class.php')) {
            include_once(_APP_ . 'controller/' . $class . '.class.php');
        }
        elseif (file_exists(_APP_ . 'model/' . $class . '.class.php')) {
            include_once(_APP_ . 'model/' . $class . '.class.php');
        }
        elseif (file_exists(_APP_ . 'model/dto/' . $class . '.class.php')) {
            include_once(_APP_ . 'model/dto/' . $class . '.class.php');
        }
        elseif (file_exists(_APP_ . 'viewer/' . $class . '.class.php')) {
            include_once(_APP_ . 'viewer/' . $class . '.class.php');
        }
        elseif (file_exists(SMARTY_SYSPLUGINS_DIR . strtolower($class) . '.php')){
            include SMARTY_SYSPLUGINS_DIR . strtolower($class) . '.php';
        }

        // PDF
        elseif(file_exists(_BASE_.'plugins/fpdf/'.strtolower($class).'.php')){
            include_once(_BASE_.'plugins/fpdf/'.strtolower($class).'.php');
        }
        elseif(file_exists(_APP_.'model/pdf/'.$class.'.class.php')){
            include_once(_APP_.'model/pdf/'.$class.'.class.php');
        }
        elseif(file_exists(_BASE_.'model/pdf/'.$class.'.class.php')){
            include_once(_BASE_.'model/pdf/'.$class.'.class.php');
        }

        // Excel
        elseif(file_exists(_BASE_.'plugins/phpexcel/'.$class.'.php')){
            include_once(_BASE_.'plugins/phpexcel/'.$class.'.php');
        }elseif(file_exists(_APP_.'model/excel/'.$class.'.class.php')){
            include_once(_APP_.'model/excel/'.$class.'.class.php');
        }elseif(file_exists(_BASE_.'model/excel/'.$class.'.class.php')){
            include_once(_BASE_.'model/excel/'.$class.'.class.php');
        }

        // Error
        elseif(file_exists(_BASE_.'config/'.$class.'.php')){
            include_once(_BASE_.'config/'.$class.'.php');
        }
    }

    spl_autoload_register('autoload');