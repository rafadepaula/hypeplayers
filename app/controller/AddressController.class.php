<?php
    
    
    class AddressController extends AppController {
        
        public function add(){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
	        $addresses = $this->model->numRows('address', array('id_user' => $this->user->get('id')));
	        if($addresses >= 2){
		        Viewer::flash('Você pode cadastrar no máximo dois endereços.');
		        return $this->view();
	        }
            if($this->request()){
                if($this->model->save()){
                    return $this->view();
                }
                unset($_POST);
            }

            $this->viewer->addJs('assets/js/cep.js');
            return $this->viewer->show('add', 'Cadastrar endereço alternativo');
        }
        
        public function view(){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            $addresses = $this->model->search('address', '*', array('id_user' => $this->user->get('id')));
            $this->viewer->set('addresses', $addresses);

	        $total = $this->model->numRows('address', array('id_user' => $this->user->get('id')));
			$this->viewer->set('totalAddresses', $total);

            return $this->viewer->show('view', 'Endereços de entrega');
        }
        

        public function delete($id){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('address', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            $address = $this->model->getAddress($id);
            if($address->get('id_user') != $this->user->get('id')){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->model->delete('address', array('id' => $id))){
                Viewer::flash(_DELETE_SUCCESS, 's');
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
            }
            return $this->view();
        }

        public function toggle($id){
            $address = $this->model->getAddress($id);
            $address->set('is_default', !$address->get('is_default'));
            if($this->model->update('address', $address, array('id' => $id))){
                Viewer::flash('Padrão alterado com sucesso.', 's');
            }else{
                Viewer::flash(_INSERT_ERROR, 'e');
            }
            return $this->view();
        }
    }
