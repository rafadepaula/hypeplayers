<?php

    trait Masks {

        public function dateMask($input) {
            return date('d/m/Y', strtotime($input));
        }

        public function datetimeMask($input) {
            return date('d/m/Y H:i:s', strtotime($input));
        }

        public function ufMask($input) {
            $input = strtoupper($input);
            $ufs = array(""   => "",
                         "AC" => "Acre",
                         "AL" => "Alagoas",
                         "AM" => "Amazonas",
                         "AP" => "Amapá",
                         "BA" => "Bahia",
                         "CE" => "Ceará",
                         "DF" => "Distrito Federal",
                         "ES" => "Espírito Santo",
                         "GO" => "Goiás",
                         "MA" => "Maranhão",
                         "MT" => "Mato Grosso",
                         "MS" => "Mato Grosso do Sul",
                         "MG" => "Minas Gerais",
                         "PA" => "Pará",
                         "PB" => "Paraíba",
                         "PR" => "Paraná",
                         "PE" => "Pernambuco",
                         "PI" => "Piauí",
                         "RJ" => "Rio de Janeiro",
                         "RN" => "Rio Grande do Norte",
                         "RO" => "Rondônia",
                         "RS" => "Rio Grande do Sul",
                         "RR" => "Roraima",
                         "SC" => "Santa Catarina",
                         "SE" => "Sergipe",
                         "SP" => "São Paulo",
                         "TO" => "Tocantins");

            return $ufs[$input];
        }

        public function reverseUf($input){
            $ufs = array(""   => "",
                         "AC" => "Acre",
                         "AL" => "Alagoas",
                         "AM" => "Amazonas",
                         "AP" => "Amapá",
                         "BA" => "Bahia",
                         "CE" => "Ceará",
                         "DF" => "Distrito Federal",
                         "ES" => "Espírito Santo",
                         "GO" => "Goiás",
                         "MA" => "Maranhão",
                         "MT" => "Mato Grosso",
                         "MS" => "Mato Grosso do Sul",
                         "MG" => "Minas Gerais",
                         "PA" => "Pará",
                         "PB" => "Paraíba",
                         "PR" => "Paraná",
                         "PE" => "Pernambuco",
                         "PI" => "Piauí",
                         "RJ" => "Rio de Janeiro",
                         "RN" => "Rio Grande do Norte",
                         "RO" => "Rondônia",
                         "RS" => "Rio Grande do Sul",
                         "RR" => "Roraima",
                         "SC" => "Santa Catarina",
                         "SE" => "Sergipe",
                         "SP" => "São Paulo",
                         "TO" => "Tocantins");
            $ufs = array_flip($ufs);
            return $ufs[$input];
        }

        public function accentOffMask($input, $enc = "UTF-8") {

            $accents = array(
                'A'  => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
                'a'  => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
                'C'  => '/&Ccedil;/',
                'c'  => '/&ccedil;/',
                'E'  => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
                'e'  => '/&egrave;|&eacute;|&ecirc;|&euml;/',
                'I'  => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
                'i'  => '/&igrave;|&iacute;|&icirc;|&iuml;/',
                'N'  => '/&Ntilde;/',
                'n'  => '/&ntilde;/',
                'O'  => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
                'o'  => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
                'U'  => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
                'u'  => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
                'Y'  => '/&Yacute;/',
                'y'  => '/&yacute;|&yuml;/',
                'a.' => '/&ordf;/',
                'o.' => '/&ordm;/',
            );

            return preg_replace($accents, array_keys($accents), htmlentities($input, ENT_NOQUOTES, $enc));
        }

        public static function monthNameMask($input) {
            switch ($input) {
                case 1:
                case "01":
                    $input = 'Janeiro';
                    break;
                case 2:
                case "02":
                    $input = 'Fevereiro';
                    break;
                case 3:
                case "03":
                    $input = 'Março';
                    break;
                case 4:
                case "04":
                    $input = 'Abril';
                    break;
                case 5:
                case "05":
                    $input = 'Maio';
                    break;
                case 6:
                case "06":
                    $input = 'Junho';
                    break;
                case 7:
                case "07":
                    $input = 'Julho';
                    break;
                case 8:
                case "08":
                    $input = 'Agosto';
                    break;
                case 9:
                case "09":
                    $input = 'Setembro';
                    break;
                case 10:
                case "10":
                    $input = 'Outubro';
                    break;
                case 11:
                case "11":
                    $input = 'Novembro';
                    break;
                case 12:
                case "12":
                    $input = 'Dezembro';
                    break;
            }

            return $input;
        }

        public function getDto($table, $id) {
            $primary = 'id';
            if (is_array($table)) {
                $primary = $table[1];
                $table = $table[0];
            }
            $model = new Model();
            $dto = $model->getDto($table, $primary, $id);
            
            return $dto;
        }

        public static function moneyMask($val) {
            return number_format($val, 2, ',', '.');
        }

        public function decimalMask($val) {
            $val = str_replace('R$', '', $val);
            $val = str_replace('.', '', $val);
            $val = str_replace(',', '.', $val);
            
            return trim($val);
        }

        public function booleanMask($val){
            return $val ? 'Sim' : 'Não';
        }

        public function upperMask($val){
            return mb_strtoupper($val);
        }
        
        public function reverseDate($val){
            $val = explode('/', $val);
            return $val[2].'-'.$val[1].'-'.$val[0];
        }
        
        public function reverseDatetime($val){
            $val = explode(' ', $val);
            return $this->reverseDate($val[0]).' '.$val[1];
        }
        
        public function cryptPass($val){
            return crypt($val, '$2a$12$'._PASS_SALT_.'$');
        }

        public function nl2brMask($val){
            return nl2br($val);
        }

    }
