<?php

    class FeaturedModel extends AppModel {
        public function getFeatured($id){
            return $this->getDto('featured', 'id', $id);
        }

        public function save($id = null){
            $featured = $this->getFeatured($id);
            $featured = $this->makeDto($featured, $id);
            $errors = $featured[1];
            $featured = $featured[0];

	        if(is_null($id))
	            $featured->set('position', $this->calcLastPosition());

            if(!empty($errors)){
                Viewer::flash($errors, 'e');
                return false;
            }else{
                $ret = is_null($id) ? $this->insert('featured', $featured) : $this->update('featured', $featured, array('id' => $id));
                if($ret) {
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }

        public function reorder($total){
            if($total){
                $featureds = $this->search('featured', '*', array('display' => 1), 'position');
                foreach($featureds as $position => $featured){
                    $featured->set('position', $position);
                    if(!$this->update('featured', $featured, array('id' => $featured->get('id')))){
                        return false;
                    }
                }
                return true;
            }
        }

        public function reorderSql($diff){
            $sql = 'update featured set position = case';
            $where = ' where position in (';
            foreach($diff as $positions){
                $oldPosition = $positions[0];
                $newPosition = $positions[1];
                $sql .= ' when position = '.$oldPosition.' then '.$newPosition;
                $where .= $oldPosition.',';
            }
            $where = trim($where,',').')';
            $sql .= ' end '.$where;
            return $sql;
        }

        public function games(){
        	$games = $this->search('game', '*');
        	$final = array();
        	foreach($games as $game){
        		$platform = '('.$game->platforms().')';
				$final[$game->get('id')] = $game->get('name').'     '.$platform;
	        }
	        return $final;
        }

	    public function calcLastPosition(){
		    return $this->numRows('featured', false);
	    }
    }