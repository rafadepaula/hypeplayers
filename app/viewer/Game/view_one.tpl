{if $modal}
<script src="plugins/gallery/dist/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="assets/js/gallery.js"></script>
<link rel="stylesheet" href="plugins/gallery/dist/magnific-popup.css">
<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

{/if}

{if $game->get('ps4')}
	{assign var="ps4" value="ps4"}
{else}
	{assign var="ps4" value=""}
{/if}
{if $game->get('xone')}
	{assign var="xone" value="xone"}
{else}
	{assign var="xone" value=""}
{/if}
{if $game->get('switch')}
	{assign var="switch" value="switch"}
{else}
	{assign var="switch" value=""}
{/if}
<div class="row" style="margin-top: 25px">

	<div class="col-sm-4 visible-xs">
		<div class="card card-profile">
			<div class="card-avatar game-cover">
				<img class="img" src="/{$cover}" />
			</div>

			<div class="content">
				<h6 class="category text-gray">{$game->get('name')}</h6>
				<h4 class="card-title">{$game->get('id_genre', true)->get('name')}</h4>
				{if !$game->get('available')}
					<p class="text-warning"><b>JOGO NÃO DISPONÍVEL ATUALMENTE</b></p>
				{/if}
				<p class="card-content">
					{$game->platforms()}
				</p>
				{if $actualUser->get('role') == 'admin'}
					<a href="/game/edit/{$game->get('id')}" class="btn btn-success btn-round">
						Editar jogo
					</a>
					<a href="/game/addMedia/{$game->get('id')}" class="btn btn-success btn-round">
						Cad. Mídia
					</a>
                {/if}
                {if $actualUser->get('role') == 'member'}
					{if $game->get('available')}
						<button type="button" class="btn btn-success btn-round add-fav" data-game="{$game->get('id')}"
							data-platforms="{$ps4} {$xone} {$switch}">
							Adicionar para minha lista
						</button>
					{/if}
				{/if}
			</div>
		</div>

		{if $actualUser->get('role') == 'admin'}
			<div class="card card-profile">
				<div class="content">
					<h4 class="card-title">Infos. Administrativas</h4>
					<div class="col-sm-12">
						<p class="text-justify">
							{if ($game->get('ps4'))}
								<b>Qtd. disp. PS4:</b> {$game->get('ps4_qtt')} <br>
							{/if}
							{if ($game->get('xone'))}
								<b>Qtd. disp. XBOX One:</b> {$game->get('xone_qtt')} <br>
							{/if}
							{if ($game->get('switch'))}
								<b>Qtd. disp. Nintendo Switch:</b> {$game->get('switch_qtt')} 
							{/if}
						</p>
						<a href="/game/delete/{$game->get('id')}" class="btn btn-danger btn-round confirm-link">
							Deletar jogo
						</a>
						<a href="/game/toggle/{$game->get('id')}" class="btn btn-warning btn-round">
							{if $game->get('available')}
								Remover da lista
							{else}
								Disponibilizar na lista
							{/if}
						</a>
					</div>
				</div>
			</div>
		{/if}
	</div>

 	<div class="col-sm-8">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="material-icons">videogame_asset</i>
            </div>
            <div class="card-content">
            	<div class="col-sm-12">
	                <p class="category">Perfil de jogo</p>
	                <h3 class="title">{$title}</h3>
                </div>

                <div class="col-sm-12">
	                <h4 class="text-left">Descrição</h4>
	                <p class="text-justify">{$game->get('description', true)}</p>
                </div>

                <div class="col-sm-12">
                	<h4 class="text-left">Imagens</h4>
                	<div id="images" class="row">
    	                {foreach $images as $image}
    	                	<div class="col-xs-4">
								<span href="/{$image->get('source')}" title="Imagem de {$title}" class="game-media-thumb">
							        <img class="align-center game-media" src="/{$image->get('thumb')}" alt="Imagem de {$title}">
							    </span>
							    {if $actualUser->get('role') == 'admin'}
								    <a class="confirm-link" 
								    	href="/game/deleteMedia/{$image->get('id')}">
								    	<small class="fa fa-remove text-danger"></small>
								    </a>
							    {/if}
						    </div>
                		{/foreach}
					</div>
                </div>

                <div class="col-sm-12" style="margin-bottom: 25px;">
                	<h4 class="text-left">Vídeos</h4>
                	{foreach $videos as $video}
                		{if $actualUser->get('role') == 'admin'}
						    <a class="confirm-link" 
						    	href="/game/deleteMedia/{$video->get('id')}">
						    	<small class="fa fa-remove text-danger"></small>
						    </a>
					    {/if}
	                	<div class="video-container">
		                	<iframe class="video" src="{$video->get('source', true)}"
			                    frameborder="0" allowfullscreen></iframe>
	                    </div>
                    {/foreach}
            	</div>

            </div>
        </div>
    </div>

	<div class="col-sm-4 hidden-xs">
		<div class="card card-profile">
			<div class="card-avatar game-cover">
				<img class="img" src="/{$cover}" />
			</div>


			<div class="content">
				<h6 class="category text-gray">{$game->get('name')}</h6>
				<h4 class="card-title">{$game->get('id_genre', true)->get('name')}</h4>
				{if !$game->get('available')}
					<p class="text-warning"><b>JOGO NÃO DISPONÍVEL ATUALMENTE</b></p>
				{/if}
				<p class="card-content">
					{$game->platforms()}
				</p>
				{if $actualUser->get('role') == 'admin'}
					<a href="/game/edit/{$game->get('id')}" class="btn btn-success btn-round">
						Editar jogo
					</a>
					<a href="/game/addMedia/{$game->get('id')}" class="btn btn-success btn-round">
						Cad. Mídia
					</a>
                {/if}
                {if $actualUser->get('role') == 'member'}
                    {if $game->get('available')}
                        <button type="button" class="btn btn-success btn-round add-fav" data-game="{$game->get('id')}"
                                data-platforms="{$ps4} {$xone} {$switch}">
                            Adicionar para minha lista
                        </button>
                    {/if}
                {/if}
                {if $actualUser->get('role') == 'visitor'}
                    <hr>
                    <a href="/member/join" class="btn btn-success btn-round" style="margin-bottom: 30px;" target="_blank">
                        Assine já!
                    </a>
                {/if}
			</div>
		</div>

		{if $actualUser->get('role') == 'admin'}
			<div class="card card-profile">
				<div class="content">
					<h4 class="card-title">Infos. Administrativas</h4>
					<div class="col-sm-12">
						<p class="text-justify">
							{if ($game->get('ps4'))}
								<b>Qtd. disp. PS4:</b> {$game->get('ps4_qtt')} <br>
							{/if}
							{if ($game->get('xone'))}
								<b>Qtd. disp. XBOX One:</b> {$game->get('xone_qtt')} <br>
							{/if}
							{if ($game->get('switch'))}
								<b>Qtd. disp. Nintendo Switch:</b> {$game->get('switch_qtt')} 
							{/if}
						</p>
						<a href="/game/delete/{$game->get('id')}" class="btn btn-danger btn-round confirm-link">
							Deletar jogo
						</a>
						<a href="/game/toggle/{$game->get('id')}" class="btn btn-warning btn-round">
							{if $game->get('available')}
								Remover da lista
							{else}
								Disponibilizar na lista
							{/if}
						</a>
					</div>
				</div>
			</div>
		{/if}
	</div>
</div>