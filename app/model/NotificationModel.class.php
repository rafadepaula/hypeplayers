<?php
    
    class NotificationModel extends AppModel  {
        public function getNotification($id){
            return $this->getDto('notification', 'id', $id);
        }
        
        public function read($user){
            switch($user->get('role')){
                case 'admin':
                    $notifications = $this->search('notification', '*', array(
                        'is_read' => 0,
                        'conscond1' => 'AND',
                        'type' => 'admin',
                    ), 'id DESC', false, 5);
                    break;
                default:
                    $notifications = $this->search('notification', '*', array(
                        'is_read' => 0,
                        'conscond1' => 'AND',
                        'id_user' => $user->get('id'),
                    ), 'id DESC', false, 5);
                break;
            }
            $this->setRead($notifications, $user->get('id'));
        }
        
        public function setRead($notifications, $id){
            foreach($notifications as $notification){
                $notification->set('is_read', 1);
                $notification->set('id_readby', $id);
                $notification->set('read_date', date('Y-m-d H:i:s'));
                $this->update('notification', $notification, array('id' => $notification->get('id')));
            }
        }

        public function send($id_user, $text, $link = null, $email = true){
            $notification = new Notification();
            $notification->set('type', 'user');
            if($id_user == 'admin')
                $notification->set('type', 'admin');
            else
                $notification->set('id_user', $id_user);
            $notification->set('text', $text);
            $notification->set('link', $link);
            $notification->set('created', date('Y-m-d H:i:s'));
            $notification->set('is_read', 0);

            if($email){
                $this->email($id_user, $text);
            }
            
            return $this->insert('notification', $notification);
        }

        public function email($id_user, $text){

            require_once 'base/plugins/phpmailer/PHPMailerAutoload.php';
            
            $email = new PHPMailer;
            
            $email->isSMTP();
            $email->Host       = 'mail.hypeplayers.com.br';
            $email->SMTPAuth   = true;
            $email->Username   = 'webmaster@hypeplayers.com.br';
            $email->Password   = 'Zs9w63Q0oy';
            $email->SMTPSecure = false;
            $email->Port       = 26;
            
            $email->From     = 'webmaster@hypeplayers.com.br';
            $email->FromName = 'HypePlayers';

            if($id_user == 'admin'){
                $users = $this->search('user', '*', array('role' => 'admin'));
                foreach($users as $user){
                    $email->addBCC($user->get('email'), $user->get('name'));
                }
            }else{
                $user = $this->getDto('user', 'id', $id_user);
                $email->addBCC($user->get('email'), $user->get('name'));
            }



            $email->isHTML(true);
            
            $email->Subject = 'HypePlayers - Nova notificação';

            $html = file_get_contents('app/viewer/Notification/email.html');
            $html = str_replace('%notification%', $text, $html);

            $email->Body    = $html;
            $email->CharSet = 'UTF-8';
            
            return $email->send();
        }

        public function newRent($id_user){
            $text = 'Um jogo foi enviado para você';
            $link = '/rent/myGames/';
            return $this->send($id_user, $text, $link);
        }

        public function rentStatusUpdated($id_user){
            $text = 'Locação com status atualizado';
            $link = '/rent/myGames/';
            return $this->send($id_user, $text, $link, false);
        }

        public function weekWarning($id_user){
            $text = 'Faltam 7 dias para a sua data de pagamento';
            $link = '/member/weekWarning';
            return $this->send($id_user, $text, $link);
        }

        public function lowFavoriteQtt($id_user){
            $text = 'Você possui poucos favoritos! Acrescente mais jogos na biblioteca';
            $link = '/game/library';
            return $this->send($id_user, $text, $link, false);
        }

        public function renewRequest($id_user, $accepted){
            if($accepted){
                $text = 'Seu pedido de renovação foi aceito';
            }else{
                $text = 'Seu pedido de renovação não foi aceito';
            }
            $link = '/rent/myGames';
            return $this->send($id_user, $text, $link, false);
        }

        public function delayedRent($id_user){
            $text = 'Você possui jogos em atraso';
            $link = '/rent/myGames/';
            return $this->send($id_user, $text, $link);
        }

        public function newMemberActivated($id_user){
            $text = 'Novo membro cadastrado';
            $link = '/member/view/'.$id_user;
            return $this->send('admin', $text, $link, false);
        }

        public function newMemberPaid($id_user){
            $text = 'Novo membro pagou 1ª mensalidade';
            $link = '/member/view/'.$id_user;
            return $this->send('admin', $text, $link, false);
        }

        public function notifyActivation($id_user){
            $text = 'Seu primeiro pagamento foi processado!';
            $link = '/signature/';
            return $this->send($id_user, $text, $link, false);
        }

        public function memberSuspended($id_user){
            $text = 'Sua assinatura foi suspensa.';
            $link = '/signature/';
            return $this->send($id_user, $text, $link);
        }

        public function paymentFeedback($id_user, $success){
            if($success){
                $text = 'Assinatura renovada com sucesso.';
                $email = true;
            }else{
                $text = 'Houve um problema no pagamento de sua assinatura.';
                $email = false;
            }
            $link = '/member/home';
            return $this->send($id_user, $text, $link, $email);
        }

        public function memberRequest($name, $request, $id_user){
            $text = $name.' solicitou '.$request;
            $link = '/rent/requests';
            return $this->send('admin', $text, $link, false);
        }

        public function postConfirmed($name, $id_user){
            $text = $name.' confirmou postagem de jogo';
            $link = '/member/view/'.$id_user;
            return $this->send('admin', $text, $link, false);
        }

        public function favoritesUpdated($name, $id_user){
            $text = $name.' alterou sua lista de preferência.';
            $link = '/member/view/'.$id_user;
            return $this->send('admin', $text, $link, false);
        }

        public function newPsUser($name, $id_user, $game){
            $text = $name.' fez uma observação em '.$game;
            $link = '/member/view/'.$id_user;
            return $this->send('admin', $text, $link, false);
        }

    }