<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/region/edit/{$region->get('id')}">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control" value="{$region->get('name')}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Primeiro CEP da faixa</label>
                            <input required type="text" name="cep_init" class="form-control mask-cep" value="{$region->get('cep_init')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Último CEP da faixa</label>
                            <input required type="text" name="cep_end" class="form-control mask-cep" value="{$region->get('cep_end')}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
