<?php

    /**
     * Class used for show pages in screen
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.1 - 2017-08-12
     *
     **/
    class Viewer extends Smarty {
        private $css;
        private $js;
        private $called;

        /**
         * @var array $fullPages - pages that doesn't need the _default/header.tpl and _default/footer.tpl
         */
        private $fullPages = array('login', 'recovery', 'newPassword');


        /**
         * Constructor method
         *
         **/
        public function __construct() {
            parent::__construct();
            $this->force_compile = true;
            $this->set('css', '');
            $this->set('js', '');
        }

        /**
         * Used to include html in screen
         *
         * @param String $page - page name
         * @param String $title - TITLE tag in html
         * @param Boolean $needLogin - page need login? Default: true
         * @param Boolean $fullPage - doesnt need header and footer. Default: false
         *
         **/
        public function show($page, $title, $needLogin = true, $fullPage = false) {
    
            if (isset($_SESSION['user'])) {
                $this->set('actualUser', unserialize($_SESSION['user']));
            }else{
            	$actualUser = new User();
            	$actualUser->set('role', 'visitor');
            	$this->set('actualUser', $actualUser);
            }
    
            if (is_array($this->js)) {
                $js = implode(PHP_EOL, $this->js);
                $this->set('js', $js);
            }
            if (is_array($this->css)) {
                $css = implode(PHP_EOL, $this->css);
                $this->set('css', $css);
            }
            $this->set('title', $title);
    
            $folder = $this->getCalled(debug_backtrace());
            $this->called = $folder;
    
            $flash = $this->getflash();
            
            $this->set('_flash', $flash);
    
            if ($needLogin)
                include(_BASECONFIG_ . '/logged.inc.php');
    
            if (!$fullPage)
                $this->display(_APP_ . 'viewer/default/header.tpl');

            echo PHP_EOL;
            if (file_exists(_APP_ . 'viewer/' . $folder . '/' . $page . '.tpl')) {
                $this->display(_APP_ . 'viewer/' . $folder.'/'.$page.'.tpl');
            } else {
                if (file_exists(_APP_ . 'viewer/default/404.tpl'))
                    $this->display(_APP_ . 'viewer/default/404.tpl');
                else
                    $this->display(_BASE_ . 'viewer/views/404.tpl');
            }
            echo PHP_EOL;
            if (!$fullPage)
                $this->display(_APP_ . 'viewer/default/footer.tpl');
        }


        /**
         * Set a global variable
         *
         * @param String $variable - var name
         * @param Mixed $value - var value
         *
         **/
        public function set($variable, $value) {
            $this->assign($variable, $value);
        }

        /**
         * Add a CSS file to the html
         *
         * @param String $filename - name of the css file
         *
         **/
        public function addCSS($filename) {
            $this->css[] = '<link href="' . $filename . '" rel="stylesheet"/>';
        }


        /**
         * Add JS file to the html
         *
         * @param String $filename - name of the JS file
         *
         **/
        public function addJS($filename) {
            $this->js[] = '<script type="text/javascript" src="' . $filename . '"></script>';
        }

        /**
         * Get the caller class name
         *
         * @param Array $backtrace - debug_backtrace()
         * @return String - name of the caller class
         *
         **/
        public function getCalled($backtrace) {
            $called = $backtrace[0]['file'];
            $called = explode(_DS, $called);
            $called = $called[count($called) - 1];
            $called = explode('.', $called);
            $called = $called[0];
            $length = strlen($called) - 10;

            return substr($called, 0, $length);
        }

        /**
         * Method used to set a flash message
         *
         * @param String $message - message to be displayed.
         * @param String $type - message type, s: success
         *                                     e: error
         *                                     w: warning
         *                                     i: information. Default: i
         *
         **/
        public static function flash($message, $type = 'i') {
            $validTypes = array(
                's',
                'e',
                'w',
                'i',
            );
            $classes = array(
                's' => 'success',
                'e' => 'danger',
                'w' => 'warning',
                'i' => 'info',
            );
            if (!in_array($type, $validTypes))
                $type = 'i';

            $_SESSION['flash'][$classes[$type]][] = $message;
        }

        /**
         * Method used to return the flashes
         *
         * @return String - string ready to show the flashes
         *
         **/
        public function getFlash() {
            if (!isset($_SESSION['flash']))
                $_SESSION['flash'] = array();
            $have = false;
            foreach ($_SESSION['flash'] as $type => $flashes) {
                if (is_array($flashes)) {
                    if (count($flashes) > 0)
                        $have = true;
                }
            }
            if (!$have)
                return false;

            $_flash = $_SESSION['flash'];
            unset($_SESSION['flash']);
            $finalFlash = '';
            if ($_flash) {
                foreach ($_flash as $_class => $_messages) {
                    $finalFlash .= '
                    <br>
                    <div class="row">
                            <div class="col-md-12 alert alert-' . $_class . ' text-center">';
                    if(is_array($_messages)){
                        foreach($_messages as $_message){
                            $finalFlash .= $_message.'<br>';
                        }
                        $finalFlash = trim($finalFlash, '<br>');
                    }
                    $finalFlash .= '</div></div>';
                }
            }

            if (isset($_SESSION['phpError'])) {
                if ($_SESSION['phpError'] != '') {
                    $finalFlash .= '<div class="container">
                        <div class="row">
                                <div class="col-md-12 alert alert-warning text-center">
                                        <span>' . $_SESSION['phpError'] . '</span>
                                </div>
                        </div>
                </div>';
                    $_SESSION['phpError'] = '';
                }
            }

            return $finalFlash;

        }

    }
