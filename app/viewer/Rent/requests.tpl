
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/rent/plan" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">
                                <i class="fa fa-arrow-left" style="margin-right: 5px;"></i> Voltar
                            </span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Membro - Jogo</th>
                            <th>Status</th>
                            <th>Período</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $rents as $rent}
                        <tr>
                            <td>
                                {$rent->get('id_user', true)->get('name')} - 
                                {$rent->get('id_game', true)->get('name')}
                                ({$rent->get('platform', true)})
                            </td>
                            <td>
                                <b>{$rent->get('status', true)}</b>
                                {if $rent->get('status') == 'renew_requested'}
                                     - {$rent->get('renew_qtt')} renovações nesta locação
                                {/if}
                            </td>
                            <td>
                                {$rent->get('start', true)} até {$rent->get('end', true)}
                            </td>
                            <td class="td-actions">
                                <a>
                                    <button type="button" rel="tooltip" title="Visualizar locação"
                                            class="btn btn-default btn-simple view-one"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a>
                                    <button type="button" rel="tooltip" title="Alterar status"
                                            class="btn btn-success btn-simple change-status"
                                            data-id-rent="{$rent->get('id')}"
                                            data-url="/rent/requests">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                {if $rent->get('status') == 'renew_requested'}
                                    <a>
                                        <button type="button" rel="tooltip" title="Liberar renovação"
                                                class="btn btn-warning btn-simple finish-renew"
                                                data-id-rent="{$rent->get('id')}"
                                                data-url="/rent/requests">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </a>
                                    <a>
                                        <button type="button" rel="tooltip" title="Negar renovação"
                                                class="btn btn-danger btn-simple deny-renew"
                                                data-id-rent="{$rent->get('id')}"
                                                data-url="/rent/requests">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </a>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>


{include file=$smarty.current_dir|cat:"/changeStatus_modal.tpl"}
{include file=$smarty.current_dir|cat:"/view_modal.tpl"}