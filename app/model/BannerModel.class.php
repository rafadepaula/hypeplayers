<?php

    class BannerModel extends AppModel {
        public function getBanner($id){
            return $this->getDto('banner', 'id', $id);
        }

        public function save($id = null){
            $banner = $this->getBanner($id);
            $cover = $banner->get('cover');
            $banner = $this->makeDto($banner, $id);
            $errors = $banner[1];
            $banner = $banner[0];

            $banner->set('cover', $cover);
            $banner->set('button_display', 0);
            if(isset($_POST['button_display']))
            	if($_POST['button_display'] == 'on')
            		$banner->set('button_display', 1);

            $banner->set('position', $this->calcLastPosition());
            $banner->set('display', 1);

            if($_FILES['cover']['name'] != ''){
                $cover = new File(_BANNERS_DIR, 'cover');
                $dir = $cover->finish();
                if(!$banner->set('cover', $dir)){
                    Viewer::flash($banner->FieldsErrors['cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }

            if(!empty($errors)){
                Viewer::flash($errors, 'e');
                return false;
            }else{
                $ret = is_null($id) ? $this->insert('banner', $banner) : $this->update('banner', $banner, array('id' => $id));
                if($ret) {
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }

        public function reorder($total){
            if($total){
                $banners = $this->search('banner', '*', array('display' => 1), 'position');
                foreach($banners as $position => $banner){
                    $banner->set('position', $position);
                    if(!$this->update('banner', $banner, array('id' => $banner->get('id')))){
                        return false;
                    }
                }
                return true;
            }
        }

        public function reorderSql($diff){
            $sql = 'update banner set position = case';
            $where = ' where position in (';
            foreach($diff as $positions){
                $oldPosition = $positions[0];
                $newPosition = $positions[1];
                $sql .= ' when position = '.$oldPosition.' then '.$newPosition;
                $where .= $oldPosition.',';
            }
            $where = trim($where,',').')';
            $sql .= ' end '.$where;
            return $sql;
        }

        public function calcLastPosition(){
        	return $this->numRows('banner', array('display' => 1));
        }
    }