<?php
/* Smarty version 3.1.28, created on 2017-09-05 01:41:30
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\save_changes.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59ae2afaaa4ce0_39501446',
  'file_dependency' => 
  array (
    '834eb5dea26f40355e4e8e4a8d3dd06c2505ee01' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\save_changes.tpl',
      1 => 1504586453,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59ae2afaaa4ce0_39501446 ($_smarty_tpl) {
?>
<div class="row">
	<div class="col-sm-12">
		<p>
			Para continuar, você deve confirmar os seus jogos favoritos. Lembre-se
			que alterações em sua lista de preferências poderão ser realizadas
			novamente <b>apenas após o prazo de 30 dias</b>, salvo alterações
			que envolvem apenas alteração de prioridades.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
            <table class="table table-hover table-striped datatable">
                <thead>
                    <tr>
                    	<th>Prioridade</th>
                        <th>Jogo</th>
                        <th>Plataforma</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php
$_from = $_smarty_tpl->tpl_vars['favorites']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_favorite_0_saved_item = isset($_smarty_tpl->tpl_vars['favorite']) ? $_smarty_tpl->tpl_vars['favorite'] : false;
$_smarty_tpl->tpl_vars['favorite'] = new Smarty_Variable();
$__foreach_favorite_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_favorite_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['favorite']->value) {
$__foreach_favorite_0_saved_local_item = $_smarty_tpl->tpl_vars['favorite'];
?>
                    <tr id="favorite-<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position');?>
">
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position')+1;?>

                        </td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('id_game',true)->get('name');?>

                        </td>
                        <td>
                            <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('platform',true);?>

                        </td>
                        <td class="td-actions">
                            <a >
                                <button onclick="removeSavechanges(<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position');?>
)"
                                		type="button" rel="tooltip" 
                                        class="btn btn-danger btn-simple remove-savechanges">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                <?php
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_local_item;
}
}
if ($__foreach_favorite_0_saved_item) {
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_item;
}
?>
                </tbody>
            </table>
        </div>
    </div>
</div><?php }
}
