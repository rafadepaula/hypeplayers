<?php

    class NewsModel extends AppModel {
        public function getNews($id){
            return $this->getDto('news', 'id', $id);
        }

        public function save($id = null){
            $news = $this->getNews($id);
            $cover = $news->get('cover');
            $small_cover = $news->get('small_cover');
            $news = $this->makeDto($news, $id);
            $errors = $news[1];
            $news = $news[0];

            $news->set('cover', $cover);
            $news->set('small_cover', $small_cover);

            if($_FILES['cover']['name'] != ''){
                $cover = new File(_NEWS_COVERS_DIR, 'cover');
                $dir = $cover->finish();
                if(!$news->set('picture', $dir)){
                    Viewer::flash($news->FieldsErrors['cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }

            if($_FILES['small_cover']['name'] != ''){
                $cover = new File(_NEWS_COVERS_DIR, 'cover');
                $dir = $cover->finish('small');
                if(!$news->set('small_cover', $dir)){
                    Viewer::flash($news->FieldsErrors['small_cover'], 'e');
                    unlink($dir);
                    return false;
                }
            }

            if(!empty($errors)){
                Viewer::flash($errors, 'e');
                return false;
            }else{
                $ret = is_null($id) ? $this->insert('news', $news) : $this->update('news', $news, array('id' => $id));
                if($ret) {
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }

        public function countVisit($news){
            $news->set('views', $news->get('views') + 1);
            return $this->update('news', $news, array('id' => $news->get('id')));
        }
    }