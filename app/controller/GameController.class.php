<?php
    
    
    class GameController extends AppController {
        
        public function add(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->request()){
                if($this->model->save()){
                    $id = $this->model->lastInserted('game');
                    return $this->view($id);
                }else{
                    unset($_POST);
                    unset($_FILES);
                    return $this->add();
                }
            }
            $genres = $this->model->search('genre');
            $this->viewer->set('genres', $genres);
            $this->viewer->addJs('assets/js/gameForm.js');
            return $this->viewer->show('add', 'Cadastrar jogo');
        }
        
        public function view($id = null, $modal = false, $platform = false){
            if(!is_null($id)){
                if(!$this->model->exists('game', 'id', $id)){
                    Viewer::flash(_EXISTS_ERROR, 'e');
                    if($this->user->get('role') == 'member')
                        return $this->library();
                    return $this->view();
                }
                $game = $this->model->getGame($id);
                $this->viewer->set('game', $game);


                //gallery
                $this->viewer->addJs('/app/plugins/gallery/dist/jquery.magnific-popup.min.js');
                $this->viewer->addJs('assets/js/gallery.js');
                $this->viewer->addCss('/app/plugins/gallery/dist/magnific-popup.css');

                //images
                $cond = array(
                    'id_game' => $id,
                    'conscond1' => 'and',
                    'type' => 0
                );
                $images = $this->model->search('game_media', '*', $cond);
                $this->viewer->set('images', $images);

                //video
                $cond = array(
                    'id_game' => $id,
                    'conscond1' => 'and',
                    'type' => 1
                );
                $videos = $this->model->search('game_media', '*', $cond);
                $this->viewer->set('videos', $videos);

                $this->viewer->set('modal', $modal);

                if($modal){
                    $cover = $platform == 'all' ? $game->firstCover() : $game->get($platform.'_cover');
                }else{
                    $cover = $game->firstCover();
                }
                $this->viewer->set('cover', $cover);

                return $this->viewer->show('view_one', $game->get('name'), false, $modal);
            }
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            $games = $this->model->search('game');
            $this->viewer->set('games', $games);

            return $this->viewer->show('view', 'Jogos');
        }
        
        public function edit($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('game', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->request()){
                if($this->model->save($id, 'admin')){
                    return $this->view($id);
                }else{
                    unset($_POST);
                    unset($_FILES);
                    return $this->edit($id);
                }
            }
            $game = $this->model->getGame($id);
            $this->viewer->set('game', $game);

            $this->viewer->addJs('assets/js/gameForm.js');
            $genres = $this->model->search('genre');
            $this->viewer->set('genres', $genres);
            return $this->viewer->show('edit', 'Editar '.$game->get('name'));
        }

        public function delete($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('game', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->model->delete('game', array('id' => $id))){
                Viewer::flash(_DELETE_SUCCESS, 's');
                return $this->view();
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
                return $this->view($id);
            }
        }

        public function deleteMedia($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('game_media', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            $media = $this->model->getMedia($id);
            if($this->model->delete('game_media', array('id' => $id))){
                if($media->get('type') == 0){
                    unlink($media->get('source'));
                    unlink($media->get('thumb'));
                }
                Viewer::flash(_DELETE_SUCCESS, 's');
                return $this->view($media->get('id_game'));
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
                return $this->view($media->get('id_game'));
            }
        }

        public function addMedia($id_game){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('game', 'id', $id_game)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->request()){
                if($this->model->saveMedia($id_game)){
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return $this->view($id_game);
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    unset($_POST);
                    unset($_FILES);
                    return $this->addMedia($id_game);
                }
            }
            $game = $this->model->getGame($id_game);
            $this->viewer->set('game', $game);
            return $this->viewer->show('addMedia', 'Cadastrar Mídia em '.$game->get('name'));
        }

        public function library(){
            
            $games = $this->model->search('game', '*', array('available' => 1), 'name');
            $this->viewer->set('games', $games);

            // to favorites list
            $id_user = $this->user->get('id');
            $favorites = $this->model->search('favorite', '*', ['id_user' => $id_user]);
            $alreadyHave = array();
            foreach($favorites as $favorite){
                $alreadyHave[] = $favorite->get('id_game');
            }

            $inCookie = isset($_COOKIE['favorites']) ? count(unserialize($_COOKIE['favorites'])) : 0;

            if(count($favorites) > 0 && $inCookie > 0){
                $mustSave = true;
            }else{
                $mustSave = false;
            }

            $calcLastModify = FavoriteModel::calcLastModify($favorites);
            foreach($calcLastModify as $key => $value){
                $this->viewer->set($key, $value);
            }

            $genres = $this->model->search('genre', '*', false, 'name');
            $this->viewer->set('genres', $genres);

            $this->viewer->set('mustSave', $mustSave);
            $this->viewer->set('alreadyHave', $alreadyHave);
            $this->viewer->set('favorites', $favorites);
            $this->viewer->set('hasFavorites', count($favorites) > 0);
            $this->viewer->set('inCookie', $inCookie);

            $this->viewer->addJs('assets/js/library.js');
            return $this->viewer->show('library', 'Biblioteca de Jogos');
        }

        public function toggle($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('game', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            $game = $this->model->getGame($id);
            $game->set('available', !$game->get('available'));
            if($this->model->update('game', $game, array('id' => $id))){
                Viewer::flash(_INSERT_SUCCESS, 's');
                return $this->view();
            }else{
                Viewer::flash(_INSERT_ERROR, 'e');
                return $this->view();
            }
        }

        public function libraryModal($id, $platform){
            if(!$this->model->exists('game', 'id', $id)){
                echo '<h4 class="text-center">Jogo não encontrado em nossa base de dados!</h4>';
                return;
            }

            return $this->view($id, true, $platform);
        }

        public function indexGames(){
	        $games = $this->model->search('game', '*', array('available' => 1), 'name');
	        $finalGames = array(
	        	array(),
		        array(),
		        array()
	        );
	        $page = array(0,0,0); // ps4, xone, switch
	        $i = array(0,0,0); // ps4, xone, switch
	        foreach($games as $game){
				if($game->get('ps4'))
					$which = 0;
				elseif($game->get('xone'))
					$which = 1;
				else
					$which = 2;
		        $i[$which]++;

	        	if(!isset($finalGames[$which][$page[$which]]))
	        		$finalGames[$which][$page[$which]] = array();

	        	$finalGames[$which][$page[$which]][] = $game;

	        	if($i[$which] == 12){
	        		$i[$which] = 0;
	        		$page[$which]++;
		        }
	        }
	        return array($finalGames, $page);
        }

        public function copy($id){
        	if(!$this->model->exists('game', 'id', $id)){
        		echo json_encode(array('error' => 'Jogo não encontrado!'));
        		return;
	        }

	        $game = $this->model->getGame($id);
        	echo json_encode(array(
        		'name' => $game->get('name'),
		        'platform' => $game->platforms(),
		        'description' => $game->get('description'),
		        'cover' => $game->firstCover(),
	        ));
        	return;
        }

    }
