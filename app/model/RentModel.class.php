<?php
    
    class RentModel extends AppModel {
        public function getRent($id){
            return $this->getDto('rent', 'id', $id);
        }

	    public function getUserRents($id_user){
			$sql = '
				select *
				from rent
				where id_user = '.$id_user.' and
					  (status = "delivered" or status = "delayed") 
			';
			$rents = $this->query($sql);
			return $this->query2dto($rents, 'rent');
	    }
        
        public function save($id = null){
            $rent = $this->getRent($id);
            $ret  = $this->makeDto($rent, $id);
            $rent = $ret[0];
            $errors = $ret[1];

            if($errors != ''){
                Viewer::flash($errors, 'e');
                
                return false;
            }else{
                $this->initTransaction();
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('rent', $rent) : $this->update('rent', $rent, array('id' => $id));
                if($ret){                    
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    $this->endTransaction();
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    $this->cancelTransaction();
                    return false;
                }
            }
        }

        public function view($request){
        	if($request && isset($_POST['_filter_period'])){
                $period = explode('/', $_POST['_filter_period']);
                $start = $period[0];
                $end = $period[1];
        	}else{
                $start = date('Y-m-01');
                $end = date('Y-m-t');
        	}
            $sql = 'select * from rent where start between "'.$start.'" and "'.$end.'"';
            $rents = $this->query($sql);
            $rents = $this->query2dto($rents, 'rent');
            return array(
                '_filter_period' => date('d/m/Y', strtotime($start)).' - '.date('d/m/Y', strtotime($end)),
                'rents' => $rents
            );
        }

        public function lastWeekRents(){
            $startDate = date('Y-m-d', strtotime('-7 days'));
            $finalDate = date('Y-m-d');
            $sql = 'select * from rent where start between "'.$startDate.'" and "'.$finalDate.'"';
            $rents = $this->query($sql);
            return $this->query2dto($rents, 'rent');
        }

        public function usersDtos(){
            $users = $this->search('user');
            $final = array();
            foreach($users as $user){
                $final[$user->get('id')] = $user;
            }
            return $final;
        }

        public function plan(){
            $availableForRent = $this->availableForRent();
            $games = $availableForRent[0];
            $gamesDto = $availableForRent[1];

            $favorites = $this->favorites();

            $ranking = $this->prepareRanking($games, $favorites);
	        $ranking = $this->calculateRanking($ranking, $favorites, $games);
	        $ranking = $this->removeSurplus($ranking, $games);

            $dividedRanking = $this->divideRanking($ranking);
            $dividedRanking = $this->dividedRankingDto($dividedRanking, $games, $favorites);
            $dividedRankingStr = $this->dividedRankingString($dividedRanking);
            return array(
                'dividedRanking' => $dividedRanking,
                'dividedRankingStr' => $dividedRankingStr,
                'lastWeekRents' => count($this->lastWeekRents()),
                'usersDtos' => $this->usersDtos(),
                'allRents' => count($this->search('rent')),
                'delayed' => count($this->search('rent', '*', array('status' => 'delayed'))),
                'requests' => $this->countRequests(),
            );
        }

        public function countRequests(){
            $sql = 'select count(*) as qtt from rent where status NOT LIKE "%complete%" AND status != "delivered" and status != "waiting_post" and status != "transit" order by start';
            $count = $this->query($sql)[0]['qtt'];
            return $count;
        }

        /**
         * Creates an array of the available games for rent with
         * respective platform
         *
         * @example array(array(id_game, platform, qtt), array(id_game, platform, qtt))
         * @return Array - array(available, auxDtosArray)
        **/
        public function availableForRent(){
            $sql = 'select g.*, f.platform from favorite f
                    join game g on g.id = f.id_game
                    where g.available = 1
                    group by f.id_game, f.platform
                    order by f.position';
            $games = $this->query($sql);

            $available = array();
            foreach($games as $key => $game){
                $platform = $game['platform'];
                if($game[$platform.'_qtt'] == 0){
                    unset($games[$key]);
                    continue;
                }
                $available[] = array($game['id'], $platform, $game[$platform.'_qtt']);
            }

            $dtos = $this->dtosArray($games);
            return array($available, $dtos);
        }

        /**
         * Transforms a query array into dtos arrays with the primary key as index
         * of the values
         * 
         * @param Array $games - query array
         * @return Array - dtos
        **/
        public function dtosArray($games){
            $dtos = array();
            foreach($games as $game){
                $dto = $this->getDto('game', 'id', $game['id'], $game);
                $dtos[$game['id']] = $dto;
            }
            return $dtos;
        }

        /**
         * Creates an array of members favorite games with id_user as index
         *
         * @return Array - users favorites
        **/
        public function favorites(){
            $sql = 'select f.* from favorite f join signature s on s.id_user = f.id_user where s.available_rent > 0 and (s.status = "Active" or s.status = "Change_requested")';
            $query = $this->query($sql);
            $dtos = $this->query2dto($query, 'favorite');

            $favorites = array();
            foreach($dtos as $favorite){
                if(!isset($favorites[$favorite->get('id_user')]))
                    $favorites[$favorite->get('id_user')] = array();
                $favorites[$favorite->get('id_user')][$favorite->get('id')] = $favorite;
            }

            return $favorites;
        }

        /**
         * Makes the game ranking of favorites, based on
         * stock, favorite position and favorite creation date.
         * The indexes MUST be the same as the $this->availableForRent()[0]

         * @param Array $games - available games for rent
         * @param Array $favorites - members favorites
         * @return Array - prepared ranking array to calculate the primary games for the members
        **/
        public function prepareRanking($games, $favoritesArray){
            $ranking = array();
            foreach($games as $game){
                $helper = array();
                foreach($favoritesArray as $id_user => $favorites){
                    foreach($favorites as $id => $favorite){
                        if($favorite->get('id_game') == $game[0] && $favorite->get('platform') == $game[1]){
                            $helper[strtotime($favorite->get('created'))] = $id_user;
                        }
                    }
                }
                ksort($helper);
                $ranking[] = $helper;
            }
            return $ranking;
        }

        /**
         * Receives the array provided by $this->prepareRanking() and calculates the
         * games that the members should receive, based on their favorites.
         * 
         * @param Array $ranking - ranking games
         * @param Array $favorites - users favorites
         * @param Array $games - array of available games
         * @param Array $count - amount of members that aren't in first position of any game ranking. Default: 0
         * @param Array $signatures - signatures of the available members for rent. Default: null
         * @return Array - ranking with the most problabe members that should receive the games
        **/
        public function calculateRanking($ranking, $favorites, $games, $count = 0, $signatures = null){
            if(is_null($signatures)){
                $signatures = $this->query('select s.* from signature s inner join favorite f on f.id_user = s.id_user where available_rent > 0 and (s.status = "Active" or s.status = "Change_requested")');
                $signatures = $this->query2dto($signatures, 'signature');
            }

            $notFirst = $this->notFirst($ranking, $signatures, $games);
            $newCount = count($notFirst);
            if($newCount != $count){
	            $ranking = $this->takeoffDuplicates($ranking, $signatures, $favorites, $games);
	            return $this->calculateRanking($ranking, $favorites, $games, $newCount, $signatures);
            }else{
                return $ranking;
            }
        }

        /**
         * Searches for any members that aren't in first position of any game rank
         * 
         * @param Array $ranking - ranking array
         * @param Array $signatures - available members
         * @param Array $games - available games
         * @return Array - members ids that aren't in first position of any game rank
        **/
        public function notFirst($ranking, $signatures, $games){
            $notFirst = array();
            foreach($signatures as $signature){
                $haveFirst = false;
                $id_user = $signature->get('id_user');
                foreach($ranking as $i => $users){
                    for($j = 0; $j < $games[$i][2]; $j++){
                        if(isset(array_keys($users)[$j])){
                            if($users[array_keys($users)[$j]] == $id_user)
                                $haveFirst = true;
                        }
                    }
                }
                if(!$haveFirst)
                    $notFirst[] = $id_user;
            }
            return $notFirst;
        }

        /**
         * Removes any duplicates first positions rankings that must be solved
         * 
         * @param Array $ranking - ranking array
         * @param Array $signatures - signatures dtos
         * @param Array $favorites - members favorites
         * @param Array $games - games array
        **/
        public function takeoffDuplicates($ranking, $signatures, $favorites, $games){
            $exceptions = $this->twoAvailable($signatures);
	        $twoAvailable = $this->makeTwoAvailableArray($signatures, $ranking, $games);
	        if($this->hasTwoAvailableDuplicates($twoAvailable)){
                return $this->removeTwoAvailable($ranking, $twoAvailable);
            }
            for($i = 0; $i < count($ranking); $i++){ // passa jogo a jogo do ranking
                $users = $ranking[$i]; // pega os usuários do ranking de um jogo
                for($k = 0; $k < $games[$i][2]; $k++){ // se tem mais de 1 em estoque, passa "k" vezes verificando
                    if(isset(array_keys($users)[$k])){
                        $firstUser = $users[array_keys($users)[$k]]; // pega um usuário que está para receber o jogo
                        if(!in_array($firstUser, $exceptions) && count($users) > 1){ // vê se ele não tá dentre os casos omissos (recebe mais de 1 jogo) ou se só tem ele para receber o jogo na lista
                            for($j = 0; $j < count($ranking); $j++){ // começa um loop pra ver se ele está para receber algum outro jogo
                                $compareUsers = $ranking[$j]; // pega o ranking do outro jogo
                                for($l = 0; $l < $games[$j][2]; $l++){ // passa "l" vezes verificando, l = qtd em estoque
                                    if(isset(array_keys($compareUsers)[$l])){
                                        $firstCompare = $compareUsers[array_keys($compareUsers)[$l]]; // pega um usuário que está para receber o jogo
                                        if($j != $i && $firstCompare == $firstUser){ // se o mesmo usuário está para receber dois jogos
                                            $which = $this->whichRemove($j, $i, $firstUser, $games, $favorites); // calcula qual deve ser removido, com base na preferêcia dele
                                            $ranking[$which] = $this->removeFromRanking($ranking[$which], $firstUser); // remove do ranking do jogo que não precisará ser enviado para ele
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $ranking;
        }

        /**
         * Tells the algorythm which ranking must be removed from the first position,
         * based on the member favorite position.
         * 
         * @param int $insideLoop - index of the "j" on $this->takeoffDuplicates()
         * @param int $outsideLoop - index of the "i" on $this->takeoffDuplicates()
         * @param int $id_user - member that are having the favorites position compared
         * @param Array $games - games array
         * @param Array $favorites - favorites array
         * @return int - i or j index
        **/
        public function whichRemove($insideLoop, $outsideLoop, $id_user, $games, $favorites){
            $insideGame = $games[$insideLoop];
            $outsideGame = $games[$outsideLoop];
            $favorites = $favorites[$id_user];
            foreach($favorites as $favorite){
                if($favorite->get('id_game') == $insideGame[0] && $favorite->get('platform') == $insideGame[1]){
                    $insideFavorite = $favorite;
                }
                if($favorite->get('id_game') == $outsideGame[0] && $favorite->get('platform') == $outsideGame[1]){
                    $outsideFavorite = $favorite;
                }
            }
            if($insideFavorite->get('position') < $outsideFavorite->get('position')) // "j" tem maior preferencia
                return $outsideLoop; // retorna a posição "i", para remover o usuário daquela lista
            else // "i" tem maior preferência
                return $insideLoop; // retorna a posição "j", para remover o usuário daquela lista
        }

        /**
         * Searches for members that have the possibility to rent more than one game
         * 
         * @param Array $signatures - signatures array
         * @return Array
        **/
        public function twoAvailable($signatures){
            $twoAvailable = array();
            foreach($signatures as $signature){
                if($signature->get('available_rent') > 1){
                    $twoAvailable[] = $signature->get('id_user');
                }
            }
            return $twoAvailable;
        }

        /**
         *   Seriam arrays com uma estrutura assim:
         *   array(
         *      'id_user' => array(0,2,4),
         *      'id_user' => array(1,3,5)
         *   )
         *
         *   O 0,2,4 e 1,3,5 seriam os índices do array produzido em $this->availableForRent()[0],
         *   pois é neste array que se encontram os jogos para serem adicionados, e estão divididos
         *   pelas plataformas deles. array(array(id_jogo, plataforma), array(id_jogo, plataforma))
         *
         *   O algoritmo separa os que podem e escolheram receber 2 jogos do que podem/escolheram 1,
         *   joga num array como estruturado lá em cima. A partir disso, começa a realizar o loop pelo array
         *   criado em $this->prepareRanking() para verificar em quais jogos ele possui a prioridade (é 1º) e
         *   grava o índice do mesmo.
        **/
        public function makeTwoAvailableArray($signatures, $ranking, $games){
            $twoAvailable = array();

            foreach($signatures as $signature){

                // dois disponíveis para receber
                if($signature->get('available_rent') == 2){
                    $helper = array();
                    $id_user = $signature->get('id_user');
                    foreach($ranking as $i => $users){
                        // pega o primeiro no ranking
                        for($j = 0; $j < $games[$i][2]; $j++){
                            if($users[array_keys($users)[$j]] == $id_user)
                                $helper[] = $i;
                        }
                    }

                    // primeiro no ranking de 2+, 1 ou nenhum
                    if(count($helper) >= 2){
                         $twoAvailable[$id_user] = $helper;
                    }
                }
            }
            
            return $twoAvailable;
        }

        /**
         * Checks if still have a member which can rent more than one game
         * with games with possibility of ranking removal
         * 
         * @param Array $twoAvailable - array of two available members
         * @return boolean
        **/
        public function hasTwoAvailableDuplicates($twoAvailable){
            foreach($twoAvailable as $id_user => $firsts){
                if(count($firsts) > 2)
                    return true;
            }
        }

        /**
         * Loops through two available array and removes the surplus of first position (max 2).
         * 
         * @param Array $ranking
         * @param Array $twoAvailable
         * @return Array - ranking array
        **/
        public function removeTwoAvailable($ranking, $twoAvailable){
            foreach($twoAvailable as $id_user => $firsts){
                if(count($firsts) > 2){
                    for($i = 2; $i < count($firsts); $i++){
                        $ranking[$firsts[$i]] = $this->removeFromRanking($ranking[$firsts[$i]], $id_user);
                    }
                }
            }

            return $ranking;
        }

        /**
         * Removes one user from a game rank
         * 
         * @param Array $gameRanking - ranking of members position of a game
         * @param int $id_user - member id
         * @return Array - ranking array
        **/
        public function removeFromRanking($gameRanking, $id_user){
            $created = array_search($id_user, $gameRanking);
            unset($gameRanking[$created]);
            return $gameRanking;
        }

        public function removeSurplus($ranking, $games){
            foreach($ranking as $i => &$users){
                if(count($users) > $games[$i][2]){
                    for($j = $games[$i][2]; $j < count($users); $j++){
                        unset($users[array_keys($users)[$j]]);
                    }
                    
                }
            }
            return $ranking;
        }

        public function divideRanking($ranking){
            $final = array();
            foreach($ranking as $i => $users){
                foreach($users as $timestamp => $id_user){
                    if(!isset($final[$id_user]))
                        $final[$id_user] = array();
                    $final[$id_user][] = $i;
                }
            }
            return $final;
        }

        public function dividedRankingDto($dividedRanking, $games, $favorites){
            $final = array();
            foreach($dividedRanking as $id_user => $gamesIndexes){
                $helper = array();
                foreach($gamesIndexes as $i){
                    $id_game = $games[$i][0];
                    $platform = $games[$i][1];
                    foreach($favorites[$id_user] as $favorite){
                        if($favorite->get('id_game') == $id_game && $favorite->get('platform') == $platform){
                            $helper[] = $favorite;
                        }
                    }
                }
                $final[$id_user] = $helper;
            }
            return $final;
        }

        public function dividedRankingString($dividedRanking){
            $final = array();
            foreach($dividedRanking as $id_user => $favorites){
                $final[$id_user] = '';
                foreach($favorites as $favorite){
                    $final[$id_user] .= $favorite->get('id').',';
                }
                $final[$id_user] = trim($final[$id_user], ',');
            }
            return $final;
        }

        public function favoritesIdsToDtos($favoritesIds){
            $favoritesIds = explode(',', $favoritesIds);
            $favorites = array();
            foreach($favoritesIds as $id){
                $favorite = $this->getDto('favorite', 'id', $id);
                $favorites[] = $favorite;
            }
            return $favorites;
        }

        public function send($id_user){
            $_POST['id_user'] = $id_user;
            $return = array(
                'return' => 0,
                'type' => 'error',
            );

            $rent = new Rent();
            $ret  = $this->makeDto($rent);
            $rent->set('status', 'transit');
            $rent->set('last_modify', date('Y-m-d H:i:s'));

            $rent = $ret[0];
            $errors = $ret[1];

            if($errors != ''){
                $return['text'] = trim(str_replace('<br>', '. ', $errors), '. ');
            }else{
                $this->initTransaction();
                if($this->insert('rent', $rent)){
                    $favoriteModel = new FavoriteModel();
                    if(!$favoriteModel->removeByRent($rent)){
                        $this->cancelTransaction();
                        $return['text'] = 'Não foi possível remover o favorito da lista do membro.';
                    }else{
                        $signatureModel = new SignatureModel();
                        if(!$signatureModel->updateAvailableRent($rent)){
                            $this->cancelTransaction();
                            $return['text'] = 'Não foi possível atualizar a assinatura do membro ou o mesmo não pode receber jogos atualmente.';
                        }else{
                            $gameModel = new GameModel();
                            if(!$gameModel->updateQttByRent($rent, 'remove')){
                                $this->cancelTransaction();
                                $return['text'] = 'Não foi possível atualizar o estoque ou jogo indisponível. Tente novamente.';
                            }else{
                                $this->endTransaction();
                                $return['return'] = 1;
                                $return['text'] = _INSERT_SUCCESS;
                                $return['type'] = 'success';
                            }
                        }
                    }
                }else{
                    $this->cancelTransaction();
                    $return['text'] = _INSERT_ERROR;
                }

            }
            echo json_encode($return);
            return $return['return'];
        }

        public function changeStatus($id){
            $rent = $this->getRent($id);
	        $oldStatus = $rent->get('status');
	        $return = array(
		        'return' => 0,
		        'type' => 'error',
	        );
            if(!$rent->set('status', $_POST['status'])){
                $return['text'] = 'Status inválido.';
            }
            if(!empty($_POST['ps_adm'])){
                $rent->set('ps_adm', $_POST['ps_adm']);
            }
            if(!empty($_POST['ps_carrier'])){
                $rent->set('ps_carrier', $_POST['ps_carrier']);
            }
            $rent->set('last_modify', date('Y-m-d H:i:s'));
            $this->initTransaction();
            if(!$this->update('rent', $rent, array('id' => $id)) && !isset($return['text'])){
                $this->cancelTransaction();
                $return['text'] = _INSERT_ERROR;
            }else{
                $action = $this->statusChangeAction($oldStatus, $rent->get('status'), $rent->get('id'));
                if($action != 'none'){
                    if($action == 'renew'){
                        if(!$this->finishRenew($rent, true)){
                            $this->cancelTransaction();
                            $return['text'] = 'Não foi possível registrar a renovação.';
                        }
                    }else{
                        $signatureModel = new SignatureModel();
                        if($action == 'change_completed'){
                            $action = 'add';
                            if(!$signatureModel->completeChange($rent)){
                                $this->cancelTransaction();
                                $return['text'] = 'Não foi possível registrar a consolidação da troca.';
                            }
                        }else{
                            if(!$signatureModel->updateAvailableRent($rent, $action)){
                                $this->cancelTransaction();
                                $return['text'] = 'Não foi possível atualizar a assinatura do membro.';
                            }
                        }
                        $gameModel = new GameModel();
                        if(!$gameModel->updateQttByRent($rent, $action)){
                            $this->cancelTransaction();
                            $return['text'] = 'Não foi possível alterar o estoque.';
                        }
                    }
                }
                if(!isset($return['text'])){
                    $this->endTransaction();
                    $return['return'] = 1;
                    $return['type'] = 'success';
                    $return['text'] = _INSERT_SUCCESS;
                }else{
                    $this->cancelTransaction();
                }
            }
            echo json_encode($return);
            return $return['return'];
        }

        public function changeStatusMass($ids){
			$this->initTransaction();
	        $return = array(
		        'return' => 0,
		        'type' => 'error',
	        );
			foreach($ids as $id){
				$rent = $this->getRent($id);
				$oldStatus = $rent->get('status');
				if(!$rent->set('status', $_POST['status'])){
					$return['text'] = 'Status inválido.';
				}
				$rent->set('last_modify', date('Y-m-d H:i:s'));
				if(!$this->update('rent', $rent, array('id' => $id)) && !isset($return['text'])){
					$return['text'] = _INSERT_ERROR;
				}else{
					$action = $this->statusChangeAction($oldStatus, $rent->get('status'), $rent->get('id'));
					if($action != 'none') {
						if ($action == 'renew') {
							if (!$this->finishRenew($rent, true)) {
								$return['text'] = 'Não foi possível registrar a renovação.';
							}
						} else {
							$signatureModel = new SignatureModel();
							if ($action == 'change_completed') {
								$action = 'add';
								if (!$signatureModel->completeChange($rent)) {
									$return['text'] = 'Não foi possível registrar a consolidação da troca.';
								}
							} else {
								if (!$signatureModel->updateAvailableRent($rent, $action)) {
									$return['text'] = 'Não foi possível atualizar a assinatura do membro.';
								}
							}
							$gameModel = new GameModel();
							if (!$gameModel->updateQttByRent($rent, $action)) {
								$return['text'] = 'Não foi possível alterar o estoque.';
							}
						}
					}
				}
			}
	        if(!isset($return['text'])){
		        $this->endTransaction();
		        $return['return'] = 1;
		        $return['type'] = 'success';
		        $return['text'] = _INSERT_SUCCESS;
	        }else{
		        $this->cancelTransaction();
	        }
	        echo json_encode($return);
	        return $return['return'];
        }

        public function statusChangeAction($old, $new, $id){
            $sql = 'select * from change_request where id_rent = '.$id.' or id_rent2 = '.$id;
            $changeRequests = $this->query($sql);
            $changeRequests = $this->query2dto($changeRequests, 'change_request');
            if(count($changeRequests) > 0){
                if($new == 'complete' || $new == 'complete_problem'){
                    return 'change_completed';
                }
            }
            if($old == 'renew_requested' && $new == 'delivered'){
                return 'renew';
            }
            if(($new == 'complete' && $old != 'complete_problem') || 
               ($new == 'complete_problem' && $old != 'complete') ){
                return 'add';
            }
            /*if(($old == 'complete' && $new != 'complete_problem') ||
               ($old == 'complete_problem' && $new != 'complete') ){
                return 'noen';
            }*/
            return 'none';
        }

        public function requestChange($rent, $signature){
            $this->initTransaction();
            $change = new Change_request();

            if(!$signature->get('can_change'))
            	return false;

            switch($signature->get('level')){
	            case 2:
	            	if($signature->get('change_count') != 0)
	            		return false;
	            	break;
	            case 3:
	            	if($signature->get('change_count') > 1)
	            		return false;
	            	break;
	            default:
	            	return false;
            }


            if($rent == 'all'){
                $id_user = unserialize($_SESSION['user'])->get('id');
                $sql = 'select * from rent where id_user = '.$id_user.' and status = "delivered" or status = "delayed"';
                $rents = $this->query($sql);
                $rents = $this->query2dto($rents, 'rent');
                if(count($rents) > 0){
                    if(count($rents) == 2){
                        $change->set('id_rent', $rents[0]->get('id'));
                        $change->set('id_rent2', $rents[1]->get('id'));
                    }else{
                        $change->set('id_rent', $rents[0]->get('id'));
                    }
                }else{
                    return false;
                }
            }else{
                $change->set('id_rent', $rent->get('id'));
            }

            $change->set('date_request', date('Y-m-d'));
            if(!$this->insert('change_request', $change)){
                $this->cancelTransaction();
                return false;
            }

            if($rent == 'all'){
                foreach ($rents as $rent) {
                    $name = $rent->get('id_user', true)->get('name');
                    $id_user = $rent->get('id_user');
                    $rent->set('status', 'change_requested');
                    $rent->set('last_modify', date('Y-m-d H:i:s'));
                    if(!$this->update('rent', $rent, array('id' => $rent->get('id')))){
                        $this->cancelTransaction();
                        return false;
                    }
                }
            }else{
                $name = $rent->get('id_user', true)->get('name');
                $id_user = $rent->get('id_user');
                $rent->set('status', 'change_requested');
                $rent->set('last_modify', date('Y-m-d H:i:s'));
                if(!$this->update('rent', $rent, array('id' => $rent->get('id')))){
                    $this->cancelTransaction();
                    return false;
                }
            }
            $notificationModel = new NotificationModel();
            $notificationModel->memberRequest($name, 
                                              'troca', 
                                              $id_user);
            $this->endTransaction();
            return true;


        }

        public function requestGiveback($rent){
            $rent->set('status', 'giveback_requested');
            $rent->set('last_modify', date('Y-m-d H:i:s'));
            $notificationModel = new NotificationModel();
            $notificationModel->memberRequest($rent->get('id_user', true)->get('name'), 
                                              'devolução', 
                                              $rent->get('id_user'));
            return $this->update('rent', $rent, array('id' => $rent->get('id')));
        }

        public function confirmPost($id_rent){
            $rent = $this->getRent($id_rent);
            if(!isset($_POST['carrier_code']))
                $_POST['carrier_code'] = '';

            $ps_carrier = $rent->get('ps_carrier');

            $carrier_code = $_POST['carrier_code'];
            if(empty($carrier_code))
                $carrier_code = 'Código de rastreio não informado pelo membro.';
            else
                $carrier_code = 'Código de rastreio informado pelo membro: '.$carrier_code;

            $rent->set('ps_carrier', $ps_carrier.PHP_EOL.$carrier_code);
            $rent->set('status', 'transit_back');
            $rent->set('last_modify', date('Y-m-d H:i:s'));

            if($this->update('rent', $rent, array('id' => $id_rent))){
                $notificationModel = new NotificationModel();
                $notificationModel->postConfirmed($rent->get('id_user', true)->get('name'),
                                                  $rent->get('id_user'));
                return array(
                    'return' => 1,
                    'text' => _INSERT_SUCCESS,
                );
            }else{
                return array(
                    'return' => 0,
                    'text' => _INSERT_ERROR,
                );
            }
        }

        public function requestRenew(Rent $rent){
            $this->initTransaction();
            $rent->set('status', 'renew_requested');
            $rent->set('last_modify', date('Y-m-d H:i:s'));
            if (!$this->update('rent', $rent, array('id' => $rent->get('id')))) {
	            $this->cancelTransaction();
	            return false;
            }
            $signature = $this->getDto('signature', 'id_user', $rent->get('id_user'));
            $signature->set('renew_requested', 1);
            if (!$this->update('signature', $signature, array('id_user' => $signature->get('id_user')))) {
	            $this->cancelTransaction();
	            return false;
            }
            $notificationModel = new NotificationModel();
            $notificationModel->memberRequest($rent->get('id_user', true)->get('name'),
	            'renovação',
	            $rent->get('id_user'));

            $this->endTransaction();
            return true;
        }

        public function canRenew($rent){
			$signature = $this->getDto('signature', 'id_user', $rent->get('id_user'));
			if($signature->get('status') != 'Active') {
				echo 'Sua assinatura não está em dia :(';
				return false;
			}
			if($signature->get('level') == 2){
				if($signature->get('renew_requested')){
					echo 'Você já solicitou a renovação de um jogo!';
					return false;
				}
				/*if($signature->get('change_count') > 0){
					echo 'Você já trocou ou renovou um jogo este mês.';
					return false;
				}*/
			}elseif($signature->get('level') == 3){
				/*if($signature->get('change_count') >= 3){
					echo 'Você já trocou ou renovou três jogos este mês.';
					return false;
				}*/
			}

	        /*if($rent->get('renew_qtt') >= 3) {
				echo 'Esta locação já foi renovada 3 vezes';
		        return false;
	        }*/

			$lastRentRenew = $this->search('renew', '*', array('id_rent' => $rent->get('id')), 'created DESC', false, 1);
			if(count($lastRentRenew) > 0) {
				$lastRentRenew = $lastRentRenew[0];
				$initPeriod = strtotime('-25 days');
				$created = strtotime($lastRentRenew->get('created'));
				if ($created > $initPeriod) {
					$next = date('d/m', strtotime('+25 days', $created));
					echo 'Renovações para esta locação apenas a partir de '.$next;
					return false;
				}
			}

			$end = strtotime('-5 days', strtotime($rent->get('end')));
			if(strtotime('today') <  $end){
				echo 'Renovações para esta locação apenas a partir de '.date('d/m', $end);
				return false;
			}
			
			return true;
        }

        public function finishRenew($rent, $finish = true){
            $this->initTransaction();
	        $signature = $this->getDto('signature', 'id_user', $rent->get('id_user'));
            if($finish){
                $rent->set('status', 'delivered');
                $end = $rent->get('end');
                $rent->set('start', $end);
                $rent->set('end', date('Y-m-d', strtotime('+30 days', strtotime($end))));
                $rent->set('renew_qtt', $rent->get('renew_qtt') + 1);
                $rent->set('last_modify', date('Y-m-d H:i:s'));

                $ps_adm = $rent->get('ps_adm');
                $ps_adm = $ps_adm.PHP_EOL.'Renovação em '.date('Y-m-d');
                $rent->set('ps_adm', $ps_adm);
                if(!$this->update('rent', $rent, array('id' => $rent->get('id')))){
                    $this->cancelTransaction();
                    return false;
                }

                $renew = new Renew();
                $renew->set('id_rent', $rent->get('id'));
                $renew->set('created', date('Y-m-d'));
                if(!$this->insert('renew', $renew)){
                    $this->cancelTransaction();
                    return false;
                }


	            $signatureModel = new SignatureModel();
	            if(!$signatureModel->updateCanChangeByRenew($signature)){
		            $this->cancelTransaction();
		            return false;
	            }

                $notificationModel = new NotificationModel();
                $notificationModel->renewRequest($rent->get('id_user'), true);
            }else{
                if(strtotime($rent->get('end')) > strtotime(date('Y-m-d'))){
                    $rent->set('status', 'delivered');
                }else{
                    $rent->set('status', 'delayed');
                }
                $rent->set('last_modify', date('Y-m-d H:i:s'));

                $ps_adm = $rent->get('ps_adm');
                $ps_adm = $ps_adm.PHP_EOL.'Renovação NEGADA em '.date('Y-m-d');
                $rent->set('ps_adm', $ps_adm);
                if(!$this->update('rent', $rent, array('id' => $rent->get('id')))){
                    $this->cancelTransaction();
                    return false;
                }

                $notificationModel = new NotificationModel();
                $notificationModel->renewRequest($rent->get('id_user'), true);
            }


            $signature->set('renew_requested', 0);
            if(!$this->update('signature', $signature, array('id_user' => $signature->get('id_user')))){
                $this->cancelTransaction();
                return false;
            }

            $this->endTransaction();
            return true;
        }

        public function delayedCron(){
            $rents = $this->search('rent');
            $notificationModel = new NotificationModel();
            foreach($rents as $rent){
                if(strtotime($rent->get('end')) < strtotime(date('Y-m-d H:i:s')) && $rent->get('status') != 'delayed'){
                    $rent->set('status', 'delayed');
                    $notificationModel->delayedRent($rent->get('id_user'));
                    $this->model->update('rent', $rent, array('id' => $rent->get('id')));
                }
            }
        }

        public function updateCanSum($rents){
        	foreach($rents as $rent){
				$rent->set('can_sum', 1);
				$this->update('rent', $rent, array('id' => $rent->get('id')));
	        }
        }

    }