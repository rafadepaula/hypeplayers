<div class="row">
	<div class="col-sm-12">
		<p>
			Para continuar, você deve confirmar os seus jogos favoritos. Lembre-se
			que alterações em sua lista de preferências poderão ser realizadas
			novamente <b>apenas após o prazo de 30 dias</b>, salvo alterações
			que envolvem apenas alteração de prioridades.
		</p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="table-responsive">
            <table class="table table-hover table-striped datatable">
                <thead>
                    <tr>
                    	<th>Prioridade</th>
                        <th>Jogo</th>
                        <th>Plataforma</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                {foreach $favorites as $favorite}
                    <tr id="favorite-{$favorite->get('position')}">
                        <td>
                            {$favorite->get('position') + 1}
                        </td>
                        <td>
                            {$favorite->get('id_game', true)->get('name')}
                        </td>
                        <td>
                            {$favorite->get('platform', true)}
                        </td>
                        <td class="td-actions">
                            <a >
                                <button onclick="removeSavechanges({$favorite->get('position')})"
                                		type="button" rel="tooltip" 
                                        class="btn btn-danger btn-simple remove-savechanges">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </a>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</div>