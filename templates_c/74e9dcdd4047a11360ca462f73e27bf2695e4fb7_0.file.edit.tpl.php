<?php
/* Smarty version 3.1.28, created on 2017-08-16 20:36:22
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Genre\edit.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_5994d6f6ba7645_85379255',
  'file_dependency' => 
  array (
    '74e9dcdd4047a11360ca462f73e27bf2695e4fb7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Genre\\edit.tpl',
      1 => 1502926470,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5994d6f6ba7645_85379255 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                </h4>
            </div>
            <div class="card-content">
                <form action="/genre/edit/<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
" method="post">
                    <div class="row">
                        <div class="col-sm-12 input-group">
                            <input type="text" name="name" class="form-control" placeholder="Nome do gênero" value="<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">Salvar</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div><?php }
}
