{if $signature->get('status') == 'Pending' || $signature->get('status') == 'Taxed'}
<div class="row help" data-help="account-warnings">
    <div class="col-lg-12 alert alert-danger text-center">
        Sua conta necessita atenção!
    </div>
    {if $signature->get('status') == 'Taxed'}
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Você recebeu uma multa</p>
                    <h4 class="title">Clique para acessar a página de multas e reativar seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">money_off</i>
                        Você recebeu uma multa. Acesse a página de multas para regularizar a situação.
                    </div>
                </div>
            </div>
        </div>
    {else}
    <div class="col-sm-8 col-sm-offset-2 col-xs-12">
        <a href="/signature/activate">
            <div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Pagamento pendente</p>
                    <h4 class="title">Clique para ativar o seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Você está com pagamento pendente! Ative sua conta efetivando o pagamento de sua assinatura.
                    </div>
                </div>
            </div>
        </a>
    </div>
    {/if}

</div>
{else}
<div class="row help" data-help="account-warnings">
    {if $signature->get('status') == 'Cancel_requested'}
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">cancel</i>
                </div>
                <div class="card-content">
                    <p class="category">Cancelamento solicitado</p>
                    <h4 class="title">Você solicitou o cancelamento de sua assinatura.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Em breve iremos processar o seu cancelamento. Você irá receber um e-mail
                        quando a ação estiver completa.
                    </div>
                </div>
            </div>
        </div>
    {/if}
    {if $signature->canceledStatus()}
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <a href="/signature/activate/0/1">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">cancel</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Assinatura inativa</p>
                        <h4 class="title">Não identificamos o pagamento de sua assinatura.</h4>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            Clique aqui para reativá-la.
                        </div>
                    </div>
                </div>
            </a>
        </div>
    {/if}
    {if $signature->get('status') == 'Preactivated'}
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">access_time</i>
                </div>
                <div class="card-content">
                    <p class="category">Processando pagamento</p>
                    <h4 class="title">Quase lá! O seu pagamento está sendo processado.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        A solicitação de assinatura foi registrada. Agora, nosso sistema está
                        se comunicando com a empresa responsável pelo processamento do cartão
                        de crédito para liberar o seu acesso!
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>
{/if}


<div class="row help" style="margin-top: 15px;" data-help="last-3-rents">
    {foreach $recents as $rent}
        {assign var="game" value=$rent->get('id_game', true)}
        <div class="col-sm-4">
            <div class="card card-profile">
                <div class="card-avatar game-cover library-game help"
                    data-id="{$game->get('id')}" data-name="{$game->get('name')}" 
                    data-platform="{$rent->get('platform')}"
                    data-help="view-game">
                    <img class="img" src="/{$game->firstCover()}"/>
                </div>
                <div class="card-content">
                    <h4 class="title">{$game->get('name')}</h4>
                    <p>
                        <b class="help" data-help="rent-status">
                            {$rent->get('status', true)}
                        </b>
                    </p>
                    <p class="category">
                        <a>
                            <button type="button" rel="tooltip" title="Visualizar locação"
                                    class="btn btn-default btn-simple view-one help"
                                    data-id-rent="{$rent->get('id')}"
                                    data-help="view-rent">
                                <i class="fa fa-eye"></i>
                            </button>
                        </a>
                        <a>
                            <button type="button" rel="tooltip" title="Enviar comentário"
                                    class="btn btn-default btn-simple send-comment"
                                    data-id-rent="{$rent->get('id')}"
                                    data-url="/member/home">
                                <i class="fa fa-edit"></i>
                            </button>
                        </a>
                        {if $rent->get('status') == 'delivered' || $rent->get('status') == 'delayed'}
                            <a>
                                <button type="button" rel="tooltip" title="{$changeText}"
                                        class="btn btn-success btn-simple give-back help"
                                        data-id-rent="{$rent->get('id')}"
                                        data-help="give-back">
                                    <i class="fa fa-exchange"></i>
                                </button>
                            </a>
                            {if $rent->get('renew_qtt') < 2}
                                <a>
                                    <button type="button" rel="tooltip" title="Solicitar renovação"
                                            class="btn btn-info btn-simple request-renew                                                   help"
                                            data-id-rent="{$rent->get('id')}"
                                            data-help="request-renew">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                </a>
                            {/if}
                        {/if}
                        {if false}
                            <a>
                                <button type="button" rel="tooltip" title="Confirmar recebimento"
                                        class="btn btn-success btn-simple confirm-deliver"
                                        data-id-rent="{$rent->get('id')}">
                                    <i class="fa fa-check"></i>
                                </button>
                            </a>
                        {/if}
                        {if $rent->get('status') == 'waiting_post'}
                            <a>
                                <button type="button" rel="tooltip" title="Confirmar postagem"
                                        class="btn btn-success btn-simple confirm-post
                                               help"
                                        data-id-rent="{$rent->get('id')}"
                                        data-help="confirm-post">
                                    <i class="fa fa-truck"></i>
                                </button>
                            </a>
                        {/if}
                    </p>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons">access_time</i> modificado em {$rent->get('last_modify', true)}
                    </div>
                </div>
            </div>
        </div>
    {/foreach}
</div>

<div class="row help favorites-history" data-help="favorites-history">
    <div class="col-xs-12">
        <div class="card card-nav-tabs">
            <div class="card-header" data-background-color="purple">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#favorites" data-toggle="tab">
                                    <i class="material-icons" style="width: 30px;">star_rate</i>
                                    Preferências
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="">
                                <a href="#history" data-toggle="tab">
                                    <i class="material-icons">history</i>
                                    Histórico
                                <div class="ripple-container"></div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="favorites">
                        <table class="table">
                            <tbody>
                                {foreach $favorites as $favorite}
                                    <tr>
                                        <td>{$favorite->get('position') + 1}</td>
                                        <td>
                                            {$favorite->get('id_game', true)->get('name')}
                                            ({$favorite->get('platform', true)})
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="history">
                        <table class="table datatable">
                            <tbody>
                                {foreach $history as $rent}
                                    <tr>
                                        <td>
                                            {$rent->get('id_game', true)->get('name')}
                                            ({$rent->get('platform', true)})
                                        </td>
                                        <td>{$rent->get('status', true)}</td>
                                        <td>{$rent->get('start', true)} - {$rent->get('end', true)}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{include file=$smarty.current_dir|cat:"/../Game/modal_library.tpl"}
{include file=$smarty.current_dir|cat:"/../Rent/view_modal.tpl"}