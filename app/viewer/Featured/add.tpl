<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/featured/add" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Copiar dados de...</label>
                            <select class="form-control select2" id="copy-from">
                                <option></option>
                                {foreach from=$games item=game key=id}
                                    <option value="{$id}">{$game}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="col-sm-3 form-group">
                            <label>Plataforma</label>
                            <input required type="text" name="platform" id="platform" class="form-control">
                        </div>
                        <div class="col-sm-3">
                            <div class="game-cover" id="cover"></div>
                            <input required type="hidden" name="cover" id="cover_input">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Descrição</label>
                            <textarea required name="description" id="description" class="form-control" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
