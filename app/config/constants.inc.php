<?php
    /**
     * Here ou can customize your constants
     *
     */
    define('_PROFILE_PICTURES_DIR', 'app/assets/img/pictures/');
    define('_COVERS_DIR', 'app/assets/img/covers/');
    define('_DEFAULT_PICTURE', 'assets/img/default-avatar.png');
    define('_DEFAULT_COVER', 'assets/img/default-cover.png');
    define('_LEVEL_1_VALUE', 39.90);
    define('_LEVEL_2_VALUE', 69.90);
    define('_LEVEL_3_VALUE', 99.90);
    define('_LEVEL_1_NAME', 'Plano Bronze');
    define('_LEVEL_2_NAME', 'Plano Prata');
    define('_LEVEL_3_NAME', 'Plano Ouro');
    define('_GAME_MEDIA_DIR', 'app/assets/img/game_media/');
    define('_GAME_MEDIA_THUMBS_DIR', 'app/assets/img/game_media/thumbs/');
    define('_BANNERS_DIR', 'app/assets/img/banners/');

    if(_IN_DEV_){
        define('_NAME_PAGSEGURO_', 'HypePlayers-TESTES');
    }else{
        define('_NAME_PAGSEGURO_', 'HypePlayers');
    }