<?php

	class FeedbackController extends AppController{
		public function send(){
            if($this->request()){
                return $this->model->send();
            }
            
            echo $this->viewer->show('send', 'Enviar feedback', true, true);
		}

		public function view(){
			if($this->user->get('role') != 'admin'){
				Viewer::flash(_PERMISSION_ERROR, 'e');
				return $this->siteIndex();
			}
            $this->model->sql('update feedback set read_by = '.$this->user->get('id').' where 
				read_by IS NULL');
            $feedbacks = $this->model->search('feedback', '*', false, 'created');

			$this->viewer->set('feedbacks', $feedbacks);

			return $this->viewer->show('view', 'Feedbacks de Membros');
		}

		public function delete($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('feedback', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            if($this->model->delete('feedback', array('id' => $id))){
                Viewer::flash(_DELETE_SUCCESS, 's');
            }else{
                Viewer::flash(_DELETE_ERROR, 'e');
            }
            return $this->view();
        }
	}