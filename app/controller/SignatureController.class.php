<?php

    class SignatureController extends AppController {
    	public function view(){
    		if($this->user->get('role') != 'member'){
    			return $this->siteIndex();
    		}
    		$signature = $this->model->getSignature($this->user->get('id'));

    		$member = $this->model->getDto('member', 'id_user', $this->user->get('id'));
    		$this->viewer->set('signature', $signature);
    		$this->viewer->set('member', $member);

    		if(!empty($member->get('picture')))
    			$this->viewer->set('picture', $member->get('picture'));
    		else
    			$this->viewer->set('picture', _DEFAULT_PICTURE);

    		$levels = array('Plano Bronze', 'Plano Prata', 'Plano Ouro');
    		$levelsNumbers = array(1, 2, 3);
    		unset($levels[$signature->get('level') - 1]);
    		unset($levelsNumbers[$signature->get('level') - 1]);
    		$this->viewer->set('levels', array_values($levels));
    		$this->viewer->set('levelsNumbers', array_values($levelsNumbers));

            $payments = $this->model->search('payment', '*', array('id_user' => $this->user->get('id')));
            $this->viewer->set('payments', $payments);

            $this->viewer->set('canCancel', $this->model->canCancel($this->user->get('id')));
            $this->viewer->set('canChange', $this->model->canChange($this->user->get('id')));


            $this->viewer->addJs('assets/js/memberViewMore.js');
    		return $this->viewer->show('view', 'Meu plano');
    	}

    	public function addPreapproval(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }
            if($this->request()){
                if($this->model->savePreapproval()){
                    $id = $this->model->lastInserted('pre_approval');
                    return $this->viewPreapproval($id);
                }else{
                    unset($_POST);
                    return $this->addPreapproval();
                }
            }
            return $this->viewer->show('add_preapproval', 'Cadastrar plano');
    	}

        public function editPreapproval($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }
            if(!$this->model->exists('pre_approval', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->viewPreapproval();
            }
            if($this->request()){
                if($this->model->savePreapproval($id)){
                    return $this->viewPreapproval($id);
                }else{
                    unset($_POST);
                    return $this->editPreapproval($id);
                }
            }
            $preApproval = $this->model->getPreapproval($id);
            $this->viewer->set('preApproval', $preApproval);
            return $this->viewer->show('edit_preapproval', 'Editar plano');
        }

        public function deletePreapproval($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }
            if(!$this->model->exists('pre_approval', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->viewPreapproval();
            }
            if(!$this->model->delete('pre_approval', array('id' => $id))){
                Viewer::flash(_DELETE_ERROR, 'e');
            }else{
                Viewer::flash(_DELETE_SUCCESS, 's');
            }
            return $this->viewPreapproval();
        }

        public function viewPreapproval($id = null){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }
            if(!is_null($id)){
                if(!$this->model->exists('pre_approval', 'id', $id)){
                    Viewer::flash(_EXISTS_ERROR, 'e');
                    return $this->viewPreapproval();
                }
                $signatures = $this->model->search('signature', '*', array('id_preapproval' => $id));
                $this->viewer->set('signatures', $signatures);

                $preApproval = $this->model->getPreapproval($id);
                $this->viewer->set('preApproval', $preApproval);

                return $this->viewer->show('view_one_preapproval', 'Visualizar plano');
            }
            $preApprovals = $this->model->search('pre_approval');
            $this->viewer->set('preApprovals', $preApprovals);
            return $this->viewer->show('view_preapproval', 'Visualizar planos');
        }

        public function weekWarningCron(){
            $this->model->weekWarningCron();
        }

        public function exclusionCron(){
            $this->model->exclusionCron();
        }

        public function activate($step = 0, $reactivate = false){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            $signature = $this->model->getSignature($this->user->get('id'));
            if($this->model->notActivated($signature)){
                switch ($step) {
                    case 0:
                        if(!_IN_DEV_)
                            $this->viewer->addJs('https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js');
                        else
                            $this->viewer->addJs('https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js');

                        $this->viewer->addJs('assets/js/pagseguro.js');

						if($reactivate) {
							$this->viewer->set('formId', 'reactivation');
							$this->viewer->set('plan1_value', Masks::moneyMask(_LEVEL_1_VALUE));
							$this->viewer->set('plan2_value', Masks::moneyMask(_LEVEL_2_VALUE));
							$this->viewer->set('plan3_value', Masks::moneyMask(_LEVEL_3_VALUE));
						}else {
							$this->viewer->set('formId', 'startActivation');
						}

                        $signature = $this->model->getSignature($this->user->get('id'));
                        $this->viewer->set('signature', $signature);
                        if(_IN_DEV_)
                            $this->viewer->show('cc_data_dev', 'Ativar assinatura');
                        else
	                        $this->viewer->show('cc_data', 'Ativar assinatura');
	                    break;

                    case 1:
                        if(!$this->request()){
                            die;
                        }
                        $id_user = $this->user->get('id');
                        $signature = $this->model->getSignature($id_user);
                        $member = $this->model->getDto('member', 'id_user', $id_user);
                        $preApproval = $this->model->getDto('pre_approval', 'level', $signature->get('level'));
                        $preApproval = $this->model->pagseguro->accession($this->user, $signature, $member, $preApproval);
                        echo json_encode($preApproval);
                        break;

                    case 2:
                        $id_user = $this->user->get('id');
                        $signature = $this->model->getSignature($id_user);
                        $this->model->checkout($signature);

                        break;
                }
            }else{
                Viewer::flash('Sua assinatura já está ativa!', 'i');
                return $this->view();
            }
        }

        public function generateSession(){
            echo json_encode($this->model->pagseguro->generateSession());
        }

        public function notificationListener(){
            $this->model->notificationListener();
        }

        public function vn(){
            echo '<h1>listen</h1>';
            $file = file_get_contents(__DIR__.'/../assets/notifications/'.date('d-m-Y').'-LISTENER.txt');
            $file = explode('---BREAK---', $file);
            foreach($file as $content){
                if(!empty($content))
                    var_dump(unserialize($content));
            }
            echo '<h1>proccess</h1>';
            $file = file_get_contents(__DIR__.'/../assets/notifications/'.date('d-m-Y').'-PROCCESS.txt');
            $file = explode('---BREAK---', $file);
            foreach($file as $content){
                if(!empty($content))
                    var_dump(unserialize($content));
            }
        }

        public function cancelCron(){
            $this->model->cancelCron();
        }

        public function requestCancel($confirm = false){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            $ret = $this->model->canCancel($this->user->get('id'));
            if($ret['canCancel'] || $confirm){
                if($confirm){
                    $this->model->requestCancel($this->user->get('id'), $this->user->get('name'));
                }else{
                    $this->viewer->addJs('assets/js/pagseguro.js');
                    return $this->viewer->show('request_cancel', 'Solicitar cancelamento');
                }
            }else{
                Viewer::flash($ret['error'], 'e');
                return $this->view();
            }
        }

        public function confirmCancel($id_user, $confirm = false){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($confirm){
                $this->model->cancel($id_user);
            }else{
                $this->viewer->set('id_user', $id_user);
                $this->viewer->addJs('assets/js/pagseguro.js');
                return $this->viewer->show('confirm_cancel', 'Confirmar cancelamento');
            }
        }

        public function rejectCancel($id_user){
            if($this->user->get('role') != 'admin'){
                die;
            }
            $this->model->rejectCancel($id_user);
        }

        public function requestChange($toLevel){
            if($this->user->get('role') != 'member'){
                echo json_encode(array(
                    'type' => 'error',
                    'text' => _PERMISSION_ERROR,
                    'timer' => 5000,
                ));
                return;
            }
            $ret = $this->model->canChange($this->user->get('id'));
            if($ret['canChange']){
                $this->model->requestChange($this->user->get('id'), $this->user->get('name'), $toLevel);
                echo json_encode(array(
                    'type' => 'success',
                    'text' => 'Troca de plano requisitada com sucesso.',
                    'timer' => 5000,
                ));
            }else{
                echo json_encode(array(
                    'type' => 'error',
                    'text' => $ret['error'],
                    'timer' => 5000,
                ));
            }
            return;
        }

        public function confirmChange($id_user, $confirm = false){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($confirm){
                $this->model->change($id_user);
            }else{
                $this->viewer->set('id_user', $id_user);
                $this->viewer->addJs('assets/js/pagseguro.js');
                return $this->viewer->show('confirm_change', 'Confirmar troca de plano');
            }
        }

        public function rejectChange($id_user){
            if($this->user->get('role') != 'admin')
                die;
            return $this->model->rejectChange($id_user);
        }


        public function changeCard(){
            if($this->request()){
                $id_user = $this->user->get('id');
                $signature = $this->model->getSignature($id_user);
                $member = $this->model->getDto('member', 'id_user', $id_user);

                $response = $this->model->pagseguro->changeCard($this->user, $signature, $member);
                if(empty($response)){
                    echo json_encode(array('return' => 1));
                }else{
                    echo json_encode($response);
                }
                return;
            }else{
                if(!_IN_DEV_)
                    $this->viewer->addJs('https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js');
                else
                    $this->viewer->addJs('https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js');

                $this->viewer->addJs('assets/js/pagseguro.js');

                $this->viewer->set('formId', 'changeCard');

                $signature = $this->model->getSignature($this->user->get('id'));
                $this->viewer->set('signature', $signature);
                return $this->viewer->show('cc_data', 'Alterar cartão de crédito');
            }
        }

        public function fakeNotify(){
            $_POST['notificationType'] = 'transaction';
            $_POST['notificationCode'] = '163855E0F56AF56AC854449B9F8264E0AA6D';
            $this->model->notificationListener();
        }

        public function cancelByError(){
            if(!$this->request())
                die;
            $this->model->pagseguro->cancel($_POST['code']);
        }

        public function status($id_user){
            $signature = $this->model->getSignature($id_user);
            $this->model->pagseguro->query($signature->get('accession_code'));
        }

    }
