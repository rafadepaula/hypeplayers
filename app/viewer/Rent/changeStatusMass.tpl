<form action="/rent/changeStatus/{$ids}/1" method="post" id="send-form">
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>Status</label>
			<select class="form-control" name="status" required>
				<option value="transit">
					Em trânsito para entrega
				</option>
				<option value="delivered">
					Entregue
				</option>
				<option value="transit_back">
					Em trânsito para devolução
				</option>
				<option value="complete">
					Completo
				</option>
				<option value="complete_problem">
					Completo mas com problemas
				</option>
				<option value="delayed">
					Atrasado
				</option>
				<option value="waiting_post">
					Aguardando postagem
				</option>
				<option value="giveback_requested">
					Devolução requisitada
				</option>
				<option value="change_requested">
					Troca requisitada
				</option>
				<option value="renew_requested">
					Renovação requisitada
				</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<button class="btn btn-success btn-block" type="submit">
				Alterar
			</button>
		</div>
	</div>
</form>