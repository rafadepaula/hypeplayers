<?php
    /**
     * Modify this file to make your app work. All those constants are
     * important.
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     *
     */
    
    /*
     * Defines the class and method to use as index
     * This method is called when an action is not found, or
     * when you want to redirect the user to home
     */
    define('_MAIN_CLASS', 'IndexController');
    define('_MAIN_METHOD', 'view');
    
    /*
     * URL for environment uses.
     * ATTENTION: WITHOUT http:// and the final slash. Example: yoursite.com, localhost, localhost:8090
     */
    define('_DEV_URL_', 'localhost:8110'); // development url
    define('_PRD_URL_', 'hypeplayers.com.br'); // production url

    /*
     * SALT used to crypt passwords
     */
    define('_PASS_SALT_', 'Cf0f11ePArplBJomM0M6aJ');
    
    /*
     * Use the permissions system or not.
     * Be careful when setting this to true. Check those files before:
     * - app/model/Trait/Functions.trait.php. If you need an example, check base/model/trait/Functions.trait.php
     * - base/controller/Controller.class.php - Check the constructor. It can't be modified!!!!!
     * - base/model/Permission.class.php - The only function that can be modified is makeForm!!!!!
     * And remember to have a column called "permission" on the user's table.
     *
     * The _MASTERS_ID are an array of users id that have total control of the system
     */
    define('_PERMISSIONS_', false);
    define('_MASTERS_ID', serialize(array()));
    
    /*
     * Defines the main language to the system and other things about locales
     * The file config/messagesLocales.inc.php contains the
     * messages.
     */
    define('_LOCALE', 'ptbr');
    define('_DATE_FORMAT_', 'd/m/Y H:i:s');
    setlocale(LC_MONETARY,"pt_BR");
    date_default_timezone_set('Brazil/East');
    
    
    /**
     * Define if the login will be by email or login field.
     */
    define('_LOGIN_TYPE', 'email');


    /**
     * Default error handling level. Values:
     * 0 - doesn't handle
     * 1 - server default
     * 2 - handles error and display default error message
     * 3 - same then 2 and log in database
    */
    define('_LOG_LEVEL', 0);