<?php
    
    class FavoriteModel extends AppModel {
        public function getFavorite($id_user){
            return $this->getDto('favorite', 'id_user', $id_user);
        }
        
        /**
         * Returns a query like this one:
         * update favorite set position = case when position = 0 then 1 when position = 1 then 0 end where position in (0,1)
         *
         * @param String $diff - the values that have been changed
         * @return String - the sql to apply the updates on the database
         *
        **/
        public function reorderSql($diff){
            $sql = 'update favorite set position = case';
            $where = ' where position in (';
            foreach($diff as $positions){
                $oldPosition = $positions[0];
                $newPosition = $positions[1];
                $sql .= ' when position = '.$oldPosition.' then '.$newPosition;
                $where .= $oldPosition.',';
            }
            $where = trim($where,',').') and id_user = '.unserialize($_SESSION['user'])->get('id');
            $sql .= ' end '.$where;
            return $sql;
        }

        public function addFav($id_game){
            $errors = '';
            $return = array();
            $firstFav = false;
			$platform = $_POST['platform'];

            $id_user = unserialize($_SESSION['user'])->get('id');
            $signature = $this->getDto('signature', 'id_user', $id_user);
            if(!$signature->activeStatus()){
                $return['error'] = 'Sua assinatura não está ativa!';
                $return['result'] = 0;
                $return['position'] = 0;
                echo json_encode($return);
                return;
            }

            $this->initTransaction();
            // check if already have the game added as favorite
            if($this->alreadyHave($id_game, false)){
            	$return['error'] = 'Você já adicionou este jogo.';
            }else{
	            // initiates the proccess of dto creation
	            $favorite = new Favorite();
	            if(!$favorite->set('id_game', $id_game)){
	                $return['error'] = $favorite->FieldsErrors['id_game'];
	            }
	            if(!$favorite->set('id_user', unserialize($_SESSION['user'])->get('id'))){
	                $return['error'] = $favorite->FieldsErrors['id_user'];
	            }
	            if(!$favorite->set('platform', $platform)){
	                $return['error'] = $favorite->FieldsErrors['platform'];
	            }

	            // calculates the position based on the already added games
	            $position = $this->calculateFavoritePosition();
	            $favorite->set('position', $position);
	            $favorite->set('created', date('Y-m-d H:i:s'));

	            if(isset($_COOKIE['favorites'])){ // in proccess of list creation/edit
	                $games = unserialize($_COOKIE['favorites']);
	                $games[] = serialize(array($favorite->get('id_game'), $platform));
	                setcookie('favorites', serialize($games), time() + 60 * 60 * 24, '/');
	                $_COOKIE['favorites'] = serialize($games);
	                if(count($games) == 10){
	                    $return['finishFirst'] = 1;
	                }
	            }else{
	                $firstFav = $position === 1; // it's the first fav of the member
	                $games = array(serialize(array($favorite->get('id_game'), $platform)));
	                setcookie('favorites', serialize($games), time() + 60 * 60 * 24, '/');
	                $_COOKIE['favorites'] = serialize($games);
	            }
            }

            $cond = array('id_user' => unserialize($_SESSION['user'])->get('id'));
            $last = $this->search('favorite', '*', $cond);
            $last = $this->calcLastModify($last);
            /*if($last['freeModify'] != ''){
                setcookie('favorites', null, time() - 3600, '/');
                unset($_COOKIE['favorites']);
                $return['error'] = 'Alterações liberadas apenas a partir de '.$last['freeModify'];    
            }*/

            if(isset($return['error'])){
                $this->cancelTransaction();
            }else{
                $this->endTransaction();
            }


            $return['result'] = isset($return['error']) ? 0 : 1;
            $return['isFirst'] = $firstFav;
            $return['position'] = (isset($position) ? $position : 0) + 1;

            echo json_encode($return);
            return;

        }

        /**
         * Calculates the next favorite order (position) based on the
         * already registered as favorite games of the member
         *
         * @return int - favorite position
        **/
        public function calculateFavoritePosition($includeCookies = true){
            $user = unserialize($_SESSION['user']);
            $position = 0;
            if($this->exists('favorite', 'id_user', $user->get('id'))){
                $position = $this->numRows('favorite', array('id_user' => $user->get('id')));
            }
            if($includeCookies){
                if(isset($_COOKIE['favorites'])){
                    $position += count(unserialize($_COOKIE['favorites']));
                }
            }

            return $position;
        }

        public function alreadyHave($id_game, $echo = true){
            $cond = array('id_game' => $id_game, 'conscond1' => 'and', 'conscond1' => 'and',
                          'id_user' => unserialize($_SESSION['user'])->get('id'));
            $exists = count($this->search('favorite', '*', $cond)) > 0;
            if($exists){
            	if($echo)
            		echo 1;
				return true;
            }else{
	            if(isset($_COOKIE['favorites'])){
		            foreach(unserialize($_COOKIE['favorites']) as $game){
			            $game = unserialize($game);
			            if($game[0] == $id_game){
				            if($echo)
				            	echo 1;
				            return true;
			            }
		            }
	            }
            }
	        if($echo)
	        	echo 0;
            return false;
        }

        public function finish(){
            $id_user = unserialize($_SESSION['user'])->get('id');
            $name = unserialize($_SESSION['user'])->get('name');
            if($this->exists('favorite', 'id_user', $id_user)){
                $hasFavorites = true;
            }else{
                $hasFavorites = false;
            }
            $favorites = $_COOKIE['favorites'];
            $favorites = unserialize($favorites);
            if(count($favorites) < 10 && !$hasFavorites){
                return json_encode(array(
                    'type' => 'error',
                    'text' => 'Você ainda não escolheu dez jogos como preferências.',
                    'return' => 0
                ));
            }

            $last = $this->search('favorite', '*', array('id_user' => $id_user));
            $last = $this->calcLastModify($last);
            /*if($last['freeModify'] != ''){
                setcookie('favorites', null, time() - 3600, '/');
                return json_encode(array(
                    'type' => 'error',
                    'text' => 'Alterações liberadas apenas a partir de '.$last['freeModify'].'.',
                    'return' => 0
                ));
            }*/

            $this->initTransaction();
            $position = $this->calculateFavoritePosition(false);
            foreach($favorites as $game){
                $game = unserialize($game);
                $id_game = $game[0];
                $platform = $game[1];
                $favorite = new Favorite();
                $favorite->set('id_game', $id_game);
                $favorite->set('id_user', unserialize($_SESSION['user'])->get('id'));
                $favorite->set('platform', $platform);
                $favorite->set('position', $position);
                $favorite->set('created', date('Y-m-d H:i:s'));

                if(!$this->insert('favorite', $favorite)){
                    $this->cancelTransaction();
                    return json_encode(array(
                        'type' => 'error',
                        'text' => 'Não foi possível salvar '.$favorite->get('id_game', true)->get('name'),
                        'return' => 0,
                    ));
                }
                $position++;
            }
            $this->endTransaction();
            setcookie('favorites', null, time() - 3600, '/');

            $notificationModel = new NotificationModel();
            $notificationModel->favoritesUpdated($name, $id_user);

            return json_encode(array(
                'type' => 'success',
                'text' => _INSERT_SUCCESS,
                'return' => 1
            ));
        }

        public function saveChangesModal(){
            $favorites = array();
            if(isset($_COOKIE['favorites'])){
                $favoritesCookie = unserialize($_COOKIE['favorites']);
                foreach($favoritesCookie as $position => $favoriteCookie){
                    $favoriteCookie = unserialize($favoriteCookie);
                    $favorite = new Favorite();
                    $favorite->set('id_game', $favoriteCookie[0]);
                    $favorite->set('platform', $favoriteCookie[1]);
                    $favorite->set('position', $position);
                    $favorites[] = $favorite;
                }
            }
            return $favorites;
        }

        public function reorder($total){
            if($total){
                $cond = array('id_user' => unserialize($_SESSION['user'])->get('id'));
                $favorites = $this->search('favorite', '*', $cond, 'position');
                foreach($favorites as $position => $favorite){
                    $favorite->set('position', $position);
                    if(!$this->update('favorite', $favorite, array('id' => $favorite->get('id')))){
                        return false;
                    }
                }
                return true;
            }
        }

        public static function calcLastModify($favorites){
            $return = array();
            if(count($favorites)){
                $last = strtotime($favorites[0]->get('created'));
                foreach($favorites as $favorite){
                    $curr = strtotime($favorite->get('created'));
                    if($curr > $last){
                        $last = $curr;
                    }
                }
                $now = time();
                $datediff = $now - $last;
                $datediff = floor($datediff / (60 * 60 * 24));
                $return['lastModify'] = date('d/m/Y', $last);

                
            }else{
                $datediff = 31;
                $return['lastModify'] = 'nunca';
            }

            if($datediff <= 30){
                $return['freeModify'] = date('d/m/Y', strtotime('+30 days', $last));
            }else{
                $return['freeModify'] = '';
            }


            $return['datediff'] = $datediff;
            $return['colorModify'] = $datediff >= 30 ? 'text-info' : 'text-danger';

            return $return;
        }

        public function removeByRent(Rent $rent){
            $id_game = $rent->get('id_game');
            $platform = $rent->get('platform');
            $id_user = $rent->get('id_user');
            $cond = array(
                'id_game' => $id_game,
                'conscond1' => 'and',
                'platform' => $platform,
                'conscond2' => 'and',
                'id_user' => $id_user,
            );
            $favorite = $this->search('favorite', '*', $cond);
            if(!count($favorite)){
                return false;
            }else{
                if($this->delete('favorite', array('id' => $favorite[0]->get('id')))){
                    return $this->reorderByRemove($id_user);
                }else{
                    return false;
                }
            }
        }

        public function reorderByRemove($id_user){
            $favorites = $this->search('favorite', '*', array('id_user' => $id_user));
            for($i = 0; $i < count($favorites); $i++){
                $favorite = $favorites[$i];
                $favorite->set('position', $i);
                if(!$this->update('favorite', $favorite, array('id' => $favorite->get('id'))))
                    return false;
            }
            return true;
        }

        public function lowQttCron(){
            $sql = 'select count(id) as qtt, id_user from favorite where qtt <= 5 group by id_user';
            $favorites = $this->query($sql);
            $notificationModel = new NotificationModel();
            foreach($favorites as $favorite){
                $notificationModel->lowFavoriteQtt($favorite['id_user']);
            }
        }

    }