<?php
    
    
    class MemberController extends AppController {

        public function home(){
            if($this->user->get('role') == 'admin'){
                $userController = new UserController();
                return $userController->home();
            }

            $data = $this->model->home();
            foreach($data as $key => $value){
                $this->viewer->set($key, $value);
            }

	        $signature = $this->model->getDto('signature', 'id_user', $this->user->get('id'));
	        if($signature->get('can_change')){
		        $this->viewer->set('changeText', 'Solicitar troca/devolução');
	        }else{
		        $this->viewer->set('changeText', 'Solicitar devolução');
	        }

            $this->viewer->addJs('assets/js/library.js');
            $this->viewer->addJs('assets/js/myGames.js');
            return $this->viewer->show('home', 'Início');
        }

        public function profile(){
            $id = $this->user->get('id');
            if(!$this->model->exists('member', 'id_user', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->view();
            }
            $member = $this->model->getMember($id);
            $signature = $this->model->getDto('signature', 'id_user', $id);
            $user = $this->model->getDto('user', 'id', $id);

            $rents = $this->model->search('rent', '*', array('id_user' => $id), 'end');
            $favorites = $this->model->search('favorite', '*', array('id_user' => $id), 'position');

            $regionModel = new RegionModel();
            $region = $regionModel->getRegionByCep(str_replace('-', '', $member->get('cep')));

            $this->viewer->set('member', $member);
            $this->viewer->set('signature', $signature);
            $this->viewer->set('user', $user);
            $this->viewer->set('rents', $rents);
            $this->viewer->set('favorites', $favorites);
            $this->viewer->set('region', $region);

            if(!empty($member->get('picture')))
                $this->viewer->set('picture', $member->get('picture'));
            else
                $this->viewer->set('picture', _DEFAULT_PICTURE);

            $this->viewer->addJs('assets/js/memberViewMore.js');
            $this->viewer->addJs('assets/js/rent.js');

            return $this->viewer->show('profile', 'Meu perfil');
        }

        public function edit(){
            if($this->user->get('role') != 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->view();
            }

            if($this->request()){
                if($this->model->edit($this->user->get('id'))){
                    return $this->profile();
                }else{
                    unset($_POST);
                }
            }
            $member = $this->model->getMember($this->user->get('id'));
            $this->viewer->set('member', $member);
            $this->viewer->addJs('assets/js/cep.js');
            return $this->viewer->show('edit', 'Editar informações pessoais');
        }

        public function join(){
            if($this->logged()){
                return $this->siteIndex();
            }
            if($this->request()){
                if($this->model->save()){
                    echo 1;
                }
                return;
            }
            return $this->viewer->show('join', 'Junte-se a nós!', false, true);
        }
        
        public function view($id = null){
            if($this->user->get('role') != 'admin'){
                return $this->profile();
            }
            if(!is_null($id)){
                if(!$this->model->exists('member', 'id_user', $id)){
                    Viewer::flash(_EXISTS_ERROR, 'e');
                    return $this->view();
                }
                $member = $this->model->getMember($id);
                $signature = $this->model->getDto('signature', 'id_user', $id);
                $user = $this->model->getDto('user', 'id', $id);

                $rents = $this->model->search('rent', '*', array('id_user' => $id), 'end');
                $favorites = $this->model->search('favorite', '*', array('id_user' => $id), 'position');

                $regionModel = new RegionModel();
                $region = $regionModel->getRegionByCep(str_replace('-', '', $member->get('cep')));

                $addresses = $this->model->search('address', '*', array('id_user' => $id));

                $payments = $this->model->search('payment', '*', array('id_user' => $id));

                $this->viewer->set('payments', $payments);
                $this->viewer->set('member', $member);
                $this->viewer->set('signature', $signature);
                $this->viewer->set('user', $user);
                $this->viewer->set('rents', $rents);
                $this->viewer->set('favorites', $favorites);
                $this->viewer->set('region', $region);
                $this->viewer->set('addresses', $addresses);

                if(!empty($member->get('picture')))
                    $this->viewer->set('picture', $member->get('picture'));
                else
                    $this->viewer->set('picture', _DEFAULT_PICTURE);

                $this->viewer->set('cancelRequested', $signature->get('status') == 'Cancel_requested');
                $changeRequested = $signature->get('status') == 'Change_requested';
                $this->viewer->set('changeRequested', $changeRequested);
                if($changeRequested){
                    $request = $this->model->getDto('plan_change', 'id_user', $id);
                    $this->viewer->set('request', $request);
                }

                $this->viewer->addJs('assets/js/memberViewMore.js');
                $this->viewer->addJs('assets/js/rent.js');

                return $this->viewer->show('view_one', 'Perfil de '.$user->get('name'));
            }
            $sql = 'select u.name, u.id, DATE_FORMAT(s.expiry, "%d/%m/%Y") as expiry,
                           DATE_FORMAT(s.last_payment, "%d/%m/%Y") as last_payment,
                           s.status
                    from user u
                    join signature s on s.id_user = u.id
                    join member m on m.id_user = u.id';
            $members = $this->model->query($sql);
            $this->viewer->set('members', $members);
            return $this->viewer->show('view', 'Membros');
        }

        public function activate($secret){
            if($this->logged()){
                return $this->siteIndex();
            }
            if($this->model->activate($secret)){
                Viewer::flash('Usuário ativado com sucesso! Seja muito bem-vindo!', 'i');
                $this->home();
            }else{
                $this->viewer->show('activationFail', 'Falha na ativação', false, true);
            }
            return;
        }

        public function exclusionCron($secret){
            if($secret != 'FD34wr8sdfgGNFd3erdfk')
                return false;
            return $this->model->exclusionCron();
        }

        public function viewMore($id_user){
            if($this->user->get('role') != 'admin'){
                echo json_encode(array(
                    'return' => 0,
                    'text' => _PERMISSION_ERROR,
                ));
                return;
            }
            if(!$this->model->exists('signature', 'id_user', $id_user)){
                echo json_encode(array(
                    'return' => 0,
                    'text' => _EXISTS_ERROR,
                ));
                return;
            }

            $signature = $this->model->getDto('signature', 'id_user', $id_user);
            $text = array(
                'can_change' => $signature->get('can_change', true),
                'change_requested' => $signature->get('change_requested', true),
                'change_count' => $signature->get('change_count'),
                'renew_requested' => $signature->get('renew_requested', true),
                'available_rent' => $signature->get('available_rent'),
                'simultaneous' => $signature->get('simultaneous'),
                'reference' => $signature->get('reference'),
                'accession_code' => $signature->get('accession_code'),
            );
            $text = '<pre>'.var_export($text, true).'</pre>';
            echo json_encode(array(
                'text' => $text,
                'return' => 1,
            ));
            return;
        }

        public function toggleDeactivated($id_user){
        	if($this->user->get('role') != 'admin'){
        		Viewer::flash(_PERMISSION_ERROR, 'e');
        		return $this->siteIndex();
	        }
            return $this->model->toggleDeactivated($id_user);

        }

        public function requests(){
	        $memberRequests = $this->model->search('member_request', '*', false, 'created');
	        $this->viewer->set('memberRequests', $memberRequests);

	        return $this->viewer->show('requests', 'Solicitações de cadastro');
        }

        public function deleteRequest($id){
	        if($this->user->get('role') != 'admin'){
		        Viewer::flash(_PERMISSION_ERROR, 'e');
		        return $this->siteIndex();
	        }
	        if($this->model->delete('member_request', array('id' => $id))){
	        	Viewer::flash(_DELETE_SUCCESS, 's');
	        }else{
	        	Viewer::flash(_DELETE_ERROR, 'e');
	        }
	        return $this->requests();
        }

    }
