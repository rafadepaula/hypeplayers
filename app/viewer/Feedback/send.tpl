<div class="row">
	<div class="col-sm-12 form-group">
		<label>Tipo</label>
		<select id="feedback-type" class="form-control">
			<option value="Elogio">Elogio</option>
			<option value="Reclamação">Reclamação</option>
			<option value="Aquisição">Aquisição</option>
		</select>
	</div>
	<div class="col-sm-12 form-group">
		<label>Mensagem</label>
		<textarea id="feedback-message" class="form-control" placeholder="Envie sua mensagem"></textarea>
	</div>
</div>