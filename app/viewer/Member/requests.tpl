<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable" data-buttons="true">
                    <thead>
                        <tr>
                            <th>CEP</th>
                            <th>Nome</th>
                            <th>Nascimento</th>
                            <th>CPF-RG</th>
                            <th>Telefone</th>
                            <th>E-mail</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $memberRequests as $request}
                        <tr>
                            <td>
                                {$request->get('id_region_request', true)->get('cep')}
                            </td>
                            <td>{$request->get('name')} {$request->get('lastnam')}</td>
                            <td>{$request->get('birthday', true)}</td>
                            <td>{$request->get('cpf')}-{$request->get('rg')}</td>
                            <td>{$request->get('phone')}</td>
                            <td>{$request->get('email')}</td>
                            <td>{$request->get('created', true)}</td>
                            <td>
                                <a class="confirm-link" href="/member/deleteRequest/{$request->get('id')}">
                                    <button type="button" rel="tooltip" title="Remover {$request->get('name')} da lista"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>