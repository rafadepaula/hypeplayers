<?php
    
    class GenreModel extends AppModel {
        public function getGenre($id){
            return $this->getDto('genre', 'id', $id);
        }
        
        public function save($id = null){
            $genre = new Genre();
            $ret  = $this->makeDto($genre, $id);
            $genre = $ret[0];
            
            $errors = $ret[1];
            
            if($errors != ''){
                Viewer::flash($errors, 'e');
                
                return false;
            }else{
                
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('genre', $genre) : $this->update('genre', $genre, array('id' => $id));
                if($ret){
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }
        
    }