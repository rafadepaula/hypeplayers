<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                        <thead>
                        <tr>
                            <th>Texto</th>
                            <th>Link</th>
                            <th>Disparada em</th>
                            <th>Lida por</th>
                            <th>Lida em</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $notifications as $notification}
                            <tr>
                                <td>
                                    {$notification->get('text')}
                                </td>
                                <td>
                                    <a href="{$notification->get('link')}">
                                        Acessar
                                    </a>
                                </td>
                                <td>
                                    {$notification->get('created', true)}
                                </td>
                                <td>
                                    {$notification->get('id_readby', true)->get('name')}
                                </td>
                                <td>
                                    {$notification->get('read_date', true)}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>