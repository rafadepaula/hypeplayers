<?php
    
    class Member extends DTO {
        public $cep;
        public $city;
        public $complement;
        public $id_region;
        public $id_user;
        public $neighborhood;
        public $number;
        public $phone;
        public $rg;
        public $cpf;
        public $birthday;
        public $picture;
        public $state;
        public $street;
        public $activated;
        public $secret;
        public $deactivated;
        
        public $FieldsValidation = array(
            'cep' => 'notEmpty,validCep',
            'neighborhood' => 'notEmpty',
            'number' => 'notEmpty',
            'phone' => 'notEmpty',
            'state' => 'notEmpty',
            'street' => 'notEmpty',
            'picture' => array('validFile', ['jpg', 'jpeg', 'png']),
	        'cpf' => 'notEmpty,validCpf',
	        'rg' => 'notEmpty',
	        'birthday' => 'notEmpty,validDate',
        );
        public $FieldsErrors     = array(
            'cep' => 'CEP inválido!',
            'neighborhood' => 'Informe seu bairro.',
            'number' => 'Informe o número de seu endereço.',
            'phone' => 'Informe seu número de telefone.',
            'state' => 'Informe seu estado.',
            'street' => 'Informe sua rua.',
            'picture' => 'Por favor, informe uma imagem no formato JPG/PNG.',
	        'cpf' => 'Informe um CPF válido.',
	        'rg' => 'Informe seu RG',
			'birthday' => 'Informe sua data de nascimento.',
        );
        public $FieldsMasks      = array(
            'id_user' => ['getDto', ['user', 'id']],
            'id_region' => ['getDto', ['region', 'id']],
            'state' => 'reverseUf',
	        'birthday' => 'dateMask',
        );
        
    }