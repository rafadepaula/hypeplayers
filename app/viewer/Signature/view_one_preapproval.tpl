
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col-sm-8 form-group">
                        <label>Código</label>
                        <input disabled type="text" name="code" class="form-control" placeholder="Código API do plano" value="{$preApproval->get('code')}">
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Nível</label>
                        <select disabled name="level" class="form-control">
                            <option value="">Selecione</option>
                            <option {if $preApproval->get('level') == 1} selected {/if} value="1">1</option>
                            <option {if $preApproval->get('level') == 2} selected {/if} value="2">2</option>
                            <option {if $preApproval->get('level') == 3} selected {/if} value="3">3</option>
                        </select>
                    </div>
                </div>
                <h4>Assinaturas com este plano</h4>
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Membro</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $signatures as $signature}
                        <tr>
                            <td>
                                {$signature->get('id_user', true)->get('name')}
                            </td>
                            <td class="td-actions">
                                <a href="/member/view/{$signature->get('id_user')}">
                                    <button type="button" rel="tooltip" title="Visualizar perfil"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>