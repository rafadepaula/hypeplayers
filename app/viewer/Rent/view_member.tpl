<h5>Última modificação em {$rent->get('last_modify', true)}</h5>
<div class="row">
	<div class="col-sm-12">
		<b>Renovações nesta locação:</b> {$rent->get('renew_qtt')}
	</div>
</div>
<div class="row text-center">
	<div class="col-sm-12">
		{$rent->get('id_game', true)->get('name')} ({$rent->get('platform', true)})
		<image src="/{$rent->get('id_game', true)->get($rent->get('platform')|cat:'_cover')}" class="game-cover library-game center-block">
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group">
		<label>Início da locação</label>
		<input disabled class="form-control" value="{$rent->get('start', true)}">
	</div>
	<div class="col-sm-6 form-group">
		<label>Final da locação</label>
		<input disabled class="form-control" value="{$rent->get('end', true)}">
	</div>
</div>
<div class="row">
	<div class="col-sm-4 form-group">
		<label>Método de envio</label>
		<input disabled class="form-control" value="{$rent->get('carrier_mode', true)}">
	</div>
	<div class="col-sm-4 form-group">
		<label>Código de rastreio</label>
		<input disabled class="form-control" value="{$rent->get('carrier_code')}">
	</div>
	<div class="col-sm-4 form-group alert alert-info" style="padding: 10px">
		<label style="color: white">Status</label>
		<input disabled class="form-control" value="{$rent->get('status', true)}" style="color: white">
	</div>
</div>

<div class="row">
	<div class="col-sm-6 form-group">
		<label>Observações sobre transporte</label>
		<textarea disabled class="form-control">{$rent->get('ps_carrier')}</textarea>
	</div>
	<div class="col-sm-6 form-group">
		<label>Suas observações</label>
		<textarea disabled class="form-control">{$rent->get('ps_user')}</textarea>
	</div>
</div>
