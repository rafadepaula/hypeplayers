<?php
    
    class UserModel extends AppModel {
        public function getUser($id){
            return $this->getDto('user', 'id', $id);
        }
        
        public function save($id = null, $role = 'member', $newMember = false){
            $user = $this->getUser($id);
            $email = $user->get('email');

            $ret  = $this->makeDto($user, $id);
            $user = $ret[0];

            $user->set('role', $role);

            if($email != $user->get('email')){
                if($this->exists('user', 'email', $user->get('email'))){
                    if($newMember){
                        echo '<li>E-mail já cadastrado no sistema!</li>';
                    }else{
                        Viewer::flash('E-mail já cadastrado no sistema!', 'e');
                    }
                    return false;
                }
            }
            
            $errors = $ret[1];


            if($errors != ''){
                if(!$newMember)
                    Viewer::flash($errors, 'e');
                else
                    echo '<li>'.substr(str_replace('<br>', '</li><li>', $errors), 0, strlen(str_replace('<br>', '</li><li>', $errors)) - 4);
                
                return false;
            }else{
                
                // if id is null insert else update
                $ret = is_null($id) ? $this->insert('user', $user) : $this->update('user', $user, array('id' => $id));
                if($ret){
                    if(!$newMember){
                        if(!is_null($id)){
                            $_SESSION['user'] = appserialize($user);
                        }
                        
                        Viewer::flash(_INSERT_SUCCESS, 's');
                    }

                    return true;
                }else{
                    if(!$newMember)
                        Viewer::flash(_INSERT_ERROR, 'e');
                    else
                        echo '<li>'._INSERT_ERROR.'</li>';
                    
                    return false;
                }
            }
        }

        public function recovery($secret = null, $newPass = false){
            if(is_null($secret)){

                $user = $this->search('user', '*', array('email' => $_POST['recovery']));
                if(!count($user)){
                    Viewer::flash('E-mail de usuário não existente no sistema.');
                    return false;
                }
                $user     = $user[0];


                $secret = date('Y-m-d H:i:s').$user->get('email');
                $secret = md5($secret);
                
                $recovery = new Recovery();
                $recovery->set('id_user', $user->get('id'));
                $recovery->set('secret', $secret);
                $recovery->set('date', date('Y-m-d H:i:s'));
                $recovery->set('done', 0);
                if($this->insert('recovery', $recovery)){
                    $this->emailRecovery($user->get('email'), $user->get('name'), $secret);
                    Viewer::flash('Um e-mail foi enviado para a sua conta: '.$user->get('email').'.
                                   Siga as instruções lá presentes!');
                    
                    return true;
                }else{
                    Viewer::flash('Não foi possível recuperar sua senha. Tente novamente.', 'e');
                    
                    return false;
                }
            }else{
                if($newPass){
                    $recovery = $this->search('recovery', '*', array('secret' => $secret))[0];
                    $user     = $recovery->get('id_user', true);
                    if(!$user->set('password', $_POST['password'])){
                        Viewer::flash('Informe uma senha válida!', 'e');
                        
                        return false;
                    }
                    $this->update('user', $user, array('id' => $user->get('id')));
                    $_SESSION['user']   = serialize($user);
                    $_SESSION['logged'] = true;
                    
                    $recovery->set('done', 1);
                    $this->update('recovery', $recovery, array('id' => $recovery->get('id')));
                    
                    Viewer::flash('Bem vindo de volta!', 's');
                    
                    return true;
                }else{
                    $recovery = $this->search('recovery', '*', array('secret' => $secret));
                    if(!count($recovery)){
                        Viewer::flash('Código inválido!', 'e');
                        
                        return false;
                    }
                    $recovery = $recovery[0];
                    $date = strtotime($recovery->get('date'));
                    $now  = strtotime(date('Y-m-d H:i:s'));
                    if($now - $date > 86400){
                        Viewer::flash('O link expirou! Repita o processo de recuperação para conseguir um novo acesso.',
                                      'i');
                        
                        return false;
                    }
                    if($recovery->get('done')){
                        Viewer::flash('A recuperação via este link já foi realizada.', 'e');
                        
                        return false;
                    }
                    
                    Viewer::flash('Insira a sua nova senha!', 's');
                    
                    return true;
                }
            }
        }
        
        public function emailRecovery($to, $name, $secret){
            require 'base/plugins/phpmailer/PHPMailerAutoload.php';
            
            $email = new PHPMailer;
            
            $email->isSMTP();
            $email->Host       = 'mail.hypeplayers.com.br';
            $email->SMTPAuth   = true;
            $email->Username   = 'webmaster@hypeplayers.com.br';
            $email->Password   = 'Zs9w63Q0oy';
            $email->SMTPSecure = false;
            $email->Port       = 26;
            
            $email->From     = 'webmaster@hypeplayers.com.br';
            $email->FromName = 'HypePlayers';
            $email->addBCC($to, $name);
            $email->isHTML(true);
            
            $email->Subject = 'Recuperação de senha';
            
            $email->Body    = '
                Olá, '.$name.'! Parece que você esqueceu sua senha de acesso ao sistema de
                assinatura de games HypePlayers. Para recuperar sua senha e voltar a obter
                acesso ao website, por favor, clique no link abaixo. Ele estará disponível
                para você nas próximas 24 horas. <br><br>
                <a href="http://hypeplayers.com.br/user/recovery/'.$secret.'" target="_blank">
                    https://hypeplayers.com.br/user/recovery/'.$secret.'
                </a>
            ';
            $email->CharSet = 'UTF-8';
            
            $email->send();
        }
    }