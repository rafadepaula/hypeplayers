<?php

    /**
     * Bootstrap forms auto constructor
     * Use it only for simple forms.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2017-08-21
     *
     **/
    class FormHelper {

    	public $form = '';
    	private $view;
    	private $defaultOptions = array(
    		'div_class' => _FORM_DIV_CLASS_,
    		'div_size' => _FORM_DIV_SIZE_,
    		'field_class' => _FORM_FIELD_CLASS_,
    		'field_object' => 'input',
    		'field_type' => 'text',
    		'field_required' => false,
    		'field_disabled' => false,
    		'field_readonly' => false,
    		'field_masks' => '',
		);

    	public function makeForm(DTO $dto){
    		if(!isset($dto->FieldsForms)){
    			$dto->FieldsForms = $this->makeFieldsForms($dto);
    		}
    		foreach($dto->FieldsForms as $field => $options){
    			if(!is_array($options)){
    				$field = $options;
    				$options = $defaultOptions;
    			}
    			switch ($options['field_object']) {
    				case 'input':
    					# code...
    					break;

					case 'checkbox':
						break;

					case 'radio':
						break;

					case 'textarea':
						break;

					case 'select':
    					break;

    				default:
    					# code...
    					break;
    			}
    		}
    	}

    	public function makeOptions($field, $options){
    		foreach($this->defaultOptions as $option => $value){
    			if(!isset($options[$option])){
    				$options[$option] = $value;
    			}
    		}
    	}

    	public function makeFieldsForms($dto){
    		$data = array();
            foreach ($dto as $atribute => $value) {
                if (!is_array($value))
                    $data[$atribute] = $value;
            }
            
            return $data;
    	}

    	public function input(){

    	}

    	public function checkbox(){

    	}

    	public function radio(){

    	}

    	public function textarea(){

    	}

    	public function select(){

    	}

    	public function title(){

    	}

    	public function finish(){

    	}
    }