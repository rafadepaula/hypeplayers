<?php
    
    class Genre extends DTO {
        public $id;
        public $name;
        
        public $FieldsValidation = array(
            'name'    => 'notEmpty',
        );
        public $FieldsErrors     = array(
            'name'    => 'Informe um nome para o gênero.',
        );
        
    }