<form action="/rent/send/{$id_user}" method="post" id="send-form">
	<div class="row">
		<div class="col-sm-12">
			<b>Plano do membro:</b> {$signature->get('level', true)}. <br>
			<b>Última locação:</b>
			{if isset($lastRent)}
				{$lastRent->get('id_game', true)->get('name')} ({$lastRent->get('platform', true)}) 
				finalizada em {$lastRent->get('last_modify', true)}
			{else}
				nenhuma registrada.
			{/if}
			<br>
			<b>Solicitou troca:</b> {$signature->get('change_requested', true)}.
			<b>Solicitou renovação:</b> {$signature->get('renew_requested', true)}. <br>
			Ainda pode receber <b>{$signature->get('available_rent')}</b> jogos(s).
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 form-group">
			<label>Jogo a ser enviado</label>
			<select class="form-control select2" name="id_game" required>
				{foreach $favorites as $favorite}
					<option value="{$favorite->get('id_game')}"
							data-platform="{$favorite->get('platform')}">
						{$favorite->get('id_game', true)->get('name')}
						({$favorite->get('platform', true)})
					</option>
				{/foreach}
			</select>
		</div>
		<input type="hidden" name="platform" value="" id="platform-input">
	</div>
	<div class="row">
		<div class="col-sm-6 form-group">
			<label>Início da locação</label>
			<input required type="date" class="form-control" name="start" value="{$start}">
		</div>
		<div class="col-sm-6 form-group">
			<label>Final da locação</label>
			<input required type="date" class="form-control" name="end" value="{$end}">
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 form-group">
			<label>Método de envio</label>
			<select required class="form-control select2" name="carrier_mode">
				<option value="courier">Motoboy</option>
				<option value="mail">Correio</option>
				<option value="withdraw">Retirada</option>
			</select>
		</div>
		<div class="col-sm-6 form-group">
			<label>Código de rastreio</label>
			<input type="text" class="form-control" name="carrier_code">
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 form-group">
			<label>Obs. administrativas</label>
			<textarea class="form-control" name="ps_adm"></textarea>
		</div>
		<div class="col-sm-6 form-group">
			<label>Obs. sobre transporte</label>
			<textarea class="form-control" name="ps_carrier"
				      placeholder="Será mostrado ao membro"></textarea>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<button class="btn btn-success btn-block" type="submit">
				Cadastrar
			</button>
		</div>
	</div>
</form>