<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <form id="{$formId}">
                    {if $formId == 'reactivation'}
                        <div class="row">
                            <div class="col-sm-12 form-group">
                                <label>Escolha seu plano</label>
                                <select name="plan_level" id="plan_level"required class="form-control">
                                    <option {if $signature->get('level') == 1} selected {/if}value="1">Plano Bronze - R${$plan1_value}</option>
                                    <option {if $signature->get('level') == 2} selected {/if}value="2">Plano Prata - R${$plan2_value}</option>
                                    <option {if $signature->get('level') == 3} selected {/if}value="3">Plano Ouro - R${$plan3_value}</option>
                                </select>
                            </div>
                        </div>
                    {/if}
                    <div class="row">
                        <div class="col-xs-9 col-sm-4 form-group">
                        	<label>Nº do Cartão</label>
                        	<input required type="text" class="form-control mask-cc" id="cardNumber" name="cardNumber" value="4111 1111 1111 111">
                        	<span class="help-block" id="cardNumberHelper"></span>
                        </div>
                        <div class="col-xs-3 col-sm-1 form-group">
                        	<label>CVV</label>
                        	<input required type="text" class="form-control" id="cvv" name="cvv" value="123">
                        </div>
                        <div class="col-xs-12 col-sm-1 form-group" id="ccimages">
                        	<label>Bandeira</label><br>
                        	<input type="hidden" id="brand" name="brand">
                        </div>
                        <div class="col-xs-6 col-sm-3 form-group">
                        	<label>Mês</label>
                        	<select required name="expirationMonth" id="expirationMonth" class="form-control">
                        		<option value="">Selecione...</option>
                        		<option value="01">Janeiro</option>
                        		<option value="02">Fevereiro</option>
                        		<option value="03">Março</option>
                        		<option value="04">Abril</option>
                        		<option value="05">Maio</option>
                        		<option value="06">Junho</option>
                        		<option value="07">Julho</option>
                        		<option value="08">Agosto</option>
                        		<option value="09">Setembro</option>
                        		<option value="10">Outubro</option>
                        		<option value="11">Novembro</option>
                        		<option selected value="12">Dezembro</option>
                        	</select>
                        </div>
                        <div class="col-xs-6 col-sm-3 form-group">
                        	<label>Ano</label>
                        	<select required name="expirationYear" id="expirationYear" class="form-control">
								<option value="">Selecione...</option>
                        	</select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 form-group">
                            <label>Nome do dono do cartão</label>
                            {if $signature->get('holder_name') == ''}
                                {assign var="holder_name" value=$signature->get('id_user', true)->get('name')}
                            {else}
                                {assign var="holder_name" value=$signature->get('holder_name')}
                            {/if}
                            <input required type="text" id="holder_name" name="holder_name" value="{$holder_name}" class="form-control">
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>Data de nascimento</label>
                            <input required type="date" id="holder_birthdate" name="holder_birthdate" class="form-control" value="{$signature->get('holder_birthdate')}">
                        </div>
                        <div class="col-sm-4 form-group">
                            <label>CPF do dono do cartão</label>
                            <input required type="text" class="form-control mask-cpf" id="holder_cpf" name="holder_cpf" value="{$signature->get('holder_cpf')}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Ativar!
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="back"></div>