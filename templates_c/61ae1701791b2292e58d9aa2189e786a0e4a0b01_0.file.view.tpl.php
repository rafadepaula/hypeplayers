<?php
/* Smarty version 3.1.28, created on 2017-08-12 17:34:19
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Region\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_598f664b529082_39445843',
  'file_dependency' => 
  array (
    '61ae1701791b2292e58d9aa2189e786a0e4a0b01' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Region\\view.tpl',
      1 => 1502570058,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_598f664b529082_39445843 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">place</i>
            </div>
            <div class="card-content">
                <p class="category">Maior demanda</p>
                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['requestedRegion']->value['name'];?>
</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-info">info</i> <?php echo $_smarty_tpl->tpl_vars['requestedRegion']->value['total'];?>
 membros atendidos.
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">local_shipping</i>
            </div>
            <div class="card-content">
                <p class="category">Total</p>
                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['regionsCount']->value;?>
</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-info">format_list_numbered</i> <?php echo $_smarty_tpl->tpl_vars['regionsCount']->value;?>
 regiões atendidas.
                </div>
            </div>
        </div>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['hasRequests']->value) {?>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <a href="/region/requests/">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">warning</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Solicitações</p>
                        <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['regionRequests']->value;?>
</h3>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            <?php if ($_smarty_tpl->tpl_vars['regionRequests']->value == 1) {?>
                                <i class="material-icons text-danger">error</i> <?php echo $_smarty_tpl->tpl_vars['regionRequests']->value;?>
 região solicitada.
                            <?php } else { ?>
                                <i class="material-icons text-danger">error</i> <?php echo $_smarty_tpl->tpl_vars['regionRequests']->value;?>
 regiões solicitadas.
                            <?php }?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    <?php } else { ?>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">check</i>
                </div>
                <div class="card-content">
                    <p class="category">Solicitações</p>
                    <h3 class="title">0</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-success">check</i> nenhuma solicitação.
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/region/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Intervalo de CEP</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['regions']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_region_0_saved_item = isset($_smarty_tpl->tpl_vars['region']) ? $_smarty_tpl->tpl_vars['region'] : false;
$_smarty_tpl->tpl_vars['region'] = new Smarty_Variable();
$__foreach_region_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_region_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['region']->value) {
$__foreach_region_0_saved_local_item = $_smarty_tpl->tpl_vars['region'];
?>
                        <tr>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['region']->value->get('name');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['region']->value->get('cep_init');?>
 - <?php echo $_smarty_tpl->tpl_vars['region']->value->get('cep_end');?>

                            </td>
                            <td class="td-actions">
                                <a href="/region/edit/<?php echo $_smarty_tpl->tpl_vars['region']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Editar <?php echo $_smarty_tpl->tpl_vars['region']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/region/view/<?php echo $_smarty_tpl->tpl_vars['region']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Visualizar <?php echo $_smarty_tpl->tpl_vars['region']->value->get('name');?>
"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="confirm-link" href="/region/delete/<?php echo $_smarty_tpl->tpl_vars['region']->value->get('id');?>
">
                                    <button type="button" rel="tooltip" title="Deletar <?php echo $_smarty_tpl->tpl_vars['region']->value->get('name');?>
"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['region'] = $__foreach_region_0_saved_local_item;
}
}
if ($__foreach_region_0_saved_item) {
$_smarty_tpl->tpl_vars['region'] = $__foreach_region_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
