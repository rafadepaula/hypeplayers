<?php

	class RentController extends AppController{

		public function view($id = null){
			if(!is_null($id)){
				if(!$this->model->exists('rent', 'id', $id)){
					Viewer::flash(_EXISTS_ERROR, 'e');
					return $this->view();
				}

				if($this->user->get('role') == 'admin'){
					$rent = $this->model->getRent($id);
					$this->viewer->set('rent', $rent);
					return $this->viewer->show('view_one', 'Locação', true, true);
				}else{
					$rent = $this->model->getRent($id);
					$this->viewer->set('rent', $rent);
					return $this->viewer->show('view_member', 'Locação', true, true);
				}
			}

			if($this->user->get('role') == 'member'){
				return $this->history();
			}

			$data = $this->model->view($this->request());
			foreach($data as $key => $value){
				$this->viewer->set($key, $value);
			}

			$this->viewer->addJs('assets/js/rent.js');
			return $this->viewer->show('view', 'Histórico de locações');
		}

		public function plan(){
			if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
			$data = $this->model->plan();
			foreach($data as $key=>$value){
				$this->viewer->set($key, $value);
			}
			$this->viewer->addJs('assets/js/rent.js');
			return $this->viewer->show('plan', 'Planejar envios');
		}

		public function send($id_user, $favorites = null){
			if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
			if($this->request()){
				if($this->model->send($id_user)){
					$notificationModel = new NotificationModel();
					$notificationModel->newRent($id_user);
				}
				return;
			}
			$favorites = $this->model->favoritesIdsToDtos($favorites);
			$this->viewer->set('favorites', $favorites);

			$this->viewer->set('start', date('Y-m-d'));
			$this->viewer->set('end', date('Y-m-d', strtotime('+30 days')));

			$signature = $this->model->getDto('signature', 'id_user', $id_user);
			$this->viewer->set('signature', $signature);

			$lastRent = $this->model->search('rent', '*', array('id_user' => $id_user), 'end DESC', false, 1);
			if(!empty($lastRent)){
				$this->viewer->set('lastRent', $lastRent[0]);
			}

			$this->viewer->set('id_user', $id_user);
			return $this->viewer->show('send', 'Enviar jogo', true, true);
		}

		public function changeStatus($id, $mass = false){
			if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }

            if($mass){
	            $ids = trim($id, '-');
	            if ($this->request()) {
		            $ids = explode('-', $ids);
		            return $this->model->changeStatusMass($ids);
	            }
	            $this->viewer->set('ids', $ids);
	            return $this->viewer->show('changeStatusMass', 'Alterar status em massa', true, true);
            }else {
	            $rent = $this->model->getRent($id);
	            if ($this->request()) {
					if ($this->model->changeStatus($id)) {
						$notificationModel = new NotificationModel();
						$notificationModel->rentStatusUpdated($rent->get('id_user'));
					}
					return;
				}
	            $this->viewer->set('rent', $rent);

	            return $this->viewer->show('changeStatus', 'Alterar status', true, true);
            }
		}

		public function getTitle($id){
			$rent = $this->model->getRent($id);
			$game = $rent->get('id_game', true)->get('name').' ('.$rent->get('platform', true).')';
			$user = $rent->get('id_user', true)->get('name');
			$title = $game.' para '.$user;
			echo $title;
		}

		public function myGames(){
			if($this->user->get('role') != 'member'){
				return $this->siteIndex();
			}

			$id_user = $this->user->get('id');
			$sql = 'select * from rent where status not like "complete%" and id_user = '.$id_user;
			$rents = $this->model->query($sql);
			$rents = $this->model->query2dto($rents, 'rent');
			$this->viewer->set('rents', $rents);
			$signature = $this->model->getDto('signature', 'id_user', $id_user);
			$this->viewer->set('signature', $signature);
			if($signature->get('can_change')){
				$this->viewer->set('changeText', 'Solicitar troca/devolução');
			}else{
				$this->viewer->set('changeText', 'Solicitar devolução');
			}

			$this->viewer->addJs('assets/js/myGames.js');

			return $this->viewer->show('myGames', 'Meus Jogos');
		}

		public function history(){
			if($this->user->get('role') != 'member'){
				return $this->siteIndex();
			}

			$id_user = $this->user->get('id');
			$rents = $this->model->search('rent', '*', array('id_user' => $id_user));
			$this->viewer->set('rents', $rents);

			$this->viewer->addJs('assets/js/myGames.js');

			return $this->viewer->show('history', 'Histórico');
		}

		public function confirmDeliver($id_rent){
			if(!$this->model->exists('rent', 'id', $id_rent)){
				echo _EXISTS_ERROR;
			}
			$rent = $this->model->getRent($id_rent);
			if($rent->get('id_user') != $this->user->get('id')){
				echo _PERMISSION_ERROR;
			}
			$rent->set('status', 'delivered');
			$rent->set('last_modify', date('Y-m-d H:i:s'));
			if(!$this->model->update('rent', $rent, array('id' => $rent->get('id')))){
				echo _INSERT_ERROR;
			}else{
				echo 'ok';
			}

		}

		public function giveBack($id_rent, $type = null){
			if($this->user->get('role') != 'member'){
				return;
			}
			$return = array(
				'return' => '0',
				'type' => 'error',
			);
			$signatureModel = new SignatureModel();
			$signature = $signatureModel->getSignature($this->user->get('id'));
			if($id_rent == 'all'){
				if(!$signature->get('can_change')){
					$return['text'] = 'Você não pode solicitar trocas.';
				}else{
					if(!$this->model->requestChange('all', $signature)){
						$return['text'] = _INSERT_ERROR;
					}else{
						$return = array(
							'return' => '1',
							'type' => 'success',
							'text' => _INSERT_SUCCESS,
						);
					}
				}
				echo json_encode($return);
				return;
			}
			if(!$this->model->exists('rent', 'id', $id_rent)){
				$return['text'] = _EXISTS_ERROR;
			}else{
				$rent = $this->model->getRent($id_rent);
				if($rent->get('id_user') != $this->user->get('id')){
					$return['text'] = _PERMISSION_ERROR;
				}else{
					if($rent->get('status') == 'delivered' || $rent->get('status') == 'delayed'){
						if(is_null($type)){
							$return = array(
								'return' => '1',
								'type' => 'success',
								'can_change' => $signature->get('can_change') ? 1 : 0,
							);
						}else{
							if($type == 'change'){
								if(!$signature->get('can_change')){
									$return['text'] = 'Você não pode solicitar trocas.';
								}else{
									if(!$this->model->requestChange($rent, $signature)){
										$return['text'] = _INSERT_ERROR;
									}else{
										$return = array(
											'return' => '1',
											'type' => 'success',
											'text' => _INSERT_SUCCESS,
										);
									}
								}
							}else{
								if(!$this->model->requestGiveback($rent)){
									$return['text'] = _INSERT_ERROR;
								}else{
									$return = array(
										'return' => '1',
										'type' => 'success',
										'text' => _INSERT_SUCCESS,
									);
								}
							}
						}
					}else{
						$return['text'] = 'Esta locação ainda não está disponível para devolução ou troca.';
					}
				}
			}

			echo json_encode($return);
			return;
 
		}

		public function requests(){
			if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }

			$sql = 'select * from rent where status NOT LIKE "%complete%" AND status != "delivered" and status != "waiting_post" and status != "transit" order by start';
			$rents = $this->model->query($sql);
			$rents = $this->model->query2dto($rents, 'rent');
			$this->viewer->set('rents', $rents);


			$this->viewer->addJs('assets/js/rent.js');
			return $this->viewer->show('requests', 'Solicitações de membros');
		}

		public function canConfirmPost($id_rent, $echo = true){
			if(!$this->model->exists('rent', 'id', $id_rent)){
				if($echo)
					echo _EXISTS_ERROR;
				return _EXISTS_ERROR;;
			}
			$rent = $this->model->getRent($id_rent);
			if($rent->get('id_user') != $this->user->get('id')){
				if($echo)
					echo _PERMISSION_ERROR;
				return _PERMISSION_ERROR;;
			}
			if($rent->get('status') != 'waiting_post'){
				if($echo)
					echo 'Esta locação não está disponível para confirmação de postagem.';
				return 'Esta locação não está disponível para confirmação de postagem.';;
			}
			if($echo)
				echo '1';
			return '1';
		}

		public function confirmPost($id_rent){
			if(!$this->request()){
				echo json_encode(array(
					'return' => 0,
					'text' => 'Erro interno no sistema.',
				));
				return;
			}
			$ret = $this->canConfirmPost($id_rent, false);
			if($ret !== '1'){
				echo json_encode(array(
					'return' => 0,
					'text' => $ret,
				));
				return;
			}
			echo json_encode($this->model->confirmPost($id_rent));
			return;

		}

		public function requestRenew($id_rent){
			if(!$this->model->exists('rent', 'id', $id_rent)){
				echo _EXISTS_ERROR;
				return;
			}
			$rent = $this->model->getRent($id_rent);
			if($rent->get('id_user') != $this->user->get('id')){
				echo _PERMISSION_ERROR;
				return;
			}
			if(!$this->model->canRenew($rent)){
				return false;
			}
			if(!$this->model->requestRenew($rent)){
				echo _INSERT_ERROR;
				return;
			}
			echo 'ok';
			return;
		}

		public function finishRenew($id_rent, $finish = true){
			if(!$this->model->exists('rent', 'id', $id_rent)){
				echo _EXISTS_ERROR;
				return;
			}
			$rent = $this->model->getRent($id_rent);
			if($this->user->get('role') != 'admin'){
				echo _PERMISSION_ERROR;
				return;
			}
			if(!$this->model->finishRenew($rent, $finish)){
				echo _INSERT_ERROR;
				return;
			}
			echo 'ok';
			return;
		}

		public function sendPsUser($id_rent){
			$return = array('return' => 0);
			if(!$this->model->exists('rent', 'id', $id_rent)){
				$return['text'] = _EXISTS_ERROR;
			}else{
				$rent = $this->model->getRent($id_rent);
				if($rent->get('id_user') != $this->user->get('id')){
					$return['text'] = _PERMISSION_ERROR;
				}else{
					$rent->set('ps_user', $rent->get('ps_user').PHP_EOL.$_POST['ps_user']);
					if(!$this->model->update('rent', $rent, array('id' => $rent->get('id')))){
						$notificationModel = new NotificationModel();
						$notificationModel->newPsUser($rent->get('id_user', true)->get('name'),
													  $rent->get('id_user'),
													  $rent->get('id_game', true)->get('name'));
						$return['text'] = _INSERT_ERROR;
					}else{
						$return['return'] = 1;
						$return['text'] = _INSERT_SUCCESS;
					}
				}
			}
			echo json_encode($return);
			return;
		}

	}