<?php
    
    class Region extends DTO {
        public $id;
        public $name;
        public $cep_init;
        public $cep_end;
        
        public $FieldsValidation = array(
            'name'    => 'notEmpty',
            'cep_init'    => 'notEmpty',
            'cep_end'    => 'notEmpty',
        );
        public $FieldsErrors     = array(
            'name'    => 'Informe um nome.',
            'cep_init'    => 'Informe um CEP inicial.',
            'cep_end'    => 'Informe um CEP final.',
        );
        
    }