<div id="modalStatus" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title">Alterar status</h4>
			</div>
			<div class="modal-body" id="modal-body">
				<h4 class="text-center">
					<i class="fa fa-spinner fa-spin"></i>
				</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					Cancelar
				</button>
			</div>
		</div>

	</div>
</div>