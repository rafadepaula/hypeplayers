<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/game/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">keyboard_arrow_left</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <form action="/genre/add" method="post">
                    <div class="row">
                        <div class="col-sm-12 input-group">
                            <input type="text" name="name" class="form-control" placeholder="Cadastrar gênero">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">Cadastrar</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $genres as $genre}
                        <tr>
                            <td>
                                {$genre->get('name')}
                            </td>
                            <td class="td-actions">
                                <a href="/genre/edit/{$genre->get('id')}">
                                    <button type="button" rel="tooltip" title="Editar {$genre->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/genre/view/{$genre->get('id')}">
                                    <button type="button" rel="tooltip" title="Visualizar {$genre->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="confirm-link" href="/genre/delete/{$genre->get('id')}">
                                    <button type="button" rel="tooltip" title="Deletar {$genre->get('name')}"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>