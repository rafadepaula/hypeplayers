<?php

    class HelpModel extends AppModel {
        public $texts = array(
			'account-warnings' => array(
				'title' => 'Avisos da sua conta',
				'content' => 'Aqui você encontra avisos sobre a sua conta, tais quais como: atrasos no pagamento, solicitações, etc.',
			),

	        'last-3-rents' => array(
		        'title' => 'Últimas locações',
		        'content' => 'Neste local, as suas últimas três locações serão disponibilizadas para você visualizar e fazer alterações.'
	        ),

	        'rent-status' => array(
	            'title' => 'Status da locação',
		        'content' => 'Pequeno texto descrevendo qual o estado atual da locação de seu game.',
	        ),

	        'view-rent' => array(
				'title' => 'Visualizar locação',
		        'content' => 'Clicando aqui, você pode visualizar detalhes da locação.',
	        ),

	        'view-game' => array(
	            'title' => 'Visualizar jogo',
		        'content' => 'Ao selecionar a imagem de um jogo, você poderá visualizar sua descrição e mais imagens sobre ele.',
	        ),

	        'give-back' => array(
	            'title' => 'Troca/devolução',
		        'content' => 'Utilize este botão para devolver seu jogo ou então solicitar uma troca, caso seu plano permitir.',
	        ),

	        'request-renew' => array(
	            'title' => 'Renovação',
		        'content' => 'Botão projetado para você solicitar uma renovação e ficar com seu jogo mais 30 dias.',
	        ),

	        'confirm-post' => array(
	            'title' => 'Confirmar postagem',
		        'content' => 'Caso você devolver seu game via correios, aqui é possível confirmar a postagem dele.',
	        ),

	        'favorites-history' => array(
		        'title' => 'Favoritos e histórico',
		        'content' => 'Uma área que resume para você as suas preferências e suas últimas 10 locações.'
	        ),

	        'search-library' => array(
	        	'title' => 'Busca de jogos',
		        'content' => 'Pesquise os jogos por nome, plataforma ou gênero',
	        ),

	        'game-container' => array(
	        	'title' => 'Jogos',
		        'content' => 'Os jogos da biblioteca serão listados nesta área do site',
	        ),

	        'game-cover' => array(
	        	'title' => 'Detalhes',
		        'content' => 'Você pode visualizar mais detalhes do game clicando na sua capa',
	        ),

	        'game-add' => array(
	        	'title' => 'Preferências',
		        'content' => 'Clicando em "Adicionar", você coloca este jogo na sua lista de favoritos. Você precisa adicionar, no mínimo, 10 jogos na sua lista',
	        ),

	        'finish-add' => array(
	        	'title' => 'Salvar preferências',
		        'content' => 'Ao finalizar a sua lista de favoritos, a opção "Salvar alterações" ficará disponível na parte inferior da sua tela.',
		        'orphan' => true
	        ),

	        'current-plan' => array(
	        	'title' => 'Plano atual',
		        'content' => 'Este é o seu plano atual, escolhido por você.',
	        ),

	        'change-plan' => array(
	        	'title' => 'Troca de plano',
		        'content' => 'Aqui você pode solicitar uma troca de plano, para subir ou descer de nível',
	        ),

	        'cant-change' => array(
	        	'title' => 'Troca de plano',
		        'content' => 'Atualmente, você não pode realizar a troca de seu plano. Este é o motivo atual para isso acontecer.',
	        ),

	        'payment-history' => array(
	        	'title' => 'Histórico de pagamento',
		        'content' => 'Todos os pagamentos realizados por você estão listados nesta tabela.',
	        ),

	        'change-card' => array(
	        	'title' => 'Trocar cartão',
		        'content' => 'Caso queira alterar sua forma de pagamento, você pode clicar neste botão',
	        ),

			'cancel-signature' => array(
				'title' => 'Cancelar assinatura',
				'content' => 'Esta é a opção para você solicitar o cancelamento da sua assinatura.',
			),

	        'cant-cancel' => array(
	        	'title' => 'Cancelar assinatura',
		        'content' => 'Atualmente, você não pode cancelar sua assinatura. Este é o motivo para isso acontecer.',
	        ),

	        'game-table' => array(
				'title' => 'Meus jogos',
		        'content' => 'Aqui são listados os jogos que você está em posse atualmente',
	        ),

			'rent-view' => array(
				'title' => 'Visualizar',
				'content' => 'Clicando aqui você pode ver mais detalhes desta locação',
			),

	        'rent-comment' => array(
	        	'title' => 'Observação',
		        'content' => 'Este botão permite você enviar um comentário sobre esta locação à administração',
	        ),

	        'rent-change' => array(
	        	'title' => 'Troca/devolução',
		        'content' => 'Esta opção permite você trocar (se seu plano permitir) ou devolver este jogo',
	        ),

	        'rent-renew' => array(
	        	'title' => 'Renovação',
		        'content' => 'Clicando aqui, você pode renovar este jogo por mais um mês',
	        ),

	        'rent-waiting' => array(
				'title' => 'Confirmar postagem',
		        'content' => 'Para confirmar que você postou seu jogo nos correios, clique aqui',
	        ),

	        'favorites-table' => array(
	        	'title' => 'Minhas preferências',
		        'content' => 'As preferências adicionadas a partir da Biblioteca de Jogos são listadas aqui',
	        ),

	        'favorites-add' => array(
	        	'title' => 'Adicionar preferências',
		        'content' => 'Você pode clicar aqui para ser redirecionado à Biblioteca e adicionar mais preferências.',
	        ),

	        'favorites-order' => array(
	        	'title' => 'Prioridades',
		        'content' => 'Clique e arraste para cima/baixo esta coluna para alterar a prioridade deste jogo. Quanto mais próxima de 1, mais importante este jogo é para você.',
	        ),

	        'favorites-remove' => array(
	        	'title' => 'Remover',
		        'content' => 'Caso queira remover este jogo de suas preferências, clique neste botão.',
	        ),

	        'history-table' => array(
	        	'title' => 'Histórico',
		        'content' => 'Todos os jogos alugados por você até hoje estão listados aqui',
	        ),

	        'address-table' => array(
	        	'title' => 'Endereços',
		        'content' => 'Caso você queira receber, eventualmente, jogos em outro endereço, você poderá cadastrá-los nesta página.',
	        ),

	        'address-remove' => array(
	        	'title' => 'Remover',
		        'content' => 'Para remover um endereço, você pode clicar neste botão.',
	        ),

	        'address-toggle' => array(
	        	'title' => 'Alterar padrão',
		        'content' => 'Clique aqui para alterar o endereço padrão. Um endereço listado em <b>negrito</b> é considerado como padrão para a entrega.',
	        )
        );

	    public function makeSteps($helps){
        	$steps = array();

			foreach($helps as $help){
				if(isset($this->texts[$help]) && !$this->alreadyAdd($steps, $help)){
					$text = $this->texts[$help];
					$text['element'] = '.help[data-help="' . $help . '"]:first';
					$steps[] = $text;
				}
			}
			echo json_encode($steps, true);
	    }

	    public function alreadyAdd($steps, $help){
	    	$element = '.help[data-help="' . $help . '"]:first';
	    	foreach($steps as $step){
	    		if($step['element'] == $element)
	    			return true;
		    }
		    return false;
	    }
	}