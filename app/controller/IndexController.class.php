<?php

    class IndexController extends AppController{
        public function view($emailSent = false){
	        #banners
	        $bannerController = new BannerController();
	        $banners = $bannerController->indexBanners();
	        $this->viewer->set('banners', $banners);

	        #games
	        $gameController = new GameController();
	        $games = $gameController->indexGames();
	        $gamesArray = array(
	        	'ps4' => array($games[0][0][0])
	        );
	        $this->viewer->set('gamesArray', $gamesArray);

	        #featured
	        $featuredController = new FeaturedController();
	        $featureds = $featuredController->indexFeatureds();
	        $this->viewer->set('featureds', $featureds);

	        $this->viewer->set('emailSent', $emailSent);

	        $this->viewer->show('index', 'HypePlayers', false, true);
        }

        public function contact(){
        	if($this->request()){
		        require_once 'base/plugins/phpmailer/PHPMailerAutoload.php';

		        $email = new PHPMailer;

		        $email->isSMTP();
		        $email->SMTPDebug  = 1;
		        $email->Host       = 'mail.hypeplayers.com.br';
		        $email->SMTPAuth   = true;
		        $email->Username   = 'webmaster@hypeplayers.com.br';
		        $email->Password   = 'Zs9w63Q0oy';
		        $email->SMTPSecure = false;
		        $email->Port       = 26;

		        $email->From     = $_POST['email'];
		        $email->FromName = $_POST['name'];

		        $email->addBCC('contato@hypeplayers.com.br', 'HypePlayers');


		        $email->isHTML(true);

		        $email->Subject = 'HypePlayers - Novo contato do site';

		        $html = '
		            <b>Nome:</b>'.$_POST['name'].'<br>
		            <b>E-mail:</b>'.$_POST['email'].'<br>
		            <b>Mensagem:</b><br>
		            '.$_POST['message'].'
		        ';
		        $email->Body    = $html;
		        $email->CharSet = 'UTF-8';

		        $email->send();
		        $this->view(true);
	        }
        }

        public function libraryModal(){
	        #games
	        $gameController = new GameController();
	        $games = $gameController->indexGames();
	        $gamesArray = array(
		        'ps4' => $games[0][0],
		        'xone' => $games[0][1],
		        'switch' => $games[0][2],
	        );
	        $pagesArray = array(
		        'ps4' => $games[1][0],
		        'xone' => $games[1][1],
		        'switch' => $games[1][2],
	        );
	        $this->viewer->set('gamesArray', $gamesArray);
	        $this->viewer->set('pagesArray', $pagesArray);

        	echo $this->viewer->show('library_modal', 'Jogos', false, true);
        }
    }