<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <form action="/address/add/" method="post">
                    <div class="row">
                        <div class="col-sm-12 input-group">
                            <input type="text" name="name" class="form-control" placeholder="Nome do gênero" value="{$genre->get('name')}">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="submit">Salvar</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>