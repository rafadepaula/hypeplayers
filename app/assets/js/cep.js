$(function(){
	$('#cep').on('change', function(){
	    $.ajax({
	        url: 'https://api.postmon.com.br/v1/cep/' + $('#cep').val(),
	    }).done(function(data){
	        $('#cidade').val(data.cidade);
	        $('#estado').val(data.estado_info.nome);
	        $('#cepEnd').val($('#cep').val());
	        if(typeof data.logradouro != 'undefined'){
	            $('#rua').val(data.logradouro);
	            $('#rua').attr('readonly', true);
	        }else{
	            $('#rua').removeAttr('readonly');
	        }
	        if(typeof data.bairro != 'undefined' && data.bairro != ''){
	            $('#bairro').val(data.bairro);
	            $('#bairro').attr('readonly', true);
	        }else{
	            $('#bairro').removeAttr('readonly');
	        }
	    });
	});
})