<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
	<base href="/app/">
	<title>Junte-se a nós :: HypePlayers</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

</head>

<body>
	<div class="image-container set-full-height" style="background-image: url('assets/img/cadastro-back.jpg')">

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="green" id="wizardProfile">
		                    <form action="/member/join" method="post" enctype="multipart/form-data" class="ignore-wait">
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title">
		                        	   Junte-se a nós!
		                        	</h3>
									<h5>Participe de nosso clube de assinatura e fique por dentro dos maiores lançamentos de games.</h5>
		                    	</div>
								<div class="wizard-navigation">
									<ul>
			                            <li><a href="#perfil" data-toggle="tab">Perfil</a></li>
                                        <li><a href="#region" data-toggle="tab">Região</a></li>
			                            <li><a href="#endereco" data-toggle="tab">Endereço (casa)</a></li>
			                            <li><a href="#entrega" data-toggle="tab">para entrega...</a></li>
			                            <li><a href="#planos" data-toggle="tab">Planos</a></li>
			                        </ul>
								</div>

		                        <div class="tab-content">
                                    <div class="tab-pane" id="perfil">
                                        <h4 class="info-text"> Conte-nos um pouco sobre você </h4>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Seu nome <small>(*)</small></label>
                                                        <input name="name" type="text" class="form-control" id="profile_name">
                                                    </div>
                                                </div>
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Seu sobrenome <small>(*)</small></label>
                                                        <input name="last_name" type="text" class="form-control" id="profile_lastname">
                                                    </div>
                                                </div>
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">date_range</i>
													</span>
                                                    <div class="form-group label-floating is-focused">
                                                        <label class="control-label"> Data de nascimento <small>(*)</small></label>
                                                        <input name="birthday" type="date" class="form-control mask-date" id="profile_birthday">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">fingerprint</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">RG <small>(*)</small></label>
                                                        <input name="rg" type="text" class="form-control" id="profile_rg">
                                                    </div>
                                                </div>

                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">payment</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">CPF <small>(*)</small></label>
                                                        <input name="cpf" type="text" class="form-control mask-cpf" id="profile_cpf">
                                                    </div>
                                                </div>

                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Celular <small>(*)</small></label>
                                                        <input name="phone" type="text" class="form-control mask-phone" id="profile_phone">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Senha <small>(mínimo 7 caracteres)</small></label>
                                                        <input name="password" type="password" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">lock_outline</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Confirmação</label>
                                                        <input name="passwordConfirm" type="password" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Email <small>(*)</small></label>
                                                        <input name="email" type="email" class="form-control" id="profile_email">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="region">
		                              	<div class="row">
		                                	<h4 class="info-text">Digite seu CEP para nós sabermos se conseguimos atender você</h4>
		                                	<div class="col-sm-10 col-sm-offset-1">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">place</i>
													</span>
													<div class="form-group label-floating">
			                                            <label class="control-label">CEP <small>(obrigatório)</small></label>
			                                            <input name="cep" id="cep" type="text" class="form-control input-lg mask-cep">
			                                        </div>
												</div>
		                                	</div>
		                                	<div class="col-sm-10 col-sm-offset-1 text-center" style="display: none" id="naoAceito">
		                                		<h3 class="text-danger">
		                                			<i class="material-icons" style="font-size: 48px;">sentiment_very_dissatisfied</i>
		                                			<p>Desculpe-nos</p>
		                                		</h3>
                                                <h5>A sua região ainda não é atendida pelo HypePlayers. Entre em contato conosco via e-mail: <a href="mailto:contato@hypeplayers.com.br">contato@hypeplayers.com.br</a> ou Facebook que encontraremos uma solução.</h5>
		                                	</div>
		                            	</div>
		                            </div>
		                            <div class="tab-pane" id="endereco">
		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <h4 class="info-text"> Só mais um pouquinho... </h4>
                                                <p class="text-center">
                                                    Nos informe o endereço de sua residência, por favor.
                                                </p>
		                                    </div>
		                                    <div class="col-sm-5 col-sm-offset-1">
	                                        	<div class="form-group label-floating">
	                                        		<label class="control-label">Rua</label>
	                                    			<input name="street" type="text" value=" " id="rua" class="form-control">
	                                        	</div>
		                                    </div>
		                                    <div class="col-sm-2">
												<div class="form-group label-floating">
	                                        		<label class="control-label">Nº</label>
	                                    			<input name="number" type="text" id="numero" class="form-control">
	                                        	</div>
		                                    </div>
		                                    <div class="col-sm-3">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Bairro</label>
		                                            <input name="neighborhood" type="text" value=" " id="bairro" class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-2  col-sm-offset-1">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Complemento</label>
		                                            <input name="complement" type="text" class="form-control" id="complemento">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-3">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Cidade</label>
		                                            <input name="city" type="text" id="cidade" value=" " readonly class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-3">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Estado</label>
	                                            	<input name="state" type="text" id="estado" value=" " readonly class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-2">
												<div class="form-group label-floating">
		                                            <label class="control-label">CEP</label>
	                                            	<input type="text" readonly value=" " class="form-control" id="cepEnd">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
                                    <div class="tab-pane" id="entrega">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="info-text">
                                                    E onde você quer receber seus jogos?
                                                </h4>
                                                <div class="col-sm-10 col-sm-offset-1 text-center">
                                                    <button type="button" class="btn btn-info" id="copyAddress">
                                                        Quero receber em casa!
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-sm-offset-1">
                                                <div class="form-group">
                                                    <label class="control-label">CEP</label>
                                                    <input type="text" class="form-control mask-cep" id="cepEndEnt" name="cepEnt" required>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Rua</label>
                                                    <input name="streetEnt" type="text" value=" " id="ruaEnt" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <label class="control-label">Nº</label>
                                                    <input name="numberEnt" type="text" id="numeroEnt" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 col-sm-offset-1">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Bairro</label>
                                                    <input name="neighborhoodEnt" type="text" value=" " id="bairroEnt" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2 ">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Complemento</label>
                                                    <input name="complementEnt" type="text" class="form-control" id="complementoEnt">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Cidade</label>
                                                    <input name="cityEnt" type="text" id="cidadeEnt" value=" " readonly class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Estado</label>
                                                    <input name="stateEnt" type="text" id="estadoEnt" value=" " readonly class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
		                            <div class="tab-pane" id="planos">
		                            	<div class="row" id="escolherPlanos">
	                            			<div class="col-sm-10 col-sm-offset-1 text-center">
	                            				<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalPlanos">
	                            					Entenda como funciona cada plano
	                            				</button>
	                            			</div>
	                            			<div class="col-sm-10 col-sm-offset-1 text-center">
	                            				<div class="col-sm-4">
		                                            <div class="choice choice-bronze" data-toggle="wizard-radio">
		                                                <input type="radio" name="level" value="1">
		                                                <div class="icon">
		                                                    <i class="fa fa-trophy"></i>
		                                                </div>
		                                                <h6>Plano Bronze</h6>
		                                                R$39,90/mês
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice choice-silver" data-toggle="wizard-radio">
		                                                <input type="radio" name="level" value="2">
		                                                <div class="icon">
		                                                    <i class="fa fa-trophy"></i>
		                                                </div>
		                                                <h6>Plano Prata</h6>
		                                                R$69,90/mês
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice choice-gold" data-toggle="wizard-radio">
		                                                <input type="radio" name="level" value="3">
		                                                <div class="icon">
		                                                    <i class="fa fa-trophy"></i>
		                                                </div>
		                                                <h6>Plano Ouro</h6>
		                                                R$99,90/mês
		                                            </div>
		                                        </div>
                                                <div class="col-sm-12 text-center" style="margin-top: 15px">
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" required name="terms" value="on">
                                                        Eu aceito os <a href="assets/files/termos.pdf" target="_blank">termos e condições.</a>
                                                    </label>
                                                </div>
	                                        </div>
		                            	</div>
		                            	<div class="row" id="enviando" style="display: none;">
		                            		<div class="col-sm-10 col-sm-offset-1 text-center">
		                                		<h3 class="text-info">
		                                			<i class="material-icons spinner" style="font-size: 48px;">refresh</i>
		                                			<p>Enviando...</p>
		                                		</h3>
		                                		<h5>Aguenta aí! Estamos realizando seu cadastro...</h5>
		                                	</div>
		                            	</div>
		                            	<div class="row" id="cadastrado" style="display: none;">
		                            		<div class="col-sm-10 col-sm-offset-1 text-center">
		                                		<h3 class="text-success">
		                                			<i class="material-icons" style="font-size: 48px;">check</i>
		                                			<p>Feito!</p>
		                                		</h3>
		                                		<h5>Lhe enviamos um e-mail para você confirmar o seu cadastro!</h5>
		                                		<p>Ficamos muito felizes em tê-lo conosco!</p>
		                                	</div>
		                            	</div>
		                            	<div class="row" id="erro" style="display: none;">
		                            		<div class="col-sm-10 col-sm-offset-1">
		                                		<h3 class="text-danger text-center">
		                                			<i class="material-icons" style="font-size: 48px;">error</i>
		                                			<p>Ops!</p>
		                                		</h3>
		                                		<h5 class="text-center">Parece que houveram alguns probemas... Dê uma olhadinha:</h5>
		                                		<ul id="problemas"></ul>
		                                	</div>
		                            	</div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-fill btn-success btn-wd' name='next' value='Próximo' />
		                                <input type='submit' id="finish" class='btn btn-finish btn-fill btn-success btn-wd' name='finish' value='Pronto!' />
		                            </div>

		                            <div class="pull-left">
		                                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' value='Anterior' />
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                        <input type="hidden" name="levelToPost">
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	        </div><!-- end row -->
	    </div> <!--  big container -->

	    <div class="footer">
	        <div class="container text-center">
	             HypePlayers
	        </div>
	    </div>
	</div>


<div id="modalPlanos" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Planos de assinatura</h4>
			</div>
			<div class="modal-body">
				<table class="table table-hovered table-striped">
					<thead>
						<tr>
							<th>-</th>
							<th>Plano Bronze</th>
							<th>Plano Prata</th>
							<th>Plano Ouro</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Quantidade de jogos</td>
							<td>1 por mês</td>
							<td>Até 3 por mês</td>
							<td>Até 5 por mês</td>
						</tr>
						<tr>
							<td>Trocas de jogos</td>
							<td>Nenhuma</td>
							<td>Troca um jogo</td>
							<td>Troca três jogos</td>
						</tr>
						<tr>
							<td>Posse simultânea</td>
							<td>Não, apenas um jogo</td>
							<td>Até dois jogos</td>
							<td>Até dois jogos</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
			</div>
		</div>

	</div>
</div>

</body>
	<!--   Core JS Files   -->
    <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/jquery.bootstrap.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="assets/js/material-bootstrap-wizard.js"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script src="assets/js/jquery.validate.min.js"></script>
    <script src="../base/js/scripts.js"></script>
	<script src="plugins/input-mask/jquery.inputmask.js"></script>
	<script src="plugins/input-mask/jquery.mask.init.js"></script>


</html>
