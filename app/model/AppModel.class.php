<?php

    class AppModel extends Model {

        public $pagseguro;

        public function __construct($debug = true, $database = _DATABASE, $user = _DATABASE_USER, $password = _DATABASE_PASSWORD, $host = _HOST){
            parent::__construct($debug, $database, $user, $password, $host);

            $this->pagseguro = new PagseguroModel();
        }

        public function login(){
            $cond = array(_LOGIN_TYPE  => $_POST[_LOGIN_TYPE],
                          'conscond1' => 'AND',
                          'password'  => $_POST['password']);
            $data = $this->search('user', '*', $cond);
            
            if (count($data) == 1) {
                $user = $data[0];
                if($user->get('role') == 'member'){
            		$member = $this->getDto('member', 'id_user', $user->get('id'));

            		if(!$member->get('activated'))
            			return 'activated';
            		if($member->get('deactivated'))
            			return 'deactivated';
                }
                $_SESSION['user'] = serialize($user);
                $_SESSION['logged'] = true;
        
                return true;
            } else {
                return false;
            }
        }

        public static function arrayToXml($data, $rootNodeName = 'root', $xml = null) {
             
            // desligamos essa opção para evitar bugs
            if (ini_get('zend.ze1_compatibility_mode') == 1) {
                ini_set('zend.ze1_compatibility_mode', 0);
            }
         
            if ($xml == null) {
                $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
            }
         
            // faz o loop no array
            foreach ($data as $key => $value) {
                // se for indice numerico ele renomeia o indice
                if (is_numeric($key)) {
                    $key = "unknownNode_" . (string) $key;
                }
         
                // substituir qualquer coisa não alfa numérico
                $key = preg_replace('/[^a-z]/i', '', $key);
         
                 
                if (is_array($value)) {
                    $node = $xml->addChild($key);
                    AppModel::arrayToXml($value, $rootNodeName, $node);
                } else {
                    $value = htmlentities($value);
                    $xml->addChild($key, $value);
                }
            }
            return $xml->asXML();
        }

        public static function xmlToArray($data){
            $xml = simplexml_load_string($data, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json,TRUE);
            return $array;
        }
    }
