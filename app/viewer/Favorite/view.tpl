
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/game/library/" class="pull-right card-header-btn help" data-help="favorites-add">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable-favorite help" data-help="favorites-table">
                    <thead>
                        <tr>
                            <th>Prioridade</th>
                            <th>Jogo</th>
                            <th>Plataforma</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $favorites as $favorite}
                        <tr>
                            <td class="drag-n-drop help" data-help="favorites-order">
                                {$favorite->get('position') + 1}
                            </td>
                            <td>
                                {$favorite->get('id_game', true)->get('name')}
                            </td>
                            <td>
                                {$favorite->get('platform', true)}
                            </td>
                            <td class="td-actions">
                                <a class="confirm-link help" href="/favorite/delete/{$favorite->get('position')}" data-help="favorites-remove">
                                    <button type="button" rel="tooltip" title="Remover {$favorite->get('id_game', true)->get('name')} da lista"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>