<?php
    
    class Plan_change extends DTO {
        public $id;
        public $id_user;
        public $from_level;
        public $to_level;

        public $FieldsMasks = array(
            'id_user' => array('getDto', ['user'])
        );
    }