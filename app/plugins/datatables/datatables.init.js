$(function(){
	opts = {
        "dom": 'Bfrtip',
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
        "iDisplayLength": 25,
        "aaSorting": [],
        "serverside" : false,
        "language": {
            "url": "plugins/datatables/datatables.portuguese.lang"
        },
        // "aoColumnDefs": [
        //     { 'bSortable': false, 'aTargets': [ 1 ] }
        // ],
        'language':{
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
        },
        'buttons': {}
    };

    $('.datatable').each(function(){
        table = $(this).DataTable(opts);
        table.buttons().destroy();
    });

});