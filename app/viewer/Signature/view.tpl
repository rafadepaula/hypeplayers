<div class="row">
	<div class="col-sm-8">
		{if $signature->get('status') == 'Cancel_requested'}
			<div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">cancel</i>
                </div>
                <div class="card-content">
                    <p class="category">Cancelamento solicitado</p>
                    <h4 class="title">Você solicitou o cancelamento de sua assinatura.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Em breve iremos processar o seu cancelamento. Você irá receber um e-mail
                        quando a ação estiver completa.
                    </div>
                </div>
            </div>
		{/if}
        {if $signature->get('status') == 'Change_requested'}
			<div class="card card-stats">
				<div class="card-header" data-background-color="orange">
					<i class="material-icons">update</i>
				</div>
				<div class="card-content">
					<p class="category">Troca de plano solicitada</p>
					<h4 class="title">Você solicitou troca de seu plano.</h4>
				</div>
				<div class="card-footer">
					<div class="stats">
						Em breve iremos processar a sua solicitação. Você irá receber um e-mail
						quando a ação estiver completa.
					</div>
				</div>
			</div>
        {/if}
		{if $signature->get('status') == 'Canceled'}
			<a href="/signature/activate/0/1">
	            <div class="card card-stats">
	                <div class="card-header" data-background-color="orange">
	                    <i class="material-icons">cancel</i>
	                </div>
	                <div class="card-content">
	                    <p class="category">Assinatura cancelada <i class="fa fa-frown-o"></i></p>
	                    <h4 class="title">Sua assinatura está cancelada. Clique para ativar novamente</h4>
	                </div>
	                <div class="card-footer">
	                    <div class="stats">
	                        Parece que você optou por cancelar sua ssinatura.
	                        Clique aqui para voltar para o clube!
	                    </div>
	                </div>
	            </div>
	        </a>
		{/if}
        {if $signature->get('status') == 'Suspended'}
			<a href="/signature/activate/0/1">
				<div class="card card-stats">
					<div class="card-header" data-background-color="orange">
						<i class="material-icons">cancel</i>
					</div>
					<div class="card-content">
						<p class="category">Assinatura suspensa <i class="fa fa-frown-o"></i></p>
						<h4 class="title">Sua assinatura está cancelada. Clique para ativar novamente</h4>
					</div>
					<div class="card-footer">
						<div class="stats">
							O sistema não identificou o pagamento de sua assinatura :(.
							Clique aqui para voltar para o clube!
						</div>
					</div>
				</div>
			</a>
        {/if}
		{if $signature->get('status') == 'Preactivated'}
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">access_time</i>
                </div>
                <div class="card-content">
                    <p class="category">Processando pagamento</p>
                    <h4 class="title">Quase lá! O seu pagamento está sendo processado.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        A solicitação de assinatura foi registrada. Agora, nosso sistema está
                        se comunicando com a empresa responsável pelo processamento do cartão
                        de crédito para liberar o seu acesso!
                    </div>
                </div>
            </div>
		{/if}
		{if $signature->get('status') == 'Pending'}
			<a href="/signature/activate">
	            <div class="card card-stats">
	                <div class="card-header" data-background-color="red">
	                    <i class="material-icons">money_off</i>
	                </div>
	                <div class="card-content">
	                    <p class="category">Pagamento pendente</p>
	                    <h4 class="title">Clique para ativar o seu plano</h4>
	                </div>
	                <div class="card-footer">
	                    <div class="stats">
	                        Você está com pagamento pendente! Ative sua conta efetivando o pagamento de sua assinatura.
	                    </div>
	                </div>
	            </div>
	        </a>
		{/if}
		{if $signature->get('status') == 'Taxed'}
			<div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Você recebeu uma multa</p>
                    <h4 class="title">Clique para acessar a página de multas e reativar seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Você recebeu uma multa. Acesse a página de multas para regularizar a situação.
                    </div>
                </div>
            </div>
		{/if}
		{if $signature->activeStatus()}
	        <div class="card card-stats help" data-help="current-plan">
	            <div class="card-header" data-background-color="green">
	                <i class="material-icons">done</i>
	            </div>
	            <div class="card-content">
	                <p class="category">Plano atual</p>
	                <h3 class="title">{$signature->get('level', true)}</h3>
	            </div>
	        </div>
	        <h3>Solicitar troca de plano</h3>
	        {if $canChange['canChange']}
				<div class="col-sm-6 change-plan help" style="cursor: pointer" data-to-level="{$levelsNumbers[0]}" data-help="change-plan">
			        <div class="card card-stats">
			            <div class="card-header" data-background-color="blue">
			                <i class="material-icons">swap_horiz</i>
			            </div>
			            <div class="card-content">
			                <p class="category">Trocar plano</p>
			                <h3 class="title">{$levels[0]}</h3>
			            </div>
			            <div class="card-footer">
			                <div class="stats">
			                    Clique para trocar para {$levels[0]}
			                </div>
			            </div>
			        </div>
			    </div>

			    <div class="col-sm-6 change-plan" style="cursor: pointer" class="change-plan" data-to-level="{$levelsNumbers[1]}">
			        <div class="card card-stats">
			            <div class="card-header" data-background-color="blue">
			                <i class="material-icons">swap_horiz</i>
			            </div>
			            <div class="card-content">
			                <p class="category">Trocar plano</p>
			                <h3 class="title">{$levels[1]}</h3>
			            </div>
			            <div class="card-footer">
			                <div class="stats">
			                    Clique para trocar para {$levels[1]}
			                </div>
			            </div>
			        </div>
			    </div>
		    {else}
		    	<div class="col-sm-12 help" data-help="cant-change-plan">
		    		<div class="card card-stats">
		    			<div class="card-content">
				    		<b>Você não pode solicitar troca de plano:</b> <br>
				    		{$canChange['error']}
				    	</div>
		    		</div>
		    	</div>
		    {/if}
			<div class="col-sm-12">
				<hr>
			</div>
	    {/if}

	    	<div class="card help" data-help="payment-history">
	    		<div class="card-header" data-background-color="green">
		    		<h4>Histórico de pagamentos</h4>
		    	</div>
		    	<div class="card-content">
	        		<div class="table-responsive">
		        		<table class="table table-striped datatable">
		        			<thead>
		        				<tr>
		                            <th>Data</th>
		                            <th>Valor</th>
		                            <th>Referência interna</th>
		                        </tr>
		        			</thead>
		                    <tbody>
		                        {foreach $payments as $payment}
			                        <tr>
			                            <td>
			                            	{$payment->get('date', true)}
			                            </td>
			                            <td>
			                            	R${$payment->get('value', true)}
			                            </td>
			                            <td>
			                            	{$payment->get('code')}
			                            </td>
			                        </tr>
			                    {/foreach}
		                    </tbody>
		                </table>
	        		</div>
	        	</div>
	        </div>
        </div>

	<div class="col-sm-4">
		<div class="card card-profile">
			<div class="card-avatar">
				<img class="img" src="{$picture}" />
			</div>

			<div class="content">
				<h6 class="category text-gray">
					{$signature->get('level', true)} (R$ {$signature->get('value', true)})
				</h6>
				<h4 class="card-title">{$actualUser->get('name')}</h4>
				<p class="card-content">
					Membro desde {$signature->get('since', true)} <br>
				</p>
				{if $signature->get('status') != 'Cancel_requested' && $signature->get('status') != 'Cancel'}
					{if $signature->activeStatus()}
						<p class="card-content help" data-help="change-card">
							<a href="/signature/changeCard" class="btn btn-info btn-round">Mudar cartão de pagamento</a>
						</p>
                    {/if}
                    <hr>
                    <div class="col-sm-12" style="margin-bottom: 25px">
                        {if $canCancel['canCancel']}
                            <a href="/signature/requestCancel" class="btn btn-round help" data-help="cancel-signature">Cancelar assinatura</a>
                        {else}
                            <p class="card-content help" data-help="cant-cancel">
                                <u>Você não pode cancelar sua assinatura:</u> {$canCancel['error']}
                            </p>
                        {/if}
                    </div>
				{/if}
			</div>
		</div>
	</div>

</div>