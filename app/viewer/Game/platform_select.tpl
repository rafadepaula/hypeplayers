<div id="platform-select" style="display: none">
	<label class="checkbox-inline" id="platform-ps4" style="display: none;">
		<input type="checkbox" class="platform-select" name="platform[]" value="ps4"> PS4
	</label>
	<label class="checkbox-inline" id="platform-xone" style="display: none;">
		<input type="checkbox" class="platform-select" name="platform[]" value="xone"> XBOX One
	</label>
	<label class="checkbox-inline" id="platform-switch" style="display: none;">
		<input type="checkbox" class="platform-select" name="platform[]" value="switch"> Nintendo Switch
	</label>
	<br>
</div>