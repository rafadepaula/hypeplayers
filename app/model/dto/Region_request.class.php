<?php
    
    class Region_request extends DTO {
        public $id;
        public $cep;
        public $location;
        public $date;
        
        public $FieldsValidation = array(
            'cep'    => 'notEmpty',
            'location'    => 'notEmpty',
            'date'    => 'validDatetime',
        );
        
        public $FieldsErrors     = array(
            'cep'    => 'CEP inválido.',
            'locaton'    => 'Localização inválida.',
            'date'    => 'Data inválida.',
        );

        public $FieldsMasks      = array(
            'date' => 'datetimeMask',
        );
        
    }