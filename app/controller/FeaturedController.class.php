<?php

    class FeaturedController extends AppController{

        public function index(){
            $userController = new UserController();
            return $userController->adminIndex();
        }

        public function add(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->index();
            }
            if($this->request()){
                if($this->model->save()){
                    $id = $this->model->lastInserted('featured');
                    return $this->view($id);
                }else{
                    unset($_POST);
                    return $this->add();
                }
            }

            $games = $this->model->games();
			$this->viewer->set('games', $games);

            $this->viewer->addJs('assets/js/featured.js');
            return $this->viewer->show('add', 'Cadastrar jogo em destaque');
        }

	    public function edit($id){
		    if($this->user->get('role') != 'admin'){
			    Viewer::flash(_PERMISSION_ERROR, 'e');
			    return $this->index();
		    }
		    if(!$this->model->exists('featured', 'id', $id)){
			    Viewer::flash(_EXISTS_ERROR, 'e');
			    return $this->index();
		    }
		    if($this->request()){
			    if($this->model->save($id)){
				    return $this->view($id);
			    }else{
				    unset($_POST);
				    return $this->add();
			    }
		    }

		    $games = $this->model->games();
		    $this->viewer->set('games', $games);

		    $featured = $this->model->getFeatured($id);
		    $this->viewer->set('featured', $featured);

		    $this->viewer->addJs('assets/js/featured.js');
		    return $this->viewer->show('edit', 'Editar destaque');
	    }

        public function view($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->index();
            }
            if(!$this->model->exists('featured', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            $featured = $this->model->getFeatured($id);
            $this->viewer->set('featured', $featured);

            return $this->viewer->show('view_one', $featured->get('name'));
        }

        public function delete($id){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->index();
            }
            if (!$this->model->exists('featured', 'id', $id)) {
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            if ($this->model->delete('featured', array('id' => $id))) {
                Viewer::flash(_DELETE_SUCCESS, 's');
            } else {
                Viewer::flash(_DELETE_ERROR, 'e');
            }
            return $this->index();
        }


        public function indexFeatureds(){
            return $this->model->search('featured', '*', false, 'position');
        }

        public function reorder($total = false){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->index();
            }
            if($this->user->get('role') != 'admin'){
                return;
            }
            if($total){
                return $this->model->reorder($total);
            }else{
                if(!isset($_POST['diff'])){
                    echo "Diferenças não encontradas.";
                    return;
                }
                if(empty($_POST['diff'])){
                    echo 1;
                    return;
                }
                $diff = $_POST['diff'];
                $sql = $this->model->reorderSql($diff);
                $this->model->sql($sql);
                echo 1;
            }
            return;
        }

    }