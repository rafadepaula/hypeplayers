<?php
    /**
     * Check the post to see if any password was posted
     * then, crypt it.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    if(isset($_POST['password'])){
        if(trim($_POST['password']) != '')
            $_POST['password'] = crypt($_POST['password'], '$2a$12$'._PASS_SALT_.'$');
    }
    if(isset($_POST['passwordConfirm'])){
        if(trim($_POST['passwordConfirm']) != '')
            $_POST['passwordConfirm'] = crypt($_POST['passwordConfirm'], '$2a$12$'._PASS_SALT_.'$');
    }