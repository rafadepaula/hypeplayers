<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6">
        <a href="/rent/requests">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">warning</i>
                </div>
                <div class="card-content">
                    <p class="category">Solicitações</p>
                    <h3 class="title">{$requests}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        {$requests} locações solicitam atenção.
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="red">
                <i class="material-icons">access_time</i>
            </div>
            <div class="card-content">
                <p class="category">Atrasados</p>
                <h3 class="title">{$delayed}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    {$delayed} jogos atrasados.
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">local_shipping</i>
            </div>
            <div class="card-content">
                <p class="category">Última semana</p>
                <h3 class="title">{$lastWeekRents}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    {$lastWeekRents} locações nos últimos 7 dias.
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6">
        <a href="/rent/view">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">format_list_numbered</i>
                </div>
                <div class="card-content">
                    <p class="category">Todos</p>
                    <h3 class="title">{$allRents}</h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        ver todas as locações
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome do membro</th>
                            <th>Jogo recomendado</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach from=$dividedRanking item=favorites key=id_user}
                        <tr>
                            <td>
                                {$usersDtos[$id_user]->get('name')}
                            </td>
                            <td>
                                {if isset($favorites[0])}
                                    {$favorites[0]->get('id_game', true)->get('name')}
                                    ({$favorites[0]->get('platform', true)})
                                {else}
                                    <span class="text-danger">
                                        <b>Não foi possível calcular uma sugestão!</b>
                                    </span>
                                {/if}
                            </td>
                            <td class="td-actions">
                                <a>
                                    <button type="button" rel="tooltip" title="Enviar jogo"
                                            class="btn btn-info btn-simple send-game"
                                            data-id-user="{$id_user}"
                                            data-ranking="{$dividedRankingStr[$id_user]}">
                                        <i class="fa fa-send"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

{include file=$smarty.current_dir|cat:"/send_modal.tpl"}