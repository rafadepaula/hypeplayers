<?php
    
    class Feedback extends DTO {
        public $id;
        public $id_user;
        public $type;
        public $message;
        public $read_by;
        public $created;
        
        public $FieldsValidation = array(
            'type'    => 'notEmpty',
            'message'    => 'notEmpty',
        );

        public $FieldsMasks = array(
            'id_user' => array('getDto', array('user', 'id')),
            'read_by' => array('getDto', array('user', 'id')),
            'created' => 'datetimeMask'

        );
    }