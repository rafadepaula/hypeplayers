$(function(){
	$('.send-game').click(function(){
		$('#modalSend').modal({
			backdrop: false
		}, $(this));
		$('#modal-body').html('<h4 class="text-center"><i class="fa fa-spinner fa-spin"></i></h4>');
		$('#modal-title').html("Enviar jogo");
	});
	
	$('#modalSend').on('show.bs.modal', function(e){
		$('.main-panel').scrollTop(0);
		game = e.relatedTarget;
		id_user = game.data('id-user');
		$.ajax({
			url: '/user/getName/' + id_user,
		}).done(function(name){
			$('#modal-title').html('Enviar jogo para ' + name);
		})
		ranking = game.data('ranking');
		$('#modal-body').load("/rent/send/" + id_user + "/" + ranking, function(){
			$('.select2').select2({
            	language: 'pt-BR'
        	});
        	$('select[name=id_game]').change(function(){
        		platform = $(this).find(':selected').data('platform');
        		$('#platform-input').val(platform);
        		
        	});
        	$('select[name=id_game]').trigger('change');
			$('#send-form').on('submit', function(e){
				e.preventDefault();
			    var formData = new FormData(this);
			    sendForm(formData, '/rent/plan/');
			});
		});
	});

	$('.change-status').on('click', function(e, mass){
		$('#modal-body').html('<h4 class="text-center"><i class="fa fa-spinner fa-spin"></i></h4>');
		if(mass != 'undefined')
		    $('#modal-title').html('Alterar status em massa');
		else
		    $('#modal-title').html('Alterar status');
		$(this).data('masschange', mass);
        $('#modalStatus').modal({
            backdrop: false
        }, $(this));
	});

	$('#modalStatus').on('show.bs.modal', function(e){
		$('.main-panel').scrollTop(0);
        game = e.relatedTarget;
		if(typeof game.data('masschange') != 'undefined'){
            ids = '';
            $('.datatable-checkbox').each(function(){
                if($(this).is(':checked'))
                    ids = ids + '-' + $(this).data('id');
            });
            if(ids != ''){
                $('#modal-body').load("/rent/changeStatus/" + ids + '/1', function(){
                    $('#send-form').on('submit', function(e){
                        e.preventDefault();
                        var formData = new FormData(this);
                        sendForm(formData, '/rent/view');
                    });
                });
            }else{
                swal({
                    type: 'error',
                    title: 'Ops!',
                    text: 'Você não selecionou nenhuma locação.',
                });
                $(this).modal('toggle');
            }
        }else {
            id_rent = game.data('id-rent');
            if (typeof game.data('url') != 'undefined') {
                urlRedirect = game.data('url');
            } else {
                urlRedirect = '/rent/view';
            }
            $('#modal-body').load("/rent/changeStatus/" + id_rent, function(){
                $('#send-form').on('submit', function(e){
                    e.preventDefault();
                    var formData = new FormData(this);
                    sendForm(formData, urlRedirect);
                });
            });
        }
	});
	$('.view-one').click(function(){
		$('#modalViewOne').modal({
			backdrop: false
		}, $(this));
		$('#modal-body-one').html('<h4 class="text-center"><i class="fa fa-spinner fa-spin"></i></h4>');
		$('#modal-title-one').html('Locação');
	});

	$('#modalViewOne').on('show.bs.modal', function(e){
		$('.main-panel').scrollTop(0);
		game = e.relatedTarget;
		id_rent = game.data('id-rent');
		$.ajax({
			url: '/rent/getTitle/' + id_rent,
		}).done(function(title){
			$('#modal-title-one').html(title);
		})
		$('#modal-body-one').load("/rent/view/" + id_rent);
	});


	$('.finish-renew').click(function(){
		id_rent = $(this).data('id-rent');
		if(typeof $(this).data('url') != 'undefined'){
			urlRedirect = $(this).data('url');
		}else{
			urlRedirect = '/rent/view';
		}
		swal({
			title: 'Liberar renovação?',
			text: "Prossiga para renovar a locação do membro.",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Sim, renovar.',
			cancelButtonText: 'Não, cancelar.',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						url: '/rent/finishRenew/' + id_rent + '/1',
					}).done(function(ret){
						console.log(ret);
						if(ret != 'ok'){
							return reject(ret);
						}
						resolve();
					});
				})
			},
		}).then(function () {
			swal(
				'Confirmado!',
				'Renovação finalizada',
				'success'
			);
			window.location = urlRedirect;
		})
	});

	$('.deny-renew').click(function(){
		id_rent = $(this).data('id-rent');
		if(typeof $(this).data('url') != 'undefined'){
			urlRedirect = $(this).data('url');
		}else{
			urlRedirect = '/rent/view';
		}
		swal({
			title: 'Negar renovação?',
			text: "Prossiga para negar a renovação do membro.",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Negar renovação',
			cancelButtonText: 'Cancelar',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						url: '/rent/finishRenew/' + id_rent + '/0', 
					}).done(function(ret){
						console.log(ret);
						if(ret != 'ok'){
							return reject(ret);
						}
						resolve();
					});
				})
			},
		}).then(function () {
			swal(
				'Ok!',
				'Renovação negada',
				'error'
			);
			window.location = urlRedirect;
		})
	});
});


function sendForm(formData, urlRedirect){
	swal({
		text: 'Aguarde...',
		showConfirmButton: false,
	});
    $.ajax({
        type: $('#send-form').attr('method'),
        url: $('#send-form').attr('action'),
        data: formData,
        success: function (data) {
        	if(!validJson(data)){
        		swal({
        			type: 'error',
        			text: 'Erro interno no sistema.',
        			timer: 3000,
        		});
        	}
        	console.log(data);
        	data = $.parseJSON(data);
        	swal({
        		type: data.type,
        		text: data.text,
        		timer: 5000
        	});
        	if(data.return == 1){
        		window.location = urlRedirect;
        	}
        },
        error: function (data) {
            console.log(data);
            swal({
            	type: 'error',
            	text: 'Erro interno no sistema.'
            });
        },
        cache: false,	
        contentType: false,
        processData: false
    });
}

$(function(){
    opts = {
        "dom": 'Bfrtip',
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
        "iDisplayLength": 25,
        "aaSorting": [],
        "serverside" : false,
        "language": {
            "url": "plugins/datatables/datatables.portuguese.lang"
        },
        // "aoColumnDefs": [
        //     { 'bSortable': false, 'aTargets': [ 1 ] }
        // ],
        'language':{
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
        },
        "select": {
            "style": 'multi+shift',
            'selector': '.datatable-checkbox',
        },
        buttons: {
            buttons: [
                /*{
                 text: 'My button',
                 action: function ( dt ) {
                 console.log( 'My custom button!' );
                 }
                 },*/
                {
                    extend: 'selectAll',
                    text: 'Selecionar todos',
                    action: function(dt){
                        $('.datatable-checkbox').each(function(){
                            $(this).trigger('click');
                        })
                    }
                },
                {
                    text: 'Alterar status',
                    action: function(dt){
                        $('.change-status').first().trigger('click', 'mass');
                    }
                }
            ]
        }
    };

    $('.datatable-rent').dataTable(opts);

});