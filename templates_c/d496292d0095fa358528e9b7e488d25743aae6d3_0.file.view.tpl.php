<?php
/* Smarty version 3.1.28, created on 2017-09-29 17:01:39
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Favorite\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59cea6a370e774_84748284',
  'file_dependency' => 
  array (
    'd496292d0095fa358528e9b7e488d25743aae6d3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Favorite\\view.tpl',
      1 => 1505155459,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59cea6a370e774_84748284 ($_smarty_tpl) {
?>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <a href="/game/library/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
                <b>
                    Última modificação:
                    <span class="<?php echo $_smarty_tpl->tpl_vars['colorModify']->value;?>
">
                        <?php echo $_smarty_tpl->tpl_vars['lastModify']->value;
if ($_smarty_tpl->tpl_vars['lastModify']->value != 'nunca') {?> (<?php echo $_smarty_tpl->tpl_vars['datediff']->value;?>
 dias atrás)<?php }?>.
                    </span>
                    <?php if ($_smarty_tpl->tpl_vars['freeModify']->value != '') {?>
                        Liberação de alterações na data <?php echo $_smarty_tpl->tpl_vars['freeModify']->value;?>
.
                    <?php }?>
                </b>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable-favorite">
                    <thead>
                        <tr>
                            <th>Prioridade</th>
                            <th>Jogo</th>
                            <th>Plataforma</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
$_from = $_smarty_tpl->tpl_vars['favorites']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_favorite_0_saved_item = isset($_smarty_tpl->tpl_vars['favorite']) ? $_smarty_tpl->tpl_vars['favorite'] : false;
$_smarty_tpl->tpl_vars['favorite'] = new Smarty_Variable();
$__foreach_favorite_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_favorite_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['favorite']->value) {
$__foreach_favorite_0_saved_local_item = $_smarty_tpl->tpl_vars['favorite'];
?>
                        <tr>
                            <td class="drag-n-drop">
                                <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position')+1;?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('id_game',true)->get('name');?>

                            </td>
                            <td>
                                <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('platform',true);?>

                            </td>
                            <td class="td-actions">
                                <a class="confirm-link" href="/favorite/delete/<?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('position');?>
">
                                    <button type="button" rel="tooltip" title="Remover <?php echo $_smarty_tpl->tpl_vars['favorite']->value->get('id_game',true)->get('name');?>
 da lista"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    <?php
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_local_item;
}
}
if ($__foreach_favorite_0_saved_item) {
$_smarty_tpl->tpl_vars['favorite'] = $__foreach_favorite_0_saved_item;
}
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div><?php }
}
