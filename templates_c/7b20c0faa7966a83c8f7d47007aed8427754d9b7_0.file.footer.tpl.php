<?php
/* Smarty version 3.1.28, created on 2017-10-16 05:00:58
  from "C:\xampp\htdocs\hypeplayers\app\viewer\default\footer.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59e4592aacca07_27702739',
  'file_dependency' => 
  array (
    '7b20c0faa7966a83c8f7d47007aed8427754d9b7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\default\\footer.tpl',
      1 => 1507489788,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e4592aacca07_27702739 ($_smarty_tpl) {
?>

                    <footer class="footer">
                        <div class="container-fluid">
                            <p class="copyright pull-right">
                                &copy; <?php echo '<script'; ?>
>document.write(new Date().getFullYear())<?php echo '</script'; ?>
> HypePlayers
                            </p>
                        </div>
                    </footer>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!--   Core JS Files   -->
    <?php echo '<script'; ?>
 src="assets/js/jquery-3.1.0.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/js/bootstrap.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/js/material.min.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="../base/js/scripts.js"><?php echo '</script'; ?>
>

    <!--  Charts Plugin -->
    

    <!--  Notifications Plugin    -->
    <?php echo '<script'; ?>
 src="assets/js/bootstrap-notify.js"><?php echo '</script'; ?>
>

    <!--  Google Maps Plugin    -->
    

    <!-- Material Dashboard javascript methods -->
    <?php echo '<script'; ?>
 src="assets/js/material-dashboard.js"><?php echo '</script'; ?>
>

    <!-- InputMask -->
    <?php echo '<script'; ?>
 src="plugins/input-mask/jquery.inputmask.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/input-mask/jquery.maskmoney.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/input-mask/jquery.mask.init.js"><?php echo '</script'; ?>
>

    <!-- Select2 -->
    <?php echo '<script'; ?>
 src="plugins/select2/dist/js/select2.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/select2/dist/js/i18n/pt.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/select2/dist/js/select2.init.js"><?php echo '</script'; ?>
>

    <!-- Datatables -->
    <?php echo '<script'; ?>
 type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.15/rr-1.2.0/datatables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/datatables/datatables.init.js"><?php echo '</script'; ?>
>

    <!-- Timeago !-->
    <?php echo '<script'; ?>
 src="plugins/timeago/jquery.timeago.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/timeago/jquery.timeago.pt-br.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="plugins/timeago/jquery.timeago.init.js"><?php echo '</script'; ?>
>

    <!-- Sweetalert2 !-->
    <?php echo '<script'; ?>
 src="plugins/sweetalert2/dist/sweetalert2.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="assets/js/notifications.js"><?php echo '</script'; ?>
>

    <!-- Dynamic JS !-->
    <?php if (isset($_smarty_tpl->tpl_vars['js']->value)) {
echo $_smarty_tpl->tpl_vars['js']->value;
}?>
</html>


<?php }
}
