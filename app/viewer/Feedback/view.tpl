
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Usuário</th>
                            <th>Tipo</th>
                            <th>Texto</th>
                            <th>Lido por</th>
                            <th>Enviada em</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $feedbacks as $feedback}
                        <tr>
                            <td>{$feedback->get('id_user', true)->get('name')}</td>
                            <td>{$feedback->get('type')}</td>
                            <td>{$feedback->get('message')}</td>
                            <td>{$feedback->get('read_by', true)->get('name')}</td>
                            <td>{$feedback->get('created', true)}</td>
                            <td class="td-actions">
                                <a href="/feedback/delete/{$feedback->get('id')}" class="confirm-link">
                                    <button type="button" rel="tooltip" title="Deletar feedback"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>