<!doctype html>
<html lang="pt-br">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
	<base href="/app/">
	<title>{$title} :: HypePlayers</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="assets/css/material-bootstrap-wizard.css" rel="stylesheet" />

</head>

<body>
	<div class="image-container set-full-height" style="background-image: url('assets/img/cadastro-back.jpg')">

	    <!--   Big container   -->
	    <div class="container">
	        <div class="row">
		        <div class="col-sm-8 col-sm-offset-2">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="green" id="wizardProfile">
		                    <form action="/member/join" method="post" enctype="multipart/form-data">
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title">
		                        	   Algo de errado aconteceu!
		                        	</h3>
									<h5>Não conseguimos ativar o seu usuário.</h5>
		                    	</div>

                              	<div class="row">
                                	<div class="col-sm-10 col-sm-offset-1 text-center">
                                		<h3 class="text-danger">
                                			<i class="material-icons" style="font-size: 48px;">sentiment_very_dissatisfied</i>
                                			<p>Desculpe-nos</p>
                                		</h3>
                                		<h5>
                                			Por favor, tente atualizar a página. Se o erro persistir, entre em contato conosco!
                                		</h5>
                                	</div>
                            	</div>
		                            
		                        </div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	        </div><!-- end row -->
	    </div> <!--  big container -->

	</div>


</body>

</html>
