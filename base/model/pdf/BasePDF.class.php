<?php
    /**
     * Default PDF generation. Receives some
     * arrays of header row and data row, simply puts in the file.
     * Makes the table grid automatically.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    class BasePDF extends FPDF{
        use PDF;
        use GetSet;
        private $pdfName = 'pdf';
        private $pdfType;
        private $headerColumns;
        private $pdfData;
        private $rowHeights = array();
        private $yStart;
        private $xStart;

        public function __construct(){
        }

        public function Initialize($orientation = 'P', $paper = 'A4'){
            parent::__construct($orientation, 'mm', $paper);
            $this->AliasNbPages();
            $this->AddPage();
        }

        public function Header(){

            $aligns = $this->get('aligns');
            $widths = $this->get('widths');

            $this->SetFont('Arial', '', '8');
            $this->set('aligns', array('L'));
            $this->set('widths', array(0));
            $this->set('borders', array('B'));
            $text = _TYPE.': '.$this->pdfType.' | '._CREATED.': '.date(_DATE_FORMAT_);
            $this->PutRow(array($text));

            if(count($widths) != count($this->headerColumns)){
                $this->CalculateWidths();
            }

            $this->set('widths', $widths);

            $this->SetFont('Arial', 'B', 10);
            $this->set('borders', array_fill(0, count($this->headerColumns), ''));
            $this->set('aligns', array_fill(0, count($this->headerColumns), ''));
            $this->PutRow($this->headerColumns);
            $this->SetFont('Arial', '', 10);
            $this->yStart = $this->GetY();
            $this->xStart = $this->GetX();


        }

        public function ShowData(){
            if(is_array($this->pdfData)){
                foreach($this->pdfData as $row){
                    if(is_array($row)){
                        $y   = $this->GetY();
                        $this->PutRow($row);
                        $yEnd               = $this->GetY();
                        $h                  = $yEnd - $y;
                        $this->rowHeights[] = $h;
                    }
                }
            }
        }

        public function Footer(){
            $this->MakeGrid();
            $this->rowHeights = array();

            $this->SetFont('Arial', '', 8);
            $this->SetY(-30);
            $this->Cell(0,30,_PAGE.' '.$this->PageNo().' / {nb}','',0,'R');
        }

        public function Finish(){
            return $this->OutPut($this->pdfName, 'I');
        }

        public function MakeGrid(){
            $y = $this->yStart;
            $x = 10;
            
            foreach($this->rowHeights as $h){
                foreach($this->get('widths') as $w){
                    $this->Rect($x, $y, $w, $h);
                    $x = $x + $w;
                }
                $x = 10;
                $y = $y + $h;
            }
        }

        public function CalculateWidths(){
            $columns = count($this->headerColumns);
            $width = 190 / $columns;
            $columns = array();
            $columns = array_fill(0, count($this->headerColumns), $width);
            $this->set('widths', $columns);
        }
    }