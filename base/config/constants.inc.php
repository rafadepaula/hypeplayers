<?php
    /**
     * This file contains the constant used inside the framework.
     * Please, do not delete any of them! Just modify.
     * Without those, your app may not work.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.1 - 2017-08-12
     *
     */
    
    /**
     * Directories list
     */
    define('_APP_', 'app/');
    define('_BASE_', 'base/');
    define('_BASECONFIG_', _BASE_.'config/');
    define('_APPCONFIG_', _APP_.'config/');
    define('_PLUGINS_', _APP_.'plugins/');
    
    /*
     * Defines the URL base of the website.
     * Used at the app/viewer/_default/header.tpl
     * to indicate to the browser the meta tag
     * "base"
     */
    define('_BASE_URL_', 'http://'.$_SERVER['HTTP_HOST']);
    // define('_IN_DEV_', $_SERVER['HTTP_HOST'] == _DEV_URL_);

    // Shortcut
    define('_DS', DIRECTORY_SEPARATOR);

    // CEP validation
    define('_CEP_URL_', 'http://api.postmon.com.br/v1/cep/');