<?php

    class HelpController extends AppController{
		public function makeSteps(){
			if(!isset($_POST['helps'])){
				$_POST['helps'] = array();
			}
			$helps = $_POST['helps'];
			$this->model->makeSteps($helps);
		}

		public function faq(){
			$this->viewer->show('faq', 'FAQ - Dúvidas Frequentes');
		}
	}