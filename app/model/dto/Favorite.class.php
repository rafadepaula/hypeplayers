<?php
    
    class Favorite extends DTO {
        public $id;
        public $id_game;
        public $id_user;
        public $created;
        public $position;
        public $platform;
        
        public $FieldsValidation = array(
            'id_game'    => array('existsForeign', ['game', 'id']),
            'id_user'    => array('existsForeign', ['member', 'id_user']),
            'platform'   => 'validPlatform',
        );
        public $FieldsErrors     = array(
            'id_game' => 'Informe um jogo válido.',
            'id_user'    => 'Usuário inválido.',
            'platform' => 'Plataforma inválida.'
        );
        public $FieldsMasks      = array(
            'id_user' => array('getDto', 'user'),
            'id_game' => array('getDto', 'game'),
            'created' => 'datetimeMask',
            'platform' => 'platformMask',
        );

        public function validPlatform($input){
            $valids = array('ps4', 'xone', 'switch');
            return in_array($input, $valids);
        }

        public function platformMask($input){
            $platforms = array(
                'ps4' => 'PS4',
                'xone' => 'XBOX One',
                'switch' => 'Nintendo Switch'
            );
            return $platforms[$input];
        }
        
    }