
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/signature/addPreapproval/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nível</th>
                            <th>Código API</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $preApprovals as $preApproval}
                        <tr>
                            <td>
                                {$preApproval->get('level')}
                            </td>
                            <td>
                                {$preApproval->get('code')}
                            </td>
                            <td class="td-actions">
                                <a href="/signature/editPreapproval/{$preApproval->get('id')}">
                                    <button type="button" rel="tooltip" title="Editar"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/signature/viewPreapproval/{$preApproval->get('id')}">
                                    <button type="button" rel="tooltip" title="Visualizar"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="confirm-link" href="/signature/deletePreapproval/{$preApproval->get('id')}">
                                    <button type="button" rel="tooltip" title="Deletar"
                                            class="btn btn-danger btn-simple">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>