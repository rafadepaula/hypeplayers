<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <base href="/app/">

        <title>{$title} :: HypePlayers</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!--  Material Dashboard CSS    -->
        <link href="assets/css/material-dashboard.css" rel="stylesheet"/>

        <!--     Fonts and icons     -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

        <!--   Select2      -->
        <link href="plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">

        <!--   SweetAlert2    -->
        <link href="plugins/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css">

        <!-- Datatables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/b-1.4.2/r-2.2.0/rr-1.2.3/sl-1.2.3/datatables.min.css"/>

        <!-- Font Awesome -->
        <link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Qtip !-->
        <link href="https://cdn.jsdelivr.net/qtip2/3.0.3/basic/jquery.qtip.min.css" rel="stylesheet" type="text/css">

        <!-- Bootstrap Tour !-->
        <link href="plugins/bootstraptour/bootstrap-tour.min.css" type="text/css">
        <style>
            .tour-backdrop {
                background-color: rgba(0,0,0,0.5);
            }
        </style>

        <!--   Dynamic CSS    -->
        {if isset($css)}{$css}{/if}

    </head>


    <body>
        <div class="wrapper">

            <div class="sidebar" data-color="green">
                <!--
                    Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                    Tip 2: you can also add an image using data-image tag
                -->

                <div class="logo" style="background-image: url('assets/img/logo-topo.png');height: 63px;">
                    {*<a href="{_BASE_URL_}" class="simple-text">
                        HypePlayers
                    </a>*}
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li>
                            <a href="/user/home/">
                                <i class="material-icons">dashboard</i>
                                <p>Início</p>
                            </a>
                        </li>

                        {if $actualUser->get('role') == 'admin'}
                            <li>
                                <a href="/user/adminIndex">
                                    <i class="material-icons">settings</i>
                                    <p>Site admin</p>
                                </a>
                            </li>
                            <li>
                                <a href="/member/">
                                    <i class="material-icons">group</i>
                                    <p>Membros</p>
                                </a>
                            </li>
                            <li>
                                <a href="/game/">
                                    <i class="material-icons">videogame_asset</i>
                                    <p>Jogos</p>
                                </a>
                            </li>
                            <li>
                                <a href="/rent/plan">
                                    <i class="material-icons">local_shipping</i>
                                    <p>Locações</p>
                                </a>
                            </li>
                            <li>
                                <a href="/region/">
                                    <i class="material-icons">map</i>
                                    <p>Regiões</p>
                                </a>
                            </li>
                            <li>
                                <a href="/signature/viewPreapproval">
                                    <i class="material-icons">attach_money</i>
                                    <p>Planos</p>
                                </a>
                            </li>
                            <li>
                                <a href="/user/">
                                    <i class="material-icons">build</i>
                                    <p>Administradores</p>
                                </a>
                            </li>
                            <li>
                                <a href="/game/library">
                                    <i class="material-icons">view_module</i>
                                    <p>Biblioteca de Jogos</p>
                                </a>
                            </li>
                        {else}
                            <li>
                                <a href="/game/library">
                                    <i class="material-icons">view_module</i>
                                    <p>Biblioteca de Jogos</p>
                                </a>
                            </li>
                            <li>
                                <a href="/signature/">
                                    <i class="material-icons">attach_money</i>
                                    <p>Minha conta</p>
                                </a>
                            </li>
                            {if $signature->activeStatus() }
                                <li>
                                    <a href="/rent/myGames">
                                        <i class="material-icons">videogame_asset</i>
                                        <p>Meus jogos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="/favorite/">
                                        <i class="material-icons">star_rate</i>
                                        <p>Minhas preferências</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="/rent/history">
                                        <i class="material-icons">history</i>
                                        <p>Histórico</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="/address/">
                                        <i class="material-icons">map</i>
                                        <p>Endereços</p>
                                    </a>
                                </li>
                            {/if}
                        {/if}
                        
                        <li class="active-pro">
                            <a href="/user/logout">
                                <i class="material-icons">exit_to_app</i>
                                <p>Sair do sistema</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="main-panel">
                <nav class="navbar navbar-transparent navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">{$title}</a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                {if $actualUser->get('role') == 'member'}
                                    <li>
                                        <a href="/help/faq">
                                            DÚVIDAS
                                        </a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer" title="Enviar sugestões" class="send-feedback qtip-init qtip-bootstrap">
                                           <i class="material-icons">info_outline</i>
                                           <p class="hidden-lg hidden-md">Sugestões</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a style="cursor: pointer" title="Ajuda" class="qtip-init qtip-bootstrap" id="help-init">
                                            <i class="material-icons">help_outline</i>
                                            <p class="hidden-lg hidden-md">Ajuda</p>
                                        </a>
                                    </li>
                                {else}
                                    <li>
                                        <a class="qtip-init" title="Ver sugestões" href="/feedback/">
                                            <i class="material-icons">info_outline</i>
                                            <p class="hidden-lg hidden-md">Feedback</p>
                                        </a>
                                    </li>
                                {/if}
                                <li class="dropdown">
                                    <a href="#" id="notifications" class="dropdown-toggle qtip-init" data-toggle="dropdown" title="Notificações">
                                        <i class="material-icons">notifications</i>
                                            {if $numNotifications > 0}
                                                <span class="notification">{$numNotifications}</span>
                                            {/if}
                                        <p class="hidden-lg hidden-md">Notificações</p>
                                    </a>
                                    <ul class="dropdown-menu">
                                    {foreach $notifications as $notification}
                                        <li>
                                            <a href="{$notification->get('link')}">
                                                <span style="font-size: 11px;">{$notification->get('created', true)}</span>
                                                <br>
                                                {$notification->get('text')}
                                            </a>
                                        </li>
                                    {/foreach}
                                    <li class="divider"></li>
                                    <li>
                                        <a href="/notification/view">
                                            Ver todas
                                        </a>
                                    </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="/user/view/" class="qtip-init" title="Meu perfil">
                                       <i class="material-icons">person</i>
                                       <p class="hidden-lg hidden-md">Perfil</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="content">
                    <div id="feedback-html" style="display: none">

                    </div>
                    <div class="container-fluid">
                        {$_flash}