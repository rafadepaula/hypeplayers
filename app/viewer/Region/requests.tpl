<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/region/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Localização</th>
                            <th>CEP</th>
                            <th>Data e hora</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $regionRequests as $region}
                        <tr>
                            <td>
                                {$region->get('location')}
                            </td>
                            <td>
                                {$region->get('cep')}
                            </td>
                            <td>
                                {$region->get('date', true)}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>