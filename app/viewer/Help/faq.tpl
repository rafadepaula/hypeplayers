


<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="panel-group" id="accordion">
                    <div class="faqHeader">Conheça cada plano</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Plano Bronze - R$39,90</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                Jogos em posse: 01 <br>
                                Trocas: nenhuma <br>
                                Total de jogos no mês: 01
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Plano Prata - R$69,90</a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse">
                            <div class="panel-body">
                                Jogos em posse: 02 <br>
                                Trocas: 01 <br>
                                Total de jogos no mês: 03
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Plano Ouro - R$99,90</a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse">
                            <div class="panel-body">
                                Jogos em posse: 02 <br>
                                Trocas: 03 <br>
                                Total de jogos no mês: 05
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom1">Como faço para trocar meu plano?</a>
                            </h4>
                        </div>
                        <div id="collapseCustom1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>É bem simples, vá em "Minha conta", note que irá aparecer os outros dois planos para troca, basta escolher o desejado e clicar. Em seguida é só aguardar o nosso retorno.</p>
                                <p>Note que após autorizarmos a troca do plano será necessário incluir o cartão novamente, como trata-se de cobrança recorrente a operadora financeira sempre solicita a confirmação dos dados do cartão quando ocorre qualquer tipo de alteração na conta do usuário.</p>
                                <p>Um ponto importante é sobre o período para troca do plano.</p>
                                <p>Como a intenção é gastar pouco e jogar muito, existe um período para a mudança do plano ser solicitada e efetivada, esse prazo é entre os últimos 05 dias antes do vencimento da sua próxima fatura, assim você não terá duas cobranças no mesmo mês e manterá a sua data de vencimento padrão além de não perder nenhuma troca dos seus jogos!</p>

                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Como funciona?</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Lista de jogos</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Assim que você acessar a sua conta pela primeira vez será necessário criar uma lista de desejos, com no mínimo 10 títulos de acordo com a sua preferência.</p>
                                <p>A função dela é organizar e deixar a sua experiência com a assinatura mais prazerosa e dinâmica.</p>
                                <p>Para adicionar basta clicar no jogo e em seguida em "adicionar para minha lista" e pronto. Os games estão separados por plataforma (PS4, Xone e Switch), eles também podem ser pesquisados por gênero (RPG, Esportes, Luta, Corrida, etc).</p>
                                <p>Você pode alterar a ordem dos games sempre que preferir, porém só poderá adicionar novos jogos no período de 05 dias após a renovação mensal, por isso, sempre adicione o máximo de games à lista, quanto mais melhor =).</p>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Envio</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Não se preocupe, o envio é totalmente grátis (é grátis mesmo). Ele é realizado em até 48hs e será entregue ao o usuário ou para alguém autorizado a receber. Poderá ser enviado via Correios ou Motoboy.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Devolução</a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Simples como derrotar um chefe em Dark Souls (haha). Brincadeiras à parte ele é bem fácil, no menu principal selecione "Meus Jogos" e em "Ações" terá a opção "Solicitar Devolução", basta clicar nesse botão e está feito! A nossa central irá receber a solicitação de devolução ou troca (dependendo do plano) e entrará em contato com o usuário via e-mail para agendamento da retirada.</p>
                                <p>Se você ainda tiver trocas dentro do mês o nosso sistema já irá preparar o envio do próximo game.</p>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Quanto tempo posso ficar com o jogo?</a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Você pode ficar com o jogo até 90 dias.</p>
                                <p>Recebeu o jogo, mas não conseguiu terminá-lo?</p>
                                <p>Calma, basta você renovar a locação do jogo, para isso vá até o menu e selecione "Meus Jogos" e em "Ações" escolha "Prorrogar". Com isso o seu jogo será prorrogado por mais 30 dias, importante notar que ao efetuar essa ação o jogo entrará como uma nova locação no período, ou seja, será considerado uma troca (de acordo com o plano escolhido).</p>
                                <p> Cada jogo poderá ser prorrogado em até 60 dias (02 vezes). Também vale ressaltar que caso o usuário não acesse a sua conta e solicite a "troca" do jogo após o período a prorrogação será realizada de maneira automática.
                                </p>

                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Cadastro</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Informações cadastrais</a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Para ser um "Hype" é necessário criar uma conta em nosso site, cadastrando seus dados, é bem rapidinho.</p>
                                <p>Após se cadastrar, o player irá receber um e-mail solicitando confirmação, é necessário acessar o link e confirmar a sua inscrição, sem isso não será possível utilizar os serviços. Sempre confirme o link na sua caixa de Spam e caso não receba, basta entrar em contato conosco via e-mail, contato@hypeplayers.com.br para reenviarmos a mensagem.</p>

                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Pagamentos</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Como funciona?</a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Para ter essa experiência incrível o usuário só precisa de um cartão de crédito.</p>
                                <p>A cobrança é realizada com base no sistema de recorrência, onde a cada 30 dias o valor do plano escolhido é creditado.</p>
                                <p>Vale lembrar que a cobrança sempre será efetuada no dia em que a conta/acesso foi criada(o).</p>
                                <p>As transações de pagamento são realizadas em conexões seguras, criptografadas e homologadas, fique tranquilo, você está em um ambiente seguro(SSL).</p>
                                <p>Nossa equipe não tem qualquer interação sobre essas transações/informações, elas ocorrem de maneira automática.</p>
                                <p>Caso ocorra algum empecilho com a cobrança, entraremos em contato.</p>

                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Cancelamento</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Como realizo?</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Basta ir até “Minha Conta” e ao lado direito da tela você encontrará o botão “Cancelar Assinatura”, basta clicar e aguardar o nosso retorno sobre o cancelamento.</p>
                                <p>Importante: o botão só estará habilitado para você caso todos os jogos em sua posse já tenham sido devidamente devolvidos para a Hype Players.</p>

                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Reativação</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Como volto para o clube?</a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse">
                            <div class="panel-body">
                                Faça o login e acesse “Minha conta”, você notará uma mensagem para reativar a conta, basta selecionar, escolher o seu plano e informar o cartão de crédito. Pronto, agora é só aproveitar novamente a sua assinatura.
                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Fale conosco</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom2">Via e-mail</a>
                            </h4>
                        </div>
                        <div id="collapseCustom2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Rolou uma dúvida e não encontrou a resposta? Quer uma dica de game ou apenas trocar uma ideia? Envie uma mensagem para contato@hypeplayers.com.br
                                    que retornaremos o mais breve possível. Nosso horário de atendimento é de segunda a sexta-feira das 8:00 até 17:30.</p>
                                <p>O prazo para retorno é de 12 horas de segunda a sexta</p>
                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Segurança e privacidade</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom3">Por que a Hype precisa do meu e-mail?</a>
                            </h4>
                        </div>
                        <div id="collapseCustom3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Todas as notificações da Hype Players são realizadas via e-mail, como por exemplo confirmação de cadastro, envio e devolução de jogos e pagamento. Por isso é muito importante habilitar o endereço contato@hypeplayers.com.br como endereço confiável, para que não se perca nenhum e-mail.</p>
                                <p>Todo o histórico de notificações poderá será acessado direto na sua conta Hype, na caixa notificações.</p>
                                <p>Quem tem acesso as minhas informações?</p>
                                <p>Apenas a Hype Players tem acesso. Não iremos vender, repassar ou compartilhar seus dados com terceiros (também não gostamos de spam ¬¬), pode confiar na gente ;).</p>

                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom4">E os Termos de uso do site e locação?</a>
                            </h4>
                        </div>
                        <div id="collapseCustom4" class="panel-collapse collapse">
                            <div class="panel-body">
                                A minuta está disponível no link "Termos de Uso" no menu principal.
                            </div>
                        </div>
                    </div>

                    <div class="faqHeader">Então...</div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom5">Posso me cadastrar sem medo?</a>
                            </h4>
                        </div>
                        <div id="collapseCustom5" class="panel-collapse collapse">
                            <div class="panel-body">
                                Com certeza, player! Nosso site possui certificado de segurança protocolado via SSL. Ou seja, todas as informações são criptografadas dando total segurança aos usuários. A sua única preocupação é jogar.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>