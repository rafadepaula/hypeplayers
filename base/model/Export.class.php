<?php

    /**
     * A place to have the PDF and Excel classes to export.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    class Export {

        public $pdf;
        public $excel;

        /**
         * @param string $pdf - class's name
         * @param string $excel - class's name
         */
        public function __construct($pdf = 'BasePDF', $excel = 'BaseExcel') {
            $this->pdf = new $pdf();
            $this->excel = new $excel();
        }

    }