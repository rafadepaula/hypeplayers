<?php
/* Smarty version 3.1.28, created on 2017-10-16 04:02:52
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Signature\view.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59e44b8c6372b5_19302121',
  'file_dependency' => 
  array (
    'f78952b41b2416c8b8bb08e84b41fdbebda03073' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Signature\\view.tpl',
      1 => 1508133770,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e44b8c6372b5_19302121 ($_smarty_tpl) {
?>
<div class="row">
	<div class="col-sm-8">
		<?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Canceled') {?>
			<a href="/signature/activate">
	            <div class="card card-stats">
	                <div class="card-header" data-background-color="orange">
	                    <i class="material-icons">cancel</i>
	                </div>
	                <div class="card-content">
	                    <p class="category">Assinatura cancelada <i class="fa fa-frown-o"></i></p>
	                    <h4 class="title">Sua assinatura está cancelada. Clique para ativar novamente</h4>
	                </div>
	                <div class="card-footer">
	                    <div class="stats">
	                        O sistema não identificou o pagamento ou você optou por cancelar sua assinatura.
	                        Clique aqui para voltar para o clube!
	                    </div>
	                </div>
	            </div>
	        </a>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Preactivated') {?>
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">access_time</i>
                </div>
                <div class="card-content">
                    <p class="category">Processando pagamento</p>
                    <h4 class="title">Quase lá! O seu pagamento está sendo processado.</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        A solicitação de assinatura foi registrada. Agora, nosso sistema está
                        se comunicando com a empresa responsável pelo processamento do cartão
                        de crédito para liberar o seu acesso!
                    </div>
                </div>
            </div>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Pending') {?>
			<a href="/signature/activate">
	            <div class="card card-stats">
	                <div class="card-header" data-background-color="red">
	                    <i class="material-icons">money_off</i>
	                </div>
	                <div class="card-content">
	                    <p class="category">Pagamento pendente</p>
	                    <h4 class="title">Clique para ativar o seu plano</h4>
	                </div>
	                <div class="card-footer">
	                    <div class="stats">
	                        Você está com pagamento pendente! Ative sua conta efetivando o pagamento de sua assinatura.
	                    </div>
	                </div>
	            </div>
	        </a>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Taxed') {?>
			<div class="card card-stats">
                <div class="card-header" data-background-color="red">
                    <i class="material-icons">money_off</i>
                </div>
                <div class="card-content">
                    <p class="category">Você recebeu uma multa</p>
                    <h4 class="title">Clique para acessar a página de multas e reativar seu plano</h4>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        Você recebeu uma multa. Acesse a página de multas para regularizar a situação.
                    </div>
                </div>
            </div>
		<?php }?>
		<?php if ($_smarty_tpl->tpl_vars['signature']->value->get('status') == 'Active') {?>
	        <div class="card card-stats">
	            <div class="card-header" data-background-color="green">
	                <i class="material-icons">done</i>
	            </div>
	            <div class="card-content">
	                <p class="category">Plano atual</p>
	                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['signature']->value->get('level',true);?>
</h3>
	            </div>
	        </div>
	        <h3>Trocar plano</h3>
			<div class="col-sm-6">
		        <div class="card card-stats">
		            <div class="card-header" data-background-color="blue">
		                <i class="material-icons">swap_horiz</i>
		            </div>
		            <div class="card-content">
		                <p class="category">Trocar plano</p>
		                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['levels']->value[0];?>
</h3>
		            </div>
		            <div class="card-footer">
		                <div class="stats">
		                    Clique para trocar para <?php echo $_smarty_tpl->tpl_vars['levels']->value[0];?>

		                </div>
		            </div>
		        </div>
		    </div>

		    <div class="col-sm-6">
		        <div class="card card-stats">
		            <div class="card-header" data-background-color="blue">
		                <i class="material-icons">swap_horiz</i>
		            </div>
		            <div class="card-content">
		                <p class="category">Trocar plano</p>
		                <h3 class="title"><?php echo $_smarty_tpl->tpl_vars['levels']->value[1];?>
</h3>
		            </div>
		            <div class="card-footer">
		                <div class="stats">
		                    Clique para trocar para <?php echo $_smarty_tpl->tpl_vars['levels']->value[1];?>

		                </div>
		            </div>
		        </div>
		    </div>
	    <?php }?>

	    <div class="col-sm-12">
	    	<div class="card">
	    		<div class="card-header" data-background-color="green">
		    		<h4>Histórico de pagamentos</h4>
		    	</div>
		    	<div class="card-content">
	        		<div class="table-responsive">
		        		<table class="table table-striped datatable">
		        			<thead>
		        				<tr>
		                            <th>Data</th>
		                            <th>Valor</th>
		                            <th>Referência interna</th>
		                        </tr>
		        			</thead>
		                    <tbody>
		                        <?php
$_from = $_smarty_tpl->tpl_vars['payments']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_payment_0_saved_item = isset($_smarty_tpl->tpl_vars['payment']) ? $_smarty_tpl->tpl_vars['payment'] : false;
$_smarty_tpl->tpl_vars['payment'] = new Smarty_Variable();
$__foreach_payment_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_payment_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['payment']->value) {
$__foreach_payment_0_saved_local_item = $_smarty_tpl->tpl_vars['payment'];
?>
			                        <tr>
			                            <td>
			                            	<?php echo $_smarty_tpl->tpl_vars['payment']->value->get('date',true);?>

			                            </td>
			                            <td>
			                            	R$<?php echo $_smarty_tpl->tpl_vars['payment']->value->get('value',true);?>

			                            </td>
			                            <td>
			                            	<?php echo $_smarty_tpl->tpl_vars['payment']->value->get('code');?>

			                            </td>
			                        </tr>
			                    <?php
$_smarty_tpl->tpl_vars['payment'] = $__foreach_payment_0_saved_local_item;
}
}
if ($__foreach_payment_0_saved_item) {
$_smarty_tpl->tpl_vars['payment'] = $__foreach_payment_0_saved_item;
}
?>
		                    </tbody>
		                </table>
	        		</div>
	        	</div>
	        </div>
        </div>
    </div>

	<div class="col-sm-4">
		<div class="card card-profile">
			<div class="card-avatar">
				<img class="img" src="<?php echo $_smarty_tpl->tpl_vars['picture']->value;?>
" />
			</div>

			<div class="content">
				<h6 class="category text-gray">
					<?php echo $_smarty_tpl->tpl_vars['signature']->value->get('level',true);?>
 (R$ <?php echo $_smarty_tpl->tpl_vars['signature']->value->get('value',true);?>
)
				</h6>
				<h4 class="card-title"><?php echo $_smarty_tpl->tpl_vars['actualUser']->value->get('name');?>
</h4>
				<p class="card-content">
					Membro desde <?php echo $_smarty_tpl->tpl_vars['signature']->value->get('since',true);?>
 <br>
					Expira em <b><?php echo $_smarty_tpl->tpl_vars['signature']->value->get('expiry',true);?>
</b> <br>
					Último pagamento: <?php echo $_smarty_tpl->tpl_vars['signature']->value->get('last_payment',true);?>

				</p>
				<?php if ($_smarty_tpl->tpl_vars['canCancel']->value['canCancel']) {?>
					<a href="/signature/cancel" class="btn btn-danger btn-round">Cancelar assinatura</a>
				<?php } else { ?>
					<p class="card-content">
						<u>Você não pode cancelar sua assinatura:</u> <?php echo $_smarty_tpl->tpl_vars['canCancel']->value['error'];?>

					</p>
				<?php }?>
				<a href="/user/edit" class="btn btn-success btn-round">Editar infos</a>
				<p class="card-content">
					<a href="/signature/changeCard" class="btn btn-info btn-round">Mudar cartão de pagamento</a>
				</p>
			</div>
		</div>
	</div>

</div><?php }
}
