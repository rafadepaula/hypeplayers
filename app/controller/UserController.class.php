<?php
    
    
    class UserController extends AppController {

        public function login(){
            if($this->logged()){
                return $this->home();
            }
            if($this->request()){
            	$ret = $this->model->login();
                if($ret === true){
                    $user = unserialize($_SESSION['user']);
                    $this->user = $user;
                    
                    Viewer::flash(_LOGIN_SUCCESS, 's');
                    return $this->redirect('/user/home');
                }else{
                	if($ret === false){
		                Viewer::flash(_LOGIN_ERROR, 'e');
	                }else{
                		if($ret == 'activated'){
                			Viewer::flash('Sua conta não está ativada!', 'e');
		                }else{
                			Viewer::flash('Sua conta foi desativada. Entre em contato conosco', 'e');
		                }
	                }
	                unset($_POST);
                }
            }
            
            return $this->viewer->show('login', 'Login', false, true);
        }
        
        public function logout($flash = 'Até logo!', $type = 's'){
            unset($_SESSION['logged']);
            unset($_SESSION['user']);
            unset($_POST);
            session_destroy();
            session_start();
            Viewer::flash($flash, $type);
            
            return $this->login();
        }
        
        public function recovery($secret = null){
            if(is_null($secret)){
                if($this->request()){
                    $this->model->recovery();
                }
                return $this->viewer->show('recovery', 'Recuperação de senha', false, true);
            }else{
                if($this->request()){
                    if(!$this->model->recovery($secret, true)){
                        unset($_POST);
                        $this->viewer->set('secret', $secret);
                        return $this->viewer->show('newPassword', 'Nova senha', false, true);
                    }
                    $this->user = unserialize($_SESSION['user']);
                    return $this->home();
                }
                if($this->model->recovery($secret)){
                    $this->viewer->set('secret', $secret);
                    return $this->viewer->show('newPassword', 'Nova senha', false, true);
                }else{
                    return $this->login();
                }
            }
        }
        
        
        public function home(){
            if($this->user->get('role') == 'member'){
                $memberController = new MemberController();
                return $memberController->home();
            }
            return $this->viewer->show('home', 'Início');
        }
        
        public function add(){
            if($this->user->get('role') == 'member'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                $memberController = new MemberController();
                return $memberController->home();
            }
            if($this->request()){
                if($this->model->save(null, 'admin')){
                    return $this->view();
                }else{
                    unset($_POST);
                    return $this->add();
                }
            }
            
            return $this->viewer->show('add', 'Cadastrar administrador');
        }
        
        public function view(){
            if($this->user->get('role') == 'member'){
                $memberController = new MemberController();
                return $memberController->view();
            }
            $users = $this->model->search('user', '*', array('role' => 'admin'));
            $this->viewer->set('users', $users);
            return $this->viewer->show('view', 'Administradores');
        }
        
        public function edit(){
            if($this->user->get('role') == 'member'){
                $memberController = new MemberController();
                return $memberController->edit();
            }
            $id = $this->user->get('id');
            
            if($this->request()){
                if($this->model->save($id, 'admin')){
                    return $this->view();
                }else{
                    unset($_POST);
                    return $this->edit($id);
                }
            }
            
            return $this->viewer->show('edit', 'Editar dados pessoais');
        }

        public function getName($id){
            $user = $this->model->getUser($id);
            echo $user->get('name');
        }

        public function teste(){
            require 'base/plugins/phpmailer/PHPMailerAutoload.php';
            
            $email = new PHPMailer;

            $email->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            
            $email->isSMTP();
            $email->Host       = 'mail.hypeplayers.com.br';
            $email->SMTPAuth   = true;
            $email->Username   = 'webmaster@hypeplayers.com.br';
            $email->Password   = 'Zs9w63Q0oy';
            $email->SMTPSecure = false;
            $email->Port       = 26;
            
            $email->From     = 'webmaster@hypeplayers.com.br';
            $email->FromName = 'HypePlayers';
            $email->addBCC('rafaelbg97@live.com', 'rafael');
            $email->isHTML(true);
            
            $email->Subject = 'HypePlayers - Nova notificação';

            $html = file_get_contents('app/viewer/Notification/email.html');
            $html = str_replace('%notification%', 'Teste de notificação', $html);

            $email->Body    = $html;
            $email->CharSet = 'UTF-8';

            
            return $email->send();
        }

        public function adminIndex(){
        	if($this->user->get('role') != 'admin'){
        		Viewer::flash(_PERMISSION_ERROR, 'e');
        		return $this->home();
	        }
            $banners = $this->model->search('banner', '*', false, 'position');
            $this->viewer->set('banners', $banners);

            $featureds = $this->model->search('featured', '*', false, 'position');
            $this->viewer->set('featureds', $featureds);

            $this->viewer->addJs('assets/js/admin_index.js');
            return $this->viewer->show('admin_index', 'Administração do site');
        }
    }
