$(function(){
	$('.library-game').click(function(){
		$('#modalLibrary').modal({
			backdrop: false
		}, $(this));
	});
	$('#modalLibrary').on('show.bs.modal', function(e){
		$('#modal-body').html();
		$('.main-panel').scrollTop(0);
		game = e.relatedTarget;
		name = game.data('name');
		id = game.data('id');
		$('#modal-title').text(name);
		platform = $('.filter-button-active').data('filter');
		if(typeof platform == 'undefined')
			platform = game.data('platform');
		$('#modal-body').load("/game/libraryModal/" + id + "/" + platform);
	});
    $('#modalLibrary').on('hidden.bs.modal', function(e){
        $('#modal-body').html('');
        $('#modal-title').text('');
        $('#modal-body').html('');
    });

	$("#game-search").on('change', function(){
		$(".filter-button").removeClass("filter-button-active");
		$('#all').addClass('filter-button-active');
		var matcher = new RegExp($(this).val(), 'gi');
		$('.filter').show('3000').not(function(){
			return matcher.test($(this).find('.game_name').text()) // tags
		}).hide('3000');
		$('#filter-genre').val('all');
	});

	$('.filter-button').click(function(){
		$('#game-search').val('');
		$('#filter-genre').val('all');
	});

	$('#filter-genre').change(function(){
		$(".filter-button").removeClass("filter-button-active");
		$('#all').addClass('filter-button-active');
		$('#game-search').val('');
		if($(this).val() == 'all'){
			$('.filter').show('3000');
			return true;
		}
		$('.genre_' + $(this).val()).show('3000');

		$('.filter').not('.genre_' + $(this).val()).hide('3000');
	});

	filterButton($('#ps4'));

	$('#save-changes-button').click(function(){
		$('#modalSaveChanges').modal({
			backdrop: false
		}, $(this));
	});
	$('#modalSaveChanges').on('show.bs.modal', function(e){
		$('.main-panel').scrollTop(0);
		$('#modal-body-savechanges').html();
		$('#modal-body-savechanges').load('/favorite/saveChangesModal/');
	});

	$('#finish-save').click(function(e){
		swal({
            text: "Processando...",
            showConfirmButton: false,
        });
		$.ajax({
			url: '/favorite/finish/',
			method: 'post',
		}).done(function(ret){
			ret = $.parseJSON(ret);
			swal({
				type: ret.type,
				text: ret.text,
				timer: 2500
			});
			if(ret.return == 1){
				$('.list-must-save').hide();
				$('#save-changes').hide();
				$('.list-not-done').hide();
				$('#view-favorites').show();
				window.location = '/favorite/';
			}
		});
	});

	async(function(){
		bigger = 0;
		$('.game-library .card-profile').each(function(){
			if($(this).height() > bigger)
				bigger = $(this).height();
		});
		$('.game-library .card-profile').css('height', bigger);
	})
});

$(document).on('click', '.add-fav', function(){
    swal({
        text: 'Aguarde...',
        showConfirmButton: false,
    });
    platform = $('.filter-button-active').data('filter');
    id = $(this).data('game');
    confirmDialog(id, platform);
});

function confirmDialog(id, platform){
	selection = swal({
		title: 'Adicionar preferência',
		type: 'question',
		text: 'Adicionar este jogo como favorito?',
		showCloseButton: true,
		showCancelButton: true,
		confirmButtonText:'Sim',
		cancelButtonText:'Não',
		showLoaderOnConfirm: true,
		preConfirm: function () {
			return new Promise(function (resolve, reject) {
				addFav(id, platform, resolve, reject);
			})
		},
	});

    function addFav(id, platform, resolve, reject){
        $.ajax({
            data: {'platform': platform},
            url: '/favorite/addFav/' + id,
            method: 'post',
        }).done(function(ret){
            ret = $.parseJSON(ret);
            if(typeof ret.error != 'undefined'){
                return reject(ret.error);
            }
            return resolve(ret);
        });
    }

	selection.then(function (ret) {
		swal({
			type: 'success',
			text: 'Preferência adicionada!',
			timer: 2000
		});

		if(ret.position < 10){
			$.notify({
				message: 'Faltam ' + (10 - ret.position) + ' jogos para você completar a sua lista!'
			});	
		}else{
			if(typeof ret.finishFirst != 'undefined'){
				$.notify({
					message: 'Lista de preferências completa! Salve a alterações.'
				});
			}else{
				$.notify({
					message: ret.position + ' jogos na sua lista.'
				});
			}
			$('#save-changes').show();
			$('.list-must-save').show();
			$('.list-not-done').hide();
		}
	})

}

function removeSavechanges(position){
	swal({
		text: 'Aguarde...',
		showConfirmButton: false,
	});
	$.ajax({
		url: '/favorite/removeFavCookie/' + position,
		method: 'post',
	}).done(function(ret){
		swal({
			text: ret
		}).then(function(){
			location.reload();
		});

	});
}