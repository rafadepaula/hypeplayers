<?php
    
    class Notification extends DTO {
        public $id;
        public $id_user;
        public $type;
        public $text;
        public $link;
        public $created;
        public $is_read;
        public $id_readby;
        public $read_date;
        
        public $FieldsMasks = array(
            'id_user'   => array('getDto', ['user', 'id']),
            'created'   => 'datetimeMask',
            'is_read'   => 'booleanMask',
            'id_readby' => array('getDto', ['user', 'id']),
            'read_date' => 'datetimeMask',
        );
    }