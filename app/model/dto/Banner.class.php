<?php

class Banner extends DTO {
    public $id;
    public $cover;
    public $title;
    public $subtitle;
    public $button_display;
    public $button_text;
    public $button_color;
    public $button_link;
    public $display;
    public $position;

    public $FieldsValidation = array(
        'cover' => array('validFile', ['jpg', 'jpeg', 'png']),
    );

    public $FieldsErrors = array(
        'cover' => 'Informe um banner válido.',
    );

    public $FieldsMasks = array(
    	'button_link' => 'transformLink'
    );

    public function transformLink($input){
    	$inputSb = substr($input, 0, 7);
    	if($inputSb == 'http://' || $inputSb == 'https:/')
    		return $input;
    	else
    		return 'http://'.$input;
    }

}