<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col-sm-12">
                        <label>Fundo</label>
                        <img src="/{$banner->get('cover')}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Título</label>
                        <input disabled type="text" name="title" class="form-control" value="{$banner->get('title')}">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Subtítulo</label>
                        <input disabled type="text" name="subtitle" class="form-control"  value="{$banner->get('subtitle')}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2 form-group">
                        <label>Mostrar botão</label> <br>
                        <input disabled type="checkbox" name="button_display" class="form-control" id="button_display" {if $banner->get('button_display')}checked{/if}>
                    </div>
                    <div class="col-sm-2 form-group">
                        <label>Cor de fundo</label> <br>
                        <div style="width: 20px;height: 20px;background-color: #{$banner->get('button_color')};>"></div>#{$banner->get('button_color')}
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Texto do botão</label>
                        <input type="text" id="button_text" name="button_text" class="form-control" disabled value="{$banner->get('button_text')}">
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Link de redirecionamento</label>
                        <input type="text" id="button_link" name="button_link" class="form-control" disabled value="{$banner->get('button_link')}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
