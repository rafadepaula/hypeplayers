$(function(){
	$('#view-more').click(function(){
		id_user = $(this).data('id');
		$.ajax({
			url: '/member/viewMore/' + id_user,
		}).done(function(ret){
			console.log(ret);
			if(!validJson(ret))
				return swal('Erro interno no sistema.');
			ret = $.parseJSON(ret);
			if(ret.return == 1){
				swal({
					html: ret.text
				});
			}else{
				swal('Erro', ret.text, 'error');
			}
		})
	});

	$('#accession-status').click(function(){
		id_user = $(this).data('id');
		$.ajax({
			url: '/signature/status/' + id_user,
		}).done(function(ret){
			ret = $.parseJSON(ret);
			console.log(ret);
		})
	});

	$('.change-plan').click(function(){
	    toLevel = $(this).data('to-level');

        swal({
            type: 'question',
            title: 'Confirma a solicitação?',
            text: 'Tem certeza que deseja realizar a troca para o Plano ' + toLevel + '?',
            showCancelButton: true,
            cancelButtonText: 'Não, cancelar',
            confirmButtonText: 'Sim, solicitar',
        }).then(function(){
            swal({
                title: 'Aguarde...',
                text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                showCancelButton: false,
                showConfirmButton: false,
                confirmButtonText: "Aguarde...",
                closeOnConfirm: false,
            });
            $.ajax({
                url: '/signature/requestChange/' + toLevel
            }).done(function(ret){
                console.log(ret);
                if(!validJson(ret)) {
                    return swal("Erro interno no sistema");
                }
                ret = $.parseJSON(ret);
                swal(ret).then(function(){
                    if(ret.type == 'success')
                        window.location = '/signature/';
                }, function(){
                    if(ret.type == 'success')
                        window.location = '/signature/';
                });
            });
        });
    });

	$('.reject-cancel').click(function(){
	    id_user = $(this).data('id-user');
	    swal({
            type: 'question',
            title: 'Confirma a rejeição?',
            text: 'Tem certeza que deseja negar o cancelamento deste membro?',
            showCancelButton: true,
            cancelButtonText: 'Não, voltar',
            confirmButtonText: 'Sim, cancelar',
        }).then(function(){
            $.ajax({
                url: '/signature/rejectCancel/' + id_user
            }).done(function(ret){
                console.log(ret);
                swal({
                    title: 'Aguarde...',
                    text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonText: "Aguarde...",
                    closeOnConfirm: false,
                });
                if(ret == 1){
                    swal({
                        type: 'success',
                        title: 'Rejeição confirmada',
                        text: 'O cancelamento não foi realizado.',
                        timer: 5000,
                    }).then(function(){
                        window.location = '/member/view/' + id_user;
                    }, function(){
                        window.location = '/member/view/' + id_user;
                    });
                }else{
                    swal({
                        type: 'error',
                        title: 'Ops!',
                        text: 'Houve um problema. Tente novamente.',
                        timer: 5000,
                    });
                }
            });
        });
    });

    $('.reject-change').click(function(){
        id_user = $(this).data('id-user');
        swal({
            type: 'question',
            title: 'Confirma a rejeição?',
            text: 'Tem certeza que deseja negar a troca de plano deste membro?',
            showCancelButton: true,
            cancelButtonText: 'Não, voltar',
            confirmButtonText: 'Sim, rejeitar',
        }).then(function(){
            $.ajax({
                url: '/signature/rejectChange/' + id_user
            }).done(function(ret){
                console.log(ret);

                swal({
                    title: 'Aguarde...',
                    text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                    showCancelButton: false,
                    showConfirmButton: false,
                    confirmButtonText: "Aguarde...",
                    closeOnConfirm: false,
                });
                if(ret == 1){
                    swal({
                        type: 'success',
                        title: 'Rejeição confirmada',
                        text: 'A troca não foi realizada.',
                        timer: 5000,
                    }).then(function(){
                        window.location = '/member/view/' + id_user;
                    }, function(){
                        window.location = '/member/view/' + id_user;
                    });
                }else{
                    swal({
                        type: 'error',
                        title: 'Ops!',
                        text: 'Houve um problema. Tente novamente.',
                        timer: 5000,
                    });
                }
            });
        });
    });

    $('#activate_member').click(function(){
        id_user = $(this).data('id-user');
        swal({
            type: 'question',
            title: 'Confirma a ativação?',
            text: 'Tem certeza que deseja ativar este membro?',
            showCancelButton: true,
            cancelButtonText: 'Não, cancelar',
            confirmButtonText: 'Sim, ativar',
        }).then(function(){
            swal({
                title: 'Aguarde...',
                text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                showCancelButton: false,
                showConfirmButton: false,
                confirmButtonText: "Aguarde...",
                closeOnConfirm: false,
            });
            $.ajax({
                url: '/member/toggleDeactivated/' + id_user
            }).done(function(ret){
                window.location = '/member/view/' + id_user;
            });
        });
    });

    $('#deactivate_member').click(function(){
        id_user = $(this).data('id-user');
        swal({
            type: 'question',
            title: 'Confirma a desativação?',
            text: 'Tem certeza que deseja desativar este membro?',
            showCancelButton: true,
            cancelButtonText: 'Não, cancelar',
            confirmButtonText: 'Sim, desativar',
        }).then(function(){
            swal({
                title: 'Aguarde...',
                text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                showCancelButton: false,
                showConfirmButton: false,
                confirmButtonText: "Aguarde...",
                closeOnConfirm: false,
            });
            $.ajax({
                url: '/member/toggleDeactivated/' + id_user
            }).done(function(ret){
                window.location = '/member/view/' + id_user;
            });
        });
    });
});