<?php
/* Smarty version 3.1.28, created on 2017-10-16 03:59:58
  from "C:\xampp\htdocs\hypeplayers\app\viewer\User\edit.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59e44ade5f4f27_66661344',
  'file_dependency' => 
  array (
    'bdc014bcaa912abd4c2525c99f07690fab2a8612' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\User\\edit.tpl',
      1 => 1503928961,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e44ade5f4f27_66661344 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/user/edit/">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['actualUser']->value->get('name');?>
">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>E-mail</label>
                            <input required type="email" name="email" class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['actualUser']->value->get('email');?>
">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Senha</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Confirme a senha</label>
                            <input type="password" name="passwordConfirm" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Editar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }
}
