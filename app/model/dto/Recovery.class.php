<?php
    
    class Recovery extends DTO {
        public $id;
        public $id_user;
        public $secret;
        public $date;
        public $done;
        
        public $FieldsMasks = array(
            'id_user' => array('getDto', ['user', 'id']),
        );
        
    }