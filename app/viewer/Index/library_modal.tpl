<div class="row text-center">
    <div class="col-sm-8 col-sm-offset-2">
        <h2 class="title-one">Biblioteca de jogos</h2>
    </div>
    <div class="col-xs-12 text-center">
        <button id="ps4" class="btn btn-default filter-button filter-button-active" data-filter="ps4">PS4</button>
        <button class="btn btn-default filter-button" data-filter="xone">XBOX One</button>
        <button class="btn btn-default filter-button" data-filter="switch">Switch</button>
    </div>
</div>
<div class="row">
    <!-- platform by platform !-->
    {foreach from=$gamesArray item=$pages key=platform}
        <div class="carousel slide filter library-carousel
                        {$platform} {$platform}-carousel" id="{$platform}-carousel">
            <div class="carousel-inner">

                <!-- page by page !-->
                {foreach from=$pages item=$games key=page}
                    {if $page == 0}
                        {assign var="active" value="active"}
                    {else}
                        {assign var="active" value=""}
                    {/if}
                    <div class="item {$active}">

                        <div class="hidden-xs">
                            <!-- game by game of above page !-->
                            {foreach $games as $game}
                                <div class="col-sm-3">
                                    <img src="/{$game->firstCover()}" class="img-responsive game-library" data-name="{$game->get('name')}" data-id="{$game->get('id')}">
                                </div>
                            {/foreach}
                        </div>

                        <div class="visible-xs">
                            <div class="row">
                                {foreach from=$games key=i item=$game}
                                {if $i % 2 == 0}
                            </div>
                            <div class="row">
                                {/if}
                                <div class="col-xs-6">
                                    <img src="/{$game->firstCover()}" class="img-responsive game-library" data-name="{$game->get('name')}" data-id="{$game->get('id')}">
                                </div>

                                {if $i == ($games|@count - 1)}
                            </div>
                            {/if}
                            {/foreach}
                        </div>

                    </div>
                {/foreach}

            </div>
            <a data-slide="prev" href="#{$platform}-carousel" class="left carousel-control">‹</a>
            <a data-slide="next" href="#{$platform}-carousel" class="right carousel-control">›</a>
        </div>
    {/foreach}
</div>