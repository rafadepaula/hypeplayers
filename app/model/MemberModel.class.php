<?php
    
    class MemberModel extends AppModel {
        public function getMember($id_user){
            return $this->getDto('member', 'id_user', $id_user);
        }

        public function home(){
            $user = unserialize($_SESSION['user']);
            $favorites = $this->search('favorite', '*', array('id_user' => $user->get('id')), 'position');
            $history = $this->search('rent', '*', array('id_user' => $user->get('id')), 'start', false, 10);
            $signatureModel = new SignatureModel();
            $signature = $signatureModel->getSignature($user->get('id'));
            $rents = $this->search('rent', '*', array('id_user' => $user->get('id')), 'end DESC', false, 3);

            return array(
                'favorites' => $favorites,
                'history' => $history,
                'signature' => $signature,
                'recents' => $rents,
            );
        }

        public function save($id_user = null){
            $this->initTransaction();
            if(!isset($_POST['terms'])){
            	echo '<li>Você precisa aceitar os termos e condições!</li>';
            	$this->cancelTransaction();
            	return false;
            }
            	
	        $_POST['name'] .= ' '.$_POST['last_name'];
	        $regionModel = new RegionModel();
            // creates the user
            $newMember = is_null($id_user);
            $userModel = new UserModel();
            if(!$userModel->save($id_user, 'member', $newMember)){
                $this->cancelTransaction();
                return false;
            }
            if($newMember)
                $id_user = $this->lastInserted('user');
            else
                $id_user = $id_user;

            // initiates the member creation proccess  
            $member = $this->getMember($id_user);
            $picture = $member->get('picture');
            $secret = $member->get('secret');
            $ret  = $this->makeDto($member, $id_user);
            $member = $ret[0];

            $member->set('id_user', $id_user);
            $member->set('picture', $picture);

	        if($newMember){
		        // secret required for the activation
		        $secret = md5(_PASS_SALT_.date('Y-m-d H:i:s')._PASS_SALT_);
		        $member->set('secret', $secret);

		        // creates new address
		        $addressModel = new AddressModel();
		        if(!$addressModel->saveByJoin($id_user, $regionModel)){
		        	$this->cancelTransaction();
		        	return false;
		        }
	        }


	        // searches the region by cep and sets "id_region" column
            $cep = str_replace('-', '', $member->get('cep'));
            $region = $regionModel->getRegionByCep($cep);
            if(empty($region->get('name'))){
                if($newMember)
                    echo '<li>Região inválida!</li>';
                else
                    Viewer::flash('Região inválida!', 'e');
                $this->cancelTransaction();
                return false;
            }
            $member->set('id_region', $region->get('id'));

            // picture upload
            /*if($_FILES['picture']['name'] != ''){
                $picture = new File(_PROFILE_PICTURES_DIR, 'picture');
                $dir = $picture->finish();
                if(!$member->set('picture', $dir)){
                    if($newMember)
                        echo '<li>'.$member->FieldsErrors['picture'].'</li>';
                    else
                        Viewer::flash($member->FieldsErrors['picture'], 'e');
                    unlink($dir);
                    $this->cancelTransaction();
                    return false;
                }
            }*/

            // signature creation
            $signatureModel = new SignatureModel();
            if(!$signatureModel->create($member, $member->get('secret'))){
                $this->cancelTransaction();
                return false;
            }
            
            $errors = $ret[1];
            if($errors != ''){
                if($newMember)
                    echo '<li>'.substr(str_replace('<br>', '</li><li>', $errors), 0, strlen(str_replace('<br>', '</li><li>', $errors)) - 4);
                else
                    Viewer::flash($errors, 'e');
                $this->cancelTransaction();
                return false;
            }else{
                
                // if id is null insert else update
                $ret = $newMember ? $this->insert('member', $member) : $this->update('member', $member, array('id_user' => $id));
                if($ret){

                    // sends the confirmation email
                    if($newMember){
                        if(!$this->confirmEmail($_POST['email'], $_POST['name'], $member->get('secret'))){
                            $this->cancelTransaction();
                            return false;
                        }

                        $notificationModel = new NotificationModel();
                        $text = 'Novo membro com cadastro prévio';
                        $notificationModel->send('admin', $text, '/member/view/'.$id_user);
                    }

                    $this->endTransaction();
                    return true;
                }else{
                    if($newMember)
                        echo '<li>'._INSERT_ERROR.'</li>';
                    else
                        Viewer::flash(_INSERT_ERROR, 'e');
                    $this->cancelTransaction();
                    return false;
                }
            }
        }

        public function edit($id_user){
            $user = $this->getDto('user', 'id', $id_user);
            $errors = '';
            if(!empty($_POST['name']))
                $user->set('name', $_POST['name']);
            else
                $errors .= 'Informe seu nome!<br>';

            $member = $this->getMember($id_user);

            if(!$member->set('phone', $_POST['phone']))
                $errors .= 'Informe um telefone válido.<br>';
            if(!$member->set('cep', $_POST['cep']))
                $errors .= 'Informe um CEP válido.<br>';
            if(!$member->set('street', $_POST['street']))
                $errors .= 'Informe uma rua válida.<br>';
            if(!$member->set('number', $_POST['number']))
                $errors .= 'Informe um número válido.<br>';
            if(!$member->set('neighborhood', $_POST['neighborhood']))
                $errors .= 'Informe um bairro válido.<br>';
            if(!$member->set('complement', $_POST['complement']))
                $errors .= 'Informe um complemento válido.<br>';
            if(!$member->set('city', $_POST['city']))
                $errors .= 'Informe uma cidade válida.<br>';
            if(!$member->set('state', $_POST['state']))
                $errors .= 'Informe um estado válido.<br>';

	        // searches the region by cep and sets "id_region" column
            $cep = str_replace('-', '', $member->get('cep'));
            $regionModel = new RegionModel();
            $region = $regionModel->getRegionByCep($cep);
            if(empty($region->get('name'))){
	            $errors .= 'Região inválida!<br>';
            }
            $member->set('id_region', $region->get('id'));

            if(!empty($errors)){
                Viewer::flash($errors, 'e');
                return false;
            }else{
                $this->initTransaction();
                if(!$this->update('user', $user, array('id' => $id_user)) ||
                   !$this->update('member', $member, array('id_user' => $id_user))){
                    $this->cancelTransaction();
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }else{
                    $_SESSION['user'] = serialize($user);
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    $this->endTransaction();
                    return true;
                }
            }

        }

        public function confirmEmail($to, $name, $secret){
            require_once 'base/plugins/phpmailer/PHPMailerAutoload.php';
            
            $email = new PHPMailer;
            
            $email->isSMTP();
            $email->Host       = 'mail.hypeplayers.com.br';
            $email->SMTPAuth   = true;
            $email->Username   = 'webmaster@hypeplayers.com.br';
            $email->Password   = 'Zs9w63Q0oy';
            $email->SMTPSecure = false;
            $email->Port       = 26;
            
            $email->From     = 'webmaster@hypeplayers.com.br';
            $email->FromName = 'HypePlayers';
            $email->addBCC($to, $name);
            $email->isHTML(true);
            
            $email->Subject = 'HypePlayers - Confirmação de e-mail';

            $html = file_get_contents('app/viewer/Member/confirmEmail.html');
            $html = str_replace('%url_confirm%', _BASE_URL_.'/member/activate/'.$secret, $html);

            $email->Body    = $html;
            $email->CharSet = 'UTF-8';
            
            return $email->send();
        }
        
        public function activate($secret){
            $member = $this->search('member', '*', array('secret' => $secret));
            if(!count($member))
                return false;
            $member = $member[0];
            $member->set('activated', 1);
            $user = $this->getDto('user', 'id', $member->get('id_user'));

            $_POST['email'] = $user->get('email');
            $_POST['password'] = $user->get('password');
            if(!$this->update('member', $member, array('id_user' => $member->get('id_user'))))
                return false;

            $notificationModel = new NotificationModel();
            $notificationModel->newMemberActivated($member->get('id_user'));

            return $this->login();
        }

		public function toggleDeactivated($id_user){
			$member = $this->getMember($id_user);
			$member->set('deactivated', !$member->get('deactivated'));

			$this->initTransaction();
			if ($this->update('member', $member, array('id_user' => $id_user))) {
				if($member->get('deactivated')) {
					$signatureModel = new SignatureModel();
					if (!$signatureModel->suspendByDeactivation($id_user)) {
						$this->cancelTransaction();
						Viewer::flash(_INSERT_ERROR, 'e');
						return false;
					}
					$signature = $this->getDto('signature', 'id_user', $id_user);
					$accession_code = $signature->get('accession_code');
					$this->endTransaction();
					Viewer::flash('Status do membro alterado com sucesso.');
					$this->pagseguro->cancel($accession_code);
				}
				$this->endTransaction();
				Viewer::flash('Status do membro alterado com sucesso.');
				return true;
			} else {
				$this->cancelTransaction();
				Viewer::flash(_INSERT_ERROR, 'e');
				return false;
			}
		}
    }