<?php
    
    class Game extends DTO {
        public $id;
        public $id_genre;
        public $name;
        public $ps4;
        public $xone;
        public $switch;
        public $ps4_cover = _DEFAULT_COVER;
        public $xone_cover = _DEFAULT_COVER;
        public $switch_cover = _DEFAULT_COVER;
        public $ps4_qtt;
        public $xone_qtt;
        public $switch_qtt;
        public $description;
        public $available = 1;
        
        public $FieldsValidation = array(
            'name'    => 'notEmpty',
            'id_genre' => array('existsForeign', ['genre', 'id']),
            'ps4_cover' => array('validFile', ['jpg', 'jpeg', 'png']),
            'xone_cover' => array('validFile', ['jpg', 'jpeg', 'png']),
            'switch_cover' => array('validFile', ['jpg', 'jpeg', 'png']),
        );
        public $FieldsErrors     = array(
            'name'    => 'Informe um nome para o jogo.',
            'id_genre' => 'Informe um gênero válido para o jogo.',
            'ps4_cover' => 'Informe uma capa válida para PS4.',
            'xone_conver' => 'Informe um gênero válido para XBOX One.',
            'switch_cover' => 'Informe um gênero válido para Nintendo Switch.',
        );
        public $FieldsMasks      = array(
            'id_genre' => array('getDto', 'genre'),
            'description' => 'nl2brMask',
            'available' => 'booleanMask',
        );

        public function platforms(){
            $platforms = '';
            if($this->ps4){
                $platforms .= 'PS4, ';
            }
            if($this->xone){
                $platforms .= 'XBOX One, ';
            }
            if($this->switch){
                $platforms .= 'Switch';
            }
            $platforms = trim($platforms, ' ,');
            return $platforms;
        }
        
        public function firstCover(){
            if(!empty($this->ps4_cover)){
                return $this->ps4_cover;
            }
            if(!empty($this->xone_cover)){
                return $this->xone_cover;
            }
            if(!empty($this->switch_cover)){
                return $this->switch_cover;
            }
            return _DEFAULT_COVER;
        }
    }