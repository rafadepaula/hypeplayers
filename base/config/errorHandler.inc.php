<?php

    /**
     * Handles the normal errors
     *
     * @param String $errno - type
     * @param String $errstr - error type
     * @param String $errfile - file
     * @param String $errline - line
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.1 - 2017-08-12
     *
     **/
    function errorHandler($errno, $errstr, $errfile, $errline) {
        if (!strpos($errstr, 'deprecated') > 0) {
            $_SESSION['phpError'] = _PHP_ERROR;

            if(_LOG_LEVEL == 3){
                $model = new Model();
                $error = new Error($errstr, $errfile, $errline, date('Y-m-d H:i:s'));
                $model->insert('error', $error);
            }
        }

    }

    /**
     * Handle the fatal errors
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
    */
    function fatalErrorHandler() {
        $lastError = error_get_last();
        if ($lastError['type'] === E_ERROR) {
            errorHandler(E_ERROR, $lastError['message'], $lastError['file'], $lastError['line']);
            Controller::siteIndex();
        }
    }

    /*
     * Activates the error handling only if not in development.
     */
    if (!_IN_DEV_ && _LOG_LEVEL > 1) {
        set_error_handler("errorHandler");
        register_shutdown_function('fatalErrorHandler');
    }
