<div class="row">
    <div class="col-sm-12">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="material-icons">money</i>
            </div>
            <div class="card-content">
                <p class="category">Pagamentos registrados</p>
                <h3 class="title">{$payments}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    Clique para visualizar os {$payments} pagamentos registrados.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Membro</th>
                            <th>Data</th>
                            <th>Valor</th>
                            <th>ID Plano</th>
                            <th>ID Acordo</th>
                            <th>ID Pagador</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $planHistories as $plan}
                        <tr>
                            <td>{$plan->get('id_user', true)->get('name')}</td>
                            <td>{$plan->get('date', true)}</td>
                            <td>{$plan->get('value', true)}</td>
                            <td>{$plan->get('id_plan')}</td>
                            <td>{$plan->get('id_agreement')}</td>
                            <td>{$plan->get('id_payer')}</td>
                            <td class="td-actions">
                                <a href="/region/view/{$region->get('id')}">
                                    <button type="button" rel="tooltip" title="Visualizar {$region->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>