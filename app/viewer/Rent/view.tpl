
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/rent/plan" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">
                                <i class="fa fa-arrow-left" style="margin-right: 5px;"></i> Voltar
                            </span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="row text-center">
                    <form action="/rent/view" method="post">
                        <div class="col-sm-12">
                            <h4>Período atual: {$_filter_period}</h4>
                        </div>
                        <div class="col-sm-4 col-sm-offset-4 col-xs-10 col-xs-offset-2 text-center input-group">
                            <input type="text" class="form-control center-placeholder mask-dateinterval"
                                   placeholder="Filtrar por período" value="{$_filter_period}" required="" name="_filter_period">
                            <div class="input-group-addon">
                                <button class="btn btn-white btn-round btn-just-icon" type="submit">
                                    <i class="material-icons">search</i><div class="ripple-container"></div>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <div class="col-sm-12">
                        <p>Ações em massa:</p>
                    </div>
                <table class="table table-hover table-striped datatable-rent">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Membro</th>
                            <th>Jogo</th>
                            <th>Status</th>
                            <th>Período</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $rents as $rent}
                        <tr>
                            <td>
                                <input type="checkbox" class="datatable-checkbox" data-id="{$rent->get('id')}">
                            </td>
                            <td>
                                {$rent->get('id_user', true)->get('name')}
                            </td>
                            <td>
                                {$rent->get('id_game', true)->get('name')}
                                ({$rent->get('platform', true)})
                            </td>
                            <td>
                                {$rent->get('status', true)}
                            </td>
                            <td>
                                {$rent->get('start', true)} até {$rent->get('end', true)}
                            </td>
                            <td class="td-actions">
                                <a>
                                    <button type="button" rel="tooltip" title="Visualizar locação"
                                            class="btn btn-info btn-simple view-one"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a>
                                    <button type="button" rel="tooltip" title="Alterar status"
                                            class="btn btn-success btn-simple change-status"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

{include file=$smarty.current_dir|cat:"/changeStatus_modal.tpl"}
{include file=$smarty.current_dir|cat:"/view_modal.tpl"}