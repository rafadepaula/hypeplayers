<?php
    
    class FeedbackModel extends AppModel {
        public function getFeedback($id){
            return $this->getDto('feedback', 'id', $id);
        }
        

        public function send(){
            $feedback = new Feedback();
            $ret = $this->makeDto($feedback, null);
            $errors = $ret[1];
            $feedback = $ret[0];

            $feedback->set('id_user', unserialize($_SESSION['user'])->get('id'));
            $feedback->set('created', date('Y-m-d H:i:s'));

            if(!empty($errors)){
                echo json_encode(array(
                    'type' => 'error',
                    'title' => 'Ops!',
                    'text' => $errors,
                    'timer' => 3000,
                ));
                return false;
            }else{
                if($this->insert('feedback', $feedback)){
                    echo json_encode(array(
                        'type' => 'success',
                        'title' => 'Muito obrigado!',
                        'text' => 'Feedback recebido. Obrigado por sua opinião!',
                        'timer' => 3000,
                    ));
                    return true;
                }else{
                    echo json_encode(array(
                        'type' => 'error',
                        'title' => 'Ops!',
                        'text' => 'Algum problema aconteceu... Por favor, tente novamente.',
                    ));
                    return false;
                }
            }
        }
        
    }