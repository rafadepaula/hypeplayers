
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/game/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                    <a href="/genre/" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">Gêneros</span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Gênero</th>
                            <th>Plataformas</th>
                            <th>Disponível</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $games as $game}
                        <tr>
                            <td>
                                {$game->get('name')}
                            </td>
                            <td>
                                {$game->get('id_genre', true)->get('name')}
                            </td>
                            <td>
                                {$game->platforms()}
                            </td>
                            <td>
                                {$game->get('available', true)}
                            </td>
                            <td class="td-actions">
                                <a href="/game/edit/{$game->get('id')}">
                                    <button type="button" rel="tooltip" title="Editar {$game->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                <a href="/game/view/{$game->get('id')}">
                                    <button type="button" rel="tooltip" title="Visualizar {$game->get('name')}"
                                            class="btn btn-info btn-simple">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                {if $game->get('available')}
                                    <a class="confirm-link" href="/game/toggle/{$game->get('id')}">
                                        <button type="button" rel="tooltip" title="Remover {$game->get('name')} da lista"
                                                class="btn btn-danger btn-simple">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </a>
                                {else}
                                    <a class="confirm-link" href="/game/toggle/{$game->get('id')}">
                                        <button type="button" rel="tooltip" title="Disponibilizar {$game->get('name')} na lista"
                                                class="btn btn-success btn-simple">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </a>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>