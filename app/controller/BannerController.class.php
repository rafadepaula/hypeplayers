<?php

    class BannerController extends AppController{

        public function index(){
            $userController = new UserController();
            return $userController->adminIndex();
        }

        public function add(){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->request()){
                if($this->model->save()){
                    $id = $this->model->lastInserted('banner');
                    return $this->view($id);
                }else{
                    unset($_POST);
                    unset($_FILES);
                    return $this->add();
                }
            }

            $this->viewer->addCss('plugins/spectrum/spectrum.css');
            $this->viewer->addJs('plugins/spectrum/spectrum.js');
            $this->viewer->addJs('assets/js/admin_index.js');

            return $this->viewer->show('add', 'Cadastrar banner');
        }

        public function view($id = null){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!is_null($id)){
                if(!$this->model->exists('banner', 'id', $id)){
                    Viewer::flash(_EXISTS_ERROR, 'e');
                    return $this->index();
                }
                $banner = $this->model->getBanner($id);
                $this->viewer->set('banner', $banner);

                return $this->viewer->show('view_one', $banner->get('title'));
            }
            $banner = $this->model->search('banner');
            $this->viewer->set('banner', $banner);

            return $this->viewer->show('view', 'Notícias do blog');
        }

        public function update($id){
            if($this->user->get('role') != 'admin'){
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if(!$this->model->exists('banner', 'id', $id)){
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            $banner = $this->model->getBanner($id);
            $this->viewer->set('banner', $banner);

            return $this->viewer->show('update', 'Atualizar notícia do blog');
        }

        public function delete($id){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if (!$this->model->exists('banner', 'id', $id)) {
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            if ($this->model->delete('banner', array('id' => $id))) {
            	$this->model->reorder(true);
                Viewer::flash(_DELETE_SUCCESS, 's');
            } else {
                Viewer::flash(_DELETE_ERROR, 'e');
            }
            return $this->index();
        }

        public function indexBanners(){
            return $this->model->search('banner', '*', array('display' => 1), 'position');
        }

        public function reorder($total = false){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if($this->user->get('role') != 'admin'){
                return;
            }
            if($total){
                return $this->model->reorder($total);
            }else{
                if(!isset($_POST['diff'])){
                    echo "Diferenças não encontradas.";
                    return;
                }
                if(empty($_POST['diff'])){
                    echo 1;
                    return;
                }
                $diff = $_POST['diff'];
                $sql = $this->model->reorderSql($diff);
                $this->model->sql($sql);
                echo 1;
            }
            return;
        }

        public function toggle($id){
            if ($this->user->get('role') != 'admin') {
                Viewer::flash(_PERMISSION_ERROR, 'e');
                return $this->siteIndex();
            }
            if (!$this->model->exists('banner', 'id', $id)) {
                Viewer::flash(_EXISTS_ERROR, 'e');
                return $this->index();
            }
            $banner = $this->model->getBanner($id);
            $banner->set('display', !$banner->get('display'));
            $this->model->reorder(true);
            if($banner->get('display')){
            	$banner->set('position', $this->model->calcLastPosition());
            }
            if($this->model->update('banner', $banner, array('id' => $id))){
                Viewer::flash(_INSERT_SUCCESS, 's');
            }else{
                Viewer::flash(_INSERT_ERROR, 'e');
            }
            return $this->index();
        }

    }