<?php
    
    /**
     * File handling.
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    class File{

        private $folder;
        private $name;
        private $extension;
        private $finalDir;
        private $tempDir;
        private $size;
    
        /**
         * File constructor. Makes everything.
         * You just need to instantiate the class, then call the method finish.
         *
         * @param String $folder - destiny folder
         * @param String $post - field name in $_FILES
         * @param int $position - if are multiple files in one field, specify the position. (optional).
         */
        public function __construct($folder, $post, $position = null){
            if(is_null($position)){
                $this->name = $_FILES[$post]['name'];
                $this->tempDir = $_FILES[$post]['tmp_name'];
                $this->size = $_FILES[$post]['size'];
            }else{
                $this->name = $_FILES[$post]['name'][$position];
                $this->tempDir = $_FILES[$post]['tmp_name'][$position];
                $this->size = $_FILES[$post]['size'][$position];
            }

            if(rtrim($folder, '/') == $folder)
                $folder .= '/';
            $this->folder = $folder;


            $this->extension = $this->getExtension();


            $this->takeOffExtension();
            $this->takeOffAccent();
            $this->takeOffBlank();

        }
    
        /**
         * Save the file in specified folder
         *
         * @param String $subfolder - if need some subfolder to save
         * @return String - file destiny, based on _APP_ dir. Usually /app/assets/files.
         */
        public function finish($subfolder = null){
            if(!is_null($subfolder))
                $this->folder .= $subfolder.'/';
            $this->folderExists();
            $this->finalDir = $this->makeDir();
            move_uploaded_file($this->tempDir, $this->finalDir);
            return $this->getFinalDir();
        }

        public function getFinalDir(){
            return $this->finalDir;
        }
    
        /**
         * Take off the file extension
         *
         * @return string - filename without extension
         */
        public function takeOffExtension(){
            $name = explode('.', $this->name);
            $qttPoints = count($name);
            $newName = '';
            for($i = 0; $i < $qttPoints - 1; $i++){
                $newName .= $name[$i];
            }
            $this->name = $newName;
            return $newName;
        }
    
        public function getExtension(){
            $extension = explode('.', $this->name);
            $extension = strtolower($extension[count($extension)-1]);
            return $extension;
        }
    
        /**
         * Makes the final directory. If the file already exists,
         * it counts until find one that doesn't exists.
         * @return string - final dir
         */
        public function makeDir(){
            $dir = $this->folder.$this->name.'.'. $this->extension ;
            if(file_exists($dir)){
                $exists = true;
                $i = 0;
                while($exists){
                    $i++;
                    $dir = $this->folder.$this->name.'('.$i.')'.'.'. $this->extension;
                    $exists = file_exists($dir);
                }
            }
            return $dir;
        }
    
        /**
         * Check if the specified folder doesn't exists. If not, creates a new one.
         */
        public function folderExists(){
            if(!is_dir($this->folder)){
                mkdir($this->folder, 0777, true);
            }
        }
    
        /**
         * Transforms the whitespaces into underlines.
         * @return string
         */
        public function takeOffBlank(){
            $this->name = str_replace(" ", "_", $this->name);
            return $this->name;
        }
    
        public function takeOffAccent(){
            $enc = 'UTF-8';
            $acentos = array(
                'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
                'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
                'C' => '/&Ccedil;/',
                'c' => '/&ccedil;/',
                'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
                'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
                'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
                'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
                'N' => '/&Ntilde;/',
                'n' => '/&ntilde;/',
                'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
                'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
                'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
                'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
                'Y' => '/&Yacute;/',
                'y' => '/&yacute;|&yuml;/',
                'a.' => '/&ordf;/',
                'o.' => '/&ordm;/'
            );

            $this->name =  preg_replace($acentos,array_keys($acentos),htmlentities($this->name,ENT_NOQUOTES, $enc));
            return $this->name;
        }

        public function size(){
            return $this->size;
        }

    }
