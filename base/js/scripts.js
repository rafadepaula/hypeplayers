$(function(){
	/**
	 * INDEX
	 * 1 - Browser detection
	 * 2 - Filters
	 * 3 - Form submition
	 * 4 - Link permissions
     * 5 - Confirm link trigger
	 * F1 - Confirm link
	*/

	// 1 - Browser detection
    isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    isExplorer = navigator.userAgent.indexOf('MSIE') > -1;
    isFirefox = navigator.userAgent.indexOf('Firefox') > -1;
    isSafari = navigator.userAgent.indexOf("Safari") > -1;
    isOpera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
    if ((isChrome)&&(isSafari)) {isSafari=false;}
    if ((isChrome)&&(isOpera)) {isChrome=false;}

    // $('.filter').on('click', function(){
    //     type = $(this).attr('filter-type');
    //     el = $(this);

    //     // Gray all elements of this type
    //     gray(type);

    //     // Blue this element
    //     el.removeClass('label-default');
    //     el.addClass('label-primary');
        
    //     // Set the filter value
    //     $('input[name=_filter_'+type+']').val(el.attr('value'));
    // })

    // // 2 - Filters
    // $('.filter-input').each(function(){
    //     val = $(this).val();
    //     type = $(this).attr('filter-type');
        
    //     gray(type);

    //     $('span[filter-type='+type+'][value='+val+']').removeClass('label-default');
    //     $('span[filter-type='+type+'][value='+val+']').addClass('label-primary');

    // })

    // 3 - Form submition
    $('form').submit(function(e){

        if($(this).attr('target') != '_blank' && !$(this).hasClass('ignore-wait')){
            var event = $(document).click(function(e) {
                            e.stopPropagation();
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            return false;
                        });
            swal({
                title: 'Aguarde...',
                text: 'A ação desejada foi efetuada. Por favor, aguarde o carregamento da página.',
                showCancelButton: false,
                showConfirmButton: false,
                confirmButtonText: "Aguarde...",
                closeOnConfirm: false,
            },
            function(){

            });
        }


        $('input').each(function(){
            if(!isChrome && !isSafari){
                if($(this).attr('type') == 'date'){
                    if($(this).val() !== ''){
                        data = $(this).val();
                        data = data.split('/');
                        if(data.length > 0){
                            data = data[2] + '-' + data[1] + '-' + data[0];
                            $(this).inputmask('9999-99-99');
                            $(this).val(data);
                        }
                    }
                }
            }
        });
    });

    // // 4 - Link permissions
    // $('a').click(function (event) {
    //     event.preventDefault();
    //     if(!confirmLink($(this))){
    //         return false;
    //     }
    //     url = $(this).attr('href');
    //     if(typeof $(this).attr('target') != 'undefined'){
    //         blank = true;
    //     }else{
    //         blank = false;
    //     }
    //     urlOriginal = url;
    //     controlador = '';
    //     acao = '';
    //     if (typeof url != 'undefined') {
    //         url = url.split('/');
    //         if (url[url.length - 1] != '#' && urlOriginal.indexOf('#') == -1) {
    //             controller = url[0];
    //             if (typeof url[0] != 'undefined' && typeof url[1] != 'undefined') {
    //                 controlador = url[0];
    //                 acao = url[1];
    //                 if(acao == '')
    //                     acao = 'view';

    //                 $.ajax({
    //                     type: "GET",
    //                     url: "index.php",
    //                     data: {ctrl: "app", act: "permission", ctrl: controlador, act: acao}
    //                 }).done(function (result) {
    //                     if (result == 0) {
    //                         swal({
    //                             title: 'Ops!',
    //                             text: 'Você não possui permissão para acessar esta página!',
    //                             showCancelButton: true,
    //                             showConfirmButton: false,
    //                             cancelButtonText: "Ok",
    //                             timer: 2000,
    //                             type: 'error'
    //                         });
    //                     } else {
    //                         if(blank){
    //                             window.open(urlOriginal);
    //                         }else {
    //                             $(location).attr('href', urlOriginal)
    //                         }
    //                     }
    //                 })
    //             }else{
    //                 if(blank){
    //                     window.open(urlOriginal);
    //                 }else {
    //                     $(location).attr('href', urlOriginal)
    //                 }
    //             }
    //         }

    //     }


    // });

    // 5 - Confirm link trigger
    $('.confirm-link').click(function (event) {
        return confirm('Você tem certeza?');
    });
})

// F1 - Confirm link
function confirmLink(e){
    if(e.hasClass('confirm-link')){
        return confirm('Você tem certeza?');
    }else{
        return true;
    }
}

$(function(){
    $('.qtip-init').each(function(){
        $(this).qtip({
            position: 'bottom'
        });
    })
})