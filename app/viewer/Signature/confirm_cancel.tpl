<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                </h4>
            </div>
            <div class="card-content">
                <div class="row text-center">
                    <div class="col-sm-12">
                        <b>ATENÇÃO:</b>
                        você está prestes a confirmar o cancelamento da assinatura do membro.
                    </div>
                    <div class="col-sm-6 col-sm-offset-3">
                        <a style="cursor: pointer;" class="confirm-cancel" data-id-user="{$id_user}">
                            <button class="btn btn-block btn-warning btn-lg">
                                Cancelar a assinatura do membro
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>