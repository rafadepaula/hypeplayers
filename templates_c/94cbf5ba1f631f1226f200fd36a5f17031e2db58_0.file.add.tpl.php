<?php
/* Smarty version 3.1.28, created on 2017-09-13 12:19:56
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\add.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59b94c9c6f1742_84024974',
  'file_dependency' => 
  array (
    '94cbf5ba1f631f1226f200fd36a5f17031e2db58' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\add.tpl',
      1 => 1505314830,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59b94c9c6f1742_84024974 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/game/add" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Gênero</label>
                            <select required name="id_genre" class="form-control">
                                <?php
$_from = $_smarty_tpl->tpl_vars['genres']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_genre_0_saved_item = isset($_smarty_tpl->tpl_vars['genre']) ? $_smarty_tpl->tpl_vars['genre'] : false;
$_smarty_tpl->tpl_vars['genre'] = new Smarty_Variable();
$__foreach_genre_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
if ($__foreach_genre_0_total) {
foreach ($_from as $_smarty_tpl->tpl_vars['genre']->value) {
$__foreach_genre_0_saved_local_item = $_smarty_tpl->tpl_vars['genre'];
?>
                                    <option value="<?php echo $_smarty_tpl->tpl_vars['genre']->value->get('id');?>
"><?php echo $_smarty_tpl->tpl_vars['genre']->value->get('name');?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_local_item;
}
}
if ($__foreach_genre_0_saved_item) {
$_smarty_tpl->tpl_vars['genre'] = $__foreach_genre_0_saved_item;
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Plataformas</label> <br>
                            <label class="checkbox-inline"><input type="checkbox" name="ps4" value="1">PS4</label>
                            <label class="checkbox-inline"><input type="checkbox" name="xone" value="1">XBOX One</label>
                            <label class="checkbox-inline"><input type="checkbox" name="switch" value="1">Nintendo Switch</label>
                        </div>
                    </div>
                    <div class="row" id="ps4_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para PS4</label> <br>
                            <input type="number" name="ps4_qtt" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para PS4</label>
                            <input type="file" name="ps4_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row" id="xone_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para XBOX One</label> <br>
                            <input type="number" name="xone_qtt" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para XBOX One</label>
                            <input type="file" name="xone_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row" id="switch_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para Nintendo Switch</label> <br>
                            <input type="number" name="switch_qtt" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para Nintendo Switch</label>
                            <input type="file" name="switch_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Fotos</label>
                            <input type="file" multiple name="images[]" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Vídeo (YouTube)</label>
                            <input type="text" name="video" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group" >
                            <label>Descrição</label>
                            <textarea name="description" rows="7" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }
}
