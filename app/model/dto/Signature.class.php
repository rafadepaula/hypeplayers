<?php
    
    class Signature extends DTO {
        public $id_user;
        public $id_preapproval;
        public $reference;
        public $accession_code;
        public $level;
        public $available_rent;
        public $since;
        public $last_payment;
        public $expiry;
        public $value;
        public $status;
        public $simultaneous;
        public $change_requested;
        public $can_change;
        public $renew_requested;
        public $change_count;
        public $holder_name;
        public $holder_birthdate;
        public $holder_cpf;
        
        public $FieldsValidation = array(
            'level' => 'validLevel',
            'value' => 'validMoney',
            'holder_cpf' => 'validCpf',
            'holder_birthdate' => 'validDate',
        );
        public $FieldsErrors     = array(
            'level' => 'Plano inválido.',
            'value' => 'Valor da mensalidade inválida.',
        );
        public $FieldsMasks      = array(
            'id_user' => ['getDto', ['user', 'id']],
            'level' => 'levelMask',
            'since' => 'dateMask',
            'last_payment' => 'datetimeMask',
            'expiry' => 'dateMask',
            'value' => 'moneyMask',
            'status' => 'statusMask',
            'change_requested' => 'booleanMask',
            'renew_requested' => 'booleanMask',
            'can_change' => 'booleanMask',
        );
        
        public function validLevel($input){
            return $input == 1 || $input == 2 || $input == 3;
        }

        public function levelMask($input){
            $levels = array('Plano Bronze', 'Plano Prata', 'Plano Ouro');
            return $levels[$input - 1];
        }

        public function statusMask($input){
            $status = array(
                'Active' => 'Ativo', // pagamento em dia
                'Canceled' => 'Cancelado', // cancelou
                'Suspended' => 'Suspenso', // não realizou pagamento
                'Pending' => 'Pendente', // ativação pendente
                'Preactivated' => 'Pré-ativado', // processando primeiro pagamento
                'Taxed' => 'Multado',
                'Cancel_requested' => 'Cancelamento requisitado',
                'Change_requested' => 'Troca de plano requisitada',
            );
            return isset($status[$input]) ? $status[$input] : 'Não identificado';
        }

        public function activeStatus(){
            return $this->status == 'Active' || $this->status == 'Change_requested';
        }

        public function canceledStatus(){
            return $this->status == 'Canceled' || $this->status == 'Suspended';
        }
    }