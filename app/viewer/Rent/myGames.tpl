
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/rent/history" class="pull-right card-header-btn" style="margin-right: 5px">
                        <button class="btn btn-white btn-round" >
                            <span style="font-size: 12px;">
                                <i class="fa fa-history" style="margin-right: 5px;"></i> Ver histórico
                            </span>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Jogos em minha posse</h4>
                    </div>
                    {if $signature->get('can_change')}
                        <div class="col-sm-4 col-sm-offset-4">
                            <button class="btn btn-default btn-success change-all">
                                Solicitar troca de todos jogos
                            </button>
                        </div>
                    {/if}
                </div>
                <div class="table-responsive">
                <table class="table table-hover table-striped help" data-help="game-table">
                    <thead>
                        <tr>
                            <th>Jogo</th>
                            <th>Status</th>
                            <th>Período</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $rents as $rent}
                        <tr>
                            <td>
                                {$rent->get('id_game', true)->get('name')}
                                ({$rent->get('platform', true)})
                            </td>
                            <td>
                                {if $rent->get('status') == 'waiting_post'}
                                    <b>{$rent->get('status', true)}</b>
                                {else}
                                    {$rent->get('status', true)}
                                {/if}
                            </td>
                            <td>
                                {$rent->get('start', true)} até {$rent->get('end', true)}
                            </td>
                            <td class="td-actions">
                                <a class="help" data-help="rent-view">
                                    <button type="button" rel="tooltip" title="Visualizar locação"
                                            class="btn btn-default btn-simple view-one"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                </a>
                                <a class="help" data-help="rent-comment">
                                    <button type="button" rel="tooltip" title="Enviar comentário"
                                            class="btn btn-default btn-simple send-comment"
                                            data-id-rent="{$rent->get('id')}">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                                {if $rent->get('status') == 'delivered' || $rent->get('status') == 'delayed'}
                                    <a class="help" data-help="rent-change">
                                        <button type="button" rel="tooltip" title="{$changeText}"
                                                class="btn btn-success btn-simple give-back"
                                                data-id-rent="{$rent->get('id')}">
                                            <i class="fa fa-exchange"></i>
                                        </button>
                                    </a>
                                    <!--if $rent->get('renew_qtt') < 2!-->
                                        <a class="help" data-help="rent-renew">
                                            <button type="button" rel="tooltip" title="Solicitar renovação"
                                                    class="btn btn-info btn-simple request-renew"
                                                    data-id-rent="{$rent->get('id')}">
                                                <i class="fa fa-refresh"></i>
                                            </button>
                                        </a>
                                    <!--/if!-->
                                {/if}
                                {if false}
                                    <a>
                                        <button type="button" rel="tooltip" title="Confirmar recebimento"
                                                class="btn btn-success btn-simple confirm-deliver"
                                                data-id-rent="{$rent->get('id')}">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </a>
                                {/if}
                                {if $rent->get('status') == 'waiting_post'}
                                    <a class="help" data-help="rent-waiting">
                                        <button type="button" rel="tooltip" title="Confirmar postagem"
                                                class="btn btn-success btn-simple confirm-post"
                                                data-id-rent="{$rent->get('id')}">
                                            <i class="fa fa-truck"></i>
                                        </button>
                                    </a>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>

{include file=$smarty.current_dir|cat:"/view_modal.tpl"}