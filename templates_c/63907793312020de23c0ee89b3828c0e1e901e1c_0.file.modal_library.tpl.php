<?php
/* Smarty version 3.1.28, created on 2017-10-16 05:00:58
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\modal_library.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59e4592aaad600_51219434',
  'file_dependency' => 
  array (
    '63907793312020de23c0ee89b3828c0e1e901e1c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\modal_library.tpl',
      1 => 1504618707,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e4592aaad600_51219434 ($_smarty_tpl) {
?>
<div id="modalLibrary" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title"></h4>
			</div>
			<div class="modal-body" id="modal-body">
				<h4 class="text-center">
					<i class="fa fa-spinner fa-spin"></i>
				</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
			</div>
		</div>

	</div>
</div>

<div id="modalSaveChanges" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Salvar alterações</h4>
			</div>
			<div class="modal-body" id="modal-body-savechanges">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">
					Cancelar
				</button>
				<button type="button" class="btn btn-success" data-dismiss="modal" id="finish-save">
					Confirmar!
				</button>
			</div>
		</div>

	</div>
</div><?php }
}
