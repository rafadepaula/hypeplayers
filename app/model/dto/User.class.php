<?php
    
    class User extends DTO {
        public $id;
        public $email;
        public $password;
        public $name;
        public $role;
        
        public $FieldsValidation = array(
            'name'     => 'notEmpty',
            'email'    => 'validEmail',
            'password' => 'notEmptyPass,equalPass',
        );
        public $FieldsErrors     = array(
            'name'     => 'Informe um nome válido.',
            'email'    => 'E-mail inválido ou já cadastrado no sistema.',
            'password' => 'Informe uma senha válida.',
        );
        public $FieldsForms = array(
            'email',
            'password',
            'name'
        );
        
        public function equalPass($input){
            if(isset($_POST['passwordConfirm']))
                return $_POST['password'] == $_POST['passwordConfirm'];
            else
                return true;
        }
        
        public function notEmptyPass($input){

            if(isset($_POST['password']))
                return trim($input) != '';
            else
                return true;
        }
    }