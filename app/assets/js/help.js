$('#help-init').click(function(){
    helps = getPageHelps();
    $.ajax({
        url: '/help/makeSteps/',
        data: {helps: helps},
        type: 'post'
    }).done(function(steps){
        console.log(steps);
        if(!validJson(steps)){
            return swal({
                title: 'Ops!',
                text: 'Um erro interno ocorreu',
                type: 'error',
            });
        }
        steps = $.parseJSON(steps);
        startTour(steps);
    });
});

function getPageHelps(){
    helps = [];
    $('.help').each(function(){
        if($(this).is(":visible") && $(this).height() > 0)
            helps.push($(this).data('help'));
    });
    return helps;
}

function startTour(steps){
    if(steps.length == 0){
        return swal({
            title: 'Ops!',
            text: 'Não existem itens de ajuda nesta página',
            type: 'info',
        });
    }
    var tour = new Tour({
        steps: steps,
        backdrop: true,
        backdropContainer: 'body',
        template:   "<div class='popover tour'>" +
                        "<div class='arrow'></div>" +
                            "<h3 class='popover-title'></h3>" +
                            "<div class='popover-content'></div>" +
                            "<div class='popover-navigation'>" +
                            "<button class='btn btn-info' data-role='prev'>« Ant.</button>" +
                            "<button class='btn btn-info' data-role='next'>Próx. »</button>" +
                            "<button class='btn btn-success' data-role='end'>Fechar ajuda</button>" +
                        "</div>" +
                    "</div>"
    });
    localStorage.clear();
    tour.init();
    tour.start(true);
}

