<?php 
	class PaymentModel extends AppModel{
		public function proccessTransaction($xml){
			if($xml['status'] == 3){
				$payment = new Payment();
				$payment->set('date', date('Y-m-d H:i:s', strtotime($xml['date'])));
				$payment->set('code', $xml['code']);
				$payment->set('reference', $xml['reference']);
				$payment->set('value', $xml['grossAmount']);

				$signature = $this->getDto('signature', 'reference', $xml['reference']);
				$payment->set('id_user', $signature->get('id_user'));

				if($this->exists('payment', 'code', $payment->get('code')))
					return true;
				$this->insert('payment', $payment);
				return true;
			}else{
				return false;
			}
		}
	}