<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">
                    {$title}
                    <a href="/user/add/" class="pull-right card-header-btn">
                        <button class="btn btn-white btn-round btn-just-icon" >
                            <i class="material-icons">add</i>
                        </button>
                    </a>
                </h4>
            </div>
            <div class="card-content">
                <div class="table-responsive">
                <table class="table table-hover table-striped datatable">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Usuário</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    {foreach $users as $user}
                        <tr>
                            <td>
                                {$user->get('name')}
                            </td>
                            <td>
                                {$user->get('email')}
                            </td>
                            <td class="td-actions">
                                {if $actualUser->get('id') == $user->get('id')}
                                    <a href="/user/edit/{$user->get('id')}">
                                        <button type="button" rel="tooltip" title="Editar dados pessoais"
                                                class="btn btn-info btn-simple">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>