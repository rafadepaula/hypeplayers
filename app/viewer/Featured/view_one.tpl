<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Nome</label>
                        <input disabled type="text" name="name" id="name" class="form-control" value="{$featured->get('name')}">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label>Plataforma</label>
                        <input disabled type="text" name="platform" id="platform" class="form-control" value="{$featured->get('platform')}">
                    </div>
                    <div class="col-sm-3">
                        <div class="game-cover" id="cover">
                            <img src="/{$featured->get('cover')}" style="max-height: 150px;width: auto;margin: 0 auto;display:block">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <label>Descrição</label>
                        <textarea disabled name="description" id="description" class="form-control" rows="5">{$featured->get('description')}</textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <a href="/featured/edit/{$featured->get('id')}" class="btn btn-success btn-block btn-lg">
                            Editar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
