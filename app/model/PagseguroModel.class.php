<?php

    \PagSeguro\Library::initialize();
    \PagSeguro\Library::cmsVersion()->setName(_NAME_PAGSEGURO_)->setRelease("1.0.0");
    \PagSeguro\Library::moduleVersion()->setName(_NAME_PAGSEGURO_)->setRelease("1.0.0");
    if(_IN_DEV_)
        \PagSeguro\Configuration\Configure::setEnvironment('sandbox');

	class PagseguroModel {
		private $token;
		private $appKey;
		private $appId;
		private $email;

		public function __construct(){
			if(_IN_DEV_){
				$this->appKey = '3956B53B3939EE9554728F9912F5D4A7';
				$this->appId = 'app0254099763';
                $this->token = '2BA38D84F8114B3BAC3AD887094398BA';
				$this->email = 'rafaelbg97@live.com';
			}else{
				$this->appKey = '57F90C664E4E414994A3AF84A3D75534';
				$this->appId = 'hypeplayers';
				$this->token = 'B901ACA121734622B99A7CA271B222C7';
				$this->email = 'contato@hypeplayers.com.br';
				/*$this->appKey = '3E9C6C85242493F994467FB8BE88656F';
				$this->appId = 'hypeplayers-testes';
				$this->token = '4E27EB04DAAD4E24ACF908B7A668F1A9';
				$this->email = 'rafaelbg97@live.com';*/
			}
		}

		public function auth(){
			// return 'appId='.$this->appId.'&appKey='.$this->appKey;
			return 'email='.$this->email.'&token='.$this->token;
		}

		public function request($url, $data = null, $header = null, $post = true){
            $ch = curl_init();
            
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, $post); 

        	if(!is_null($header))
            	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

            if(!is_null($data)){
                $data = is_array($data) ? Model::http_build_query_for_curl($data) : $data;
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            
            $result = curl_exec($ch);
            
            curl_close($ch);
            
            return $result;
        }

        public function generateSession(){
            try{
                $sessionCode = \PagSeguro\Services\Session::create(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );

                return array('id' => $sessionCode->getResult());
            }catch(Exception $e){
                die($e->getMessage());
            }
        }

        public function accession($user, $signature, $member, $preApproval){
            $phone = $member->get('phone');
            $phone = explode(' ', $phone);
            $phone[0] = str_replace('(', '', $phone[0]);
            $phone[0] = str_replace(')', '', $phone[0]);
            $phone[1] = str_replace('-', '', $phone[1]);
            $plan = $preApproval->get('code');
            $preApproval = new \PagSeguro\Domains\Requests\DirectPreApproval\Accession();
            $preApproval->setPlan($plan);
            $preApproval->setReference($signature->get('reference'));
            $preApproval->setSender()->setName($user->get('name'));
            $preApproval->setSender()->setHash($_POST['sender_hash']);
            if(_IN_DEV_)
                $preApproval->setSender()->setEmail('c20149685947047241649@sandbox.pagseguro.com.br');
            else
                $preApproval->setSender()->setEmail($user->get('email'));
            $preApproval->setSender()->setAddress()->withParameters(
                $member->get('street'),
                $member->get('number'),
                $member->get('neighborhood'),
                $member->get('cep'),
                $member->get('city'),
                $member->get('state', true),
                'BRA'
            );

            $document = new \PagSeguro\Domains\DirectPreApproval\Document();
            $document->withParameters('CPF', str_replace('.', '', str_replace('-', '', $_POST['holder_cpf'])));
            $preApproval->setSender()->setDocuments($document);

            $preApproval->setSender()->setPhone()->withParameters(trim($phone[0]), trim($phone[1]));
            $preApproval->setPaymentMethod()->setCreditCard()->setToken($_POST['cardToken']);
            $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setName($_POST['holder_name']);
            $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBirthDate(date('d/m/Y', strtotime($_POST['holder_birthdate'])));
            $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setDocuments($document);
            $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setPhone()->withParameters(trim($phone[0]), trim($phone[1]));
            $preApproval->setPaymentMethod()->setCreditCard()->setHolder()->setBillingAddress()->withParameters(
                $member->get('street'),
                $member->get('number'),
                $member->get('neighborhood'),
                $member->get('cep'),
                $member->get('city'),
                $member->get('state', true),
                'BRA'
            );

            try {
                $response = $preApproval->register(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );

                return $response;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function proccessNotification(Notification_pagseguro $notification){
            switch ($notification->get('type')) {
                case 'transaction':
                    $url = _IN_DEV_ ? 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' : 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/';
                    $url .= $notification->get('code').'?'.$this->auth();
                    $response = $this->request($url, null, null, false);
                    return AppModel::xmlToArray($response);
                    break;
                case 'preApproval':
                    $queryNotification = new \PagSeguro\Domains\Requests\DirectPreApproval\QueryNotification(null, null, 20, $notification->get('code'));
                    $response = $queryNotification->register(
                        \PagSeguro\Configuration\Configure::getAccountCredentials()
                    );
                    return $response;
                    break;
                default:
                    return array();
                    break;
            }
        }

        public function cancel($code){
            $status = new \PagSeguro\Domains\Requests\DirectPreApproval\Cancel();
            $status->setPreApprovalCode($code);
            try {
                $response = $status->register(
                    new \PagSeguro\Domains\AccountCredentials($this->email, $this->token)
                );
                return $response;
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function changeCard($user, $signature, $member){
            $phone = $member->get('phone');
            $phone = explode(' ', $phone);
            $phone[0] = str_replace('(', '', $phone[0]);
            $phone[0] = str_replace(')', '', $phone[0]);
            $phone[1] = str_replace('-', '', $phone[1]);


            $changePayment = new \PagSeguro\Domains\Requests\DirectPreApproval\ChangePayment();
            $changePayment->setPreApprovalCode($signature->get('accession_code'));
            $changePayment->setSender()->setHash($_POST['sender_hash']);
            $changePayment->setCreditCard()->setToken($_POST['cardToken']);
            $changePayment->setCreditCard()->setHolder()->setName($_POST['holder_name']);
            $changePayment->setCreditCard()->setHolder()->setBirthDate(date('d/m/Y', strtotime($_POST['holder_birthdate'])));
            $document = new \PagSeguro\Domains\DirectPreApproval\Document();
            $document->withParameters('CPF', str_replace('.', '', str_replace('-', '', $_POST['holder_cpf'])));
            $changePayment->setCreditCard()->setHolder()->setDocuments($document);

            $changePayment->setCreditCard()->setHolder()->setPhone()->withParameters($phone[0], $phone[1]);
            $changePayment->setCreditCard()->setHolder()->setBillingAddress()->withParameters(
                $member->get('street'),
                $member->get('number'),
                $member->get('neighborhood'),
                $member->get('cep'),
                $member->get('city'),
                $member->get('state', true),
                'BRA'
            );

            try {
                $response = $changePayment->register(
                    \PagSeguro\Configuration\Configure::getAccountCredentials()
                );
                return $response;
            } catch (Exception $e) {
                die($e->getMessage());
            }

        }

        public function query($accession_code){
            $url = _IN_DEV_ ? 'https://ws.sandbox.pagseguro.uol.com.br/pre-approvals/' : 'https://ws.pagseguro.uol.com.br/pre-approvals/';
            $url .= $accession_code.'?'.$this->auth();
            $response = $this->request($url, null, array('Accept: application/vnd.pagseguro.com.br.v3+json;charset=ISO-8859-1', ), false);
            echo $response;
        }
	}