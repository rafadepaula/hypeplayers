<?php

    /**
     * Passes through $_POST to search for unformatted dates (brazilian ones, at least)
     * Transform the dd/mm/YYYY to YYYY-mm-dd
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     *
     */
    if(isset($_POST)){
        foreach($_POST as $item => $value){
            if(is_array($value))
                continue;
            $oldValue = $value;
            $value = explode('/', $value);
            if(count($value) == 3){
                if(is_numeric($value[1])) {
                    if (checkdate($value[1], $value[0], $value[2])) {
                        $value = $value[2] . '-' . $value[1] . '-' . $value[0];
                        $_POST[$item] = $value;
                    }
                }
            }else{
                $value = explode('-', $oldValue);

                if(count($value) == 2){
                    $first = $value[0];
                    $sec = $value[1];

                    $first = explode('/', $first);
                    if(count($first) == 3) {
                        if (checkdate(intval($first[1]), intval($first[0]), intval($first[2]))) {
                            $first = trim($first[2]) . '-' . trim($first[1]) . '-' . trim($first[0]);
                            $sec = $sec = explode('/', $sec);

                            if (checkdate($sec[1], $sec[0], $sec[2])) {
                                $sec = trim($sec[2]) . '-' . trim($sec[1]) . '-' . trim($sec[0]);

                                $value[0] = $first;
                                $value[1] = $sec;
                                $value = $value[0].' / '.$value[1];
                                $_POST[$item] = $value;
                            }
                        }
                    }

                }
            }
        }
    }

    /**
     * This, catches an date period (25/12/2016 - 31/12/2016, for example)
     * an transforms it to 2016-12-25 / 2016-12-31
     * @param String $date - date period
     * @return String - date period, formatted
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    function filter_period($date){
        $date = explode('-', $date);
        
        $date[0] = explode('/', $date[0]);
        $date[0] = $date[0][2].'-'.$date[0][1].'-'.$date[0][0];
        
        $date[1] = explode('/', $date[1]);
        $date[1] = $date[1][2].'-'.$date[1][1].'-'.$date[1][0];
        
        return $date[0].' / '.$date[1];
    }
    
    /**
     * Catches an formated date period (2016-12-25 / 2016-12-31, for example)
     * and transforms it to 25/12/2016 - 31/12/2016
     * @param $date
     * @return string
     *
     * @author Rafael de Paula - <rafael@bentonet.com.br>
     * @version 1.0.0 - 2016-09-13
     */
    function unfilter_period($date){
        $date = explode('/', $date);
        $date[0] = date('d/m/Y', strtotime($date[0]));
        $date[1] = date('d/m/Y', strtotime($date[1]));
        return $date[0].' - '.$date[1];
    }