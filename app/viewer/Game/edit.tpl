<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">{$title}</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/game/edit/{$game->get('id')}" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Nome</label>
                            <input required type="text" name="name" class="form-control" value="{$game->get('name')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Gênero</label>
                            <select required name="id_genre" class="form-control">
                                {foreach $genres as $genre}
                                    <option {if $genre->get('id') == $game->get('id_genre')}selected{/if} 
                                    value="{$genre->get('id')}">{$genre->get('name')}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>Plataformas</label> <br>
                            <label class="checkbox-inline">
                                <input {if $game->get('ps4')}checked{/if} type="checkbox" name="ps4" value="1">PS4
                            </label>
                            <label class="checkbox-inline">
                                <input {if $game->get('xone')}checked{/if} type="checkbox" name="xone" value="1">XBOX One
                            </label>
                            <label class="checkbox-inline">
                                <input {if $game->get('switch')}checked{/if} type="checkbox" name="switch" value="1">Nintendo Switch
                            </label>
                        </div>
                    </div>
                    <div class="row" id="ps4_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para PS4</label> <br>
                            <input type="number" name="ps4_qtt" class="form-control" value="{$game->get('ps4_qtt')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para PS4 (ignorar caso não alterar)</label>
                            <input type="file" name="ps4_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row" id="xone_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para XBOX One</label> <br>
                            <input type="number" name="xone_qtt" class="form-control" value="{$game->get('xone_qtt')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para XBOX One (ignorar caso não alterar)</label>
                            <input type="file" name="xone_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row" id="switch_qtt">
                        <div class="col-sm-6 form-group">
                            <label>Quantidade para Nintendo Switch</label> <br>
                            <input type="number" name="switch_qtt" class="form-control" value="{$game->get('switch_qtt')}">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Capa para Nintendo Switch (ignorar caso não alterar)</label>
                            <input type="file" name="switch_cover" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 form-group" >
                            <label>Descrição</label>
                            <textarea name="description" rows="7" class="form-control">{$game->get('description')}</textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
