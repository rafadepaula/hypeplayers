<?php
    
    class AddressModel extends AppModel {
        public function getAddress($id){
            return $this->getDto('address', 'id', $id);
        }

        public function saveByJoin($id_user, RegionModel $regionModel){
	        $addressErrors = array();
	        $address = new Address();
	        $address->set('id_user', $id_user);
	        if(!$address->set('cep', $_POST['cepEnt']))
		        $addressErrors[] = 'CEP inválido!';
	        if(!$regionModel->checkCep($address->get('cep'), false))
		        $addressErrors[] = 'CEP de entrega não atendido!';
	        $address->set('street', $_POST['streetEnt']);
	        $address->set('neighborhood', $_POST['neighborhoodEnt']);
	        $address->set('number', $_POST['numberEnt']);
	        $address->set('complement', $_POST['complementEnt']);
	        $address->set('city', $_POST['cityEnt']);
	        $address->set('state', $_POST['stateEnt']);
	        $address->set('is_default', 1);
	        $cep = str_replace('-', '', $address->get('cep'));
	        $region = $regionModel->getRegionByCep($cep);
	        if(empty($region->get('name'))){
		        $addressErrors[] = 'Não atendemos o seu endereço de entrega! :(';
	        }
	        $address->set('id_region', $region->get('id'));

	        if(!empty($addressErrors)){
		        foreach($addressErrors as $error){
			        echo '<li>'.$error.'</li>';
		        }
		        return false;
	        }
	        if(!$this->insert('address', $address)){
		        echo '<li>Não foi possível cadastrar o endereço de entrega.</li>';
		        return false;
	        }else{
	        	return true;
	        }
        }

        public function save(){
            $_POST['id_user'] = unserialize($_SESSION['user'])->get('id');
            $address = new Address();
            $ret  = $this->makeDto($address, null);
            $address = $ret[0];
            
            $errors = $ret[1];
            
            if($errors != ''){
                Viewer::flash($errors, 'e');
                
                return false;
            }else{
                
                // if id is null insert else update
                if($this->insert('address', $address)){
                    Viewer::flash(_INSERT_SUCCESS, 's');
                    return true;
                }else{
                    Viewer::flash(_INSERT_ERROR, 'e');
                    return false;
                }
            }
        }
        
    }