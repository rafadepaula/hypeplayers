{if $cancelRequested}
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <a href="/signature/confirmCancel/{$signature->get('id_user')}">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="red">
                        <i class="material-icons">cancel</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Cancelamento solicitado</p>
                        <h4 class="title">Este membro solicitou cancelamento de assinatura</h4>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            Clique aqui para efetivar o cancelamento da assinatura deste membro.
                        </div>
                    </div>
                </div>
            </a>
            <p class="text-center">
                <b style="cursor: pointer" class="reject-cancel" data-id-user="{$user->get('id')}">
                    Clique aqui para negar o pedido de cancelamento do membro
                </b>
            </p>
        </div>
    </div>
{/if}

{if $changeRequested}
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 col-xs-12">
            <a href="/signature/confirmChange/{$signature->get('id_user')}">
                <div class="card card-stats">
                    <div class="card-header" data-background-color="orange">
                        <i class="material-icons">update</i>
                    </div>
                    <div class="card-content">
                        <p class="category">Troca de plano solicitada</p>
                        <h4 class="title">Este membro solicitou uma mudança de plano</h4>
                    </div>
                    <div class="card-footer">
                        <div class="stats">
                            Ele deseja trocar do Plano {$request->get('from_level')} para o Plano {$request->get('to_level')}.
                            Clique aqui para concluir esta ação.
                        </div>
                    </div>
                </div>
            </a>
            <p class="text-center">
                <b style="cursor: pointer" class="reject-change" data-id-user="{$user->get('id')}">
                    Clique aqui para negar o pedido de troca do membro
                </b>
            </p>
        </div>
    </div>
{/if}
<div class="row">
	<div class="col-sm-8">
    	<div class="card">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Dados pessoais</h4>
            </div>
            <div class="card-content">
            	<div class="row">
            		<div class="col-sm-4 form-group">
            			<label>Nome</label> <br>
            			{$user->get('name')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>E-mail</label> <br>
            			{$user->get('email')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>Telefone</label> <br>
            			{$member->get('phone')}
            		</div>
            	</div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label>RG</label><br>
                        {$member->get('rg')}
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>CPF</label><br>
                        {$member->get('cpf')}
                    </div>
                    <div class="col-sm-4 form-group">
                        <label>Data de nascimento</label><br>
                        {$member->get('birthday', true)}
                    </div>
                </div>
            	<div class="row">
            		<div class="col-sm-4 form-group">
            			<label>Rua</label> <br>
            			{$member->get('street')}
            		</div>
            		<div class="col-sm-2 form-group">
            			<label>Número</label> <br>
            			{$member->get('number')}
            		</div>

            		<div class="col-sm-2 form-group">
            			<label>Complemento</label> <br>
            			{$member->get('complement')}
            		</div>
            		<div class="col-sm-4 form-group">
            			<label>Bairro</label> <br>
            			{$member->get('neighborhood')}
            		</div>
            	</div>
            	<div class="row">
            		<div class="col-sm-8 form-group">
            			<label>Cidade</label> <br>
            			{$member->get('city')} - {$member->get('state')}
            		</div>

            		<div class="col-sm-4 form-group">
            			<label>CEP</label> <br>
            			<a href="/region/view/{$region->get('id')}" target="_blank">
            				{$member->get('cep')}
            			</a>
            		</div>
            	</div>
                {if !empty($addresses)}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Rua</th>
                                        <th>Nº</th>
                                        <th>Bairro</th>
                                        <th>Complemento</th>
                                        <th>Cidade</th>
                                        <th>Estado</th>
                                        <th>CEP</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {foreach $addresses as $address}
                                    {if $address->get('is_default')}
                                        {assign var="style" value="style='font-weight: bold'"}
                                    {else}
                                        {assign var="style" value=""}
                                    {/if}
                                    <tr>
                                        <td {$style}>{$address->get('street')}</td>
                                        <td {$style}>{$address->get('number')}</td>
                                        <td {$style}>{$address->get('neighborhood')}</td>
                                        <td {$style}>{$address->get('complement')}</td>
                                        <td {$style}>{$address->get('city')}</td>
                                        <td {$style}>{$address->get('state')}</td>
                                        <td {$style}>{$address->get('cep')}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
    	</div>
    </div>

	<div class="col-sm-4">
		<div class="card card-profile">
			<div class="card-avatar">
				<img class="img" src="{$picture}" />
			</div>

			<div class="content">
				<h6 class="category text-gray">
					{$signature->get('level', true)} (R$ {$signature->get('value', true)})
				</h6>
				<h4 class="card-title">Status: {$signature->get('status', true)}</h4>
				<p class="card-content">
					<p>
						Membro desde {$signature->get('since', true)} <br>
						Expira em <b>{$signature->get('expiry', true)}</b> <br>
						Último pagamento: {$signature->get('last_payment', true)}
					</p>
					<p>
						<a style="cursor: pointer;" id="view-more" data-id="{$user->get('id')}">
							Ver infos do desenvolvedor
						</a> <br>
                        <a style="cursor: pointer;" id="accession-status" data-id="{$user->get('id')}">
                            Dados crus do pagseguro
                        </a>

                        <hr>

                        <div class="col-sm-12" style="margin-bottom: 25px">
                        {if $member->get('deactivated')}
                            O membro está <b>DESATIVADO</b>.
                            <a id="activate_member" data-id-user="{$member->get('id_user')}" class="btn btn-round">
                                Reativar membro
                            </a>
                        {else}
                            O membro está ativo.
                            <a id="deactivate_member" data-id-user="{$member->get('id_user')}" class="btn btn-round">
                                Desativar membro
                            </a>
                        {/if}

                        </div>
					</p>
				</p>
			</div>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-sm-8">
    	<div class="card card-nav-tabs">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Histórico</h4>
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#rents" data-toggle="tab">
                                    <i class="material-icons" style="width: 30px;">star_rate</i>
                                    de locações
                                <div class="ripple-container"></div></a>
                            </li>
                            <li class="">
                                <a href="#payments" data-toggle="tab">
                                    <i class="material-icons">attach_money</i>
                                    de pagamentos
                                <div class="ripple-container"></div></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="rents">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Jogo</th>
                                        <th>Status</th>
                                        <th>Período</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach $rents as $rent}
                                        <tr>
                                            <td>
                                                {$rent->get('id_game', true)->get('name')}
                                                ({$rent->get('platform', true)})
                                            </td>
                                            <td>
                                                {$rent->get('status', true)}
                                            </td>
                                            <td>
                                                {$rent->get('start', true)} até {$rent->get('end', true)}
                                            </td>
                                            <td class="td-actions">
                                                <a>
                                                    <button type="button" rel="tooltip" title="Visualizar locação"
                                                            class="btn btn-info btn-simple view-one"
                                                            data-id-rent="{$rent->get('id')}">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </a>
                                                <a>
                                                    <button type="button" rel="tooltip" title="Alterar status"
                                                            class="btn btn-success btn-simple change-status"
                                                            data-id-rent="{$rent->get('id')}"
                                                            data-url="/member/view/{$user->get('id')}">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="payments">
                        <table class="table table-striped datatable">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Valor</th>
                                    <th>Referência interna</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $payments as $payment}
                                    <tr>
                                        <td>
                                            {$payment->get('date', true)}
                                        </td>
                                        <td>
                                            R${$payment->get('value', true)}
                                        </td>
                                        <td>
                                            {$payment->get('code')}
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
			</div>
    	</div>
    </div>
	<div class="col-sm-4">
    	<div class="card">
    		<div class="card-header" data-background-color="green">
                <h4 class="title">Favoritos</h4>
            </div>
            <div class="card-content">
            	<div class="row">
            		<div class="col-sm-12">
		            	<div class="table-responsive">
		            		<table class="table">
		            			<thead>
		            				<tr>
		            					<th>#</th>
		            					<th>Jogo</th>
		            				</tr>
		            			</thead>
                                <tbody>
                                    {foreach $favorites as $favorite}
                                        <tr>
                                            <td>{$favorite->get('position') + 1}</td>
                                            <td>
                                                {$favorite->get('id_game', true)->get('name')}
                                                ({$favorite->get('platform', true)})
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
		            	</div>
		            </div>
		        </div>
			</div>
    	</div>
    </div>

</div>

{include file=$smarty.current_dir|cat:"/../Rent/changeStatus_modal.tpl"}
{include file=$smarty.current_dir|cat:"/../Rent/view_modal.tpl"}