$(function(){
	$('input[type=checkbox]').click(function(){
		toggle($(this));
	})

	$('input[type=checkbox]').each(function(){
		toggle($(this));
	})

	function toggle(el){
		name = el.attr('name');
		if(el.is(':checked'))
			$('#' + name + '_qtt').show();
		else
			$('#' + name + '_qtt').hide();
	}
})