<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <base href="/app/">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width , initial-scale=1" />
        <meta name="description" content="Clube de assinatura de games">
        <meta name="keywords" content="Jogos, games, locadora, ps4, xone, switch, nintendo" />
        <meta name="author" content="Rafael de Paula - <rafael@bentonet.com.br>">
        <title>HypePlayers</title>

        <link href="assets/css/material-dashboard.css" rel="stylesheet"/>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/prettyPhoto.css" rel="stylesheet">
        <link href="assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="assets/css/animate.css" rel="stylesheet">
        <link href="assets/css/main.css" rel="stylesheet">
        <link href="assets/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]> <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script> <![endif]-->
        <link rel="shortcut icon" href="assets/img/sell/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144"
              href="assets/img/sell/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114"
              href="assets/img/sell/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72"
              href="assets/img/sell/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/img/sell/ico/apple-touch-icon-57-precomposed.png">



        <link href="https://cdn.jsdelivr.net/qtip2/3.0.3/basic/jquery.qtip.min.css" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    </head><!--/head-->
    <body>
    <div class="preloader">
        <div class="preloder-wrap">
            <div class="preloder-inner">
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
                <div class="ball"></div>
            </div>
        </div>
    </div><!--/.preloader-->
    <header id="navigation">

        <div class="navbar navbar-inverse navbar-fixed-top" role="banner" id="topBar">
            <div class="container" style="margin-top: -10px">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <h1>
                            <img src="assets/img/sell/logo.png" alt="logo" id="logo-top">
                        </h1>
                    </a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="scroll active"><a href="#navigation">Home</a></li>
                        <li class="scroll"><a href="#como-funciona">Como funciona</a></li>
                        <li class="scroll"><a href="#vantagens">Vantagens</a></li>
                        <li class="scroll"><a href="#planos">Planos</a></li>
                        <li class="scroll"><a href="#jogos">Jogos</a></li>
                        <li class="scroll"><a href="#sobre-nos">
                        
                        sobre nós</a></li>
                        <li class="scroll"><a href="#contato">Contato</a></li>
                        <li class="scroll"><a href="/user/login">Login</a></li>
                    </ul>
                </div>
            </div>
        </div><!--/navbar-->
    </header> <!--/#navigation-->

    <section id="home">
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators hidden-xs">
                {for $i=0 to ($banners|@count) - 1}
                    {if $i == 0}
                        {assign var="active" value="active"}
                    {else}
                        {assign var="active" value=""}
                    {/if}
                    <li data-target="#main-carousel" data-slide-to="{$i}" class="{$active}"></li>
                {/for}
            </ol><!--/.carousel-indicators-->
            <div class="carousel-inner">
                {for $i=0 to ($banners|@count) - 1}
                    {if $i == 0}
                        {assign var="active" value="active"}
                    {else}
                        {assign var="active" value=""}
                    {/if}
                    {assign var="banner" value=$banners[$i]}
                    <div class="item {$active}" style="background-image: url('/{$banner->get('cover')}')">
                        <div class="carousel-caption">
                            <div>
                                <h2 class="heading animated bounceInDown">
                                    {$banner->get('title')}
                                </h2>
                                <p class="animated bounceInUp">
                                    {$banner->get('subtitle')}
                                </p>
                                {if $banner->get('button_display')}
                                    <a class="btn btn-default slider-btn animated fadeIn hidden-xs" href="{$banner->get('button_link', true)}" target="_blank" style="background-color: #{$banner->get('button_color')}">
                                        {$banner->get('button_text')}
                                    </a>
                                {/if}
                            </div>
                        </div>
                    </div>
                {/for}
            </div><!--/.carousel-inner-->

            <a class="carousel-left member-carousel-control hidden-xs" href="#main-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="carousel-right member-carousel-control hidden-xs" href="#main-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
        </div>

    </section><!--/#home-->

    <section id="como-funciona">
        <div class="container">
            <div class="text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <h2 class="title-one">Como funciona</h2>
                    <div class="hidden-xs">
                        <img src="assets/img/sell/steps.png" class="img-responsive">
                        <p>
                            <b>Escolha</b> o plano que mais se adequa a você: bronze, prata ou ouro. <b>Assine</b> o clube para aproveitar todos os benefícios. <b>Receba</b> os seus jogos favoritos em sua casa. <b>Aproveite</b> os games que você escolheu. <b>Recomece</b> o processo  trocando ou devolvendo o jogo, e recebendo outro!
                        </p>
                    </div>
                    <div class="visible-xs">
                        <img src="assets/img/sell/Step 1.png" class="img-responsive center-block">
                        <p><b>Escolha</b> o plano que mais se adequa a você: bronze, prata ou ouro.</p>
                        <img src="assets/img/sell/Step 2.png" class="img-responsive center-block">
                        <p><b>Assine</b> o clube para aproveitar todos os benefícios.</p>
                        <img src="assets/img/sell/Step 3.png" class="img-responsive center-block">
                        <p><b>Receba</b> os seus jogos favoritos em sua casa.</p>
                        <img src="assets/img/sell/Step 4.png" class="img-responsive center-block">
                        <p><b>Aproveite</b> os games que você escolheu.</p>
                        <img src="assets/img/sell/Step 5.png" class="img-responsive center-block">
                        <p><b>Recomece</b> o processo  trocando ou devolvendo o jogo, e recebendo outro!</p>
                    </div>
                </div>
            </div>

        </div>
    </section><!--/#como-funciona-->

    <section id="vantagens" class="parallax-section">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <h2 class="title-one">Vantagens</h2>
                    <p>
                        Imagine poder aproveitar seus jogos preferidos por um preço <u>Hype</u>? Esta é a nossa proposta. Oferecemos um serviço de locação completamente on-line, onde o player escolhe suas preferências e aguarda a <u>mídia física</u> no conforto de sua casa.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="our-service">
                        <div class="services row">
                            <div class="col-sm-4">
                                <div class="single-service">
                                    <i class="fa fa-book"></i>
                                    <h2>Mídia física</h2>
                                    <p>
                                        Sinta o prazer de possuir o seu jogo preferido em suas mãos, nada de mídia digital. Aqui na Hype Players trabalhamos apenas com mídias físicas.
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-service">
                                    <i class="fa fa-truck"></i>
                                    <h2>Frete grátis</h2>
                                    <p>
                                        O frete é por nossa conta =). Conosco você paga apenas o valor de sua assinatura, não há outros custos com transporte.
                                        
                                         
                                    </p>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="single-service">
                                    <i class="fa fa-dollar"></i>
                                    <h2>Preço justo</h2>
                                    <p>
                                        Nosso objetivo é a sua diversão. Contamos com valores completamente acessíveis para que você possa aproveitar todos os jogos que sempre quis, sem precisar desembolsar um caminhão de dinheiro. Guarde a grana para comprar um HD externo, porque com a Hype você irá precisar!
                                    </p>
                                </div>
                            </div></div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#service-->

    <section id="planos">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <h2 class="title-one" style="margin-bottom: 0px">Planos</h2>
                    <h4>Dê um NEW GAME na diversão</h4>
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <!-- Pricing -->
                    <div class="col-md-4">
                        <div class="pricing pricing-bronze hover-effect">
                            <div class="pricing-head pricing-head-bronze">
                                <h3>Bronze</h3>
                                <h4>
                                    <i>R$</i>39<i>,90</i>
                                    <span>mensais</span>
                                </h4>
                            </div>
                            <ul class="pricing-content list-unstyled">
                                <li>Um game por mês</li>
                                <li>Sem posse simultânea</li>
                                <li>Sem trocas</li>
                            </ul>
                            <div class="pricing-footer">
                                <p>Plano ideal para jogadores casuais</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pricing hover-effect pricing-silver">
                            <div class="pricing-head pricing-head-silver">
                                <h3>Prata</h3>
                                <h4>
                                    <i>R$</i>69<i>,90</i>
                                    <span>mensais</span>
                                </h4>
                            </div>
                            <ul class="pricing-content list-unstyled">
                                <li>Até 3 games por mês</li>
                                <li>2 jogos simultaneamente</li>
                                <li>1 troca por mês</li>
                            </ul>
                            <div class="pricing-footer">
                                <p>Plano ideal para jogadores intermediários</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="pricing hover-effect pricing-gold">
                            <div class="pricing-head pricing-head-gold">
                                <h3>Ouro</h3>
                                <h4>
                                    <i>$</i>99<i>,90</i>
                                    <span>mensais</span>
                                </h4>
                            </div>
                            <ul class="pricing-content list-unstyled">
                                <li>Até 5 jogos por mês</li>
                                <li>2 jogos simultaneamente</li>
                                <li>Até 3 trocas por mês</li>
                            </ul>
                            <div class="pricing-footer">
                                Plano ideal para jogadores experientes
                            </div>
                        </div>
                    </div>
                    <!--//End Pricing -->
                </div>
                <div class="col-sm-8 col-sm-offset-2">
                    <a class="btn btn-success slider-btn animated fadeIn" href="/member/join" target="_blank" style="position: relative">
                        Assine já!
                    </a>
                </div>
            </div>
        </div>
    </section><!--/#Our-Team-->

    <section id="clients" class="parallax-section">
        <div class="container">
            <div class="clients-wrapper">
                <div class="row text-center">
                    <div class="col-sm-8 col-sm-offset-2">
                        <h2 class="title-one">Jogos em destaque</h2>
                    </div>
                </div>
                <div id="clients-carousel" class="carousel slide" data-ride="carousel"> <!-- Indicators -->
                    <ol class="carousel-indicators">
                        {for $i=0 to ($featureds|@count) - 1}
                            {if $i == 0}
                                {assign var="active" value="active"}
                            {else}
                                {assign var="active" value=""}
                            {/if}
                            <li data-target="#clients-carousel" data-slide-to="{$i}" class="{$active}"></li>
                        {/for}
                    </ol> <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        {for $i=0 to ($featureds|@count) - 1}
                            {assign var="featured" value=$featureds[$i]}
                            {if $i == 0}
                                {assign var="active" value="active"}
                            {else}
                                {assign var="active" value=""}
                            {/if}
                            <div class="item {$active}">
                                <div class="single-client">
                                    <div class="media">
                                        <div class="media-body">
                                            <a href="/user/login">
                                                <img class="pull-left game-cover" src="/{$featured->get('cover')}" alt="">
                                            </a>
                                            <blockquote class="featured-game">
                                                <p>
                                                    <a href="/user/login" style="color: white;">
                                                        {$featured->get('name')} - {$featured->get('platform')}
                                                    </a>
                                                </p>
                                                <small>{$featured->get('description')}</small>
                                            </blockquote>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/for}
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#clients-->

    <section id="jogos">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <h2 class="title-one">Biblioteca de jogos</h2>
                </div>
                <div class="col-xs-12 text-center">
                    <button class="btn btn-success all-games">Ver todos os jogos</button>
                    <hr>
                </div>
            </div>
            <div class="row">
                <!-- platform by platform !-->
                {foreach from=$gamesArray item=$pages key=platform}
                <div class="carousel slide library-carousel">
                    <div class="carousel-inner">

                        <!-- page by page !-->
                        {foreach from=$pages item=$games key=page}
                            {if $page == 0}
                                {assign var="active" value="active"}
                            {else}
                                {assign var="active" value=""}
                            {/if}
                        <div class="item {$active}">

                            <div class="hidden-xs">
                            <!-- game by game of above page !-->
                            {foreach $games as $game}
                                <div class="col-sm-3">
                                    <img src="/{$game->firstCover()}" class="img-responsive game-library" data-name="{$game->get('name')}" data-id="{$game->get('id')}">
                                </div>
                            {/foreach}
                            </div>

                            <div class="visible-xs">
                                <div class="row">
                                {foreach from=$games key=i item=$game}
                                    {if $i % 2 == 0}
                                        </div>
                                        <div class="row">
                                    {/if}
                                    <div class="col-xs-6">
                                        <img src="/{$game->firstCover()}" class="img-responsive game-library" data-name="{$game->get('name')}" data-id="{$game->get('id')}">
                                    </div>

                                    {if $i == ($games|@count - 1)}
                                        </div>
                                    {/if}
                                {/foreach}
                            </div>

                        </div>
                        {/foreach}

                    </div>
                </div>
                {/foreach}
            </div>
            <div class="row">
                <div class="col-xs-12 text-center">
                    <hr>
                    <button class="btn btn-success all-games">Ver todos os jogos</button>
                </div>
            </div>
        </div>

    </section> <!--/#jogos-->

    <div id="modalLibrary" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <div class="modal-body" id="modal-body">
                    <h4 class="text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
                </div>
            </div>

        </div>
    </div>

    <div id="modalLibraryAll" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Voltar</button>
                    <h4 class="modal-title" id="modal-title-all"></h4>
                </div>
                <div class="modal-body" id="modal-body-all">
                    <h4 class="text-center">
                        <i class="fa fa-spinner fa-spin"></i>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
                </div>
            </div>

        </div>
    </div>

    <div id="modalFaq" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modal-title">Perguntas frequentes</h4>
                </div>
                <div class="modal-body" id="modal-body">
                    <div class="panel-group" id="accordion">
                        <div class="faqHeader">Conheça cada plano</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Plano Bronze - R$39,90</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    Jogos em posse: 01 <br>
                                    Trocas: nenhuma <br>
                                    Total de jogos no mês: 01
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Plano Prata - R$69,90</a>
                                </h4>
                            </div>
                            <div id="collapseTen" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Jogos em posse: 02 <br>
                                    Trocas: 01 <br>
                                    Total de jogos no mês: 03
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Plano Ouro - R$99,90</a>
                                </h4>
                            </div>
                            <div id="collapseEleven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Jogos em posse: 02 <br>
                                    Trocas: 03 <br>
                                    Total de jogos no mês: 05
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom1">Como faço para trocar meu plano?</a>
                                </h4>
                            </div>
                            <div id="collapseCustom1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>É bem simples, vá em "Minha conta", note que irá aparecer os outros dois planos para troca, basta escolher o desejado e clicar. Em seguida é só aguardar o nosso retorno.</p>
                                    <p>Note que após autorizarmos a troca do plano será necessário incluir o cartão novamente, como trata-se de cobrança recorrente a operadora financeira sempre solicita a confirmação dos dados do cartão quando ocorre qualquer tipo de alteração na conta do usuário.</p>
                                    <p>Um ponto importante é sobre o período para troca do plano.</p>
                                    <p>Como a intenção é gastar pouco e jogar muito, existe um período para a mudança do plano ser solicitada e efetivada, esse prazo é entre os últimos 05 dias antes do vencimento da sua próxima fatura, assim você não terá duas cobranças no mesmo mês e manterá a sua data de vencimento padrão além de não perder nenhuma troca dos seus jogos!</p>

                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Como funciona?</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Lista de jogos</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Assim que você acessar a sua conta pela primeira vez será necessário criar uma lista de desejos, com no mínimo 10 títulos de acordo com a sua preferência.</p>
                                    <p>A função dela é organizar e deixar a sua experiência com a assinatura mais prazerosa e dinâmica.</p>
                                    <p>Para adicionar basta clicar no jogo e em seguida em "adicionar para minha lista" e pronto. Os games estão separados por plataforma (PS4, Xone e Switch), eles também podem ser pesquisados por gênero (RPG, Esportes, Luta, Corrida, etc).</p>
                                    <p>Você pode alterar a ordem dos games sempre que preferir, porém só poderá adicionar novos jogos no período de 05 dias após a renovação mensal, por isso, sempre adicione o máximo de games à lista, quanto mais melhor =).</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Envio</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Não se preocupe, o envio é totalmente grátis (é grátis mesmo). Ele é realizado em até 48hs e será entregue ao o usuário ou para alguém autorizado a receber. Poderá ser enviado via Correios ou Motoboy.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Devolução</a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Simples como derrotar um chefe em Dark Souls (haha). Brincadeiras à parte ele é bem fácil, no menu principal selecione "Meus Jogos" e em "Ações" terá a opção "Solicitar Devolução", basta clicar nesse botão e está feito! A nossa central irá receber a solicitação de devolução ou troca (dependendo do plano) e entrará em contato com o usuário via e-mail para agendamento da retirada.</p>
                                    <p>Se você ainda tiver trocas dentro do mês o nosso sistema já irá preparar o envio do próximo game.</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Quanto tempo posso ficar com o jogo?</a>
                                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Você pode ficar com o jogo até 90 dias.</p>
                                    <p>Recebeu o jogo, mas não conseguiu terminá-lo?</p>
                                    <p>Calma, basta você renovar a locação do jogo, para isso vá até o menu e selecione "Meus Jogos" e em "Ações" escolha "Prorrogar". Com isso o seu jogo será prorrogado por mais 30 dias, importante notar que ao efetuar essa ação o jogo entrará como uma nova locação no período, ou seja, será considerado uma troca (de acordo com o plano escolhido).</p>
                                    <p> Cada jogo poderá ser prorrogado em até 60 dias (02 vezes). Também vale ressaltar que caso o usuário não acesse a sua conta e solicite a "troca" do jogo após o período a prorrogação será realizada de maneira automática.
                                    </p>

                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Cadastro</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Informações cadastrais</a>
                                </h4>
                            </div>
                            <div id="collapseEight" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Para ser um "Hype" é necessário criar uma conta em nosso site, cadastrando seus dados, é bem rapidinho.</p>
                                    <p>Após se cadastrar, o player irá receber um e-mail solicitando confirmação, é necessário acessar o link e confirmar a sua inscrição, sem isso não será possível utilizar os serviços. Sempre confirme o link na sua caixa de Spam e caso não receba, basta entrar em contato conosco via e-mail, contato@hypeplayers.com.br para reenviarmos a mensagem.</p>

                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Pagamentos</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Como funciona?</a>
                                </h4>
                            </div>
                            <div id="collapseNine" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Para ter essa experiência incrível o usuário só precisa de um cartão de crédito.</p>
                                    <p>A cobrança é realizada com base no sistema de recorrência, onde a cada 30 dias o valor do plano escolhido é creditado.</p>
                                    <p>Vale lembrar que a cobrança sempre será efetuada no dia em que a conta/acesso foi criada(o).</p>
                                    <p>As transações de pagamento são realizadas em conexões seguras, criptografadas e homologadas, fique tranquilo, você está em um ambiente seguro(SSL).</p>
                                    <p>Nossa equipe não tem qualquer interação sobre essas transações/informações, elas ocorrem de maneira automática.</p>
                                    <p>Caso ocorra algum empecilho com a cobrança, entraremos em contato.</p>

                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Cancelamento</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Como realizo?</a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Basta ir até “Minha Conta” e ao lado direito da tela você encontrará o botão “Cancelar Assinatura”, basta clicar e aguardar o nosso retorno sobre o cancelamento.</p>
                                    <p>Importante: o botão só estará habilitado para você caso todos os jogos em sua posse já tenham sido devidamente devolvidos para a Hype Players.</p>

                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Reativação</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Como volto para o clube?</a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Faça o login e acesse “Minha conta”, você notará uma mensagem para reativar a conta, basta selecionar, escolher o seu plano e informar o cartão de crédito. Pronto, agora é só aproveitar novamente a sua assinatura.
                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Fale conosco</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom2">Via e-mail</a>
                                </h4>
                            </div>
                            <div id="collapseCustom2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Rolou uma dúvida e não encontrou a resposta? Quer uma dica de game ou apenas trocar uma ideia? Envie uma mensagem para contato@hypeplayers.com.br
                                        que retornaremos o mais breve possível. Nosso horário de atendimento é de segunda a sexta-feira das 8:00 até 17:30.</p>
                                    <p>O prazo para retorno é de 12 horas de segunda a sexta</p>
                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Segurança e privacidade</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom3">Por que a Hype precisa do meu e-mail?</a>
                                </h4>
                            </div>
                            <div id="collapseCustom3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Todas as notificações da Hype Players são realizadas via e-mail, como por exemplo confirmação de cadastro, envio e devolução de jogos e pagamento. Por isso é muito importante habilitar o endereço contato@hypeplayers.com.br como endereço confiável, para que não se perca nenhum e-mail.</p>
                                    <p>Todo o histórico de notificações poderá será acessado direto na sua conta Hype, na caixa notificações.</p>
                                    <p>Quem tem acesso as minhas informações?</p>
                                    <p>Apenas a Hype Players tem acesso. Não iremos vender, repassar ou compartilhar seus dados com terceiros (também não gostamos de spam ¬¬), pode confiar na gente ;).</p>

                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom4">E os Termos de uso do site e locação?</a>
                                </h4>
                            </div>
                            <div id="collapseCustom4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    A minuta está disponível no link "Termos de Uso" no menu principal.
                                </div>
                            </div>
                        </div>

                        <div class="faqHeader">Então...</div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseCustom5">Posso me cadastrar sem medo?</a>
                                </h4>
                            </div>
                            <div id="collapseCustom5" class="panel-collapse collapse">
                                <div class="panel-body">
                                    Com certeza, player! Nosso site possui certificado de segurança protocolado via SSL. Ou seja, todas as informações são criptografadas dando total segurança aos usuários. A sua única preocupação é jogar.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Entendi!</button>
                </div>
            </div>

        </div>
    </div>

    <section id="sobre-nos">
        <div class="container">
            <div class="text-center">
                <div class="col-sm-8 col-sm-offset-2">
                    <h2 class="title-one">
                    sobre nós</h2>
                    
                       <p>Quem somos?</p>                         

                        <p>Players apaixonados pelo universo dos games...</p>
                        
                        <p>A nossa proposta é proporcionar à todos os jogadores a chance de aproveitar ao máximo a diversão do seu console de mesa, sem se preocupar com os valores de cada lançamento.</p>
                        
                     <p><b>Quem não tem vontade de jogar 1, 2 ou vários jogos diferentes todos os meses? Mas infelizmente os preços dos jogos não ajudam, não é mesmo? Gastar entre R$150,00 e R$250,00 ou mais em cada jogo todo mês é complicado.</b> </p>
                        
                        Já imaginou poder aproveitar os seus jogos favoritos no conforto da sua casa por um preço <b>Hype?</b> 
                        
                        <p>É exatamente essa a proposta da HypePlayers, fazer com que todos os players possam <b>zerar</b> quantos jogos quiserem sem se preocupar com o preço! E o melhor, após finalizar aquele jogo fera você poderá solicitar um novo.</p>
                        
                        <p>Para isso oferecemos o serviço de locação totalmente online, onde o player simplesmente escolhe o seu game, e o aguarda <b>em mídia física</b> no conforto da sua casa, sim, é simplesmente isso. 
Não haverá valores fora do normal, o player não precisará passar horas na internet pesquisando o melhor valor de compra, ficar acordado madrugada a fora aguardando uma liquidação que acaba antes mesmo dele clicar em “comprar”. Todo esse tempo ele irá aproveitar jogando o game de sua preferência. Sem contar que você poderá desfrutar daquele jogo que talvez tenha passado despercebido, ou no caso mais comum, o dinheiro foi destinado para aquele jogo “hype”.</p> 

                    
                    <p>
                        <b>Seja bem-vindo ao mundo Hype, pegue seu controle e comece a jogatina!</b>
                    </p>
                    <p>Nos acompanhe no Facebook e fique por dentro de todas novidades</p>
                    <div class="social-icons" style="margin-bottom: 15px">
                        <a href="https://www.facebook.com/HypePlayers/" target="_blank" style="background-color: #3b5998; color: white">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </section><!--/#sobre-nos-->

    <section id="contato">
        <div class="container">
            <div class="row text-center clearfix">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="contact-heading">
                        <h2 class="title-one">Entre em contato</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="contact-details">
                <div class="row text-center clearfix">
                    <div class="col-sm-6">
                        <div class="contact-address">
                            <img src="assets/img/sell/logo.png" class="img-responsive" style="max-height: 150px;display: block; margin: 0 auto">
                            <h4 class="text-center">
                                <u>
                                    <a style="cursor: pointer;" id="faq">Perguntas frequentes</a>
                                </u>
                            </h4>
                            <h5>
                                <u>
                                    <a href="assets/files/termos.pdf" target="_blank">Política de privacidade e termos de uso</a>
                                </u>
                            </h5>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div id="contact-form-section">
                            {if $emailSent}
                                <div class="status alert alert-success">
                                    E-mail enviado com sucesso!
                                </div>
                            {/if}
                            <form id="contact-form" class="contact" name="contact-form" method="post" action="/index/contact">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control name-field" required="required" placeholder="Seu nome"></div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control mail-field" required="required" placeholder="Seu e-mail">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" id="message" required class="form-control" rows="8" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--/#contact-->

    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> &copy; HypePlayers {'Y'|date} - Todos direitos reservados</p>
            </div>
        </div>
    </footer> <!--/#footer-->

    <script type="text/javascript" src="assets/js/jquery.js"></script>
    <script type="text/javascript" src="assets/js/old.bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
    <script type="text/javascript" src="assets/js/jquery.isotope.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    </body>
</html>