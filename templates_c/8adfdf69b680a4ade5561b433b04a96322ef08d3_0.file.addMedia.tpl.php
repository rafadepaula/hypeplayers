<?php
/* Smarty version 3.1.28, created on 2017-08-20 04:32:05
  from "C:\xampp\htdocs\hypeplayers\app\viewer\Game\addMedia.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.28',
  'unifunc' => 'content_59993af5e786d5_45660312',
  'file_dependency' => 
  array (
    '8adfdf69b680a4ade5561b433b04a96322ef08d3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\hypeplayers\\app\\viewer\\Game\\addMedia.tpl',
      1 => 1503214322,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59993af5e786d5_45660312 ($_smarty_tpl) {
?>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h4>
            </div>
            <div class="card-content">
                <form method="post" action="/game/addMedia/<?php echo $_smarty_tpl->tpl_vars['game']->value->get('id');?>
" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <label>Fotos</label>
                            <input type="file" multiple name="images[]" class="form-control">
                        </div>
                        <div class="col-sm-6 form-group">
                            <label>Vídeo (YouTube)</label>
                            <input type="text" name="video" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <button type="submit" class="btn btn-success btn-block btn-lg">
                                Cadastrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php }
}
